﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/head.php" ?> 
        <style type="text/css">
            .textonormal{
                font:normal 0.9em helveticaneueltstdmdcn;
            }
            .textonormal2
            {
                font:normal 0.9em verdana;
            }
        </style>
    <script type="text/javascript">
            function carregaidentificador()
            {
                var url = "includes/carregaidentificador.php";
                formdata = "cd_plataforma=" + $("#cd_plataforma").val();
                $.ajax({
                    url: url,
                    data: formdata,
                    type: 'GET',
                    dataType: 'html',
                    success: function(data) {
                        $("#identificador").html(data);
                    }
                });

                }
            </script>        
    </head>
    <body>
        <?php include "includes/topoemenu.php" ?> 

        <div class="container">
            <?php
            require_once "App_Code/Campeonatos.php";
            require_once "App_Code/Jogos.php";
            require_once "App_Code/CampeonatosFotos.php";
            $campeonato = new Campeonatos();
            $cd_campeonato = "";
            if (isset($_GET["cd_campeonato"]))
                $cd_campeonato = $_GET["cd_campeonato"];
            $campeonato->getUmItem($cd_campeonato);
            $jogo = new Jogos();
            $jogo->getUmItem($campeonato->getCd_jogo());
            //echo $campeonato->getNm_campeonato();
//            date_default_timezone_set('America/Sao_Paulo');
//            $timezone = date_default_timezone_get();
//echo "The current server timezone is: " . $timezone;
//$date = date('Y/m/d H:i:s', time());
//echo "data ".$date;
            ?>

            <div class="col-sm-6 textonormal" style="margin-top:20px;margin-bottom:20px;font-size:0.9em;font-weight:normal;">
                <h2><?php echo $campeonato->getNm_campeonato(); ?></h2>
                <img src="fotoscampeonatos/<?php echo $cd_campeonato ?>/fotos/<?php echo CampeonatosFotos::GetPrimeiraFoto($cd_campeonato); ?>" width="100px" height="100px">
                <br/><br/>
                <strong>Jogo: </strong><span class="textonormal2"><?php echo $jogo->getNm_jogo() ?></span>
                <br/><br/>
                <strong>Categoria: </strong><span class="textonormal2"><?php echo $jogo->getNm_categoria() ?></span><br/><br/>
                <?php
                $time = array(1 => "Individual", 2 => "Dupla", 3 => "Trio", 4 => "Quarteto");
                ?>
                <strong>Jogadores por time:&nbsp; </strong>
                    <span class="textonormal2"> <?php echo $time[$campeonato->getQt_jogadores_time()]; ?></span>
                <br/><br/>
                <strong>Valor da Inscrição:&nbsp; </strong>
                    <?php 
                       $vl_inscrição = "<strong>Gratuito</strong>";
                       if($campeonato->getVl_inscricao()!= "" && $campeonato->getVl_inscricao()>0) 
                       {
                           $vl_inscrição = number_format($campeonato->getVl_inscricao(), 2,',','.');
                       }
                    ?>
                    <span class="textonormal2"> <?php echo $vl_inscrição; ?>
                      <?php
                          if($campeonato->getQt_jogadores_time()>1 && $campeonato->getVl_inscricao()>0)
                          {  //number_format($produto->getVl_de(), 2,',','.')
                              echo " ( sai a ". (number_format($campeonato->getVl_inscricao()/$campeonato->getQt_jogadores_time(), 2,',','.')). " por membro do time)";
                          }
                      ?>
                    
                    </span>
                <br/><br/>                
                
                <strong>Data prevista :</strong> <span class="textonormal2"><?php echo $campeonato->getDt_campeonato() . " as " . $campeonato->getDs_hora_campeonato() ?></span><br/><br/>
                
               <label>
                    <strong>Plataformas:</strong></label>
                <table class="textonormal2" >
                    <?php
                    require_once "App_Code/Conexao.php";
                    $conexao = new Conexao();
                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                    $mysqli->set_charset("utf8");
                    $query = "select p.nm_plataforma from plataformas_campeonato pc ".
                             "join plataformas p on pc.cd_plataforma = p.cd_plataforma ".
                             "where pc.cd_campeonato = ".$cd_campeonato ;
                    $rs = $mysqli->query($query);
                      echo "<tr>";
                    while ($row = $rs->fetch_assoc()) {
                        echo "<td style='padding-right:10px;'>".$row["nm_plataforma"]. "</td>";
                        }
                        echo "</tr>";
                    $rs->free();
                    ?>
                </table>  <br/>
                <?php
                if ($campeonato->getDs_campeonato() != "") {
                    echo "<strong> Descrição:<strong><br/>";
                    echo "<div class='textonormal2' style='font:normal 0.9em verdana'>".$campeonato->getDs_campeonato()."</div>";
                }
                ?>
            </div> 
            <div class="col-sm-6" style="margin-top:50px;margin-bottom:20px;font-size:0.9em">
                <form class="form"  name="formidentcliente" method="post" action="includes/campeonatoselecionadestino.php" onsubmit="return validaformidentcliente();
                        return true;">
                        <input type="hidden" name="origem" value="campeonato.php" />
                        <input type="hidden" name="qt_jogadores_time" value="<?php echo $campeonato->getQt_jogadores_time(); ?>" />
                         <input type="hidden" name="cd_campeonato" value="<?php echo $cd_campeonato; ?>" />
                    <div class="row">
                        <div class="col-sm-12">
                        <label class="col-sm-4" >Você vai jogar com: </label>
                            <div class="form-group col-sm-8">
                                <select name="cd_plataforma" id="cd_plataforma" class="form-control" onchange="carregaidentificador();" id="cd_plataforma" >
                                    <option value="0" selected>Selecione</option>
                                    <?php
                                   // require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select cd_plataforma,nm_plataforma from plataformas where ".
                                              "cd_plataforma in ".
                                              "(select cd_plataforma from plataformas_campeonato where cd_campeonato = $cd_campeonato)";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_plataforma"] . ">" . $row ["nm_plataforma"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                            </div>     
                        </div>
                    </div>                     
                    
                    <div class="row" id="identificador">
                    </div>
                    <?php
                      if($campeonato->getCd_jogo() == 14)
                      {
                          ?>
                            <div class="row">
                                 <div class="col-sm-12">
                                     <label class="col-sm-4" >Fighter ID: </label>
                                     <div class="form-group col-sm-8">
                                         <input  class="form-control" id="fighter_id" placeholder="Fighter ID" name="fighter_id">
                                     </div>
                                 </div>
                             </div>   
                          <?php
                      }
                    ?>
                
                    <?php
                      $i=1;
                       for($i==1;$i< $campeonato->getQt_jogadores_time();$i++)
                      {
                          ?>
                         <div class="row" style="margin-top: 15px;background-color: #dadada;padding-top: 15px;border-radius: 5px 5px 0 0 ;">
                            <div class="col-sm-12">
                                <label class="col-sm-4" >
                                  <?php 
//                                     if($i==0)
//                                         echo "Seu login neste campeonato:";
//                                     else {
                                         echo "Login do seu colega ".$i." :";
                                    // }
                                  ?>
                                </label>
                                <div class="form-group col-sm-8">
                                    <input  class="form-control" id="nm_login" placeholder="Login do seu colega <?php echo $i; ?> " name="nm_login[]">
                                </div>
                            </div>
                        </div>  
                        
                        <div class="row" style="background-color:#dadada;border-radius:0 0 5px 5px" >
                        <div class="col-sm-12">
                        <label class="col-sm-4" >Ele(a) vai jogar com: </label>
                            <div class="form-group col-sm-8">
                                <select name="cd_plataforma_login[]" id="cd_plataforma" class="form-control"   >
                                    <option value="0" selected>Selecione</option>
                                    <?php
                                   // require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select cd_plataforma,nm_plataforma from plataformas where ".
                                              "cd_plataforma in ".
                                              "(select cd_plataforma from plataformas_campeonato where cd_campeonato = $cd_campeonato)";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_plataforma"] . ">" . $row ["nm_plataforma"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                            </div>     
                        </div>
                    </div>                             
                         <?php
                      }
                    
                    ?>
                      <?php
                            if($campeonato->getQt_jogadores_time() > 1)
                            {
                                ?>
                                    <div class="row"  style="margin-top: 15px;">
                                       <div class="col-sm-12">
                                           <label class="col-sm-4" >
                                               Nome do Time
                                           </label>
                                           <div class="form-group col-sm-8">
                                               <input  class="form-control" id="nm_time" placeholder="Nome do Time" name="nm_time">
                                           </div>
                                       </div>
                                   </div>                       
                                <?php
                            }
                      ?>
                    <div class="form-group col-sm-12">
                        <button type="submit" class="btn btn-default"  >Cadastre-se</button>
                    </div>
                </form>
                

            </div> 
        </div>
<?php include "includes/proximoscampeonatos.php" ?>  
<?php include "includes/rodape.php" ?> 
<?php include "includes/scriptsfinais.php" ?>         
    </body>
</html>