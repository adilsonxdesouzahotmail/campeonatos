﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/head.php" ?> 
    </head>
    <body>
        <?php include "includes/topoemenu.php" ?> 

        <div class="container">
            <h1>Identifica&ccedil;&atilde;o</h1>
            <!--------------------------------->
            <?php
            $origem = "";
            if (isset($_GET["origem"]))
                $origem = $_GET["origem"];
            ?>
            <div class="col-sm-6">
                <h3>Sou novo cliente e quero me cadastrar</h3>
                <div style="height: 49px;" >Clique no bot&atilde;o abaixo para se cadastrar. </div>

                <div >
                     <button type="button" class="btn btn-default"  onclick="window.location = 'cadastrese.php?origem=<?php echo $origem; ?>';">Continuar >></button>
                </div>
               
                <div style="text-align:center;" > D&uacute;vidas? <a href="contato">Fale conosco.</a> </div>
            </div>


            <div class="col-sm-6" >
                <h3>J&aacute; sou cliente</h3>
                <form action="clientearqauxiliares/clientelogin.php" name="formlogin" method="post">
                    <input type="hidden" name="origem" value="<?php echo $origem; ?>"/>
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="col-sm-3" >Email: </label>  
                            <div class="form-group col-sm-9">
                                <input type="text" class="form-control" id="nm_email" placeholder="Email" name="nm_email">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-sm-3" >Senha: </label>  
                            <div class="form-group col-sm-9">
                                <input type="password" class="form-control" id="nm_senha" placeholder="Senha" name="nm_senha">
                            </div>                            
                        </div>
                    </div> 
                    <button type="submit"  class="btn btn-default" >Continuar >></button>
                </form>
                <div  >
                         

                </div>
                <div style="text-align:center;" > <a href="esquecisenha.php">Esqueci minha senha.</a> </div>
            </div>
            <!--------------------------------->

        </div>
        <?php include "includes/rodape.php" ?> 
        <?php include "includes/scriptsfinais.php" ?>         
    </body>
</html>