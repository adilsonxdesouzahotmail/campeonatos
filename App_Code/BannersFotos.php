<?php

require_once 'Conexao.php';
class BannersFotos {
 //put your code here
    // <editor-fold desc="Atributos">
        
        private $cd_banner_foto;
        public function setCd_banner_foto($value) {
		$this->cd_banner_foto = $value;
	}
	public function getCd_banner_foto() {
		return $this->cd_banner_foto;
	}
        
      
        private $nm_url_foto;
	public function setNm_url_foto($value) {
		$this->nm_url_foto = $value;
	}
	public function getNm_url_foto() {
		return $this->nm_url_foto;
	}
        
        private $vl_posicao;
	public function setVl_posicao($value) {
		$this->vl_posicao = $value;
	}
	public function getVl_posicao() {
		return $this->vl_posicao;
	}
        
        var $erro = null;
      // </editor-fold>

        public function Inserir($nm_url_foto,$vl_posicao) {
             $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("insert into banners_fotos (nm_url_foto,vl_posicao) values (?,?)");
                $stmt->execute(array($nm_url_foto,$vl_posicao));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
               $this->erro= $exception->getMessage();
             }
//                 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                  $mysqli->set_charset("utf8");
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("insert into banners_fotos (nm_url_foto,vl_posicao) values (?,?)");
//		 $stmt->bind_param('si',$nm_url_foto,$vl_posicao);
//                if(!$stmt->execute())
//		{
//                     $this->erro =$mysqli->error;
//    		}
//		
//                $stmt->close();
//                $mysqli->close();
        
	}
          public function Excluir($cd_banner_foto) {
                
		 $conexao= new Conexao();
		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
                  $mysqli->set_charset("utf8");
                  $query = "delete from banners_fotos where cd_banner_foto=".$cd_banner_foto;
//                  $result = $mysqli->query($query, MYSQLI_STORE_RESULT);
//
                  if(! $mysqli->query($query, MYSQLI_STORE_RESULT))
                  {
                       $this->erro =$mysqli->error;
                  }
              
                $linhas = $mysqli->affected_rows;
                
                $mysqli->close();
                return $linhas;
        
	}
        public function AtualizarOrdemFotos($cd_banner_foto,$vl_posicao) {
             $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("update banners_fotos set vl_posicao=? where cd_banner_foto=?");
                $stmt->execute(array($vl_posicao,$cd_banner_foto));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
               $this->erro= $exception->getMessage();
             }
//             echo "classe ".$cd_banner_foto."-".$vl_posicao;
//             $conexao= new Conexao();
//             $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//              $mysqli->set_charset("utf8");
//             $stmt = $mysqli->stmt_init();
//             $stmt->prepare("update banners_fotos set vl_posicao=? where cd_banner_foto=?");
//             $stmt->bind_param('ii',$vl_posicao,$cd_banner_foto);
//            if(!$stmt->execute())
//            {
//                 $this->erro =$mysqli->error;
//                 
//            }
//            echo "ok dfd";
//            $stmt->close();
//            $mysqli->close();
        
	}
        public function GetVlPosicao()
        {
            $conexao= new Conexao();
            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
             $mysqli->set_charset("utf8");
             $rs =$mysqli->query("select max(vl_posicao) as vl_posicao from banners_fotos" );

            $vl_posicao="";
            while ($row = $rs->fetch_assoc ()) 
            {
               $vl_posicao=$row["vl_posicao"];
            }
            if($vl_posicao=="")
               $vl_posicao=1;
            else
               $vl_posicao++;
          
            return $vl_posicao;
        }
}
