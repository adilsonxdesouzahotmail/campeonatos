<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Plataformas_campeonato
 *
 * @author Adilson
 */
require_once 'Conexao.php';

class Plataformas_campeonato {

    //put your code here
    ####   Atributos  ##########

    private $cd_plataforma;

    public function setCd_plataforma($value) {
        $this->cd_plataforma = $value;
    }

    public function getCd_plataforma() {
        return $this->cd_plataforma;
    }

    private $cd_campeonato;

    public function setCd_campeonato($value) {
        $this->cd_campeonato = $value;
    }

    public function getCd_campeonato() {
        return $this->cd_campeonato;
    }

    #### Fim  Atributos  ##########
    ####   Métodos   ########

    var $mysqli = null;
    var $meuarray = array();
    var $erro = null;

    public function temPlataforma($cd_campeonato, $cd_plataforma) {
        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $rs = $mysqli->query("select count(cd_plataforma) as ic_templataforma  from plataformas_campeonato " .
                "where cd_campeonato=" . $cd_campeonato . " and cd_plataforma=" . $cd_plataforma);
        list($ic_templataforma ) = $rs->fetch_row();
        $rs->close();
        $mysqli->close();
        return $ic_templataforma;
    }

    public function Atualizar($cd_campeonato, $arrayplataformas) {
        //echo "idi51".$cd_campeonato ."-".$arrayplataformas."<br/>";
        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());

        $qtde = count($arrayplataformas);
        if ($qtde > 0) {
            $sqlplataforma = "insert into plataformas_campeonato values ";
            $values = "";
            $contador = 1;
            foreach ($arrayplataformas as $cd_plataforma) {

                $values .= "(" . $cd_campeonato . "," . $cd_plataforma . ")";
                if ($contador < $qtde)
                    $values .= ",";
                $contador++;
            }
            $sqlplataforma .= $values;
            // echo "sqlidi".$sqlplataforma;
            if (!$mysqli->query($sqlplataforma)) {
                $this->erro = $mysqli->error;
            }
        }
    }

    public function Excluir($cd_campeonato) {
        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $dbh->prepare("delete from plataformas_campeonato where cd_campeonato=?");
            $stmt->execute(array($cd_campeonato));
            $this->erro = "";
        } catch (PDOException $exception) {
            echo $exception->getMessage();
            $this->erro = $exception->getMessage();
        }
    }

    public function ExcluirPlataforma($cd_plataforma) {
        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $dbh->prepare("delete from plataformas_campeonato where cd_plataforma=?");
            $stmt->execute(array($cd_plataforma));
            $this->erro = "";
        } catch (PDOException $exception) {
            $this->erro = $exception->getMessage();
        }
    }

}
