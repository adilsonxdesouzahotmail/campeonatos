<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Campeonatos
 *
 * @author Windows 10
 */
require_once 'Conexao.php';

class ClientesCampeonatos {

    //put your code here
    // <editor-fold desc="Atributos">

    private $cd_cliente_campeonato;

    public function setCd_cliente_campeonato($value) {
        $this->cd_cliente_campeonato = $value;
    }

    public function getCd_cliente_campeonato() {
        return $this->cd_cliente_campeonato;
    }

    private $cd_cliente;

    public function setCd_cliente($value) {
        $this->cd_cliente = $value;
    }

    public function getCd_cliente() {
        return $this->cd_cliente;
    }

    private $cd_campeonato;

    public function setCd_campeonato($value) {
        $this->cd_campeonato = $value;
    }

    public function getCd_campeonato() {
        return $this->cd_campeonato;
    }

    private $dt_inscricao;

    public function setDt_inscricao($value) {
        $this->dt_inscricao = $value;
    }

    public function getDt_inscricao() {
        return $this->dt_inscricao;
    }

    private $ic_pago;

    public function setIc_pago($value) {
        $this->ic_pago = $value;
    }

    public function getIc_pago() {
        return $this->ic_pago;
    }

    private $id_psn;

    public function setId_psn($value) {
        $this->id_psn = $value;
    }

    public function getId_psn() {
        return $this->id_psn;
    }

    private $id_xboxlive;

    public function setId_xboxlive($value) {
        $this->id_xboxlive = $value;
    }

    public function getId_xboxlive() {
        return $this->id_xboxlive;
    }

    private $id_pc;

    public function setId_pc($value) {
        $this->id_pc = $value;
    }

    public function getId_pc() {
        return $this->id_pc;
    }

    private $fighter_id;

    public function setFighter_id($value) {
        $this->fighter_id = $value;
    }

    public function getFighter_id() {
        return $this->fighter_id;
    }

    private $nm_time;

    public function setNm_time($value) {
        $this->nm_time = $value;
    }

    public function getNm_time() {
        return $this->nm_time;
    }

    private $nm_url_brazao_time;

    public function setNm_url_brazao_time($value) {
        $this->nm_url_brazao_time = $value;
    }

    public function getNm_url_brazao_time() {
        return $this->nm_url_brazao_time;
    }

    private $nm_status_pagamento;

    public function setNm_status_pagamento($value) {
        $this->nm_status_pagamento = $value;
    }

    public function getNm_status_pagamento() {
        return $this->nm_status_pagamento;
    }

    private $cd_plataforma;

    public function setCd_plataforma($value) {
        $this->cd_plataforma = $value;
    }

    public function getCd_plataforma() {
        return $this->cd_plataforma;
    }

    private $vl_inscricao;

    public function setVl_inscricao($value) {
        $this->vl_inscricao = $value;
    }

    public function getVl_inscricao() {
        return $this->vl_inscricao;
    }    
    // </editor-fold>
    #### Fim  Atributos  ##########
    ####   Métodos   ########
    var $mysqli = null;
    var $meuarray = array();
    var $erro = null;

    public function Inserir(ClientesCampeonatos $clientecampeonato) {
       // echo " 168cdcli".$clientecampeonato->getCd_campeonato()."<br/>";
        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "insert into clientes_campeonatos (cd_cliente,cd_campeonato,dt_inscricao,ic_pago,id_psn,id_xboxlive,id_pc,fighter_id, " .
                    "nm_time,nm_url_brazao_time,nm_status_pagamento,cd_plataforma,vl_inscricao) " .
                    "values(?,?,now(),?,?,?,?,?,?,?,?,?,?)";

            $stmt = $dbh->prepare($query);

            $cd_cliente = $clientecampeonato->getCd_cliente();
            $cd_campeonato = $clientecampeonato->getCd_campeonato();

//                 $myDateTime = DateTime::createFromFormat('d/m/Y', $clientecampeonato->getDt_inscricao() );
//                $dt_inscricao = $myDateTime->format('Y-m-d ');

            $ic_pago = $clientecampeonato->getIc_pago();
            $id_psn = $clientecampeonato->getId_psn();
            $id_xboxlive = $clientecampeonato->getId_xboxlive();
            $id_pc = $clientecampeonato->getId_pc();
            $fighter_id = $clientecampeonato->getFighter_id();
            $nm_time = $clientecampeonato->getNm_time();
            $nm_url_brazao_time = $clientecampeonato->getNm_url_brazao_time();
            $nn_status_pagamento = $clientecampeonato->getNm_status_pagamento();
            $cd_plataforma = $clientecampeonato->getCd_plataforma();
            $vl_inscricao = $clientecampeonato->getvl_inscricao();

            $stmt->execute(array($cd_cliente, $cd_campeonato, $ic_pago, $id_psn, $id_xboxlive, $id_pc,
                $fighter_id, $nm_time, $nm_url_brazao_time, $nn_status_pagamento, $cd_plataforma,$vl_inscricao));
            $this->erro = "";
        } catch (PDOException $exception) {
            if (strpos($exception->getMessage(), 'UQ_clientes_campeonatos'))
                $this->erro = "Você já está cadastrado para este campeonato.";
            else
                $this->erro = "erro inserir clicamp". $exception->getMessage();
        }
    }
    
   public function AtualizarStatusPagamento($cd_cliente_campeonato,$nm_status_pagamento,$ic_pago) {
      // echo "<script>alert('"."atu st pag 208"."');</script>";
        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "update clientes_campeonatos set nm_status_pagamento = ?,ic_pago = ? ".
                     "where cd_cliente_campeonato = ?";
            $stmt = $dbh->prepare($query);
            
            $stmt->execute(array($nm_status_pagamento,$ic_pago,$cd_cliente_campeonato));
            $this->erro = "";
           // echo "ok";
        }
        catch (PDOException $exception) {
            echo  "erro ".$exception->getMessage();
            if (strpos($exception->getMessage(), 'UQ_campeonatos'))
                $this->erro = "Ítem já está cadastrado.";
            else
                $this->erro = $exception->getMessage();
        }            
        
   }
    

    public function Atualizar(Campeonatos $campeonato) {
        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "update campeonatos set nm_campeonato=?,cd_jogo=?," .
                    "ic_ativo=?,qt_participantes_previsto=?,dt_campeonato=?," .
                    "ds_hora_campeonato=?,vl_inscricao=?,qt_jogadores_time=?,ds_campeonato=? " .
                    "where cd_campeonato=?";
            $stmt = $dbh->prepare($query);

            $nm_campeonato = $campeonato->getNm_campeonato();
            $cd_jogo = $campeonato->getCd_jogo();
            $ic_ativo = $campeonato->getIc_ativo();
            $qt_participantes_previsto = $campeonato->getQt_participantes_previsto();

            $myDateTime = DateTime::createFromFormat('d/m/Y', $campeonato->getDt_campeonato());
            $dt_campeonato = $myDateTime->format('Y-m-d');

            $ds_hora_campeonato = $campeonato->getDs_hora_campeonato();
            $vl_inscricao = $campeonato->getVl_inscricao();
            $qt_jogadores_time = $campeonato->getQt_jogadores_time();
            $ds_campeonato = $campeonato->getDs_campeonato();
            $cd_campeonato = $campeonato->getCd_campeonato();

            $stmt->execute(array($nm_campeonato, $cd_jogo, $ic_ativo, $qt_participantes_previsto,
                $dt_campeonato, $ds_hora_campeonato, $vl_inscricao, $qt_jogadores_time, $ds_campeonato, $cd_campeonato));
            $this->erro = "";
        } catch (PDOException $exception) {
            if (strpos($exception->getMessage(), 'UQ_campeonatos'))
                $this->erro = "Ítem já está cadastrado.";
            else
                $this->erro = $exception->getMessage();
        }
    }

    public function AtualizarClientesCampeonatosLogins($cd_cliente_campeonato, $array_nm_login, $array_cd_plataforma_login, $array_fighter_id_login) {
        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $qtde = count($array_nm_login);
        if ($qtde > 0) {
            $sqllogins = "insert into clientes_campeonatos_logins values ";
            $values = "";
            $i = 1;
            for ($i = 1; $i <= $qtde; $i++) {
                $values .= "(" . $cd_cliente_campeonato . ",'" . $array_nm_login[$i] . "'," . $array_cd_plataforma_login[$i]. 
                        ",'" . $array_fighter_id_login[$i]."')";
                if($i < $qtde)
                    $values .= ",";
            }
            $sqllogins .= $values;
           // echo "sqlidi " . $sqllogins . "<br/>";
            if (!$mysqli->query($sqllogins)) {
                $this->erro = $mysqli->error;
            }
        }
    }

    public function UltimoClienteCampeonato() {
        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $mysqli->set_charset("utf8");
        $rs = $mysqli->query("select max(cd_cliente_campeonato) as cd_cliente_campeonato from clientes_campeonatos");
        list($cd_cliente_campeonato) = $rs->fetch_row();
        $rs->close();
        $mysqli->close();
        return $cd_cliente_campeonato;
    }

    public function ListaTodosGridAdmin($limit, $subquery) {
        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $mysqli->set_charset("utf8");


        $rs = $mysqli->query("select c.*,j.nm_jogo from campeonatos c " .
                "join jogos j on c.cd_jogo = j.cd_jogo " . $subquery . " order by c.nm_campeonato " . $limit);

        $acampeonatos = array();

        while ($row = $rs->fetch_assoc()) {
            $tempcampeonatos = new Campeonatos();
            $tempcampeonatos->setCd_campeonato($row ['cd_campeonato']);
            $tempcampeonatos->setNm_campeonato($row ['nm_campeonato']);
            $tempcampeonatos->setNm_jogo($row ['nm_jogo']);
            $tempcampeonatos->setQt_participantes_previsto($row ['qt_participantes_previsto']);
            $tempcampeonatos->setIc_ativo($row ['ic_ativo']);
            $myDateTime = DateTime::createFromFormat('Y-m-d h:i:s', $row["dt_campeonato"]);
            $tempcampeonatos->setDt_campeonato($myDateTime->format('d/m/Y'));
            $acampeonatos [] = $tempcampeonatos;
        }
        $rs->close();
        $mysqli->close();

        $this->meuarray = $acampeonatos;
    }

    public function Excluir($cd_campeonato) {
        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $dbh->prepare("delete from campeonatos where cd_campeonato=?");
            $stmt->execute(array($cd_campeonato));
            $this->erro = "";
        } catch (PDOException $exception) {
            $this->erro = $exception->getMessage();
        }
    }

    public function getUmItem($cd_cliente_campeonato) {
        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "select * from clientes_campeonatos where cd_cliente_campeonato=?";

            $stmt = $dbh->prepare($query);
            $stmt->execute(array($cd_cliente_campeonato));
            
           // echo "316".$cd_cliente_campeonato."<br/>";

            while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                         // echo "320<br/>";
                $this->cd_cliente_campeonato = $row["cd_cliente_campeonato"];
                $this->cd_cliente = $row["cd_cliente"];
                $this->cd_campeonato = $row["cd_campeonato"];
                
                $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $row["dt_inscricao"]);
                $this->dt_inscricao = $myDateTime->format('d/m/Y');                
                
                $this->ic_pago = $row["ic_pago"];
                $this->id_psn = $row["id_psn"];
                $this->id_xboxlive = $row["id_xboxlive"];
                $this->id_pc = $row["id_pc"];
                $this->fighter_id = $row["fighter_id"];
                $this->nm_time = $row["nm_time"];
                $this->nm_url_brazao_time = $row["nm_url_brazao_time"];
                $this->nm_status_pagamento = $row["nm_status_pagamento"];
                $this->cd_plataforma = $row["cd_plataforma"];
                $this->vl_inscricao= $row["vl_inscricao"];


            }
            $this->erro = "";
        } catch (PDOException $exception) {
            $this->erro = $exception->getMessage();
        }
    }

}
