<?php
error_reporting(E_ALL);
require_once "Config.php";
require_once "PagSeguroLibrary/PagSeguroLibrary.php";
require_once "Conexao.php";

class CreatePaymentRequest {

    //put your code here
    public static $erro;

    public static function main($cd_cliente_campeonato) {
        $paymentRequest = new PagSeguroPaymentRequest();
        $paymentRequest->setCurrency("BRL");
 
        $conexao = new Conexao();

        $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $query = "select ca.nm_campeonato,ca.vl_inscricao,cli.nome_cliente,cli.cpf_cliente, " .
                "cli.email_cliente,cli.ddd_cliente,cli.telefone_cliente,cli.celular_cliente " .
                "from clientes_campeonatos clica " .
                "join campeonatos ca on clica.cd_campeonato = ca.cd_campeonato " .
                "join clientes cli on clica.cd_cliente = cli.cd_cliente " .
                "where clica.cd_cliente_campeonato = ?";
        $stmt = $dbh->prepare($query);
        $stmt->execute(array($cd_cliente_campeonato));

        while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
            $paymentRequest->addItem($cd_cliente_campeonato, $row["nm_campeonato"], 1, $row["vl_inscricao"]);
            $paymentRequest->setReference($cd_cliente_campeonato);
            $telefone = "";
            if ($row["telefone_cliente"] == "") {
                $telefone = $row["telefone_cliente"];
            } else {
                $telefone = $row["celular_cliente"];
            }
            $paymentRequest->setSender(
                    $row["nome_cliente"], $row["email_cliente"], $row["ddd_cliente"], $telefone, 'CPF', $row["cpf_cliente"]
            );
        }

        try {
            $credentials = new PagSeguroAccountCredentials(emailpagseguro, tokenpagseguro);
            // Register this payment request in PagSeguro, to obtain the payment URL for redirect your customer.
            $url = $paymentRequest->register($credentials);
            self::$erro = "";
            ?>
            <script>window.location = "<?php echo $url ?>";</script>

            <?php

        } catch (PagSeguroServiceException $e) {
            // echo "http status".$e->getHttpStatus()."<br/>"; // imprime o código HTTP 
            $erropagseguro = "";
            foreach ($e->getErrors() as $error) {

                $erropagseguro .= $error->getMessage();
             }

            self::$erro .= "erro: " . $erropagseguro;
        }
    }

    public static function printPaymentUrl($url) {
        if ($url) {
            echo "<h2>Criando requisi&ccedil;&atilde;o de pagamento</h2>";
            echo "<p>URL do pagamento: <strong>$url</strong></p>";
            echo "<p><a title=\"URL do pagamento\" href=\"$url\">Ir para URL do pagamento.</a></p>";
        }
    }

}
