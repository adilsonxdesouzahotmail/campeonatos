<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of NoticiasComentarios
 *
 * @author Adilson
 */
require_once 'Conexao.php';
class NoticiasComentarios {
    //put your code here
   
      ####   Atributos  ##########

        private $cd_noticia_comentario;
	public function setCd_noticia_comentario($value) {
		$this->cd_noticia_comentario = $value;
	}
	public function getCd_noticia_comentario() {
		return $this->cd_noticia_comentario;
	}
	
        
        private $cd_noticia;
	public function setCd_noticia($value) {
		$this->cd_noticia = $value;
	}
	public function getCd_noticia() {
		return $this->cd_noticia;
	}

        private $cd_cliente;
	public function setCd_cliente($value) {
		$this->cd_cliente = $value;
	}
	public function getCd_cliente() {
		return $this->cd_cliente;
	}
          
        private $Ds_comentario;
	public function setDs_comentario($value) {
		$this->ds_comentario = $value;
	}
	public function getDs_comentario() {
		return $this->ds_comentario;
	}
                
        private $Ic_aprovado;
	public function setIc_aprovado($value) {
		$this->ic_aprovado = $value;
	}
	public function getIc_aprovado() {
		return $this->ic_aprovado;
	}
        
        private $Ic_lido;
	public function setIc_lido($value) {
		$this->ic_lido = $value;
	}
	public function getIc_lido() {
		return $this->ic_lido;
	}

        private $Dt_comentario;
	public function setDt_comentario($value) {
		$this->dt_comentario = $value;
	}
	public function getDt_comentario() {
		return $this->dt_comentario;
	}        

        private $Ds_resposta;
	public function setDs_resposta($value) {
		$this->ds_resposta = $value;
	}
	public function getDs_resposta() {
		return $this->ds_resposta;
	}
        
        private $Dt_resposta;
	public function setDt_resposta($value) {
		$this->dt_resposta = $value;
	}
	public function getDt_resposta() {
		return $this->dt_resposta;
	}

        private $cd_usuario_resposta;
	public function setCd_usuario_resposta($value) {
		$this->cd_usuario_resposta = $value;
	}
	public function getCd_usuario_resposta() {
		return $this->cd_usuario_resposta;
	}
        
	public function setNm_titulo($value) {
		$this->nm_titulo = $value;
	}
	public function getNm_titulo() {
		return $this->nm_titulo;
	}

	public function setNm_usuario($value) {
		$this->nm_usuario = $value;
	}
	public function getNm_usuario() {
		return $this->nm_usuario;
	}

	public function setNome_cliente($value) {
		$this->nome_cliente = $value;
	}
	public function getNome_cliente() {
		return $this->nome_cliente;
	}        
        ####  Fim Atributos  ##########
        ####   M�todos   ########
	var $mysqli = null;
	var $meuarray = array ();
	var $erro = null;
	

       public function Inserir(NoticiasComentarios $noticiacomentario) {
		
             $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("insert into noticia_comentarios (cd_noticia,cd_cliente,ds_comentario,ic_aprovado,ic_lido,dt_comentario) ".
                                      "values (?,?,?,false,false,now())");
                $stmt->execute(array($noticiacomentario->getCd_noticia(),$noticiacomentario->getCd_cliente(),
                    $noticiacomentario->getDs_comentario()));
                $this->erro = "";   

             } 
             catch (PDOException $exception) 
             {
                    $this->erro= $exception->getMessage();

             }


    }
    

     public function Atualizar(NoticiasComentarios $noticiacomentario) {
           $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("update noticia_comentarios set ds_comentario  = ? , ic_aprovado=?,".
                                      "ic_lido=?, ds_resposta=?,cd_usuario_resposta=? dt_resposta=? "
                        . "where cd_noticia_comentario=?");
                $stmt->execute(array($noticiacomentario->getDs_comentario(),$noticiacomentario->getIc_aprovado(),
                               $noticiacomentario->getIc_lido(),$noticiacomentario->getDs_comentario(),$noticiacomentario->getCd_usuario_resposta(),
                               $noticiacomentario->getDt_comentario(),$noticiacomentario->getCd_noticia_comentario()));
                        
                $this->erro = "";   

             } 
             catch (PDOException $exception) 
             {
                    $this->erro= $exception->getMessage();

             }

            
    }

   public function Excluir(NoticiasComentarios $noticiacomentario) {

             $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from noticia_comentarios where cd_noticia_comentario=?");
                $stmt->execute(array($noticiacomentario->getCd_noticia_comentario()));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                    $this->erro= $exception->getMessage();
             }
            
    }
     public function getUmItem($cd_noticia_comentario) {

          $conexao = new Conexao();
            try {
               $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
               $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
               $stmt = $dbh->prepare("select noco.*,noti.nm_titulo,cli.nome_cliente,us.nm_usuario, ".
                            "(case when noco.ic_lido is null then 0 else noco.ic_lido end) as lido,  ".
                            "(case when noco.ic_aprovado is null then 0 else noco.ic_aprovado end) as aprovado  ".
                            " from noticias_comentarios noco ".
                            "join clientes cli on noco.cd_cliente = cli.cd_cliente ".
                            " join noticias noti on noco.cd_noticia = noti.cd_noticia ".
                            " left join usuarios us on noco.cd_usuario_resposta = us.cd_usuario ".
                            "  where noco.cd_noticia_comentario=? ".
                            " order by lido");
               $stmt->execute(array($cd_noticia_comentario));
//cd_usuario_resposta	dt_resposta	nm_titulo	nome_cliente	nm_usuario	lido	aprovado
               while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                    $this->cd_noticia_comentario = $row["cd_noticia_comentario"];
                   $this->cd_cliente = $row["cd_cliente"];
                    $this->cd_noticia = $row["cd_noticia"];
                    $this->ds_comentario = $row["ds_comentario"];
                    $this->ic_aprovado = $row["ic_aprovado"];
                    $this->ic_lido = $row["ic_lido"];
                    $this->dt_comentario = $row["dt_comentario"];
                    $this->ds_resposta = $row["ds_resposta"];
                    $this->cd_usuario_resposta = $row["cd_usuario_resposta"];
                    $this->dt_resposta = $row["dt_resposta"];
                    $this->nm_titulo = $row["nm_titulo"];
                    $this->nome_cliente = $row["nome_cliente"];
                    $this->nm_usuario = $row["nm_usuario"];
                    $this->ic_lido = $row["lido"];
                    $this->ic_aprovado = $row["aprovado"];
                 }
                $this->erro = "";   
            } 
            catch (PDOException $exception) 
            {
               $this->erro= $exception->getMessage();
            }
    }
     public function proxCdNoticiasComentarios() {

        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $mysqli->set_charset("utf8");
        $query = "select max(cd_noticia_comentario) as ultimo_cd_noticia_comentario from noticia_comentarios";
        $rs = $mysqli->query($query);

        while ($row = $rs->fetch_assoc()) {
            $this->prox_cd_noticia_comentario = $row ["ultimo_cd_noticia_comentario"];
        }
        if ($this->prox_cd_noticia_comentario == "")
            $this->prox_cd_noticia_comentario = 1;
        else
            $this->prox_cd_noticia_comentario++;
        $mysqli->close();
    }
   
      public function ListaTodosGridAdmin($limit, $subquery) {

        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
       // $mysqli->set_charset("utf8");
        $sql = "select cli.cd_cliente,cli.nome_cliente,cli.ddd_cliente,cli.telefone_cliente,".
               "(select count(clica.cd_cliente_campeonato) from clientes_campeonatos clica where clica.cd_cliente = cli.cd_cliente) as qt_campeonatos ".
               "from clientes cli ".
                $subquery . " order by cli.cd_cliente " . $limit;
              
        $rs = $mysqli->query($sql);

        $aclientes = array();

        while ($row = $rs->fetch_assoc()) {
            $tempclientes = new clientes();
            $tempclientes->setCd_cliente($row ['cd_cliente']);
            $tempclientes->setNome_cliente($row ['nome_cliente']);
            $tempclientes->setDdd_cliente($row ['ddd_cliente']);
            $tempclientes->setTelefone_cliente($row ['telefone_cliente']);
            $tempclientes->setQt_campeonatos($row ['qt_campeonatos']);
            $aclientes [] = $tempclientes;
        }
        $rs->close();
        $mysqli->close();

        $this->meuarray = $aclientes;
    }


}
