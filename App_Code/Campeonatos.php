<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Campeonatos
 *
 * @author Windows 10
 */
require_once 'Conexao.php';

class Campeonatos {

    //put your code here
    // <editor-fold desc="Atributos">

    private $cd_campeonato;

    public function setCd_campeonato($value) {
        $this->cd_campeonato = $value;
    }

    public function getCd_campeonato() {
        return $this->cd_campeonato;
    }

    private $nm_campeonato;

    public function setNm_campeonato($value) {
        $this->nm_campeonato = $value;
    }

    public function getNm_campeonato() {
        return $this->nm_campeonato;
    }

    private $dt_campeonato;

    public function setDt_campeonato($value) {
        $this->dt_campeonato = $value;
    }

    public function getDt_campeonato() {
        return $this->dt_campeonato;
    }

    private $ds_hora_campeonato;

    public function setDs_hora_campeonato($value) {
        $this->ds_hora_campeonato = $value;
    }

    public function getDs_hora_campeonato() {
        return $this->ds_hora_campeonato;
    }

    private $cd_jogo_plataforma;

    public function setCd_jogo_plataforma($value) {
        $this->cd_jogo_plataforma = $value;
    }

    public function getCd_jogo_plataforma() {
        return $this->cd_jogo_plataforma;
    }

    private $cd_jogo;

    public function setCd_jogo($value) {
        $this->cd_jogo = $value;
    }

    public function getCd_jogo() {
        return $this->cd_jogo;
    }

    private $qt_participantes_previsto;

    public function setQt_participantes_previsto($value) {
        $this->qt_participantes_previsto = $value;
    }

    public function getQt_participantes_previsto() {
        return $this->qt_participantes_previsto;
    }

    private $qt_participantes_real;

    public function setQt_participantes_real($value) {
        $this->qt_participantes_real = $value;
    }

    public function getQt_participantes_real() {
        return $this->qt_participantes_real;
    }

        private $qt_participantes_inscritos;

    public function setQt_participantes_inscritos($value) {
        $this->qt_participantes_inscritos = $value;
    }

    public function getQt_participantes_inscritos() {
        return $this->qt_participantes_inscritos;
    }
    private $ds_campeonato;

    
     private $qt_participantes_pagantes;

    public function setQt_participantes_pagantes($value) {
        $this->qt_participantes_pagantes = $value;
    }

    public function getQt_participantes_pagantes() {
        return $this->qt_participantes_pagantes;
    }
    public function setDs_campeonato($value) {
        $this->ds_campeonato = $value;
    }

    public function getDs_campeonato() {
        return $this->ds_campeonato;
    }

    private $nm_jogo;

    public function setNm_jogo($value) {
        $this->nm_jogo = $value;
    }

    public function getNm_jogo() {
        return $this->nm_jogo;
    }

    private $nm_plataforma;

    public function setNm_plataforma($value) {
        $this->nm_plataforma = $value;
    }

    public function getNm_plataforma() {
        return $this->nm_plataforma;
    }

    ///////////////////////////
    private $cd_plataforma;

    public function setCd_plataforma($value) {
        $this->cd_plataforma = $value;
    }

    public function getCd_plataforma() {
        return $this->cd_plataforma;
    }

    private $cd_subcategoria;

    public function setCd_subcategoria($value) {
        $this->cd_subcategoria = $value;
    }

    public function getCd_subcategoria() {
        return $this->cd_subcategoria;
    }

    private $cd_categoria;

    public function setCd_categoria($value) {
        $this->cd_categoria = $value;
    }

    public function getCd_categoria() {
        return $this->cd_categoria;
    }

    private $nm_categoria;

    public function setNm_categoria($value) {
        $this->nm_categoria = $value;
    }

    public function getNm_categoria() {
        return $this->nm_categoria;
    }

    private $nm_url_1;

    public function setNm_url_1($value) {
        $this->nm_url_1 = $value;
    }

    public function getNm_url_1() {
        return $this->nm_url_1;
    }

    private $nm_url_2;

    public function setNm_url_2($value) {
        $this->nm_url_2 = $value;
    }

    public function getNm_url_2() {
        return $this->nm_url_2;
    }

    private $ic_ativo;

    public function setIc_ativo($value) {
        $this->ic_ativo = $value;
    }

    public function getIc_ativo() {
        return $this->ic_ativo;
    }

    private $nm_url_primeira_foto;

    public function setNm_url_primeira_foto($value) {
        $this->nm_url_primeira_foto = $value;
    }

    public function getNm_url_primeira_foto() {
        return $this->nm_url_primeira_foto;
    }

    private $vl_inscricao;

    public function setVl_inscricao($value) {
        $this->vl_inscricao = $value;
    }

    public function getVl_inscricao() {
        return $this->vl_inscricao;
    }

    private $ic_encerrado;

    public function setIc_encerrado($value) {
        $this->ic_encerrado = $value;
    }

    public function getIc_encerrado() {
        return $this->ic_encerrado;
    }

    private $qt_jogadores_time;

    public function setQt_jogadores_time($value) {
        $this->qt_jogadores_time = $value;
    }

    public function getQt_jogadores_time() {
        return $this->qt_jogadores_time;
    }

    // </editor-fold>
    #### Fim  Atributos  ##########
    ####   Métodos   ########
    var $mysqli = null;
    var $meuarray = array();
    var $erro = null;

    public function Inserir(Campeonatos $campeonato) {

        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "insert into campeonatos (nm_campeonato,qt_participantes_previsto,qt_participantes_real," .
                    "ds_campeonato,ic_ativo,dt_campeonato,ds_hora_campeonato,cd_jogo,vl_inscricao,ic_encerrado,qt_jogadores_time)" .
                    "values(?,?,?,?,?,?,?,?,?,?,?)";

            $stmt = $dbh->prepare($query);
            $nm_campeonato = $campeonato->getNm_campeonato();
            $qt_participantes_previsto = $campeonato->getQt_participantes_previsto();
            $qt_participantes_real = 0;
            $ds_campeonato = $campeonato->getDs_campeonato();
            $ic_ativo = $campeonato->getIc_ativo();

            $myDateTime = DateTime::createFromFormat('d/m/Y', $campeonato->getDt_campeonato());
            $dt_campeonato = $myDateTime->format('Y-m-d');

            $ds_hora_campeonato = $campeonato->getDs_hora_campeonato();
            $cd_jogo = $campeonato->getCd_jogo();
            $ic_encerrado = 0;
            $vl_inscricao = $campeonato->getVl_inscricao();
            $qt_jogadores_time = $campeonato->getQt_jogadores_time();

            $stmt->execute(array($nm_campeonato, $qt_participantes_previsto, $qt_participantes_real,
                $ds_campeonato, $ic_ativo, $dt_campeonato, $ds_hora_campeonato, $cd_jogo, $vl_inscricao, $ic_encerrado, $qt_jogadores_time));
            $this->erro = "";
            self::geraMenuXml();
        } catch (PDOException $exception) {
            if (strpos($exception->getMessage(), 'UQ_campeonatos'))
                $this->erro = "Ítem já está cadastrado.";
            else
                $this->erro = $exception->getMessage();
        }
    }

    public function Atualizar(Campeonatos $campeonato) {
        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "update campeonatos set nm_campeonato=?,cd_jogo=?," .
                    "ic_ativo=?,qt_participantes_previsto=?,dt_campeonato=?," .
                    "ds_hora_campeonato=?,vl_inscricao=?,qt_jogadores_time=?,ds_campeonato=? " .
                    "where cd_campeonato=?";
            $stmt = $dbh->prepare($query);

            $nm_campeonato = $campeonato->getNm_campeonato();
            $cd_jogo = $campeonato->getCd_jogo();
            $ic_ativo = $campeonato->getIc_ativo();
            $qt_participantes_previsto = $campeonato->getQt_participantes_previsto();

            $myDateTime = DateTime::createFromFormat('d/m/Y', $campeonato->getDt_campeonato());
            $dt_campeonato = $myDateTime->format('Y-m-d');

            $ds_hora_campeonato = $campeonato->getDs_hora_campeonato();
            $vl_inscricao = $campeonato->getVl_inscricao();
            $qt_jogadores_time = $campeonato->getQt_jogadores_time();
            $ds_campeonato = $campeonato->getDs_campeonato();
            $cd_campeonato = $campeonato->getCd_campeonato();

            $stmt->execute(array($nm_campeonato, $cd_jogo, $ic_ativo, $qt_participantes_previsto,
                $dt_campeonato, $ds_hora_campeonato, $vl_inscricao, $qt_jogadores_time, $ds_campeonato, $cd_campeonato));
            $this->erro = "";
            self::geraMenuXml();
        } catch (PDOException $exception) {
            if (strpos($exception->getMessage(), 'UQ_campeonatos'))
                $this->erro = "Ítem já está cadastrado.";
            else
                $this->erro = $exception->getMessage();
        }
    }

    public function UltimoCampeonato() {
        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $mysqli->set_charset("utf8");
        $rs = $mysqli->query("select max(cd_campeonato) as cd_campeonato from campeonatos");
        list($cd_campeonato) = $rs->fetch_row();
        $rs->close();
        $mysqli->close();
        return $cd_campeonato;
    }

    public function ListaTodosGridAdmin($limit, $subquery) {
        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $mysqli->set_charset("utf8");


        $rs = $mysqli->query("select c.*,j.nm_jogo,".
                "(select count(cc.cd_cliente_campeonato) from clientes_campeonatos cc ".
                "where cc.cd_campeonato = c.cd_campeonato) as qt_participantes_inscritos, ".
                "(select count(cc.cd_cliente_campeonato) from clientes_campeonatos cc ".
                "where cc.cd_campeonato = c.cd_campeonato and cc.ic_pago =1) as qt_participantes_pagantes ".
                "from campeonatos c  ".
                "join jogos j on c.cd_jogo = j.cd_jogo " . $subquery . " order by c.cd_campeonato " . $limit);

        $acampeonatos = array();

        while ($row = $rs->fetch_assoc()) {
            $tempcampeonatos = new Campeonatos();
            $tempcampeonatos->setCd_campeonato($row ['cd_campeonato']);
            $tempcampeonatos->setNm_campeonato($row ['nm_campeonato']);
            $tempcampeonatos->setNm_jogo($row ['nm_jogo']);
            $tempcampeonatos->setVl_inscricao($row ['vl_inscricao']);
            $tempcampeonatos->setQt_participantes_previsto($row ['qt_participantes_previsto']);
            $tempcampeonatos->setQt_jogadores_time($row ['qt_jogadores_time']);
            $tempcampeonatos->setQt_participantes_inscritos($row ['qt_participantes_inscritos']);            
            $tempcampeonatos->setQt_participantes_pagantes($row ['qt_participantes_pagantes']);                        
            $tempcampeonatos->setIc_ativo($row ['ic_ativo']);
            $myDateTime = DateTime::createFromFormat('Y-m-d h:i:s', $row["dt_campeonato"]);
            $tempcampeonatos->setDt_campeonato($myDateTime->format('d/m/Y'));
             $tempcampeonatos->setIc_encerrado($row ['ic_encerrado']);
            $acampeonatos [] = $tempcampeonatos;
        }
        $rs->close();
        $mysqli->close();

        $this->meuarray = $acampeonatos;
    }

    public function Excluir($cd_campeonato) {
        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $dbh->prepare("delete from campeonatos where cd_campeonato=?");
            $stmt->execute(array($cd_campeonato));
            $this->erro = "";
            self::geraMenuXml();
        } catch (PDOException $exception) {
            if (strpos($exception->getMessage(), 'FK_clientes_campeonatos2'))
                $this->erro = "Existe(m) cliente(s) cadastrados para este campeonato. Exclusão não permitida.";
            else
                $this->erro = $exception->getMessage();
           
        }
    }

    public function getUmItem($cd_campeonato) {
        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = "select * from campeonatos where cd_campeonato=?";

            $stmt = $dbh->prepare($query);
            $stmt->execute(array($cd_campeonato));

            while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                $this->cd_campeonato = $row["cd_campeonato"];
                $this->nm_campeonato = $row["nm_campeonato"];
                $this->cd_jogo = $row["cd_jogo"];
                $this->ds_campeonato = $row["ds_campeonato"];
                $this->qt_participantes_previsto = $row["qt_participantes_previsto"];
                $this->qt_participantes_real = $row["qt_participantes_real"];
                $this->ic_ativo = $row["ic_ativo"];
                $this->dt_campeonato = $row["dt_campeonato"];
                $this->ds_hora_campeonato = $row["ds_hora_campeonato"];
                $this->qt_jogadores_time = $row["qt_jogadores_time"];
                $this->vl_inscricao = $row["vl_inscricao"];
                $this->ic_encerrado = $row["ic_encerrado"];

                $myDateTime = DateTime::createFromFormat('Y-m-d h:i:s', $row["dt_campeonato"]);
                $this->dt_campeonato = $myDateTime->format('d/m/Y');
            }
            $this->erro = "";
         } catch (PDOException $exception) {
            $this->erro = $exception->getMessage();
        }
    }
    
        
    
    public function checaEncerrado($cd_campeonato) 
    {
        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $mysqli->set_charset("utf8");
        $sqlquery = "select (select count(cc.cd_cliente_campeonato) from clientes_campeonatos cc ".
                            "where cc.cd_campeonato =$cd_campeonato and cc.ic_pago =1 ) >= ".
                            "(select c.qt_participantes_previsto from campeonatos c ".
                            "where c.cd_campeonato =$cd_campeonato ) ".
                            " as encerrado";
        $rs = $mysqli->query($sqlquery);
//        echo $sqlquery."<br/>";
//        echo
        list($encerrado) = $rs->fetch_row();
        $rs->close();
        $mysqli->close();
        
        if ($encerrado) 
        {
            try 
            {
                $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $stmt = $dbh->prepare("update campeonatos set ic_encerrado=1 where cd_campeonato=?");
                $stmt->execute(array($cd_campeonato));
                $this->erro = "";
            } 
            catch (PDOException $exception) 
            {
                $this->erro = $exception->getMessage();
            }
        }
        self::geraMenuXml();
        return $encerrado;
    }

    public static function geraMenuXml()
    {
        
        $xmlmenu = "<?xml version='1.0' encoding='UTF-8'?>";
        $xmlmenu.="<menu>";
        $conexao= new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
         //$mysqli->set_charset("utf8");
        $rs = $mysqli->query ( "select * from campeonatos where ic_ativo = true" );

        while ( $row = $rs->fetch_assoc () ) {
               
               $xmlmenu.="<campeonato>";
               $xmlmenu.="<cd_campeonato>".$row ['cd_campeonato']."</cd_campeonato>";
               $xmlmenu.="<nm_campeonato>".$row ['nm_campeonato']."</nm_campeonato>";
               $xmlmenu.="<ic_encerrado>".$row ['ic_encerrado']."</ic_encerrado>";

               $xmlmenu.="</campeonato>";
        }
        
        $rs->close ();
        $mysqli->close ();
        $xmlmenu.="</menu>";
      
        file_put_contents("../../menu/menucampeonatos.xml",$xmlmenu);
               
    }
}
