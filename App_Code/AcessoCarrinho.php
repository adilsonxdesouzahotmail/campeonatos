<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of AcessoCarrinho
 *
 * @author Adilson
 */
require_once 'Config.php';
require_once 'Conexao.php';
class AcessoCarrinho {

    //put your code here
    ####   Atributos  ##########
    // Stores the visitor's Cart ID
    private static $cd_carrinho;

    // Private constructor to prevent direct creation of object
    private function __construct() {
        
    }
    var $erro = null;
    public static function setCd_carrinho() {
       
        // If the cart ID hasn't already been set ...
        if (self::$cd_carrinho == '') {
            // If the visitor's cart ID is in the session, get it from there
            if (isset($_SESSION['cd_carrinho'])) {
                self::$cd_carrinho = $_SESSION['cd_carrinho'];
            }
            // If not, check whether the cart ID was saved as a cookie
            elseif (isset($_COOKIE['cd_carrinho'])) {
                // Save the cart ID from the cookie
                self::$cd_carrinho = $_COOKIE['cd_carrinho'];
                $_SESSION['cd_carrinho'] = self::$cd_carrinho;
                // Regenerate cookie to be valid for 7 days (604800 seconds)
                setcookie('cd_carrinho', self::$cd_carrinho, time() + 604800);
            } else {
                /* Generate cart id and save it to the $cd_carrinho class member,
                  the session and a cookie (on subsequent requests $cd_carrinho
                  will be populated from the session) */
                self::$cd_carrinho = md5(uniqid(rand(), true));
                // Store cart id in session
                $_SESSION['cd_carrinho'] = self::$cd_carrinho;
                // Cookie will be valid for 7 days (604800 seconds)
                setcookie('cd_carrinho', self::$cd_carrinho, time() + (86400 * 7 ));
            }
        }

    }

    // Returns the current visitor's card id
    public static function getCd_carrinho() {
        // echo "dias " . qtDiasCarrinhoCookie;
        // Ensure we have a cart id for the current visitor
        if (!isset(self::$cd_carrinho))
            self::setCd_carrinho();
        return self::$cd_carrinho;
    }

    public static function AddItem($cd_produto)
    {
        try {
                    $conexao= new Conexao();
         $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
         $mysqli->set_charset("utf8");
         $stmt = $mysqli->stmt_init();
                  echo "ass 73".self::getCd_carrinho()."-".$cd_produto;
         $stmt->prepare("CALL sp_carrinho_add_item2(?,?)");
         echo "ass 74".self::getCd_carrinho()."-".$cd_produto;
         $stmt->bind_param('si',self::getCd_carrinho(),$cd_produto);
        if(!$stmt->execute())
        {
            echo $mysqli->error;
        }

        $stmt->close();
        $mysqli->close();
        
            echo "ok 83";
        }

         catch (PDOException $exception) 
             {
                echo "erro". $exception->getMessage();
             }
    }
   
     public static function GetQtdProd()
    {
        $conexao= new Conexao();
         $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
         $mysqli->set_charset("utf8");
        $rs = $mysqli->query ( "select sum(qt_quantidade) as qt_itens from carrinhos where cd_carrinho ='".self::getCd_carrinho()."'" );

        $acategorias = array ();
        $qt_itens=0;
        while ( $row = $rs->fetch_assoc () ) {
            $qt_itens=$row ['qt_itens'];
        }
        $rs->close ();
        $mysqli->close ();

        return $qt_itens;
    }
    public static function esvaziarCarrinho()
    {
          $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from carrinhos where cd_carrinho=?");
                $stmt->execute(array(self::getCd_carrinho()));
                
             } 
             catch (PDOException $exception) 
             {
                  echo $exception->getMessage();
             }
    }
    public static function atualizarQuantidade($qt_quantidade,$cd_produto,$cd_carrinho)
    {
       
       if($qt_quantidade > 0 )
       {
            $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("update carrinhos set qt_quantidade=? where cd_produto=?  and cd_carrinho=? ");
                $stmt->execute(array($qt_quantidade,$cd_produto,$cd_carrinho));
                
             } 
             catch (PDOException $exception) 
             {
                 echo $exception->getMessage();
             }
       }
       else {
           self::excluirItem($cd_produto, $cd_carrinho);
       }
    }
     public static function excluirItem($cd_produto,$cd_carrinho)
    {
       
          $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from  carrinhos where cd_produto=?  and cd_carrinho=?");
                $stmt->execute(array($cd_produto,$cd_carrinho));
                
             } 
             catch (PDOException $exception) 
             {
                  echo $exception->getMessage();
             }
    }
}
