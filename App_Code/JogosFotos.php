 <?php

require_once 'Conexao.php';
class JogosFotos {
    //put your code here
    // <editor-fold desc="Atributos">
        
        private $cd_jogo_foto;
        public function setCd_jogo_foto($value) {
		$this->cd_jogo_foto = $value;
	}
	public function getCd_jogo_foto() {
		return $this->cd_jogo_foto;
	}
        
        private $cd_jogo;
	public function setCd_jogo($value) {
		$this->cd_jogo = $value;
	}
	public function getCd_jogo() {
		return $this->cd_jogo;
	}
        private $nm_url_foto;
	public function setNm_url_foto($value) {
		$this->nm_url_foto = $value;
	}
	public function getNm_url_foto() {
		return $this->nm_url_foto;
	}
        
        private $vl_posicao;
	public function setVl_posicao($value) {
		$this->vl_posicao = $value;
	}
	public function getVl_posicao() {
		return $this->vl_posicao;
	}
        
        var $erro = null;
      // </editor-fold>

        public function Inserir($cd_jogo,$nm_url_foto,$vl_posicao) {
		  $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("insert into jogos_fotos (cd_jogo,nm_url_foto,vl_posicao) values (?,?,?)");
                $stmt->execute(array($cd_jogo,$nm_url_foto,$vl_posicao));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                       $this->erro= $exception->getMessage();
             }
//                 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                  $mysqli->set_charset("utf8");
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("insert into jogos_fotos (cd_jogo,nm_url_foto,vl_posicao) values (?,?,?)");
//		 $stmt->bind_param('isi',$cd_jogo,$nm_url_foto,$vl_posicao);
//                if(!$stmt->execute())
//		{
//                     $this->erro =$mysqli->error;
//    		}
//		
//                $stmt->close();
//                $mysqli->close();
        
	}
          public function Excluir($cd_jogo_foto) {
              $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from jogos_fotos where cd_jogo_foto=?");
                $stmt->execute(array($cd_jogo_foto));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                       $this->erro= $exception->getMessage();
             }
//		 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                  $mysqli->set_charset("utf8");
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("delete from jogos_fotos where cd_jogo_foto=?");
//		 $stmt->bind_param('i',$cd_jogo_foto);
//                if(!$stmt->execute())
//		{
//                     $this->erro =$mysqli->error;
//        	}
//                $linhas = $mysqli->affected_rows;
//                $stmt->close();
//                $mysqli->close();
//                return $linhas;
        
	}
        public function AtualizarOrdemFotos($cd_jogo_foto,$vl_posicao) {
                $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("update jogos_fotos set vl_posicao=? where cd_jogo_foto=?");
                $stmt->execute(array($vl_posicao,$cd_jogo_foto));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                       $this->erro= $exception->getMessage();
             }
//             echo "classe ".$cd_jogo_foto."-".$vl_posicao;
//             $conexao= new Conexao();
//             $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//              $mysqli->set_charset("utf8");
//             $stmt = $mysqli->stmt_init();
//             $stmt->prepare("update jogos_fotos set vl_posicao=? where cd_jogo_foto=?");
//             $stmt->bind_param('ii',$vl_posicao,$cd_jogo_foto);
//            if(!$stmt->execute())
//            {
//                 $this->erro =$mysqli->error;
//                 
//            }
//            echo "ok dfd";
//            $stmt->close();
//            $mysqli->close();
        
	}
        public function GetVlPosicao($cd_jogo)
        {
            $conexao= new Conexao();
            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
             $mysqli->set_charset("utf8");
             $rs =$mysqli->query("select max(vl_posicao) as vl_posicao from jogos_fotos where cd_jogo=".$cd_jogo );

            $vl_posicao="";
            while ($row = $rs->fetch_assoc ()) 
            {
               $vl_posicao=$row["vl_posicao"];
            }
            if($vl_posicao=="")
               $vl_posicao=1;
            else
               $vl_posicao++;
          
            return $vl_posicao;
        }
}
