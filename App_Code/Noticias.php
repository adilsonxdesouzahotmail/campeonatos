<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Noticias
 *
 * @author Adilson
 */
error_reporting(E_ALL);
require_once 'Conexao.php';
class Noticias {
    //put your code here
    // <editor-fold desc="Atributos">
        function __construct() {
	
	}
        private $cd_noticia;
        public function setCd_noticia($value) {
		$this->cd_noticia = $value;
	}
	public function getCd_noticia() {
		return $this->cd_noticia;
	}
        
        private $nm_titulo;
	public function setNm_titulo($value) {
		$this->nm_titulo = $value;
	}
	public function getNm_titulo() {
		return $this->nm_titulo;
	}
        
        private $cd_menu;
	public function setCd_menu($value) {
		$this->cd_menu = $value;
	}
	public function getCd_menu() {
		return $this->cd_menu;
	}
        private $nm_menu;
	public function setNm_menu($value) {
		$this->nm_menu = $value;
	}
	public function getNm_menu() {
		return $this->nm_menu;
	}
        
        private $ds_noticia;
	public function setDs_noticia($value) {
		$this->ds_noticia = $value;
	}
	public function getDs_noticia() {
		return $this->ds_noticia;
	}
        
        private $nm_url_foto;
	public function setNm_url_foto($value) {
		$this->nm_url_foto = $value;
	}
	public function getNm_url_foto() {
		return $this->nm_url_foto;
	}
        
         private $nm_posicao_foto;
	public function setNm_posicao_foto($value) {
		$this->nm_posicao_foto = $value;
	}
	public function getNm_posicao_foto() {
		return $this->nm_posicao_foto;
	}
        
        private $dt_noticia;
        public function setDt_noticia($value) {
		$this->dt_noticia = $value;
	}
	public function getDt_noticia() {
		return $this->dt_noticia;
	}        
        
        private $qt_comentario;
        public function setQt_comentario($value) {
		$this->qt_comentario = $value;
	}
	public function getQt_comentario() {
		return $this->qt_comentario;
	} 

        private $qt_comentario_nao_lido;
        public function setQt_comentario_nao_lido($value) {
		$this->qt_comentario_nao_lido = $value;
	}
	public function getQt_comentario_nao_lido() {
		return $this->qt_comentario_nao_lido;
	} 
        
        private $nm_url_youtube;
	public function setNm_url_youtube($value) {
		$this->nm_url_youtube = $value;
	}
	public function getNm_url_youtube() {
		return $this->nm_url_youtube;
	}    
        
        private $nm_usuario;
	public function setNm_usuario($value) {
		$this->nm_usuario = $value;
	}
	public function getNm_usuario() {
		return $this->nm_usuario;
	}

       private $cd_usuario;
	public function setCd_usuario($value) {
		$this->cd_usuario = $value;
	}
	public function getCd_usuario() {
		return $this->cd_usuario;
	}

        private $ic_ativo;
        
    public function setIc_ativo($value) {
        $this->ic_ativo = $value;
    }

    public function getIc_ativo() {
        return $this->ic_ativo;
    }
    
    private $ic_mostra_carrossel;

    public function setIc_mostra_carrossel($value) {
        $this->ic_mostra_carrossel = $value;
    }

    public function getIc_mostra_carrossel() {
        return $this->ic_mostra_carrossel;
    }    
    private $cd_campeonato;

    public function setCd_campeonato($value) {
        $this->cd_campeonato = $value;
    }

    public function getCd_campeonato() {
        return $this->cd_campeonato;
    }        
    //</editor-fold>
        var $erro = null;
        public function getTodosItens($ic_ativo,$qtd_registros) {
		$conexao= new Conexao();
                // echo "bd    ".$conexao->getNm_bd()."....<br/>";
		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
               //  echo $conexao->getNm_bd();
               //  $mysqli->set_charset("utf8");
                $sql_query="select no.*,us.nm_usuario, ".
                                      "(select count(cd_noticia_comentario) from noticias_comentarios where cd_noticia= no.cd_noticia) as qt_comentario, ".
                                      "(select count(cd_noticia_comentario) from noticias_comentarios where cd_noticia= no.cd_noticia and (ic_lido is null or ic_lido=0)) as qt_comentario_nao_lido ".
                                       "from noticias no ".
                                       "join usuarios us on no.cd_usuario = us.cd_usuario ".
                                       "where 1=1 ";
                if($ic_ativo != "")
                {
                    $sql_query .= "and ic_ativo=".$ic_ativo." ";
                }
                $sql_query .="order by no.cd_noticia desc";
                if($qtd_registros != "")
                {
                    $sql_query .=" limit ".$qtd_registros;
                }
		$rs = $mysqli->query ( $sql_query);
		echo $mysqli->error;
		$anoticias = array ();
		
		while ( $row = $rs->fetch_assoc () ) {
			$tempnoticias = new Noticias ();
			$tempnoticias->setCd_noticia ( $row ['cd_noticia'] );
			$tempnoticias->setNm_titulo ( $row ['nm_titulo'] );
			$tempnoticias->setCd_menu ( $row ['cd_menu'] );
                        $tempnoticias->setDs_noticia ( $row ['ds_noticia'] );
                        $tempnoticias->setDt_noticia ( $row ['dt_noticia'] );     
                        $tempnoticias->setNm_url_youtube($row ['nm_url_youtube']);
                        $tempnoticias->setNm_url_foto($row ['nm_url_foto']);
                        $tempnoticias->setNm_usuario ( $row ['nm_usuario'] );
                        $tempnoticias->setCd_campeonato($row ['cd_campeonato'] );
                        $tempnoticias->setQt_comentario($row ['qt_comentario']);
                        $tempnoticias->setQt_comentario_nao_lido($row ['qt_comentario_nao_lido']);
			$anoticias [] = $tempnoticias;
		}
		$rs->close ();
		$mysqli->close ();
		
		$this->meuarray = $anoticias;
	
	}
        
        public function Inserir(Noticias $noticia) {
                $conexao= new Conexao();
                try {
                   $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                   $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                   $stmt = $dbh->prepare("insert into noticias (nm_titulo,cd_menu,ds_noticia,dt_noticia,nm_url_youtube,cd_usuario,cd_campeonato,ic_ativo,ic_mostra_carrossel)".
                      "values(?,?,?,now(),?,?,?,?,?)");
           
                  $nm_titulo=$noticia->getNm_titulo();
                  $cd_menu=$noticia->getCd_menu();
                  $ds_noticia=$noticia->getDs_noticia();
                  $nm_url_youtube=$noticia->getNm_url_youtube();
                  $cd_usuario =  $noticia->getCd_usuario();
                  $cd_campeonato =$noticia->getCd_campeonato();
                  $ic_ativo = $noticia->getIc_ativo();
                  $ic_mostra_carrossel = $noticia->getIc_mostra_carrossel();

                   $stmt->execute(array($nm_titulo,$cd_menu,$ds_noticia,$nm_url_youtube,$cd_usuario,$cd_campeonato,
                       $ic_ativo,$ic_mostra_carrossel));
                   $this->erro = "";   

                } 
                catch (PDOException $exception) 
                {
                       $this->erro= $exception->getMessage();
                       echo  $exception->getMessage();
                }

                 //self::geraMenuXml();
	}
        public function Atualizar(Noticias $noticia) {
		$conexao= new Conexao();
                try {
                   $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                   $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                   $stmt = $dbh->prepare("update noticias set nm_titulo=?,cd_menu=?,ds_noticia=?,nm_url_foto = ?,nm_posicao_foto=?,".
                                        "dt_noticia=now(),nm_url_youtube=?,cd_campeonato=?,ic_ativo=?,ic_mostra_carrossel=? where cd_noticia=?");
              
                    $nm_titulo=$noticia->getNm_titulo();
                    $cd_menu=$noticia->getCd_menu();
                    $ds_noticia=$noticia->getDs_noticia();
                    $nm_url_foto  = $noticia->getNm_url_foto();
                    $nm_posicao_foto = $noticia->getNm_posicao_foto();
                    $nm_url_youtube = $noticia->getNm_url_youtube();
                    $cd_campeonato = $noticia->getCd_campeonato();
                    $ic_ativo = $noticia->getIc_ativo();
                    $ic_mostra_carrossel = $noticia->getIc_mostra_carrossel();
                    $cd_noticia=$noticia->getCd_noticia();

                  
                   $stmt->execute(array($nm_titulo,$cd_menu,$ds_noticia,$nm_url_foto,$nm_posicao_foto,$nm_url_youtube,$cd_campeonato,$ic_ativo,$ic_mostra_carrossel,$cd_noticia));
                   $this->erro = "";   

                } 
                catch (PDOException $exception) 
                {
                       $this->erro= $exception->getMessage();
                }

        
	}
        
        public function Excluir($cd_noticia) {
                $conexao= new Conexao();
                try {
                   $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                   $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                   $stmt = $dbh->prepare("delete from noticias where cd_noticia=?");
                   $stmt->execute(array($cd_noticia));
                   $this->erro = "";   

                } 
                catch (PDOException $exception) 
                {
                       $this->erro= $exception->getMessage();
                }

                 
          }
          

          public function getUmItem($cd_noticia) {
		
                 $conexao= new Conexao();
                 try {
                    $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                    
//                    $stmt = $dbh->prepare("select pa.*,me.nm_menu from noticias pa ".
//                                "inner join menus me on pa.cd_menu  = me.cd_menu ".
//                                "where pa.cd_noticia=?; ");
                    $stmt = $dbh->prepare("select no.*,us.nm_usuario from noticias no ".
                                       "join usuarios us on no.cd_usuario = no.cd_usuario ".
                                      "where no.cd_noticia=? order by no.cd_noticia");
                    $stmt->execute(array($cd_noticia));
                    
                    while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {

                        $this->cd_noticia = $row["cd_noticia"];
                        $this->nm_titulo = $row["nm_titulo"];
                        $this->cd_menu = $row["cd_menu"];
                        $this->ds_noticia = $row["ds_noticia"];
                        $this->nm_url_foto  = $row["nm_url_foto"];
                        $this->nm_posicao_foto = $row["nm_posicao_foto"];
                      //  $this->nm_menu = $row["nm_menu"];
                        $this->nm_url_youtube = $row["nm_url_youtube"];
                        $this->dt_noticia =  $row["dt_noticia"];
                        $this->nm_usuario = $row["nm_usuario"];
                        $this->cd_campeonato = $row["cd_campeonato"];
                        $this->ic_ativo = $row["ic_ativo"];
                        $this->ic_mostra_carrossel = $row["ic_mostra_carrossel"];
                      }
                                     
                 } 
                 catch (PDOException $exception) 
                 {
                    echo "Connection error: " . $exception->getMessage();
                 }
                 
 	}
        
        public static function geraMenuXml()
        {
            
            $xmlmenu = "<?xml version='1.0' encoding='UTF-8'?>";
            $xmlmenu.="<menu>";
            $conexao= new Conexao();
            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
             $mysqli->set_charset("utf8");
            $rs = $mysqli->query ( "select * from menus" );

            while ( $row = $rs->fetch_assoc () ) {

                   $xmlmenu.="<menuoutros>";
                   $xmlmenu.="<nm_menu>".$row ['nm_menu']."</nm_menu>";
                   $xmlmenu.="<cd_menu>".$row ['cd_menu']."</cd_menu>";
                   $rs2 = $mysqli->query ( "select * from noticias where cd_menu=".$row ['cd_menu']);
                   while ( $row2 = $rs2->fetch_assoc () ) {

                       $xmlmenu.="<noticia>";
                       $xmlmenu.="<nm_titulo>".$row2 ['nm_titulo']."</nm_titulo>";
                       $xmlmenu.="<cd_noticia>".$row2 ['cd_noticia']."</cd_noticia>";
                       $xmlmenu.="</noticia>";
                   }
                   $xmlmenu.="</menuoutros>";
            }

            $rs->close ();
            $mysqli->close ();
            $xmlmenu.="</menu>";
          // echo dirname(__FILE__);
            //unlink("../../menu/menuoutros.xml" );dirname(__FILE__)
            //file_put_contents("../../menu/menuoutros.xml",$xmlmenu);
            file_put_contents(dirname(__FILE__)."/../menu/menuoutros.xml",$xmlmenu);
        }
        
        public function UltimaNoticia()
        {
                 $conexao= new Conexao();
		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
                  $mysqli->set_charset("utf8");
		$rs = $mysqli->query ( "select max(cd_noticia) as cd_noticia from noticias" );
		list($cd_noticia)= $rs->fetch_row();
		$rs->close ();
               	$mysqli->close ();
                return $cd_noticia;
	 }
}
