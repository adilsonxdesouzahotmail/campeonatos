<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Idiomas_jogo
 *
 * @author Adilson
 */
require_once 'Conexao.php';
class Idiomas_jogo {
    //put your code here
     ####   Atributos  ##########

        private $cd_idioma;
	public function setCd_idioma($value) {
		$this->cd_idioma = $value;
	}
	public function getCd_idioma() {
		return $this->cd_idioma;
	}
        private $cd_jogo;
	public function setCd_jogo($value) {
		$this->cd_jogo = $value;
	}
	public function getCd_jogo() {
		return $this->cd_jogo;
	}
       #### Fim  Atributos  ##########
         ####   Métodos   ########
	var $mysqli = null;
	var $meuarray = array ();
	var $erro = null;
	public function temIdioma($cd_jogo,$cd_idioma)
        {
                $conexao= new Conexao();
		$mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
		$rs = $mysqli->query ( "select count(cd_idioma) as ic_temidioma  from idiomas_jogo ".
                        "where cd_jogo=".$cd_jogo." and cd_idioma=".$cd_idioma);
                list($ic_temidioma )= $rs->fetch_row();
		$rs->close ();
               	$mysqli->close ();
                return $ic_temidioma;
	 }
        public function Atualizar($cd_jogo ,$arrayidiomas)
        {
           //echo "idi51<br/>";
           $conexao= new Conexao();
           $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());

              $qtde = count($arrayidiomas);
              if($qtde>0)
              {
                   $sqlidioma = "insert into idiomas_jogo values ";
                    $values="";
                    $contador=1;
                    foreach($arrayidiomas as $cd_idioma)
                    {
                      
                        $values.="(".$cd_jogo.",".$cd_idioma.")";
                        if($contador < $qtde)
                            $values.=",";
                        $contador++;
                    }
                    $sqlidioma.=$values;
                    //echo "sqlidi".$sqlidioma;
                    if(!$mysqli->query ($sqlidioma))
                    {
                         $this->erro =$mysqli->error;
                    }
               }
           
        }
         public function Excluir($cd_jogo) {
               $idioma_jogo = new Idiomas_jogo();
               // $idioma_jogo->ExcluirIdioma($cd_idioma);
                
             $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from idiomas_jogo where cd_jogo=?");
                $stmt->execute(array($cd_jogo));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                    $this->erro= $exception->getMessage();
             }

          }
          public function ExcluirIdioma($cd_idioma) {
              $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from idiomas_jogo where cd_idioma=?");
                $stmt->execute(array($cd_idioma));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                    $this->erro= $exception->getMessage();
             }

}
}
