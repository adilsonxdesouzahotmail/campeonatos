 <?php

require_once 'Conexao.php';
class Usuarios {

        private $cd_usuario;
	public function setCd_usuario($value) {
		$this->cd_usuario = $value;
	}
	public function getCd_usuario() {
		return $this->cd_usuario;
	}
        private $nm_login;
	public function setNm_login($value) {
		$this->nm_login = $value;
	}
	public function getNm_login() {
		return $this->nm_login;
	}
        private $nm_senha;
	public function setNm_senha($value) {
		$this->nm_senha = $value;
	}
	public function getNm_senha() {
		return $this->nm_senha;
	}
        
        private $nm_usuario;
	public function setNm_usuario($value) {
		$this->nm_usuario = $value;
	}
	public function getNm_usuario() {
		return $this->nm_usuario;
	}
        
         private $nm_email;
	public function setNm_email($value) {
		$this->nm_email = $value;
	}
	public function getNm_email() {
		return $this->nm_email;
	}
        
        private $qt_nivel;
	public function setQt_nivel($value) {
		$this->qt_nivel = $value;
	}
	public function getQt_nivel() {
		return $this->qt_nivel;
	}
        ####  Fim Atributos  ##########
        var $mysqli = null;
	var $meuarray = array ();
	var $erro = null;
//         public function Login($nm_login,$nm_senha) {
//		
//                 $conexao= new Conexao();
//                 echo $conexao->getNm_senha();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                 if ($mysqli->errno) {
//                printf("Unable to connect to the database:<br /> %s",
//                $mysqli->error);
//                exit();
//                }
//                 $mysqli->set_charset("utf8");
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("select nm_usuario,cd_usuario from usuarios where nm_login=? and nm_senha=?");
//
//                 $stmt->bind_param('ss',$nm_login,$nm_senha);
//                 $stmt->execute();
//                 $stmt->bind_result($nm_usuario,$cd_usuario);
//                 while($stmt->fetch())
//                 {
//                     $this->nm_usuario = $nm_usuario;
//                     $this->cd_usuario = $cd_usuario;
//                 }
//                 $this->erro=$mysqli->error;
//                 echo $this->erro;
//                $stmt->close();
//                $mysqli->close();
// 	}
         public function Login($nm_login,$nm_senha) {
		
                 $conexao= new Conexao();
                 try {
                    $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                    $stmt = $dbh->prepare("select nm_usuario,cd_usuario,nm_login from usuarios where nm_login=? and nm_senha=?");
                    $stmt->execute(array($nm_login, $nm_senha));
                    
                    while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {

                        $this->nm_usuario = $row["nm_usuario"];
                        $this->cd_usuario = $row["cd_usuario"];
                      }
                     $this->erro = "";   
                 } 
                 catch (PDOException $exception) 
                 {
                    $this->erro= $exception->getMessage();
                 }
             
 	}
        public function AlterarSenha($cd_usuario,$nm_senha_atual,$nm_senha_nova) {
		$conexao = new Conexao();
                $cd_usuario2="";
                try {
                    $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                    $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                    $stmt = $dbh->prepare("select cd_usuario from usuarios where cd_usuario=? and nm_senha=?");
                    $stmt->execute(array($cd_usuario,$nm_senha_atual));
                    while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                        $cd_usuario2 = $row["cd_usuario"];
                    }
                  }
                  catch (PDOException $exception) 
                   {
                         $this->erro= $exception->getMessage();
                   }
                 if($cd_usuario2!="")
                {
                     try {
                        $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                        $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                        $stmt = $dbh->prepare("update usuarios set nm_senha=? where cd_usuario=?");
                        $stmt->execute(array($nm_senha_nova,$cd_usuario2));
                        $this->erro = "";   
                     } 
                     catch (PDOException $exception) 
                     {
                           $this->erro= $exception->getMessage();
                     }
                }      
                else
                {
                         $this->erro ="Senha Incorreta";

                }
//                 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                 $mysqli->set_charset("utf8");
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("select cd_usuario from usuarios where cd_usuario=? and nm_senha=?");
//
//                 $stmt->bind_param('is',$cd_usuario,$nm_senha_atual);
//                 $stmt->execute();
//                 $stmt->bind_result($cd_usuario2);
//                 $stmt->fetch();
//
//                 if($cd_usuario2!="")
//                 {
//                     $stmt->close();
//                    $mysqli->close();
//                    $mysqli2 = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                     $sql2="update usuarios set nm_senha=? where cd_usuario=?";
//                        $stmt2 = $mysqli2->stmt_init();
//                        $stmt2->prepare($sql2);
//                       
//                        $stmt2->bind_param('si',$nm_senha_nova,$cd_usuario2);
//                       if(!$stmt2->execute())
//                       {
//                           $this->erro =$mysqli2->error;
//                       }
//                       $stmt2->close();
//                       $mysqli2->close();
//                 }
//                 else
//                  {
//                           $this->erro ="Senha Incorreta";
//                            $stmt->close();
//                           $mysqli->close();
//                  }
//                 
              
               
 	}
          public function getUmItem($cd_usuario) {
		 $conexao = new Conexao();
                try {
                   $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                   $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                   $stmt = $dbh->prepare("select * from usuarios where cd_usuario=?");
                   $stmt->execute(array($cd_usuario));

                   while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                      	$this->cd_usuario= $row["cd_usuario"];
                        $this->nm_login= $row["nm_login"];
                        $this->qt_nivel= $row["qt_nivel"];
                        $this->nm_usuario= $row["nm_usuario"];
                        $this->nm_email= $row["nm_email"];
                     }
                    $this->erro = "";   
                } 
                catch (PDOException $exception) 
                {
                   $this->erro= $exception->getMessage();
                }
//                 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                 $mysqli->set_charset("utf8");
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("select * from usuarios where cd_usuario=?");
//
//                 $stmt->bind_param('i',$cd_usuario);
//                 $stmt->execute();
//                 $stmt->bind_result($cd_usuario,$nm_login,$nm_senha,$qt_nivel,$nm_usuario,$nm_email);
//                 while($stmt->fetch())
//                 {
//                     $this->cd_usuario = $cd_usuario;
//                     $this->nm_login = $nm_login;
//                     $this->qt_nivel = $qt_nivel;
//                     $this->nm_usuario = $nm_usuario;
//                     $this->nm_email = $nm_email;
//                    
//                 }
//                 $this->erro=$mysqli->error;
//                 echo $this->erro;
//                $stmt->close();
//                $mysqli->close();
 	}
         public function Inserir(Usuarios $usuario) {
            $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $query="insert into usuarios (nm_login,nm_senha,qt_nivel,nm_usuario,nm_email)".
                      "values(?,?,?,?,?)";
                $stmt = $dbh->prepare($query);
                
                $nm_login=$usuario->getNm_login();
                 $nm_senha=$usuario->getNm_senha();
                 $qt_nivel=$usuario->getQt_nivel();
                 $nm_usuario=$usuario->getNm_usuario();
                 $nm_email=$usuario->getNm_email();
                 
                $stmt->execute(array($nm_login,$nm_senha,$qt_nivel,$nm_usuario,$nm_email));
                $this->erro = "";   

             } 
             catch (PDOException $exception) 
             {
                 if(strpos($exception->getMessage(), 'UQ_usuarios'))
                    $this->erro = "Este login já está sendo usado." ;     
                else
                    $this->erro= $exception->getMessage();

             }
//                 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                  $mysqli->set_charset("utf8");
//                 $sql="insert into usuarios (nm_login,nm_senha,qt_nivel,nm_usuario,nm_email)".
//                      "values(?,?,?,?,?)";
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare($sql);
//                 
//                 $nm_login=$usuario->getNm_login();
//                 $nm_senha=$usuario->getNm_senha();
//                 $qt_nivel=$usuario->getQt_nivel();
//                 $nm_usuario=$usuario->getNm_usuario();
//                 $nm_email=$usuario->getNm_email();
//                 
//		 $stmt->bind_param('ssiss',$nm_login,$nm_senha,$qt_nivel,$nm_usuario,$nm_email);
//                if(!$stmt->execute())
//		{
//                    if(strpos($mysqli->error, 'UQ_usuarios'))
//                        $this->erro = "Este login já está sendo usado." ;     
//                    else
//                        $this->erro =$mysqli->error;
//    		}
//		
//                $stmt->close();
//                $mysqli->close();
        
	}
          public function Atualizar(Usuarios $usuario) {
		$conexao= new Conexao();
                try {
                   $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                   $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                   $query ="update usuarios set nm_login=?,qt_nivel=?,nm_usuario=?,nm_email=? ".
                      "where cd_usuario=?";
                   $stmt = $dbh->prepare($query);
                  
                   $nm_login=$usuario->getNm_login();
                    $qt_nivel=$usuario->getQt_nivel();
                    $nm_usuario=$usuario->getNm_usuario();
                    $nm_email=$usuario->getNm_email();
                     $cd_usuario=$usuario->getCd_usuario();
                     
                   $stmt->execute(array($nm_login,$qt_nivel,$nm_usuario,$nm_email,$cd_usuario));
                   $this->erro = "";   

                } 
                catch (PDOException $exception) 
                {
                    if(strpos($exception->getMessage(), 'UQ_usuarios'))
                       $this->erro = "Este login já está sendo usado." ;     
                   else
                       $this->erro= $exception->getMessage();

                }
//                 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                 $mysqli->set_charset("utf8");
//                 $sql="update usuarios set nm_login=?,qt_nivel=?,nm_usuario=?,nm_email=? ".
//                      "where cd_usuario=?";
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare($sql);
//                 
//                 $nm_login=$usuario->getNm_login();
//                 $qt_nivel=$usuario->getQt_nivel();
//                 $nm_usuario=$usuario->getNm_usuario();
//                 $nm_email=$usuario->getNm_email();
//                 $cd_usuario=$usuario->getCd_usuario();
//                 
//		 $stmt->bind_param('sissi',$nm_login,$qt_nivel,$nm_usuario,$nm_email,$cd_usuario);
//                if(!$stmt->execute())
//		{
//                    if(strpos($mysqli->error, 'UQ_usuarios'))
//                        $this->erro = "Este login já está sendo usado." ;     
//                    else if(strpos($mysqli->error, 'UQ_usuarios'))
//                        $this->erro = "Este login já está sendo usado." ;  
//                    else
//                        $this->erro =$mysqli->error;
//    		}
//		
//                $stmt->close();
//                $mysqli->close();
        
	}
         public function Excluir($cd_usuario) {
             $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from usuarios where cd_usuario=?");
                $stmt->execute(array($cd_usuario));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                    $this->erro= $exception->getMessage();
             }
//		 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("delete from usuarios where cd_usuario=?");
//		 $stmt->bind_param('i',$cd_usuario);
//                if(!$stmt->execute())
//		{
//                   $this->erro =$mysqli->error;
//       		}
//               
//                $stmt->close();
//                $mysqli->close();
          }
           public function getTodosItens($cd_usuario) {
		$conexao= new Conexao();
		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
                 $mysqli->set_charset("utf8");
		$rs = $mysqli->query ( "select * from usuarios where cd_usuario !=".$cd_usuario );
		echo $mysqli->error;
		$ausuarios = array ();
		
		while ( $row = $rs->fetch_assoc () ) {
			$tempusuarios = new Usuarios ();
                        $tempusuarios->setCd_usuario ( $row ['cd_usuario'] );
			$tempusuarios->setNm_login ( $row ['nm_login'] );
			$tempusuarios->setQt_nivel ( $row ['qt_nivel'] );
			$tempusuarios->setNm_usuario ( $row ['nm_usuario'] );
                        $tempusuarios->setNm_email ( $row ['nm_email'] );
                        
			$ausuarios [] = $tempusuarios;
		}
		$rs->close ();
		$mysqli->close ();
		
		$this->meuarray = $ausuarios;
	
	}
}
