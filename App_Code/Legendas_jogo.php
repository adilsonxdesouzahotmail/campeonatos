<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Legendas_jogo
 *
 * @author Adilson
 */
require_once 'Conexao.php';

class Legendas_jogo {

    //put your code here
    ####   Atributos  ##########

    private $cd_legenda;

    public function setCd_legenda($value) {
        $this->cd_legenda = $value;
    }

    public function getCd_legenda() {
        return $this->cd_legenda;
    }

    private $cd_jogo;

    public function setCd_jogo($value) {
        $this->cd_jogo = $value;
    }

    public function getCd_jogo() {
        return $this->cd_jogo;
    }

    #### Fim  Atributos  ##########
    ####   Métodos   ########

    var $mysqli = null;
    var $meuarray = array();
    var $erro = null;

    public function temLegenda($cd_jogo, $cd_legenda) {
        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $rs = $mysqli->query("select count(cd_legenda) as ic_temlegenda  from legendas_jogo " .
                "where cd_jogo=" . $cd_jogo . " and cd_legenda=" . $cd_legenda);
        list($ic_temlegenda ) = $rs->fetch_row();
        $rs->close();
        $mysqli->close();
        return $ic_temlegenda;
    }

    public function Atualizar($cd_jogo, $arraylegendas) {
        //echo "idi51<br/>";
        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());

        $qtde = count($arraylegendas);
        if ($qtde > 0) {
            $sqllegenda = "insert into legendas_jogo values ";
            $values = "";
            $contador = 1;
            foreach ($arraylegendas as $cd_legenda) {

                $values .= "(" . $cd_jogo . "," . $cd_legenda . ")";
                if ($contador < $qtde)
                    $values .= ",";
                $contador++;
            }
            $sqllegenda .= $values;
            //echo "sqlidi".$sqllegenda;
            if (!$mysqli->query($sqllegenda)) {
                $this->erro = $mysqli->error;
            }
        }
    }

    public function Excluir($cd_jogo) {
        $legenda_jogo = new Legendas_jogo();
        // $legenda_jogo->ExcluirLegenda($cd_legenda);

        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $dbh->prepare("delete from legendas_jogo where cd_jogo=?");
            $stmt->execute(array($cd_jogo));
            $this->erro = "";
        } catch (PDOException $exception) {
            $this->erro = $exception->getMessage();
        }
    }

    public function ExcluirLegenda($cd_legenda) {
        $conexao = new Conexao();
        try {
            $dbh = new PDO("mysql:host=" . $conexao->getNm_servidor() . ";dbname=" . $conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $dbh->prepare("delete from legendas_jogo where cd_legenda=?");
            $stmt->execute(array($cd_legenda));
            $this->erro = "";
        } catch (PDOException $exception) {
            $this->erro = $exception->getMessage();
        }
    }

}
