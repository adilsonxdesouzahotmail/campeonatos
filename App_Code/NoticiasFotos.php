 <?php

require_once 'Conexao.php';
class NoticiasFotos {
    //put your code here
    // <editor-fold desc="Atributos">
        
        private $cd_noticia_foto;
        public function setCd_noticia_foto($value) {
		$this->cd_noticia_foto = $value;
	}
	public function getCd_noticia_foto() {
		return $this->cd_noticia_foto;
	}
        
        private $cd_noticia;
	public function setCd_noticia($value) {
		$this->cd_noticia = $value;
	}
	public function getCd_noticia() {
		return $this->cd_noticia;
	}
        private $nm_url_foto;
	public function setNm_url_foto($value) {
		$this->nm_url_foto = $value;
	}
	public function getNm_url_foto() {
		return $this->nm_url_foto;
	}
        
        private $vl_posicao;
	public function setVl_posicao($value) {
		$this->vl_posicao = $value;
	}
	public function getVl_posicao() {
		return $this->vl_posicao;
	}
        
        var $erro = null;
      // </editor-fold>

        public function Inserir($cd_noticia,$nm_url_foto,$vl_posicao) {
		  $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("insert into noticias_fotos (cd_noticia,nm_url_foto,vl_posicao) values (?,?,?)");
                $stmt->execute(array($cd_noticia,$nm_url_foto,$vl_posicao));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                       $this->erro= $exception->getMessage();
             }
//                 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                  $mysqli->set_charset("utf8");
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("insert into noticias_fotos (cd_noticia,nm_url_foto,vl_posicao) values (?,?,?)");
//		 $stmt->bind_param('isi',$cd_noticia,$nm_url_foto,$vl_posicao);
//                if(!$stmt->execute())
//		{
//                     $this->erro =$mysqli->error;
//    		}
//		
//                $stmt->close();
//                $mysqli->close();
        
	}
          public function Excluir($cd_noticia_foto) {
              $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from noticias_fotos where cd_noticia_foto=?");
                $stmt->execute(array($cd_noticia_foto));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                       $this->erro= $exception->getMessage();
             }
//		 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                  $mysqli->set_charset("utf8");
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("delete from noticias_fotos where cd_noticia_foto=?");
//		 $stmt->bind_param('i',$cd_noticia_foto);
//                if(!$stmt->execute())
//		{
//                     $this->erro =$mysqli->error;
//        	}
//                $linhas = $mysqli->affected_rows;
//                $stmt->close();
//                $mysqli->close();
//                return $linhas;
        
	}
        public function AtualizarOrdemFotos($cd_noticia_foto,$vl_posicao) {
                $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("update noticias_fotos set vl_posicao=? where cd_noticia_foto=?");
                $stmt->execute(array($vl_posicao,$cd_noticia_foto));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                       $this->erro= $exception->getMessage();
             }
//             echo "classe ".$cd_noticia_foto."-".$vl_posicao;
//             $conexao= new Conexao();
//             $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//              $mysqli->set_charset("utf8");
//             $stmt = $mysqli->stmt_init();
//             $stmt->prepare("update noticias_fotos set vl_posicao=? where cd_noticia_foto=?");
//             $stmt->bind_param('ii',$vl_posicao,$cd_noticia_foto);
//            if(!$stmt->execute())
//            {
//                 $this->erro =$mysqli->error;
//                 
//            }
//            echo "ok dfd";
//            $stmt->close();
//            $mysqli->close();
        
	}
        public function GetVlPosicao($cd_noticia)
        {
            $conexao= new Conexao();
            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
             $mysqli->set_charset("utf8");
             $rs =$mysqli->query("select max(vl_posicao) as vl_posicao from noticias_fotos where cd_noticia=".$cd_noticia );

            $vl_posicao="";
            while ($row = $rs->fetch_assoc ()) 
            {
               $vl_posicao=$row["vl_posicao"];
            }
            if($vl_posicao=="")
               $vl_posicao=1;
            else
               $vl_posicao++;
          
            return $vl_posicao;
        }
}
