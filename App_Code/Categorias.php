<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Categorias
 *
 * @author Adilson
 */
require_once 'Conexao.php';
class Categorias {
    //put your code here
   
      ####   Atributos  ##########

        private $cd_categoria;
	public function setCd_categoria($value) {
		$this->cd_categoria = $value;
	}
	public function getCd_categoria() {
		return $this->cd_categoria;
	}
	
        private $nm_categoria;
	public function setNm_categoria($value) {
		$this->nm_categoria = $value;
	}
	public function getNm_categoria() {
		return $this->nm_categoria;
	}
        private $prox_cd_categoria;
	public function setProx_cd_categoria($value) {
		$this->prox_cd_categoria = $value;
	}
	public function getProx_cd_categoria() {
		return $this->prox_cd_categoria;
	}
        ####  Fim Atributos  ##########
        ####   M�todos   ########
	var $mysqli = null;
	var $meuarray = array ();
	var $erro = null;
	

       public function Inserir($nm_categoria) {
		
             $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("insert into categorias (nm_categoria) values (?)");
                $stmt->execute(array($nm_categoria));
                $this->erro = "";   

             } 
             catch (PDOException $exception) 
             {
                 if(strpos($exception->getMessage(), 'UQ_categorias'))
                    $this->erro = "Categoria já está cadastrada."  ;     
                else
                    $this->erro= $exception->getMessage();

             }

            self::geraMenuXml();

    }
     public function Atualizar(Categorias $categoria) {
           $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("update categorias set nm_categoria = ? where cd_categoria=?");
                $stmt->execute(array($categoria->getNm_categoria(),$categoria->getCd_categoria()));
                $this->erro = "";   

             } 
             catch (PDOException $exception) 
             {
                 if(strpos($exception->getMessage(), 'UQ_categorias'))
                    $this->erro = "Categoria já está cadastrada."  ;     
                else
                    $this->erro= $exception->getMessage();

             }

            self::geraMenuXml();

    }

   public function Excluir(Categorias $categoria) {

             $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from categorias where cd_categoria=?");
                $stmt->execute(array($categoria->getCd_categoria()));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                 if(strpos($exception->getMessage(), 'FK_jogos'))
                     $this->erro = "Categoria está associada a jogo(s).Exclusão não permitida."  ;
                else
                    $this->erro= $exception->getMessage();
             }
            self::geraMenuXml2();
    }
     public function getUmItem($cd_categoria) {

          $conexao = new Conexao();
            try {
               $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
               $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
               $stmt = $dbh->prepare("select * from categorias where cd_categoria=? ");
               $stmt->execute(array($cd_categoria));

               while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                    $this->cd_categoria = $row["cd_categoria"];
                   $this->nm_categoria = $row["nm_categoria"];
                 }
                $this->erro = "";   
            } 
            catch (PDOException $exception) 
            {
               $this->erro= $exception->getMessage();
            }
    }
     public function proxCdCategoria() {

        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $mysqli->set_charset("utf8");
        $query = "select max(cd_categoria) as ultimo_cd_categoria from categorias";
        $rs = $mysqli->query($query);

        while ($row = $rs->fetch_assoc()) {
            $this->prox_cd_categoria = $row ["ultimo_cd_categoria"];
        }
        if ($this->prox_cd_categoria == "")
            $this->prox_cd_categoria = 1;
        else
            $this->prox_cd_categoria++;
        $mysqli->close();
    }
    public static function geraMenuXml()
    {
       
        $xmlmenu = "<?xml version='1.0' encoding='UTF-8'?>";
        $xmlmenu.="<menu>";
        $conexao= new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
        // $mysqli->set_charset("utf8");
        $rs = $mysqli->query ( "select * from categorias" );

        while ( $row = $rs->fetch_assoc () ) {
               
               $xmlmenu.="<categoria>";
               $xmlmenu.="<nm_categoria>".$row ['nm_categoria']."</nm_categoria>";
               $xmlmenu.="<cd_categoria>".$row ['cd_categoria']."</cd_categoria>";
               $rs2 = $mysqli->query ( "select * from subcategorias where cd_categoria=".$row ['cd_categoria']);
               while ( $row2 = $rs2->fetch_assoc () ) {
                  
                   $xmlmenu.="<subcategoria>";
                   $xmlmenu.="<nm_subcategoria>".$row2 ['nm_subcategoria']."</nm_subcategoria>";
                   $xmlmenu.="<cd_subcategoria>".$row2 ['cd_subcategoria']."</cd_subcategoria>";
                   $xmlmenu.="</subcategoria>";
               }
               $xmlmenu.="</categoria>";
        }
        
        $rs->close ();
        $mysqli->close ();
        $xmlmenu.="</menu>";
     
        file_put_contents("../../menu/menu.xml",$xmlmenu);
               
    }
    public static function geraMenuXml2()
    {
        
        $xmlmenu = "<?xml version='1.0' encoding='UTF-8'?>";
        $xmlmenu.="<menu>";
        $conexao= new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
         //$mysqli->set_charset("utf8");
        $rs = $mysqli->query ( "select * from categorias" );

        while ( $row = $rs->fetch_assoc () ) {
               
               $xmlmenu.="<categoria>";
               $xmlmenu.="<nm_categoria>".$row ['nm_categoria']."</nm_categoria>";
               $xmlmenu.="<cd_categoria>".$row ['cd_categoria']."</cd_categoria>";
               $rs2 = $mysqli->query ( "select * from subcategorias where cd_categoria=".$row ['cd_categoria']);
               while ( $row2 = $rs2->fetch_assoc () ) {
                  
                   $xmlmenu.="<subcategoria>";
                   $xmlmenu.="<nm_subcategoria>".$row2 ['nm_subcategoria']."</nm_subcategoria>";
                   $xmlmenu.="<cd_subcategoria>".$row2 ['cd_subcategoria']."</cd_subcategoria>";
                   $xmlmenu.="</subcategoria>";
               }
               $xmlmenu.="</categoria>";
        }
        
        $rs->close ();
        $mysqli->close ();
        $xmlmenu.="</menu>";
      
        file_put_contents("../menu/menu.xml",$xmlmenu);
               
    }

}
