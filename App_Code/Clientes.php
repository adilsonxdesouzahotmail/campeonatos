<?php

require_once 'Conexao.php';

class Clientes {

    // <editor-fold defaultstate="collapsed" desc="Atributos">
    private $cd_cliente;
///////
//
///////    
    public function setCd_cliente($value) {
        $this->cd_cliente = $value;
    }

    public function getCd_cliente() {
        return $this->cd_cliente;
    }

    private $nome_cliente;

    public function setNome_cliente($value) {
        $this->nome_cliente = $value;
    }

    public function getNome_cliente() {
        return $this->nome_cliente;
    }

    private $cpf_cliente;

    public function setCpf_cliente($value) {
        $this->cpf_cliente = $value;
    }

    public function getCpf_cliente() {
        return $this->cpf_cliente;
    }

    private $cep_cliente;
    public function setCep_cliente($value) {
        $this->cep_cliente = $value;
    }
    public function getCep_cliente() {
        return $this->cep_cliente;
    }

    private $rua_cliente;

    public function setRua_cliente($value) {
        $this->rua_cliente = $value;
    }

    public function getRua_cliente() {
        return $this->rua_cliente;
    }

    private $num_cliente;

    public function setNum_cliente($value) {
        $this->num_cliente = $value;
    }

    public function getNum_cliente() {
        return $this->num_cliente;
    }

    private $complemento_cliente;

    public function setComplemento_cliente($value) {
        $this->complemento_cliente = $value;
    }

    public function getComplemento_cliente() {
        return $this->complemento_cliente;
    }

    private $bairro_cliente;

    public function setBairro_cliente($value) {
        $this->bairro_cliente = $value;
    }

    public function getBairro_cliente() {
        return $this->bairro_cliente;
    }

    private $cidade_cliente;

    public function setCidade_cliente ($value) {
        $this->cidade_cliente = $value;
    }

    public function getCidade_cliente() {
        return $this->cidade_cliente;
    }

    private $uf_cliente;

    public function setUf_cliente($value) {
        $this->uf_cliente = $value;
    }

    public function getUf_cliente() {
        return $this->uf_cliente;
    }

    private $ddd_cliente;

    public function setDdd_cliente($value) {
        $this->ddd_cliente = $value;
    }

    public function getDdd_cliente() {
        return $this->ddd_cliente;
    }

    private $telefone_cliente;

    public function setTelefone_cliente($value) {
        $this->telefone_cliente = $value;
    }

    public function getTelefone_cliente() {
        return $this->telefone_cliente;
    }

    private $celular_cliente;

    public function setCelular_cliente($value) {
        $this->celular_cliente = $value;
    }

    public function getCelular_cliente() {
        return $this->celular_cliente;
    }

    private $nm_email;

    public function setEmail_cliente($value) {
        $this->email_cliente = $value;
    }

    public function getEmail_cliente() {
        return $this->email_cliente;
    }

    private $senha_cliente;

    public function setSenha_cliente($value) {
        $this->senha_cliente = $value;
    }

    public function getSenha_cliente() {
        return $this->senha_cliente;
    }

    private $dt_cadastro;

    public function setDt_cadastro($value) {
        $this->dt_cadastro = $value;
    }

    public function getDt_cadastro() {
        return $this->dt_cadastro;
    }

    private $dt_ultimo_acesso;

    public function setDt_ultimo_acesso($value) {
        $this->dt_ultimo_acesso = $value;
    }

    public function getDt_ultimo_acesso() {
        return $this->dt_ultimo_acesso;
    }

    private $ic_bloqueado;

    public function setIc_bloqueado($value) {
        $this->ic_bloqueado = $value;
    }

    public function getIc_bloqueado() {
        return $this->ic_bloqueado;
    }

    private $ic_sexo;

    public function setIc_sexo($value) {
        $this->ic_sexo = $value;
    }

    public function getIc_sexo() {
        return $this->ic_sexo;
    }

    private $qt_campeonatos;

    public function setQt_campeonatos($value) {
        $this->qt_campeonatos = $value;
    }

    public function getQt_campeonatos() {
        return $this->qt_campeonatos;
    }

// </editor-fold>
    var $mysqli = null;
    var $meuarray = array();
    var $erro = null;

    public function Inserir(Clientes $cliente) {
         $conexao= new Conexao();
         try {
            $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
            $query = "insert into clientes(nome_cliente,cpf_cliente,cep_cliente,rua_cliente,num_cliente,complemento_cliente," .
            "bairro_cliente,cidade_cliente,uf_cliente,ddd_cliente,telefone_cliente,celular_cliente,email_cliente," .
            "senha_cliente,dt_cadastro,dt_ultimo_acesso,ic_bloqueado) " .
            "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,current_date(),current_date(),false)";

            $stmt = $dbh->prepare($query);

            $nome_cliente = $cliente->getNome_cliente();
            $cpf_cliente = $cliente->getCpf_cliente();
            $cep_cliente = $cliente->getCep_cliente();
            $rua_cliente = $cliente->getRua_cliente();
            $num_cliente = $cliente->getNum_cliente();
            $complemento_cliente = $cliente->getComplemento_cliente();
            $bairro_cliente = $cliente->getBairro_cliente();
            $cidade_cliente = $cliente->getCidade_cliente();
            $uf_cliente = $cliente->getUf_cliente();
            $ddd_cliente = $cliente->getDdd_cliente();
            $telefone_cliente = $cliente->getTelefone_cliente();
            $celular_cliente = $cliente->getCelular_cliente();
            $email_cliente = $cliente->getEmail_cliente();
            $senha_cliente = $cliente->getSenha_cliente();
            $stmt->execute(array($nome_cliente, $cpf_cliente, $cep_cliente, $rua_cliente, $num_cliente, $complemento_cliente, $bairro_cliente, $cidade_cliente, $uf_cliente, $ddd_cliente, $telefone_cliente, $celular_cliente, $email_cliente, $senha_cliente));
            $this->erro = "";   

         } 
         catch (PDOException $exception) 
         {
          //   echo $exception->getMessage();
            if (strpos($exception->getMessage(), 'UQ_clientes'))
                $this->erro = "Este e-mail já está cadastrado.";
            else
                $this->erro = $exception->getMessage();
         }

    }

    public function Alterar(Clientes $cliente) {
        $conexao= new Conexao();
         try {
            $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
            $query = "update clientes set nome_cliente = ?,cpf_cliente = ?,cep_cliente=?,rua_cliente=?,num_cliente=?,nm_complemento=?," .
                "bairro_cliente=?,cidade_cliente=?,uf_cliente=?,ddd_cliente=?,telefone_cliente=?,celular_cliente=?,nm_email=? " .
                " where cd_cliente=?";

            $stmt = $dbh->prepare($query);

            $nome_cliente = $cliente->getNome_cliente();
            $cpf_cliente = $cliente->getCpf_cliente();
            $cep_cliente = $cliente->getCep_cliente();
            $rua_cliente = $cliente->getRua_cliente();
            $num_cliente = $cliente->getNum_cliente();
            $nm_complemento = $cliente->getComplemento_cliente();
            $bairro_cliente = $cliente->getBairro_cliente();
            $cidade_cliente = $cliente->getCidade_cliente();
            $uf_cliente = $cliente->getUf_cliente();
            $ddd_cliente = $cliente->getDdd_cliente();
            $telefone_cliente = $cliente->getTelefone_cliente();
            $celular_cliente = $cliente->getCelular_cliente();
            $nm_email = $cliente->getEmail_cliente();
            $cd_cliente = $cliente->getCd_cliente();
            
            $stmt->execute(array($nome_cliente, $cpf_cliente, $cep_cliente, $rua_cliente, $num_cliente, $nm_complemento, $bairro_cliente, $cidade_cliente, $uf_cliente, $ddd_cliente, $telefone_cliente, $celular_cliente, $nm_email, $cd_cliente));
            $this->erro = "";   

         } 
         catch (PDOException $exception) 
         {
            if (strpos($exception->getMessage(), 'uq_clientes_email'))
                $this->erro = "Este e-mail já está em uso.Clique em 'Esqueci minha senha' para gerar um novo";
            else
                $this->erro = $exception->getMessage();
         }

    }
     public function AlterarSenha($senha_cliente,$cd_cliente) {
          $conexao= new Conexao();
         try {
            $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
            $stmt = $dbh->prepare("update clientes set senha_cliente = ?  where cd_cliente=?");
            $stmt->execute(array($senha_cliente,$cd_cliente));
            $this->erro = "";   

         } 
         catch (PDOException $exception) 
         {
               $this->erro= $exception->getMessage();

         }

    }

    public function UltimoCliente() {
        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $mysqli->set_charset("utf8");
        $rs = $mysqli->query("select max(cd_cliente) as ultimo_id from clientes;");
        list($cd_cliente) = $rs->fetch_row();
        $rs->close();
        $mysqli->close();
        //echo "cdcliuu".$cd_cliente."<br/>";
        return $cd_cliente;
    }


    public function Login($nm_email, $senha_cliente)
    {
        $conexao = new Conexao();
         try {
            $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
            $stmt = $dbh->prepare("select cd_cliente,nome_cliente from clientes where email_cliente=? and senha_cliente=?");
            $stmt->execute(array($nm_email, $senha_cliente));

            while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                 $this->cd_cliente = $row["cd_cliente"];
                $this->nome_cliente = $row["nome_cliente"];
                echo "335 ".$row["nome_cliente"];
              }
             $this->erro = "";   
         } 
         catch (PDOException $exception) 
         {
             echo "erro ".$exception->getMessage();
            $this->erro= $exception->getMessage();
         }
         echo "342 ". $this->getNome_cliente();

    }
    public function temEmail($nm_email) {
         $conexao = new Conexao();
         try {
            $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
            $stmt = $dbh->prepare("select cd_cliente,senha_cliente from clientes where nm_email=?");
            $stmt->execute(array($nm_email));

            while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                 $this->cd_cliente = $row["cd_cliente"];
                $this->senha_cliente = $row["senha_cliente"];
              }
             $this->erro = "";   
         } 
         catch (PDOException $exception) 
         {
            $this->erro= $exception->getMessage();
         }

    }

    public function getUmItem($cd_cliente) {
        $conexao = new Conexao();
        try {
           $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
           $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
           $stmt = $dbh->prepare("select * from clientes where cd_cliente=?");
           $stmt->execute(array($cd_cliente));

           while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
             
                $this->cd_cliente= $row["cd_cliente"];
                $this->nome_cliente= $row["nome_cliente"];
                $this->cpf_cliente= $row["cpf_cliente"];
                $this->cep_cliente= $row["cep_cliente"];
                $this->rua_cliente= $row["rua_cliente"];
                $this->num_cliente= $row["num_cliente"];
                $this->nm_complemento= $row["complemento_cliente"];
                $this->bairro_cliente= $row["bairro_cliente"];
                $this->cidade_cliente= $row["cidade_cliente"];
                $this->cep_cliente= $row["cep_cliente"];
                $this->uf_cliente= $row["uf_cliente"];
                $this->ddd_cliente= $row["ddd_cliente"];
                $this->telefone_cliente= $row["telefone_cliente"];
                $this->celular_cliente= $row["celular_cliente"];
                $this->email_cliente= $row["email_cliente"];
                $this->senha_cliente= $row["senha_cliente"];
                $this->dt_cadastro= $row["dt_cadastro"];
                $this->dt_ultimo_acesso= $row["dt_ultimo_acesso"];
                $this->ic_bloqueado= $row["ic_bloqueado"];
               // $this->ic_sexo= $row["ic_sexo"];

             }
            $this->erro = "";   
        } 
        catch (PDOException $exception)
        {
           $this->erro= $exception->getMessage();
        }

    }

    public function ListaTodosGridAdmin($limit, $subquery) {

        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
       // $mysqli->set_charset("utf8");
        $sql = "select cli.cd_cliente,cli.nome_cliente,cli.ddd_cliente,cli.telefone_cliente,".
               "(select count(clica.cd_cliente_campeonato) from clientes_campeonatos clica where clica.cd_cliente = cli.cd_cliente) as qt_campeonatos ".
               "from clientes cli ".
                $subquery . " order by cli.cd_cliente " . $limit;
              
        $rs = $mysqli->query($sql);

        $aclientes = array();

        while ($row = $rs->fetch_assoc()) {
            $tempclientes = new clientes();
            $tempclientes->setCd_cliente($row ['cd_cliente']);
            $tempclientes->setNome_cliente($row ['nome_cliente']);
            $tempclientes->setDdd_cliente($row ['ddd_cliente']);
            $tempclientes->setTelefone_cliente($row ['telefone_cliente']);
            $tempclientes->setQt_campeonatos($row ['qt_campeonatos']);
            $aclientes [] = $tempclientes;
        }
        $rs->close();
        $mysqli->close();

        $this->meuarray = $aclientes;
    }

    public function Excluir($cliente) {
          $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from clientes where cd_cliente=?");
                $stmt->execute(array($cliente));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                  $this->erro= $exception->getMessage();
             }
 
    }

}
