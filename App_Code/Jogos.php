<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Jogos
 *
 * @author Adilson
 */
require_once 'Conexao.php';
class Jogos {
    //put your code here
    // <editor-fold desc="Atributos">

        private $cd_jogo;
	public function setCd_jogo($value) {
		$this->cd_jogo = $value;
	}
	public function getCd_jogo() {
		return $this->cd_jogo;
	}
        private $nm_jogo;
	public function setNm_jogo($value) {
		$this->nm_jogo = $value;
	}
	public function getNm_jogo() {
		return $this->nm_jogo;
	}
         private $cd_plataforma;
	public function setCd_plataforma($value) {
		$this->cd_plataforma = $value;
	}
	public function getCd_plataforma() {
		return $this->cd_plataforma;
	}
        private $nm_plataforma;
	public function setNm_plataforma($value) {
		$this->nm_plataforma = $value;
	}
	public function getNm_plataforma() {
		return $this->nm_plataforma;
	}        
         private $cd_subcategoria;
	public function setCd_subcategoria($value) {
		$this->cd_subcategoria = $value;
	}
	public function getCd_subcategoria() {
		return $this->cd_subcategoria;
	}
        private $cd_categoria;
	public function setCd_categoria($value) {
		$this->cd_categoria = $value;
	}
	public function getCd_categoria() {
		return $this->cd_categoria;
	}
        private $nm_categoria;
	public function setNm_categoria($value) {
		$this->nm_categoria = $value;
	}
	public function getNm_categoria() {
		return $this->nm_categoria;
	}        
        private $ds_jogo;
	public function setDs_jogo($value) {
		$this->ds_jogo = $value;
	}
	public function getDs_jogo() {
		return $this->ds_jogo;
	}
         private $nm_url_1;
	public function setNm_url_1($value) {
		$this->nm_url_1 = $value;
	}
	public function getNm_url_1() {
		return $this->nm_url_1;
	}
        private $nm_url_2;
	public function setNm_url_2($value) {
		$this->nm_url_2 = $value;
	}
	public function getNm_url_2() {
		return $this->nm_url_2;
	}

         private $ic_ativo;
	public function setIc_ativo($value) {
		$this->ic_ativo = $value;
	}
	public function getIc_ativo() {
		return $this->ic_ativo;
	}
        
         private $nm_url_primeira_foto;
	public function setNm_url_primeira_foto($value) {
		$this->nm_url_primeira_foto = $value;
	}
	public function getNm_url_primeira_foto() {
		return $this->nm_url_primeira_foto;
	}
 
         // </editor-fold>
       #### Fim  Atributos  ##########
       ####   Métodos   ########
	var $mysqli = null;
	var $meuarray = array ();
	var $erro = null;
         public function Inserir(Jogos $jogo) {
             
            $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $query="insert into jogos (nm_jogo,cd_categoria,ds_jogo,".
                      "nm_url_1,nm_url_2,ic_ativo)".
                      "values(?,?,?,?,?,?)";
                
                $stmt = $dbh->prepare($query);
                  $nm_jogo=$jogo->getNm_jogo();
                  $cd_categoria = $jogo->getCd_categoria();
                 $ds_jogo=$jogo->getDs_jogo();
                 $nm_url_1 = $jogo->getNm_url_1();
                 $nm_url_2=$jogo->getNm_url_2();
                 $ic_ativo = $jogo->getIc_ativo();

                 
                $stmt->execute(array($nm_jogo,$cd_categoria,$ds_jogo,$nm_url_1,$nm_url_2,$ic_ativo));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                 if(strpos($exception->getMessage(), 'UQ_jogos'))
                     $this->erro = "Ítem já está cadastrado."  ;  
                else
                    $this->erro= $exception->getMessage();
             }

        
	}
          public function Atualizar(Jogos $jogo) {
		$conexao= new Conexao();
                try {
                   $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                   $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                   $query="update jogos set nm_jogo=?,cd_categoria=?,".
                       "ds_jogo=?,ic_ativo=? where cd_jogo=?";

                   $stmt = $dbh->prepare($query);
                   
                     $nm_jogo=$jogo->getNm_jogo();
                     $cd_categoria=$jogo->getCd_categoria();
                     $ds_jogo=$jogo->getDs_jogo();
                     $ic_ativo = $jogo->getIc_ativo();
                     $cd_jogo= $jogo->getCd_jogo();
                   
                   $stmt->execute(array($nm_jogo,$cd_categoria,$ds_jogo,
                         $ic_ativo,$cd_jogo));
                   $this->erro = "";   
                } 
                catch (PDOException $exception) 
                {
//                    if(strpos($exception->getMessage(), 'UQ_jogos'))
//                     $this->erro = "Ítem já está cadastrado."  ;  
//                    else
                        $this->erro= $exception->getMessage();
                }
        
	}


        public function UltimoJogo()
        {
                 $conexao= new Conexao();
		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
                  $mysqli->set_charset("utf8");
		$rs = $mysqli->query ( "select max(cd_jogo) as cd_jogo from jogos" );
		list($cd_jogo)= $rs->fetch_row();
		$rs->close ();
               	$mysqli->close ();
                return $cd_jogo;
	 }
         public function ListaTodosGridAdmin($limit,$subquery)
         {
             $conexao= new Conexao();
		$mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
                // $mysqli->set_charset("utf8");
                
               	//$rs = $mysqli->query ( "select p.*,(select sum(qt_estoque) from tamanhos_jogo where cd_jogo = p.cd_jogo) as qt_estoque2 from jogos p $subquery order by cd_jogo $limit" );
//                 $rs = $mysqli->query ( "select p.*,".
//                                        "(select sum(mlb_available_quantity) from mlbdados_jogo where cd_jogo=p.cd_jogo) as mlb_qt_estoque " .
//                                      "from jogos p ".$subquery. " order by mlb_qt_estoque ". $limit) ;
                 $rs = $mysqli->query ("select j.*,c.nm_categoria from jogos j ".
                                      "join categorias c on j.cd_categoria = c.cd_categoria ".$subquery. " order by j.nm_jogo ". $limit);
                 
		$ajogos = array ();
		
		while ( $row = $rs->fetch_assoc () ) {
			$tempjogos = new Jogos();
			$tempjogos->setCd_jogo ( $row ['cd_jogo'] );
			$tempjogos->setNm_jogo ( $row ['nm_jogo'] );
                        $tempjogos->setIc_ativo($row ['ic_ativo']);
                         $tempjogos->setNm_categoria($row ['nm_categoria']);                        
                       
			$ajogos [] = $tempjogos;
		}
		$rs->close ();
		$mysqli->close ();
		
		$this->meuarray = $ajogos;
         }
         
         public function Excluir($cd_jogo) {
                 $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from jogos where cd_jogo=?");
                $stmt->execute(array($cd_jogo));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                 if(strpos($exception->getMessage(), 'FK_campeonatos1'))
                     $this->erro = "Jogo associado a campeonatos(s).Exclusão não permitida."  ;
                else
                    $this->erro= $exception->getMessage();
             }

          }
           public function getUmItem($cd_jogo) {
		 $conexao = new Conexao();
                try {
                   $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                   $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                   $query = "select p.*,".
                                        "(select nm_url_foto from jogos_fotos where cd_jogo=p.cd_jogo ".
                                        "order by vl_posicao limit 1) as nm_url_foto, ".
                                        "c.nm_categoria ".
                                        "from jogos p ".
                                        "join categorias c on p.cd_categoria = c.cd_categoria ".
                                         "where cd_jogo=?";
                  
                   $stmt = $dbh->prepare($query);
                   $stmt->execute(array($cd_jogo));

                   while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                        $this->cd_jogo = $row["cd_jogo"];
                        $this->nm_jogo = $row["nm_jogo"];
                        $this->ds_jogo = $row["ds_jogo"];
                        $this->nm_url_1 = $row["nm_url_1"];
                        $this->nm_url_2 = $row["nm_url_2"];
                        $this->ic_ativo = $row["ic_ativo"];
                        $this->cd_categoria = $row["cd_categoria"];
                        $this->nm_url_primeira_foto = $row["nm_url_foto"];
                        $this->nm_categoria = $row["nm_categoria"];

                     }
                    $this->erro = "";   
                } 
                catch (PDOException $exception)
                {
                   $this->erro= $exception->getMessage();
                }
 	}
    
   
}
