<?php

require_once 'Conexao.php';
class Configuracoes {
    //put your code here
    // <editor-fold desc="Atributos">
    
   
        private $nm_telefones_linha1;
	public function setNm_telefones_linha1($value) {
		$this->nm_telefones_linha1 = $value;
	}
	public function getNm_telefones_linha1() {
		return $this->nm_telefones_linha1;
	}
        
        private $nm_telefones_linha2;
	public function setNm_telefones_linha2($value) {
		$this->nm_telefones_linha2 = $value;
	}
	public function getNm_telefones_linha2() {
		return $this->nm_telefones_linha2;
	}
        
        private $nm_endereco_linha1;
	public function setNm_endereco_linha1($value) {
		$this->nm_endereco_linha1 = $value;
	}
	public function getNm_endereco_linha1() {
		return $this->nm_endereco_linha1;
	}
        
        private $nm_endereco_linha2;
	public function setNm_endereco_linha2($value) {
		$this->nm_endereco_linha2 = $value;
	}
	public function getNm_endereco_linha2() {
		return $this->nm_endereco_linha2;
	}
        
        private $nm_email;
	public function setNm_email($value) {
		$this->nm_email = $value;
	}
	public function getNm_email() {
		return $this->nm_email;
	}
        
        private $nm_url_facebook;
	public function setNm_url_facebook($value) {
		$this->nm_url_facebook = $value;
	}
	public function getNm_url_facebook() {
		return $this->nm_url_facebook;
	}
        
        private $nm_url_twitter;
	public function setNm_url_twitter($value) {
		$this->nm_url_twitter = $value;
	}
	public function getNm_url_twitter() {
		return $this->nm_url_twitter;
	}
      
         // </editor-fold>
       #### Fim  Atributos  ##########
       ####   Métodos   ########
	var $mysqli = null;
	var $meuarray = array ();
	var $erro = null;
      /*
create table confirugacoes
(nm_telefones_linha1 varchar(255),
nm_telefones_linh2 varchar(255),
nm_endereco_linha1 varchar(255),
nm_endereco_linha2 varchar(255),
nm_email varchar(255),
nm_url_facebook varchar(255),
nm_url_twiter varchar(255)
);
       */
         public function getUmItem() {
            $conexao= new Conexao();
             echo ("dados ".$conexao->getNm_servidor()."-". $conexao->getNm_usuario()."-". $conexao->getNm_senha()."-".$conexao->getNm_bd());
             
            // echo $conexao->getNm_servidor(). $conexao->getNm_usuario(). $conexao->getNm_senha().$conexao->getNm_bd();
             $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
            
             if ($mysqli->errno) {
                printf("Unable to connect to the database:<br /> %s",
                $mysqli->error);
                exit();
                }
             $mysqli->set_charset("utf8");
                 
           
            $query = "select * from configuracoes";
            $rs = $mysqli->query($query);
             
            //$row ["cd_pedido_status"
            while ($row = $rs->fetch_assoc()) {
             $this->nm_telefones_linha1 = $row["nm_telefones_linha1"];
             $this->nm_telefones_linha2 = $row["nm_telefones_linha2"];
             $this->nm_endereco_linha1 = $row["nm_endereco_linha1"];
             $this->nm_endereco_linha2 = $row["nm_endereco_linha2"];
             $this->nm_email = $row["nm_email"];
             $this->nm_url_facebook = $row["nm_url_facebook"];
             $this->nm_url_twitter = $row["nm_url_twitter"];
            }
            $rs->free();
                                    
              
                 echo $this->erro;
               
                $mysqli->close();
 	}
       public function Atualizar(Configuracoes $configuracoes) {
		 $conexao= new Conexao();
                try {
                   $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                   $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                   $stmt = $dbh->prepare("update configuracoes set nm_telefones_linha1=?,nm_telefones_linha2= ?,nm_endereco_linha1=?,".
                     "nm_endereco_linha2=?,nm_email=?,nm_url_facebook=?,nm_url_twitter=?");
                   
                   $nm_telefones_linha1 = $configuracoes->getNm_telefones_linha1();
                    $nm_telefones_linha2 = $configuracoes->getNm_telefones_linha2();
                    $nm_endereco_linha1 = $configuracoes->getNm_endereco_linha1();
                    $nm_endereco_linha2 = $configuracoes->getNm_endereco_linha2();
                    $nm_email = $configuracoes->getNm_email();
                    $nm_url_facebook = $configuracoes->getNm_url_facebook();
                    $nm_url_twitter = $configuracoes->getNm_url_twitter();
                   
                   $stmt->execute(array($nm_telefones_linha1,$nm_telefones_linha2,$nm_endereco_linha1,$nm_endereco_linha2,$nm_email,$nm_url_facebook,$nm_url_twitter));
                   $this->erro = "";   

                } 
                catch (PDOException $exception) 
                {
                     $this->erro= $exception->getMessage();

                }
//                 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                 $mysqli->set_charset("utf8");
//                 $sql="update configuracoes set nm_telefones_linha1=?,nm_telefones_linha2= ?,nm_endereco_linha1=?,".
//                      "nm_endereco_linha2=?,nm_email=?,nm_url_facebook=?,nm_url_twitter=?";
//                      
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare($sql);
//                 
//                 $nm_telefones_linha1 = $configuracoes->getNm_telefones_linha1();
//                 $nm_telefones_linha2 = $configuracoes->getNm_telefones_linha2();
//                 $nm_endereco_linha1 = $configuracoes->getNm_endereco_linha1();
//                 $nm_endereco_linha2 = $configuracoes->getNm_endereco_linha2();
//                 $nm_email = $configuracoes->getNm_email();
//                 $nm_url_facebook = $configuracoes->getNm_url_facebook();
//                 $nm_url_twitter = $configuracoes->getNm_url_twitter();
//                 $stmt->bind_param('sssssss',$nm_telefones_linha1,$nm_telefones_linha2,$nm_endereco_linha1,$nm_endereco_linha2,$nm_email,$nm_url_facebook,$nm_url_twitter);
//
//                if(!$stmt->execute())
//		{
//                     $this->erro =$mysqli->error;
//    		}
//		
//                $stmt->close();
//                $mysqli->close();
        
	}
   
}

