<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Subcategorias
 *
 * @author Adilson
 */
require_once 'Conexao.php';
require_once 'Categorias.php';
class Subcategorias {
    //put your code here
      ####   Atributos  ##########
        private $cd_subcategoria;
	public function setCd_subcategoria($value) {
		$this->cd_subcategoria = $value;
	}
	public function getCd_subcategoria() {
		return $this->cd_subcategoria;
	}
        
        private $cd_categoria;
	public function setCd_categoria($value) {
		$this->cd_categoria = $value;
	}
	public function getCd_categoria() {
		return $this->cd_categoria;
	}
	
        private $nm_subcategoria;
	public function setNm_subcategoria($value) {
		$this->nm_subcategoria = $value;
	}
	public function getNm_subcategoria() {
		return $this->nm_subcategoria;
	}
 
       private $prox_cd_subcategoria;
	public function setProx_cd_subcategoria($value) {
		$this->prox_cd_subcategoria = $value;
	}
	public function getProx_cd_subcategoria() {
		return $this->prox_cd_subcategoria;
	}
        ####  Fim Atributos  ##########
        var $mysqli = null;
	var $meuarray = array ();
	var $erro = null;
        
        public function Excluir($cd_subcategoria) {
              $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from subcategorias where cd_subcategoria=?");
                $stmt->execute(array($cd_subcategoria));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                 if(strpos($exception->getMessage(), 'FK_categoria'))
                     $this->erro = "Subcategoria está associada a produto(s).Exclusão não permitida."  ;
                else
                    $this->erro= $exception->getMessage();
             }
            Categorias::geraMenuXml2();

        
	}
      
        public function Inserir(Subcategorias $subcategoria) {
             $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("insert into subcategorias (nm_subcategoria,cd_categoria) values (?,?)");
                $stmt->execute(array($subcategoria->getNm_subcategoria(),$subcategoria->getCd_categoria()));
                $this->erro = "";   

             } 
             catch (PDOException $exception) 
             {
                   $this->erro= $exception->getMessage();
             }
           Categorias::geraMenuXml();
        
	}
     public function proxCdSubcategoria() {

        $conexao = new Conexao();
        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
        $mysqli->set_charset("utf8");
        $query = "select max(cd_subcategoria) as ultimo_cd_subcategoria from subcategorias";
        $rs = $mysqli->query($query);

        while ($row = $rs->fetch_assoc()) {
            $this->prox_cd_subcategoria = $row ["ultimo_cd_subcategoria"];
        }
        if ($this->prox_cd_subcategoria == "")
            $this->prox_cd_subcategoria = 1;
        else
            $this->prox_cd_subcategoria++;
        $mysqli->close();
    }
    
       public function Atualizar(Subcategorias $subcategoria) {

            $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("update subcategorias set nm_subcategoria = ? where cd_subcategoria=?");
                $stmt->execute(array($subcategoria->getNm_subcategoria(),$subcategoria->getCd_subcategoria()));
                $this->erro = "";   

             } 
             catch (PDOException $exception) 
             {
                   $this->erro= $exception->getMessage();

             }
             Categorias::geraMenuXml();
               
        
	}
       public  function getUmItem($cd_subcategoria) {
            $conexao = new Conexao();
            try {
               $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
               $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
               $stmt = $dbh->prepare("select * from subcategorias where cd_subcategoria=? ");
               $stmt->execute(array($cd_subcategoria));

               while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                   $this->cd_subcategoria = $row["cd_subcategoria"];
                   $this->nm_subcategoria = $row["nm_subcategoria"];
                   $this->cd_categoria = $row["cd_categoria"];
                 }
                $this->erro = "";   
            } 
            catch (PDOException $exception) 
            {
               $this->erro= $exception->getMessage();
            }

    }
}
