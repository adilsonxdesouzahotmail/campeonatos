<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Caracteristicas
 *
 * @author Adilson
 */
require_once 'Conexao.php';
require_once 'Caracteristicas_produto.php';
class Caracteristicas {
    //put your code here
    
    
    
      ####   Atributos  ##########

        private $cd_caracteristica;
	public function setCd_caracteristica($value) {
		$this->cd_caracteristica = $value;
	}
	public function getCd_caracteristica() {
		return $this->cd_caracteristica;
	}
	
        private $nm_caracteristica;
	public function setNm_caracteristica($value) {
		$this->nm_caracteristica = $value;
	}
	public function getNm_caracteristica() {
		return $this->nm_caracteristica;
	}
        ####  Fim Atributos  ##########
        ####   M�todos   ########
	var $mysqli = null;
	var $meuarray = array ();
	var $erro = null;
	public function getTodosItens() {
		$conexao= new Conexao();
		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
                  $mysqli->set_charset("utf8");
		$rs = $mysqli->query ( "select * from caracteristicas" );
		
		$acaracteristicas = array ();
		
		while ( $row = $rs->fetch_assoc () ) {
			$tempcaracteristicas = new Caracteristicas ();
			$tempcaracteristicas->setCd_caracteristica ( $row ['cd_caracteristica'] );
			$tempcaracteristicas->setNm_caracteristica ( $row ['nm_caracteristica'] );
			
			$acaracteristicas [] = $tempcaracteristicas;
		}
                
		$rs->close ();
		$mysqli->close ();
		
		$this->meuarray = $acaracteristicas;
	
	}
         public function Inserir($nm_caracteristica) {
            $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("insert into caracteristicas (nm_caracteristica) values (?)");
                $stmt->execute(array($nm_caracteristica));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                 if(strpos($exception->getMessage(), 'UQ_caracteristicas'))
                     $this->erro = "Característica já está cadastrada."  ;  
                else
                    $this->erro= $exception->getMessage();
             }
//                 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                  $mysqli->set_charset("utf8");
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("insert into caracteristicas (nm_caracteristica) values (?)");
//		 $stmt->bind_param('s',$nm_caracteristica);
//                if(!$stmt->execute())
//		{
//                    if(strpos($mysqli->error, 'UQ_caracteristicas'))
//                        $this->erro = "Característica já está cadastrada."  ;     
//                    else
//                        $this->erro =$mysqli->error;
//    		}
//		
//                $stmt->close();
//                $mysqli->close();
        
	}
         public function Alterar(Caracteristicas $caracteristica) {
             $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("update caracteristicas set nm_caracteristica = ? where cd_caracteristica=?");
                $stmt->execute(array($caracteristica->getNm_caracteristica(),$caracteristica->getCd_caracteristica()));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                 if(strpos($exception->getMessage(), 'UQ_caracteristicas'))
                     $this->erro = "Característica já está cadastrada."  ;  
                else
                    $this->erro= $exception->getMessage();
             }
//		 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                  $mysqli->set_charset("utf8");
//                 $stmt = $mysqli->stmt_init();
//                 $sql= "update caracteristicas set nm_caracteristica = ? where cd_caracteristica=?";
//                  $stmt->prepare($sql);
//                $cd_caracteristica =  $caracteristica->getCd_caracteristica();
//                $nm_caracteristica = $caracteristica->getNm_caracteristica();
//               
//                 $stmt->bind_param('si',$nm_caracteristica,$cd_caracteristica);
//                if(!$stmt->execute())
//		{
//                    if(strpos($mysqli->error, 'UQ_caracteristicas'))
//                        $this->erro = "Característica já está cadastrada."  ;     
//                    else
//                        $this->erro =$mysqli->error;
//                    
//    		}
//                $linhas = $mysqli->affected_rows;
//                $stmt->close();
//                $mysqli->close();
//                return $linhas;
        
	}
       public function Excluir($cd_caracteristica) {
                $caracteristica_produto = new Caracteristicas_produto();
                $caracteristica_produto->ExcluirCaracteristica($cd_caracteristica);
                
             $conexao= new Conexao();
             try {
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("delete from caracteristicas where cd_caracteristica=?");
                $stmt->execute(array($cd_caracteristica));
                $this->erro = "";   
             } 
             catch (PDOException $exception) 
             {
                 if(strpos($exception->getMessage(), 'UQ_caracteristicas'))
                     $this->erro = "Característica já está cadastrada."  ;  
                else
                    $this->erro= $exception->getMessage();
             }
//		 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("delete from caracteristicas where cd_caracteristica=?");
//		 $stmt->bind_param('i',$cd_caracteristica);
//                if(!$stmt->execute())
//		{
//                    if(strpos($mysqli->error, 'FK_caracteristicas_produto2'))
//                        $this->erro = "Característica está associada a produto(s).Exclusão não permitida."  ;     
//                    else
//                        $this->erro =$mysqli->error;
//                    
//    		}
//                $linhas = $mysqli->affected_rows;
//                $stmt->close();
//                $mysqli->close();
//                return $linhas;
        }
         public function getUmItem($cd_caracteristica) {
            $conexao = new Conexao();
            try {
               $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
               $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
               $stmt = $dbh->prepare("select * from caracteristicas where cd_caracteristica=? ");
               $stmt->execute(array($cd_caracteristica));

               while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                   $this->cd_caracteristica = $row["cd_caracteristica"];
                   $this->nm_caracteristica = $row["nm_caracteristica"];
                 
                 }
                $this->erro = "";   
            } 
            catch (PDOException $exception)
            {
               $this->erro= $exception->getMessage();
            }
            
//                 $conexao= new Conexao();
//		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
//                  $mysqli->set_charset("utf8");
//                 $stmt = $mysqli->stmt_init();
//                 $stmt->prepare("select * from caracteristicas where cd_caracteristica=? ");
//
//                 $stmt->bind_param('i',$cd_caracteristica);
//                 $stmt->execute();
//                 $stmt->bind_result($cd_caracteristica,$nm_caracteristica);
//                 while($stmt->fetch())
//                 {
//                     $this->cd_caracteristica = $cd_caracteristica;
//                     $this->nm_caracteristica = $nm_caracteristica;
//                 }
//                 $this->erro=$mysqli->error;
//                $stmt->close();
//                $mysqli->close();
 	}
}
