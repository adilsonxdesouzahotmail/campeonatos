<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Paginas
 *
 * @author Adilson
 */
error_reporting(E_ALL);
require_once 'Conexao.php';
class MercadoLivre {
    //put your code here
    // <editor-fold desc="Atributos">
     var $erro = null;
      public function Inserir($mlb_id,$cd_produto) {
		
             $conexao= new Conexao();
             
             try {
                $mlb= self::getProdutoMercardoLivre($mlb_id);
                $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
                $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
                $stmt = $dbh->prepare("insert into mlbdados_produto (mlb_id,mlb_title,mlb_price,mlb_available_quantity"
                        . ",mlb_thumbnail,mlb_seller_id,mlb_status,cd_produto) values (?,?,?,?,?,?,?,?)");
                echo "mlbtit:".$mlb["available_quantity"];
                $stmt->execute(array($mlb["id"],$mlb["title"],$mlb["price"],$mlb["available_quantity"],
                               $mlb["thumbnail"],$mlb["seller_id"],$mlb["status"],$cd_produto));
                $this->erro = "";   

             } 
             catch (PDOException $exception) 
             {
                 if(strpos($exception->getMessage(), 'UQ_mlbdados_produto'))
                    $this->erro = "Produto do Mercado Livre já cadastrado para este produto."  ;     
                else
                    $this->erro= $exception->getMessage();

             }

           
    }

     public function getProdutoMercardoLivre($mlb_id) {
        $url = "https://api.mercadolibre.com/items/MLB" . $mlb_id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($result, true);
        $mlb = array("id"=>$data["id"],"title" => $data["title"], "price" => $data["price"], 
                     "available_quantity"=>$data["available_quantity"],"thumbnail" => $data["thumbnail"],
                     "seller_id" => $data["seller_id"],"status" => $data["status"]);
        return $mlb;
    }

}
