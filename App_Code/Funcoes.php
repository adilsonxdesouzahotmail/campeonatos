<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Funcoes
 *
 * @author Windows 10
 */
class Funcoes {

    public static function trocaTexto($texto, $procura, $trocapor) {
        $count = 0;

        for ($i = 0; $i < strlen($texto); $i++) {
            $pos = strpos($texto, $procura, $count);
            if ($pos == $count && $pos > -1) {
                $texto = substr_replace($texto, $trocapor, $pos, strlen($procura));
            }
            $count++;
            if ($i > strlen($texto))
                return;
        }

        return $texto;
    }

}
