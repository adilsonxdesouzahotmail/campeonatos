 <?php
   require_once "App_Code/Noticias.php";
 ?>

<div class="col-sm-8" style="margin-top:20px;margin-bottom:20px;font-size:0.9em">
            <h2><strong>Noticias</strong></h2>
            <?php
               $noticia = new Noticias();
               $noticia->getTodosItens(1);
               for ($i = 0; $i < count($noticia->meuarray); $i++) 
              {
                 ?>
            
            <a href="noticia/<?php echo str_replace(" ","-",$noticia->meuarray[$i]->getNm_titulo()); ?>/<?php echo $noticia->meuarray[$i]->getCd_noticia(); ?>" >
                  <div class="row is-table-row" >
                    <div class="col-sm-4">
                        <?php
                           if($noticia->meuarray[$i]->getNm_url_foto()=="" || $noticia->meuarray[$i]->getNm_url_foto()== null)
                           {
                               ?>
                                 <img class="img-responsive img-rounded" src="fotosnoticias/foto1.jpg" />
                               <?php
                           }
                           else 
                           {
                               //echo "foto ".$noticia->meuarray[$i]->getNm_url_foto()."<br/>";
                               ?>
                                   <img class="img-responsive img-rounded" src="fotosnoticias/<?php echo $noticia->meuarray[$i]->getCd_noticia(); ?>/foto1.jpg" />
                               <?php
                           }
                        ?>
                       
                    </div>
                    <div class="col-sm-8" style="vertical-align: middle;">
                        <div class="linha1">
                            <?php 
                              $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $noticia->meuarray[$i]->getDt_noticia());
                              $dt_noticia= $myDateTime->format('d/m/Y H:i:s') ;      
                              echo "Postado por ".$noticia->meuarray[$i]->getNm_usuario()." em ".$dt_noticia;
                            ?>
                        </div>
                        <div class="linha2"> <?php echo $noticia->meuarray[$i]->getNm_titulo(); ?></div>
                        <div class="linha3"><?php echo substr($noticia->meuarray[$i]->getDs_noticia(),0,100)."... <span style='color:red;text-decoration:underline;'>Leia mais</span>"; ?> </div>
                    </div>
                 </div>
                </a>
                 <?php
              }
            ?>

           </div>