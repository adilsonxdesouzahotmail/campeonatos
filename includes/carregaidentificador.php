<?php

     require_once "../App_Code/Conexao.php";
     $cd_plataforma = $_GET["cd_plataforma"];
    $conexao = new Conexao();
         try {
            $dbh = new PDO("mysql:host=".$conexao->getNm_servidor().";dbname=".$conexao->getNm_bd(), $conexao->getNm_usuario(), $conexao->getNm_senha());
            $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
            $stmt = $dbh->prepare("select * from plataformas where cd_plataforma =?");
            $stmt->execute(array($cd_plataforma));

            while ($row = $stmt->fetch(PDO::FETCH_NAMED, PDO::FETCH_ORI_NEXT)) {
                ?>
                        <div class="col-sm-12">
                            <label class="col-sm-4" ><?php echo $row["identificador"]?>: </label>
                        <div class="form-group col-sm-8">
                            <input type="text" class="form-control" id="<?php echo $row["identificador"]?>" placeholder="<?php echo $row["identificador"]?>" name="<?php echo $row["identificador_campo"]?>">
                        </div>
                        </div>
                <?php
              }
            
         } 
         catch (PDOException $exception) 
         {
            echo $exception->getMessage();
         }
