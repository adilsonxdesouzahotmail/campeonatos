  <div class="container-fluid" id="carrosselcontainer" style="padding-left:0px;padding-right:0px;margin-top:15px">
    <div id="bootstrap-touch-slider" class="carousel bs-slider fade  control-round indicators-line" data-ride="carousel" data-pause="hover" data-interval="7000">

      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#bootstrap-touch-slider" data-slide-to="0" class="active"></li>
        <li data-target="#bootstrap-touch-slider" data-slide-to="1"></li>
        <li data-target="#bootstrap-touch-slider" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper For Slides -->
      <div class="carousel-inner" role="listbox">

        <!-- Third Slide -->
        <div class="item active">

          <!-- Slide Background -->
          <img src="imagens/streetfighterhorizontal.jpg" alt="Bootstrap Touch Slider" class="slide-image" />
          <div class="bs-slider-overlay"></div>

          <div class="container">
            <div class="row">
              <!-- Slide Text Layer -->
              <div class="slide-text slide_style_left">
                <h1 data-animation="animated zoomInRight">Campeonato Street Fighet 2017 </h1>
                <p data-animation="animated fadeInLeft">Inscri&ccedil;&otilde;es Abertas</p>
                <a href="campeonato.php?cd_campeonato=12" target="_blank" class="btn btn-default" data-animation="animated fadeInLeft">Inscreva-se</a>

              </div>
            </div>
          </div>
        </div>
        <!-- End of Slide -->
        <!-- Second Slide -->
        <div class="item">

          <!-- Slide Background -->
          <img src="imagens/streetfighterhorizontal.jpg" alt="Bootstrap Touch Slider" class="slide-image" />
          <div class="bs-slider-overlay"></div>
          <!-- Slide Text Layer -->
          <div class="slide-text slide_style_center">
            <h1 data-animation="animated flipInX">Campeonato Street Fighet 2017</h1>
            <p data-animation="animated lightSpeedIn">Inscri&ccedil;&otilde;es Abertas.</p>
            <a href="campeonato.php?cd_campeonato=12" target="_blank" class="btn btn-default" data-animation="animated fadeInUp">Inscreva-se</a>
            <a href="faleconosco.html" target="_blank" class="btn btn-primary" data-animation="animated fadeInDown">Regulamento</a>
          </div>
        </div>
        <!-- End of Slide -->
        <!-- Third Slide -->
        <div class="item">

          <!-- Slide Background -->
          <img src="imagens/banner3.jpg" alt="Bootstrap Touch Slider" class="slide-image" />
          <div class="bs-slider-overlay"></div>
          <!-- Slide Text Layer -->
          <div class="slide-text slide_style_right">
            <h1 data-animation="animated zoomInLeft">Beautiful Animations</h1>
            <p data-animation="animated fadeInRight">Lots of css3 Animations to make slide beautiful .</p>
            <a href="faleconosco.html" target="_blank" class="btn btn-default" data-animation="animated fadeInLeft">select one</a>
            <a href="faleconosco.html" target="_blank" class="btn btn-primary" data-animation="animated fadeInRight">select two</a>
          </div>
        </div>
        <!-- End of Slide -->


      </div><!-- End of Wrapper For Slides -->
      <!-- Left Control -->
      <a class="left carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="prev">
        <span class="fa fa-angle-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <!-- Right Control -->
      <a class="right carousel-control" href="#bootstrap-touch-slider" role="button" data-slide="next">
        <span class="fa fa-angle-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div> <!-- End  bootstrap-touch-slider Slider -->
  </div>