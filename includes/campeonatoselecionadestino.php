<?php

session_start();
$origem = "";
if (isset($_POST["origem"]))
    $origem = $_POST["origem"];

if (isset($_GET["origem"]))
    $origem = $_GET["origem"];

 $cd_campeonato = "";
 if (isset($_POST["cd_campeonato"]))
     $cd_campeonato = $_POST["cd_campeonato"];
 
 if(isset($_GET["cd_campeonato"]))
 {
     $cd_campeonato = $_GET["cd_campeonato"] ;
 }

$vl_inscricao = "";
if (isset($_POST["vl_inscricao"]))
    $vl_inscricao = $_POST["vl_inscricao"];

if (isset($_GET["vl_inscricao"]))
    $vl_inscricao = $_GET["vl_inscricao"];


// <editor-fold defaultstate="collapsed" desc="Origem: campeonato.php">
// Client sem login ainda cadastando inscrição em um campeonato
if ($origem == "campeonato.php") 
{

    
  //  echo "16 ".$_GET["cd_campeonato"];

    $qt_jogadores_time = "";
    if (isset($_POST["qt_jogadores_time"]))
        $qt_jogadores_time = $_POST["qt_jogadores_time"];

    $cd_plataforma = "";
    if (isset($_POST["cd_plataforma"]))
        $cd_plataforma = $_POST["cd_plataforma"];

    $id_psn = "";
    if (isset($_POST["id_psn"]))
        $id_psn = $_POST["id_psn"];

    $id_xboxlive = "";
    if (isset($_POST["id_xboxlive"]))
        $id_xboxlive = $_POST["id_xboxlive"];

    $id_pc = "";
    if (isset($_POST["id_pc"]))
        $id_pc = $_POST["id_pc"];

    $fighter_id = "";
    if (isset($_POST["fighter_id"]))
        $fighter_id = $_POST["fighter_id"];

    $nm_time = "";
    if (isset($_POST["nm_time"]))
        $nm_time = $_POST["nm_time"];
    
//    $nm_login = "";
//    if (isset($_POST["nm_login"]))
//        $arraynm_login = $_POST["nm_login"];    

    $cd_cliente = "";
    if (isset($_SESSION["cd_cliente"]))
        $cd_cliente = $_SESSION["cd_cliente"];
    
//    $cd_plataforma_login = "";
//    if (isset($_POST["cd_plataforma_login"]))
//        $arraycd_plataforma_login = $_POST["cd_plataforma_login"];    
//echo " 75 cd_camp".$cd_campeonato; 
        $_SESSION["cd_campeonato"] = $cd_campeonato;        
        $_SESSION["cd_plataforma"] = $cd_plataforma;
        $_SESSION["id_psn"] = $id_psn;
        $_SESSION["id_xboxlive"] = $id_xboxlive;
        $_SESSION["id_pc"] = $id_pc;
        $_SESSION["fighter_id"] = $fighter_id;
        $_SESSION["nm_time"] = $nm_time;
        $_SESSION["fighter_id"] = $fighter_id;
        $_SESSION["qt_jogadores_time"] = $qt_jogadores_time;       
        $_SESSION["vl_inscricao"] = $vl_inscricao;

        if ($qt_jogadores_time > 1) {

            $arraynm_login = $_POST["nm_login"];
            $i = 1;
            foreach ($arraynm_login as $nm_login) {
                $_SESSION["nm_login" . $i] = $nm_login;
                $i++;
            }

            $arraycd_plataforma_login = $_POST["cd_plataforma_login"];
            $i = 1;
            foreach ($arraycd_plataforma_login as $cd_plataforma_login) {
                $_SESSION["cd_plataforma_login" . $i] = $cd_plataforma_login;
                $i++;
            }
            
            $arrayfighter_id = $_POST["fighter_id_login"];
            $i = 1;
            foreach ($arrayfighter_id as $fighter_id) {
                $_SESSION["fighter_id_login" . $i] = $fighter_id;
                $i++;
            }             
        }
        
    
   header("Location: ../identcliente.php?origem=" . $origem);
}
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Origem: cadastreseinserir.php">
// Trata-se de um novo cliente que acabou de passar pelas telas campeonato e cadastro
if ($origem == "cadastreseinserir.php" || $origem == "clientelogin.php") {
    $cd_campeonato = "";
    if (isset($_SESSION["cd_campeonato"]))
        $cd_campeonato = $_SESSION["cd_campeonato"];
    
  // echo "cd camp 87 - ". $_SESSION["cd_campeonato"];
   require_once "../App_Code/ClientesCampeonatos.php";
   $clienteCampeonato = new ClientesCampeonatos();
   $clienteCampeonato->setCd_cliente($_SESSION["cd_cliente"]);
   $clienteCampeonato->setCd_campeonato($_SESSION["cd_campeonato"]);
   $clienteCampeonato->setIc_pago(0);
   $clienteCampeonato->setId_psn($_SESSION["id_psn"]);
   $clienteCampeonato->setId_xboxlive($_SESSION["id_xboxlive"]);
   $clienteCampeonato->setId_pc($_SESSION["id_pc"]);   
   $clienteCampeonato->setFighter_id($_SESSION["fighter_id"]);
   $clienteCampeonato->setNm_time($_SESSION["nm_time"]);
   $clienteCampeonato->setNm_status_pagamento("não finalizado");    
   $clienteCampeonato->setCd_plataforma($_SESSION["cd_plataforma"]);  
   $clienteCampeonato->setVl_inscricao(floatval($_SESSION["vl_inscricao"]));     
   
   $clienteCampeonato->Inserir($clienteCampeonato);
   $cd_cliente_campeonato = $clienteCampeonato->UltimoClienteCampeonato();
   
   if ( $clienteCampeonato->erro == "") {

       $qt_jogadores_time = "";
        if (isset($_SESSION["qt_jogadores_time"]))
         $qt_jogadores_time = $_SESSION["qt_jogadores_time"];
       
        if($qt_jogadores_time  != "")
        {
            $array_nm_login= array ();
            $array_cd_plataforma_login= array ();
            $array_fighter_id_login= array ();            
            $i=1;
            for ($i=1;$i<$qt_jogadores_time;$i++)
            {
                $array_nm_login[$i] = $_SESSION["nm_login" . $i];
                $array_cd_plataforma_login[$i] = $_SESSION["cd_plataforma_login" . $i];
               $array_fighter_id_login[$i] = $_SESSION["fighter_id_login" . $i];                
            }
               
             $clienteCampeonato->AtualizarClientesCampeonatosLogins($cd_cliente_campeonato, $array_nm_login, $array_cd_plataforma_login,$array_fighter_id_login);
                if ($clienteCampeonato->erro != "") 
           {
                ?>
                   <script>alert("<?php echo "Erro na inclusão de logins: ".$clienteCampeonato->erro ?>");history.back();</script>
                <?php
            }
          else{
                  $_SESSION["cd_cliente_campeonato"]=$cd_cliente_campeonato;
                  require_once "../App_Code/Campeonatos.php";
                  $campeonatoAtual = new Campeonatos();
                  $campeonatoAtual->getUmItem($cd_campeonato);
//                  if($campeonatoAtual->getVl_inscricao()!=0)
//                  {
                      echo "<script>alert('Inscrição cadastrada com sucesso.');window.location='../pagamento.php';</script>" ;
//                  }
//                  else
//                  {
//                       echo "<script>alert('Inscrição cadastrada com sucesso.');window.location='../index.php';</script>" ;
//                  }
                  
             }

        }

    } 
    else
    {
        ?>
             <script>alert("<?php echo  $clienteCampeonato->erro  ?>");history.back();</script>
        <?php

    }
    
}


// </editor-fold>
   

    
