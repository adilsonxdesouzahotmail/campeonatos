<?php
session_start();
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php

require_once "../App_Code/Clientes.php";

$cliente = new Clientes();
$cliente->setNm_cliente($_POST["nm_cliente"]);
$cliente->setNm_cpf_cnpj($_POST["nm_cpf_cnpj"]);
$cliente->setNm_cep($_POST["nm_cep"]);
$cliente->setNm_endereco($_POST["nm_endereco"]);
$cliente->setNm_num_endereco($_POST["nm_num_endereco"]);
$cliente->setNm_complemento($_POST["nm_complemento"]);
$cliente->setNm_bairro($_POST["nm_bairro"]);
$cliente->setNm_cidade($_POST["nm_cidade"]);
$cliente->setNm_uf($_POST["nm_uf"]);
$cliente->setNm_ddd($_POST["nm_ddd"]);
$cliente->setNm_telefone($_POST["nm_telefone"]);
$cliente->setNm_celular($_POST["nm_celular"]);
$cliente->setNm_email($_POST["nm_email"]);
$cliente->setNm_senha($_POST["nm_senha"]);

$cliente->Inserir($cliente);


if ($cliente->erro == "") 
{
    $_SESSION["nm_email"]=$_POST["nm_email"];
    $_SESSION["nm_cliente"]=$_POST["nm_cliente"];
    $_SESSION["cd_cliente"] = $cliente->UltimoCliente();
  //  echo $cliente->UltimoCliente();
    $origem="";
    if(isset($_POST["origem"]))
    {
         $origem=$_POST["origem"];
    }
    if ($origem==""){
        $origem=index;
    }
    
    echo "<script>alert('Seu cadastro foi finalizado com sucesso');window.location='../$origem.php';</script>";
}

else
{
    ?>
         <script>alert("<?php echo $cliente->erro  ?>");history.back();</script>
    <?php
   
}

?>
