<?php
session_start();
error_reporting(E_ALL);
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php

require_once "../App_Code/Clientes.php";
$cliente = new Clientes();
$nm_email  = $_POST["nm_email"];
$nm_senha  = $_POST["nm_senha"];
$nm_senha_nova  = $_POST["nm_senha_nova"];
$cliente->Login($nm_email, $nm_senha);
if($cliente->getNm_cliente()!="")
{
    $cliente->AlterarSenha($nm_senha_nova, $cliente->getCd_cliente());
}
 else {
    $cliente->erro = "Senha atual informada incorreta";
}

if ($cliente->erro == "") 
{

    echo "<script>alert('Senha alterada com sucesso');window.location='../home';</script>";
}

else
{
    ?>
         <script>alert("<?php echo $cliente->erro  ?>");history.back()</script>
    <?php
   
}

?>
