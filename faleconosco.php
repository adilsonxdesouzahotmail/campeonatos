﻿<?php
    session_start();
    //error_reporting(E_ALL);
?>﻿
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/head.php" ?> 
    </head>
    <body>
       <?php include "includes/topoemenu.php" ?> 
        <div class="container">
            <h2 style="text-decoration: underline;font-weight:bold;">Fale Conosco</h2>

            <div>
                Envie aqui suas d&uacute;vidas, sugest&otilde;es, etc. <br/>
                Será um prazer respond&ecirc;-lo assim que poss&iacute;vel.
            </div>
            <div class="col-sm-3" style="margin-top:20px;margin-bottom:20px;font-size:0.9em">
                
                <div><img class="img-responsive img-rounded" src="fotosnoticias/foto1.jpg" /></div>

                <div style="margin-top:15px;">
                   <br/><br/>
                    contato@combozone.com.br
                </div>
            </div>

            <div class="col-sm-9" style="margin-top:20px;margin-bottom:20px;">
                <form class="form-horizontal" action="faleconoscoenviar.php" method="post">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Nome:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nome" placeholder="Seu nome" name="nome">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Email:</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" placeholder="Seu email" name="email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Telefone:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="telefone" placeholder="Seu telefone com DDD" name="telefone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="comment">Campeonato (opcional):</label>
                        <div class="col-sm-10">
                               <select class="form-control" id="campeonato" name="campeonato">
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from campeonatos";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_campeonato"] .">" . $row ["nm_campeonato"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="comment">Mensagem:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="5" id="mensagem" name ="mensagem" placeholder="Digite sua mensagem"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-default botaosubmit">Enviar</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>

       <?php include "includes/rodape.php" ?> 
     
    </body>
</html>            
