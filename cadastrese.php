﻿<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/head.php" ?> 
        <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
        <script src="js/validadores.js" type="text/javascript"></script>
        <script>
            function validaformidentcliente()
            {
                var ok = true;
                var erro = "";

                $nome_cliente = formidentcliente.nome_cliente.value;
                $cpf_cliente = formidentcliente.cpf_cliente.value.replace("-", "").replace(".", "");
                $cep_cliente = formidentcliente.cep_cliente.value.replace("-", "");
                $rua_cliente = formidentcliente.rua_cliente.value;
                $num_cliente = formidentcliente.num_cliente.value;
                $complemento_cliente = formidentcliente.complemento_cliente.value;
                $bairro_cliente = formidentcliente.bairro_cliente.value;
                $cidade_cliente = formidentcliente.cidade_cliente.value;
                $uf_cliente = formidentcliente.uf_cliente.value;
                $ddd_cliente = formidentcliente.ddd_cliente.value;
                $telefone_cliente = formidentcliente.telefone_cliente.value;
                $celular_cliente = formidentcliente.celular_cliente.value;
                $email_cliente = formidentcliente.email_cliente.value;
                $senha_cliente = formidentcliente.senha_cliente.value;
                $senha_cliente_confirmar = formidentcliente.senha_cliente_confirmar.value;
                if ($nome_cliente == "")
                {
                    erro += "Digite seu nome<br/>";
                    ok = false;
                } else
                {

                    if ($nome_cliente.trim().indexOf(" ") == -1)
                    {
                        erro += "Digite seu sobrenome<br/>";
                        ok = false;
                    }
                }

                if ($email_cliente == "")
                {
                    erro += "Informe o seu email<br/>";
                    ok = false;
                } else
                {
                    if (!IsValidEmail($email_cliente))
                    {
                        erro += "Email inválido <br/>";
                        ok = false;
                    }
                }

                if ($cpf_cliente == "")
                {
                    erro += "Digite seu CPF<br/>";
                    ok = false;
                } else
                {
                    if (isNaN($cpf_cliente))
                    {
                        erro += "Digite apenas numeros para o CPF<br/>";
                        ok = false;
                    } else
                    if ($cpf_cliente.length != 11)
                    {
                        erro += "CPF inválido. Verifique<br/>";
                        ok = false;
                    } else
                    {
                        if (!TestaCPF($cpf_cliente))
                        {
                            erro += "CPF inválido. Verifique<br/>";
                            ok = false;
                        }
                    }
                }

                if ($cep_cliente == "")
                {
                    erro += "Informe seu CEP<br/>";
                    ok = false;
                } else
                {
                    if (isNaN($cep_cliente))
                    {
                        erro += "Digite apenas numeros para o CEP<br/>";
                        ok = false;
                    } else
                    if ($cep_cliente.replace("-", "").length < 8)
                    {
                        erro += "CEP inválido<br/>";
                        ok = false;
                    }

                }

                if ($rua_cliente == "")
                {
                    erro += "Informe o logradouro<br/>";
                    ok = false;
                }

                if ($num_cliente == "")
                {
                    erro += "Informe o número<br/>";
                    ok = false;
                }


                if ($bairro_cliente == "")
                {
                    erro += "Informe o bairro<br/>";
                    ok = false;
                }
                if ($cidade_cliente == "")
                {
                    erro += "Informe a cidade<br/>";
                    ok = false;
                }

                if ($uf_cliente == "")
                {
                    erro += "Informe a UF<br/>";
                    ok = false;
                }
                if ($ddd_cliente == "")
                {
                    erro += "Informe o DDD<br/>";
                    ok = false;
                } else
                {
                  if ($ddd_cliente.length != 2)
                  {
                    erro += "DDD invalido. Verifique<br/>";
                    ok = false;
                  } 
                }


                if ($telefone_cliente == "")
                {
                    erro += "Informe o telefone<br/>";
                    ok = false;
                } else
                {
                   if ($telefone_cliente.length < 7)
                    {
                        erro += "Telefone invalido. Verifique<br/>";
                        ok = false;
                    }
                }


                if ($celular_cliente == "")
                {
                    erro += "Informe o celular<br/>";
                    ok = false;
                } 
                else
                {
                    if ($celular_cliente.length < 7)
                    {
                        erro += "Celular invalido. Verifique<br/>";
                        ok = false;
                    }
                }


                if ($senha_cliente == "")
                {
                    erro += "Informe sua senha<br/>";
                    ok = false;
                }
                if ($senha_cliente_confirmar == "")
                {
                    erro += "Confirme sua senha<br/>";
                    ok = false;
                }
                if ($senha_cliente != "" && $senha_cliente_confirmar != "")
                {
                    if ($senha_cliente != $senha_cliente_confirmar)
                    {
                        erro += "A senha e a confirmação da senha não conferem<br/>";
                        ok = false;
                    }

                }
                //alert("139 " + erro);
                if (erro != "")
                {
                    $("#mensagemerro").html(erro);
                    $("#myModal").modal();
                }
                //alert("145");
               // alert(ok);
                return ok;
            }


        </script>     

    </head>
    <body>
        <?php include "includes/topoemenu.php" ?> 

        <div class="container">
            <h2 style="text-decoration: underline;font-weight:bold;">Faça seu cadastro</h2>
            <div>
                Seja mais um de nossos <strong>CLIENTES SATISFEITOS </strong><br />
                Solicite sua proposta e descubra o que podemos fazer por voc&ecirc; e sua empresa
            </div>
            <div class="row" style="margin-top:20px;margin-bottom:20px;">
<!--                <form class="form"  name="formidentcliente" action="cadastreseinserir.php" >-->

                <form class="form"  name="formidentcliente" action="cadastreseinserir.php" onsubmit="return validaformidentcliente();
                        return false;">                    
                    
                    <!--                    <div class="col-sm-6">-->
                    <input type="hidden" name="origem" value="<?php echo $_GET["origem"] ?>" />
                    <div class="row">
                        <div class="col-sm-12">
                        <label class="col-sm-1" >Nome: </label>
                        <div class="form-group col-sm-11">
                            <input  class="form-control" id="nome_cliente" placeholder="Nome" name="nome_cliente">
                        </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">                        
                        <label class="col-sm-1" >Email: </label>
                        <div class="form-group  col-sm-11">
                            <input  class="form-control" id="email_cliente" placeholder="Email" name="email_cliente">
                        </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label class="col-sm-3" >DDD: </label>
                            <div class="form-group col-sm-9">

                                <input  class="form-control" id="ddd_cliente" onkeypress="return isNumberKey(event,false)" placeholder="DDD (somente números)" name="ddd_cliente">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label class="col-sm-3" >Telefone: </label>
                            <div class="form-group col-sm-9">
                                <input  class="form-control" id="telefone_cliente"  onkeypress="return isNumberKey(event,false)" placeholder="Telefone  (somente números)" name="telefone_cliente">
                            </div>                            
                        </div>
                        <div class="col-sm-4">
                            <label class="col-sm-3" >Celular: </label>
                            <div class="form-group col-sm-9">
                                <input  class="form-control" id="celular_cliente" placeholder="Celular  (somente números)"  onkeypress="return isNumberKey(event,false)" name="celular_cliente">
                            </div>                            
                        </div>                            
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <label class="col-sm-3" >CPF: </label>
                            <div class="form-group col-sm-9">
                                <input  class="form-control" id="cpf_cliente" placeholder="CPF  (somente números)"  onkeypress="return isNumberKey(event,false)" name="cpf_cliente">
                            </div>
                        </div>

                    </div>                        
                    <div class="row">
                        <div class="col-sm-8">
                             <label class="col-sm-3" >Logradouro: </label>
                            <div class="form-group col-sm-9">
                                <input  class="form-control" id="rua_cliente" placeholder="Rua" name="rua_cliente">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label class="col-sm-4" >Numero: </label>
                            <div class="form-group col-sm-8">
                                <input  class="form-control" id="num_cliente" placeholder="Num." name="num_cliente">
                            </div>                            
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <label class="col-sm-4" >Complemento: </label>                            
                            <div class="form-group col-sm-8">
                                <input  class="form-control" id="complemento_cliente" placeholder="Complemento" name="complemento_cliente">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-sm-4" >Bairro: </label>                             
                            <div class="form-group col-sm-8">
                                <input  class="form-control" id="bairro_cliente" placeholder="Bairro" name="bairro_cliente">
                            </div>                            
                        </div>
                    </div>   

                    <div class="row">
                        <div class="col-sm-4">
                             <label class="col-sm-3" >Cidade: </label>  
                            <div class="form-group col-sm-9">
                                <input  class="form-control" id="cidade_cliente" placeholder="Cidade" name="cidade_cliente">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label class="col-sm-3" >UF: </label>  
                            <div class="form-group col-sm-9">
                                <select class="form-control" id="uf_cliente" name="uf_cliente">
                                    <option value="">Selecione a UF</option>
                                    <option value="AC">AC</option><option value="AL">AL</option><option value="AP">AP</option><option value="AM">AM</option><option value="BA">BA</option><option value="CE">CE</option><option value="DF">DF</option><option value="ES">ES</option><option value="GO">GO</option><option value="MA">MA</option><option value="MS">MS</option><option value="MT">MT</option><option value="MG">MG</option><option value="PA">PA</option><option value="PB">PB</option><option value="PR">PR</option><option value="PE">PE</option><option value="PI">PI</option><option value="RJ">RJ</option><option value="RN">RN</option><option value="RS">RS</option><option value="RO">RO</option><option value="RR">RR</option><option value="SC">SC</option><option value="SP">SP</option><option value="SE">SE</option><option value="TO">TO</option> 
                                </select>
                            </div>                            
                        </div>
                         <div class="col-sm-4">
                              <label class="col-sm-3" >CEP: </label>  
                            <div class="form-group col-sm-9">
                                <input  class="form-control" id="cep_cliente" placeholder="CEP (somente números)"   onkeypress="return isNumberKey(event,false)"  name="cep_cliente">
                            </div>
                        </div>
                    </div>   
                    <div class="row">


                    </div>                          
                    <div class="row">
                        <div class="col-sm-6">
                            <label class="col-sm-4" >Senha: </label>  
                            <div class="form-group col-sm-8">
                                <input type="password" class="form-control" id="senha_cliente" placeholder="Senha" name="senha_cliente">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label class="col-sm-4" >Confirme a senha: </label>  
                            <div class="form-group col-sm-8">
                                <input type="password" class="form-control" id="senha_cliente_confirmar" placeholder="Confirme a Senha" name="senha_cliente_confirmar">
                            </div>                            
                        </div>
                    </div>   
                    <div class="form-group col-sm-12">
                        <button type="submit" class="btn btn-default botaosubmit">Enviar</button>
<!--                        <button type="button" onclick="validaformidentcliente();" class="btn btn-default">Enviar</button>-->
                    </div>

                    <!--                    </div>-->
                    <!--                    <div class="col-sm-6">
                                            Texto a definir
                    
                                        </div>-->
                </form>
            </div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Erros encontrados</h4>
                        </div>
                        <div class="modal-body" >
                            <p id="mensagemerro">This is a small modal.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <?php include "includes/rodape.php" ?> 
        <?php include "includes/scriptsfinais.php" ?>         
    </body>
</html>
