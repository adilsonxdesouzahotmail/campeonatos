﻿<?php
session_start();
//error_reporting(E_ALL);
require_once 'App_Code/Funcoes.php';
?>﻿
<!DOCTYPE html>
<html lang="en">
    <head>
        <?php include "includes/head.php" ?> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>        
    </head>
    <body>
        <?php include "includes/topoemenu.php" ?> 

        <div class="container">
            <?php
            require_once "App_Code/Noticias.php";
            $noticia = new Noticias();
            $cd_noticia = "";
            if (isset($_GET["cd_noticia"]))
                $cd_noticia = $_GET["cd_noticia"];
            $noticia->getUmItem($cd_noticia);


            //////////// Videos Youtube ////////
            $texto = $noticia->getDs_noticia();
            $procura = "{{https://youtu.be/";
            $trocaporinicio = "<div class='row'>" .
                    "<div class='col-sm-2'></div>" .
                    "<div class='col-sm-8'>" .
                    "<div class='embed-responsive embed-responsive-16by9'>" .
                    "<iframe class='embed-responsive-item' width='400' height='400' src='https://www.youtube.com/embed/";

            $texto = Funcoes::trocaTexto($texto, $procura, $trocaporinicio);

            $procura = "}}";
            $trocaporfinal = "' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>" .
                    "</div>" .
                    "</div>" .
                    "<div class='col-sm-2'></div>" .
                    "</div>";

            $texto = Funcoes::trocaTexto($texto, $procura, $trocaporfinal);
            
            //////////// Botão inscreva-se campeonato ////////    
           if($noticia->getCd_campeonato() != 0 && $noticia->getCd_campeonato() != NULL && $noticia->getCd_campeonato() != "")
           {
             $botao = "<a class='btn btn-default botaosubmit' ".
                    " href='campeonato.php?cd_campeonato=".$noticia->getCd_campeonato()."'".">Inscreva-se</a>";
            $procurabotao = "[[campeonato]]";
            
          //  $texto = substr_replace($texto,,)
            $texto = Funcoes::trocaTexto($texto, $procurabotao, $botao);
           }
           else {
              // echo "camp".$noticia->getCd_campeonato();
           }
           
           //////////// Imagem no meio da notícia inicio////////  
            $procura = "++";
            $trocaporinicio = "<img style='width:100%;' src='";

            $texto = Funcoes::trocaTexto($texto, $procura, $trocaporinicio);

            $procura = "--";
            $trocaporfinal = "'/>";

            $texto = Funcoes::trocaTexto($texto, $procura, $trocaporfinal);
           //////////// Imagem no meio da noticia fim///////////
      
            ?>
   
            <div class="col-sm-8" >
                <div class="row" >
                    <h1><?php echo $noticia->getNm_titulo() ?></h1>
                    <div class="row-sm-12" >
                        <?php
                        if ($noticia->getNm_url_foto() != "") {
                            ?>
                            <img class="img-responsive" src="fotosnoticias/<?php echo $noticia->getCd_noticia(); ?>/foto1.jpg" />
                            <?php
                        }
                        ?>
                    </div>
                    <div class="row-sm-12" style="font:normal 0.8em tahoma;margin-top:10px;">
                        <?php echo $texto ?>
                    </div>

                      <?php
                      require_once 'App_Code/Conexao.php';
                      
                      $conexao= new Conexao();
 
                     $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
                     $sql_query = "select nf.* from noticias_fotos nf join noticias no on nf.cd_noticia = no.cd_noticia ".
                         "where nf.cd_noticia=".$cd_noticia." and no.ic_mostra_carrossel = 1 order by vl_posicao";
                      $rs = $mysqli->query ( $sql_query);
 
                        $posfoto=0;
                        $carrossel_inner="";
                        $temfoto=false;
                        while ( $row = $rs->fetch_assoc () ) 
                        {
                            $temfoto=true;
                            $carrossel_inner.= "<div class='item";
                            if($posfoto==0)
                            {
                                $carrossel_inner.= " active' >" ;
                            }
                            else{
                                $carrossel_inner.= "' >" ;
                            }
                   
                            $carrossel_inner.="<img src='fotosnoticias/".$row ['cd_noticia']."/fotos/".
                                    $row ['nm_url_foto']."' alt=''/></div>";
                            $carrossel_inner."</div>";
                            $posfoto ++;
                        }
                        $rs->close ();
                        $mysqli->close ();
                        ?>
                      
                        <?php
                       // $temfoto=false;
                        if($temfoto)
                        {
                        ?>
                        <div class="row-sm-12" style="font:normal 0.8em tahoma;margin-top:10px;">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <?php
                                  for($i=0; $i<$posfoto ; $i++)
                                    {
                                      echo "<li data-target='#myCarousel' data-slide-to='".$i."'";
                                      if($i==0)
                                      {
                                          echo " class='active'";
                                      }
                                      echo "></li>";
                                    }
                                ?>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">

                                <?php echo $carrossel_inner ?>

                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                        </div>    
                        <?php
                        }
                        ?>
            
                </div>


            </div><!--nFim do sm12 -->
             <?php include "includes/ultimasnoticias.php" ?>     


        </div> 
       <?php include "includes/rodape.php" ?> 

    </body>
</html>
