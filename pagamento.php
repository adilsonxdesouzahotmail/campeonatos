<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
error_reporting(E_ALL);
header('Accept: application/ws.pagseguro.uol.com.br;charset=ISO-8859-1');
//header('Content-Type: application/json; charset=UTF-8');

session_start();
require_once 'App_Code/Conexao.php';
require_once "App_Code/ClientesCampeonatos.php";

$clienteCampeonato = new ClientesCampeonatos();

$cd_cliente_campeonato = "";
if (isset($_SESSION["cd_cliente_campeonato"])) {
    $cd_cliente_campeonato = $_SESSION["cd_cliente_campeonato"];
}
$clienteCampeonato->getUmItem($cd_cliente_campeonato);

//$vl_inscricao = "";
//if (isset($_SESSION["vl_inscricao"])) {
//    $vl_inscricao = $_SESSION["vl_inscricao"];
//}
if (floatval($clienteCampeonato->getVl_inscricao()) > 0) 
{
    include "App_Code/CreatePaymentRequest.php";

   // echo "clicamp " . $cd_cliente_campeonato . "<br/>";
    CreatePaymentRequest::main($cd_cliente_campeonato);
    //echo "clicamp 21" . $cd_cliente_campeonato . "<br/>";
    if (CreatePaymentRequest::$erro != "") {
        ?>
        <script >
            //alert("Ocorreu um erro inexperado. Tente mais tarde");
            alert("Erro do PagSeguro:<?php echo CreatePaymentRequest::$erro; ?>");
        </script>
        <?php
    }
    
}
 else 
{
     
     $clienteCampeonato->AtualizarStatusPagamento($cd_cliente_campeonato, "Gratuito", 1);
 
     require_once "App_Code/Campeonatos.php";
     $campeonato = new Campeonatos();
     $campeonato->checaEncerrado($clienteCampeonato->getCd_campeonato());

     echo"<script>window.location='index.php';</script>";
 }


    