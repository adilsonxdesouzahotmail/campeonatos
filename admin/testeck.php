<!DOCTYPE html>
<html>
    <head>
        <title>A Simple Page with CKEditor</title>
        <!-- Make sure the path to CKEditor is correct. -->
        <style>
           
            .ckeditorcontainer{
                width:800px;float:left;clear:left;
                height:800px;
            }
            .conteudocentro{
                width:800px;height:800px;
                float:left;clear:left;background-color:red;
            }
        </style>
        <script src="ckeditor/ckeditor.js"></script>
    </head>
    <body>
        <form>
            <div class="conteudocentro">
                <div class="ckeditorcontainer">
                    <textarea name="editor1"  id="editor1" rows="10" cols="80" style="height:100%;">
                     This is my textarea to be replaced with CKEditor.
                    </textarea>
                </div>
            </div>
            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('editor1');
                
            </script>
        </form>
    </body>
</html>