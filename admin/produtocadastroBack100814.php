<?php
session_start();
require_once "includes/autentica.php";
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
        <script src="js/jquery.maskMoney.js" type="text/javascript"></script>
         <script type="text/javascript">
            function carregasubcategorias()
            {
                var url = "subcategoriaarqauxiliares/carregasubcategorias.php";
                formdata = "cd_categoria=" + $("#cd_categoria").val();
                $.ajax({
                    url: url,
                    data: formdata,
                    type: 'POST',
                    dataType: 'html',
                    success: function(data) {
                        $("#cd_subcategoria").html("<option value='0'>Selecione</option>" + data);
                    }
                });

            }
            $(document).ready(funtion(){
                
                 $("#qt_peso").onkeypress = function(e) {
                     alert(e.keyCode);
                }

            });
        </script>
        
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="produtoslista.php">Produtos</a> > <strong>Cadastrar</strong>
                </nav>
                <nav class="navabas">
                    <div class="aba">
                        Dados
                    </div>
<!--                    <div class="abadisabled">
                        Imagem 1
                    </div>
                    <div class="abadisabled">
                        Imagem 2
                    </div>-->
                    <div class="abadisabled" style="width: 200px;">
                        Galeria de Fotos
                    </div>
                </nav>
                <form action="produtoarqauxiliares/produtocadastroinserir.php" method="post"
                      onsubmit="return validaformprodutos();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Cadastrar novo produto</legend>
                        <ol>
                            <li>
                                <label>
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_produto" id="nm_produto" >
                                <label>
                                    Ativo:</label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_ativo" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" value="0" name="ic_ativo" />&nbsp;&nbsp; </li>
                            <li>
                                <label>
                                    Categoria:
                                </label>
                                <select name="cd_categoria" id="cd_categoria" onchange="carregasubcategorias();" class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from categorias";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_categoria"] . ">" . $row ["nm_categoria"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                                <label>
                                    Subcategoria:
                                </label>
                                <select name="cd_subcategoria" id="cd_subcategoria" class="Campos">
                                    <option value="0">Selecione</option>
                                </select>
                            </li>
                            <li>
                                <label>
                                    Tamanhos:</label>
                                <table class="tbltamanhos">
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from tamanhos";
                                    $rs = $mysqli->query($query);
                                    $cont = 1;
                                    echo "<tr>";
                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<td>";
                                        echo "<input type='checkbox' value='" . $row ["cd_tamanho"] . "' name='cd_tamanho[]' />&nbsp;" . $row ["nm_tamanho"] . "&nbsp;";
                                        echo "</td>";
                                        if ($cont % 5 == 0)
                                            echo "</tr><tr>";
                                        $cont++;
                                    }
                                    echo "</tr>";
                                    $rs->free();
                                    ?>
                                </table>
                            </li>
                            <li>
                                <label>
                                    Campo 1:
                                </label>
                                <input class="Campos required" name="nm_campo_extra_1">
                            </li>
                            <li>
                                <label>
                                    Campo 2:
                                </label>
                                <input class="Campos required" name="nm_campo_extra_2">
                            </li>
                            <li>
                                <label>
                                    Características:</label>
                                <table class="tblcaracteristicas">
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from caracteristicas";
                                    $rs = $mysqli->query($query);
                                    $cont = 1;
                                    echo "<tr>";
                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<td>";
                                        echo "<input type='checkbox' value='" . $row ["cd_caracteristica"] . "' name='cd_caracteristica[]' />&nbsp;" . $row ["nm_caracteristica"] . "&nbsp;";
                                        echo "</td>";
                                        if ($cont % 5 == 0)
                                            echo "</tr><tr>";
                                        $cont++;
                                    }
                                    echo "</tr>";
                                    $rs->free();
                                    ?>
                                </table>
                            </li>
                            <li style="margin-top: 10px;">
                                <label>
                                    Descrição:</label>
                                <textarea id="txtdescricao" rows="5" class="Campos required" name="ds_produto"></textarea>
                            </li>
                            <li>
                                <label>
                                    Valor (de):
                                </label>
                                <input class="Campos required" name="vl_de" id="vl_de">
                                 <input type="hidden" name="vl_dehd" id="vl_dehd">
                                 <script type="text/javascript">  $("#vl_de").maskMoney({ prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: true }); </script>
                                 <label>
                                    Valor (por):
                                </label>
                                <input class="Campos required" name="vl_por" id="vl_por">
                                <input type="hidden" name="vl_porhd" id="vl_porhd">
                                 <script type="text/javascript">  $("#vl_por").maskMoney({ prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: true }); </script>
                            </li>
                            <li>
                                <label>
                                    Peso (gramas):
                                </label>
                                <input class="Campos required" name="qt_peso" id="qt_peso"    >
                            </li>
                          
                            <li>
                                <label>
                                    Estoque:
                                </label>
                                <input class="Campos required" name="qt_estoque" id="qt_estoque">
                                
                                <label>
                                    Estoque min.:
                                </label>
                                <input class="Campos required" name="qt_estoque_minimo" id="qt_estoque_minimo">
                            </li>
                             
                            <li>
                                <label>
                                    Destaque:
                                </label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_destaque" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" value="0" name="ic_destaque" />&nbsp;&nbsp; 
                            </li>
                            <li>
                                <label>
                                    Oferta:
                                </label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_oferta" style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" value="0" name="ic_oferta" checked  />&nbsp;&nbsp; 
                            </li>
                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
