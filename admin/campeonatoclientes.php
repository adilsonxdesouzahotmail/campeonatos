<?php
session_start();
require_once "includes/autentica.php";


error_reporting(E_ALL);

require_once "../App_Code/Clientes.php";
require_once "../App_Code/Conexao.php";
require_once "../App_Code/Config.php";
$conexao = new Conexao();

$cliente = new Clientes();

$cd_campeonato = "";
if (isset($_GET["cd_campeonato"]))
    $cd_campeonato = $_GET["cd_campeonato"];

     require_once "../App_Code/Campeonatos.php";
      $campeonato = new Campeonatos();
     $campeonato->getUmItem($cd_campeonato);                    
                    
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 

    </head>
    <body >

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita"  >
            <div class="container">
                <nav class="breadcrumb">
                    <a href="campeonatoslista.php">Campeonatos</a> > <strong>Editar</strong>
                </nav>

                <nav class="navabas">
                    <div class="aba2" onclick="window.location = 'campeonatoedicao.php?cd_campeonato=<?php echo $cd_campeonato ?>'">
                        Campeonato
                    </div>

                    <div class="aba2" style="width: 120px;"
                         onclick="window.location = 'campeonatogaleriafotos.php?cd_campeonato=<?php echo $cd_campeonato ?>'" >
                        Galeria de Fotos
                    </div>
                  <div class="aba" style="width: 120px;"
                       onclick="window.location = 'campeonatoclientes.php?cd_campeonato=<?php echo $cd_campeonato ?>'" >
                      Clientes
                  </div>                      
                </nav>
               

                <table class="tbllistagem" id="tbllistagem" >
                    <tr>
                        <th style="width: 30px;">
                            C&oacute;d.
                        </th>
                        <th style="width: 100px;">
                            Nome
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Plataforma
                        </th>                        
                        <th style="width: 40px; text-align: center;">
                            ID PSN
                        </th>
                        <th style="width: 40px; text-align: center;">
                            ID XBox Live
                        </th>
                        
                         <th style="width: 40px; text-align: center;">
                            ID PC
                        </th>                        
                       
                        <th style="width: 40px; text-align: center;">
                            Fighter ID
                        </th>
                        <?php
                        if ($campeonato->getQt_jogadores_time() > 1) {
                            ?>                        
                            <th style="width: 40px; text-align: center;">
                                Time
                            </th>  

                            <th style="width: 100px; text-align: center;">
                                Colegas
                            </th> 
                            <?php
                        }
                        ?>                        
                        
                        <th style="width: 40px; text-align: center;">
                            Status Pagseguro
                        </th>   
                        <th style="width: 40px; text-align: center;">
                           Pago
                        </th> 
                        <th style="width: 40px; text-align: center;">
                           Vl_inscricao
                        </th>                        


                        
                    </tr>
                    <?php


                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                    $mysqli->set_charset("utf8");
                    $query = "select cli.cd_cliente,cli.nome_cliente,clica.id_psn,clica.id_xboxlive,clica.id_pc,".
                             "clica.fighter_id,clica.nm_time,pla.nm_plataforma,clica.nm_status_pagamento,".
                            "clica.ic_pago,clica.vl_inscricao ".
                             "from clientes_campeonatos clica ".
                             "join clientes cli on clica.cd_cliente = cli.cd_cliente ".
                             "join plataformas pla on clica.cd_plataforma = pla.cd_plataforma ".
                             "where clica.cd_campeonato = ".$cd_campeonato;
                    //echo "<br/>".$query."<br/>";
                    $rs = $mysqli->query($query);

                       while ($row = $rs->fetch_assoc()) {
                          echo "<tr>";
                           echo "<td style='padding-right:10px;'>".$row["cd_cliente"]. "</td>";
echo "<td  style='padding-right:10px;'><a target='_blank' href='clientedetalhe.php?cd_cliente=".$row["cd_cliente"]."'>" . $row["nome_cliente"] . "</a></td>";                           
                           //echo "<td style='padding-right:10px;'>".$row["nome_cliente"]. "</td>";    
                           echo "<td style='padding-right:10px;'>".$row["nm_plataforma"]. "</td>";     
                           echo "<td style='padding-right:10px;'>".$row["id_psn"]. "</td>";                                   
                           echo "<td style='padding-right:10px;'>".$row["id_xboxlive"]. "</td>";                                  
                           echo "<td style='padding-right:10px;'>".$row["id_pc"]. "</td>";    
                           echo "<td style='padding-right:10px;'>".$row["fighter_id"]. "</td>";                             
   
                       
                         if($campeonato->getQt_jogadores_time() > 1)
                         {
                             echo "<td style='padding-right:10px;'>".$row["nm_time"]. "</td>"; 
                             ?>
                            <td style=" padding-left:5px;">
                                <?php
                                  $query2 = "select  concat(clicalo.nm_login, ' - ', pla.nm_plataforma,'<br/>') ".
                                            "as colega_plataforma,clicalo.fighter_id from clientes_campeonatos_logins clicalo ".
                                            "join plataformas pla on clicalo.cd_plataforma = pla.cd_plataforma ".
                                            "join clientes_campeonatos clica on clicalo.cd_cliente_campeonato = clica.cd_cliente_campeonato ".
                                            "where clica.cd_cliente = ".$row["cd_cliente"];
                                   //echo $query2."<br/>"; 
                                   $rs2 = $mysqli->query($query2);
                                    $fighter_id = "";
                                    while ($row2 = $rs2->fetch_assoc()) {
                                        echo $row2["colega_plataforma"];
                                        $fighter_id= $row2["fighter_id"];
                                    }
                                    if($fighter_id != "")
                                        echo $fighter_id." - Fighter id";
                                ?>
                            </td> 
                             <?php
                         }
                       
                                                  
                           echo "<td style='padding-right:10px;'>".$row["nm_status_pagamento"]. "</td>";                           
                           echo "<td style='padding-right:10px;'>".$row["ic_pago"]. "</td>"; 
                           echo "<td style='padding-right:10px;'>".$row["vl_inscricao"]. "</td>";                            
                           echo "</tr>";
                        }

                    $rs->free();
                    ?>
                 
                </table>

            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
