
<form id="cadastro"  name="formidentcliente"  >

    <input  class="Campos required"  disabled type="hidden" name="origem" value="<?php echo $origem; ?>"/>
    <fieldset ><legend style="margin-left:30px"> Dados do cliente c&oacute;d: <?php echo $cd_cliente ?></legend>
        <ol>

            <li>
                <label>
                    Nome:
                </label>
                <input  class="Campos required"  disabled type="text" name="nome_cliente" style="width:345px;"
                        value="<?php echo $cliente->getNome_cliente(); ?>">
                
                         <input type="hidden" name="cd_cliente" value="<?php echo $cliente->getCd_cliente() ?>" >
                        <label>
                         Bloqueado:</label>
                         <?php
                           if($cliente->getIc_bloqueado())
                           {
                               $checkedbloqueado = " checked ";
                               $checkednaobloqueado = "";
                           }
                           else
                           {
                               $checkedbloqueado = "";
                               $checkednaobloqueado = " checked ";
                           }
                         ?>
                         Sim&nbsp;<input type="radio" value="1" name="ic_encerrado" <?php echo $checkedbloqueado ?> style="margin-top: 8px;"  />&nbsp;&nbsp;
                         Não&nbsp;<input type="radio" value="0" name="ic_encerrado" <?php echo $checkednaobloqueado ?>  />&nbsp;&nbsp;      
                
            </li>
            <li>
                <label>
                    CPF:
                </label>
                <input  class="Campos required"  disabled type="text" name="cpf_cliente" id="cpf_cliente" style="width:180px;"
                        value="<?php echo $cliente->getCpf_cliente(); ?>">

            </li>
            <li>
                <label>
                    CEP:
                </label>
                <input  class="Campos required"  disabled type="text" name="cep_cliente" style="width:90px;" id="cep_cliente"
                        value="<?php echo $cliente->getCep_cliente(); ?>">

            </li>
            <li>
                <label>
                    Endere&ccedil;o:
                </label>
                <input  class="Campos required"  disabled type="text" name="endereco_cliente" style="width:345px;" id="endereco_cliente"
                        value="<?php echo $cliente->getRua_cliente(); ?>">
            </li>
            <li>
                <label>
                    Num:
                </label>
                <input  class="Campos required"  disabled type="text" name="num_cliente" style="width:90px;" id="num_cliente"
                        value="<?php echo $cliente->getNum_cliente(); ?>">

            </li>
            <li>
                <label>
                    Complemento:
                </label>
                <input  class="Campos required"  disabled type="text" name="complemento_cliente" style="width:345px;" id="complemento_cliente"
                        value="<?php echo $cliente->getComplemento_cliente(); ?>">
            </li>
            <li>
                <label>
                    Bairro:
                </label>
                <input  class="Campos required"  disabled type="text" name="Bairro_cliente" style="width:345px;"
                        value="<?php echo $cliente->getBairro_cliente(); ?>">

            </li>
            <li>
                <label>
                    Cidade:
                </label>
                <input  class="Campos required"  disabled type="text" name="cidade_cliente" style="width:345px;"
                        value="<?php echo $cliente->getCidade_cliente(); ?>">

            </li>
            <li>
                <label>
                    Estado:
                </label>
                <input  class="Campos required"  disabled type="text" name="uf_cliente" style="width:40px;text-transform:uppercase;"
                        value="<?php echo $cliente->getUf_cliente(); ?>">

            </li>
            <li>
                <label>
                    DDD:
                </label>
                <input  class="Campos required"  disabled type="text" name="ddd_cliente" style="width:90px;"
                        value="<?php echo $cliente->getDdd_cliente(); ?>">
            </li>
            <li>
                <label>
                    Telefone:
                </label>
                <input  class="Campos required"  disabled type="text" name="telefone_cliente" style="width:180px;"
                        value="<?php echo $cliente->getTelefone_cliente(); ?>">

            </li>
            <li>
                <label>
                    Celular:
                </label>
                <input  class="Campos required"  disabled type="text" name="celular_cliente" style="width:180px;"
                        value="<?php echo $cliente->getCelular_cliente(); ?>">

            </li>
            <li>
                <label>
                    E-mail:
                </label>
                <input  class="Campos required"  disabled type="text" name="email_cliente" style="width:345px;"
                        value="<?php echo $cliente->getEmail_cliente(); ?>">

            </li>


        </ol>
    </fieldset>
</form>