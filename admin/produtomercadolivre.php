<?php
error_reporting(E_ALL);
session_start();
 $cd_produto=0;
 if(isset($_GET["cd_produto"]))
   $cd_produto = $_GET["cd_produto"];
require_once "includes/autentica.php";
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
      
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="produtoslista.php">Produtos</a> > <strong>Cadastrar</strong>
                </nav>
                <nav class="navabas">
                    <div class="aba2" onclick="window.location = 'produtoedicao.php?cd_produto=<?php echo $cd_produto ?>'">
                        Dados
                    </div>
                    <div class="aba2" style="width: 120px;" 
                          onclick="window.location = 'produtogaleriafotos.php?cd_produto=<?php echo $cd_produto ?>'" >
                        Galeria de Fotos
                    </div>
                    <div class="aba" style="width: 120px;">
                        Mercado Livre
                    </div>                    
                </nav>
                <form action="produtoarqauxiliares/produtomercadolivreinserir.php" method="get"
                      onsubmit="return validaformprodutos();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Mercado Livre</legend>
                        <ol>
                           <li>
                                <label style="line-height: inherit;">
                                    ID Mercado Livre:
                                </label>
                                <input class="Campos required" name="id_mercadolivre" id="id_mercadolivre" >
                                <input type="hidden" value="<?php echo $_GET['cd_produto'] ?>" name="cd_produto" />
                               <input type="submit" class="botaopadrao" value="Importar do Mercado Livre" 
                                      style="text-transform:none;margin-left:15px;float:left;" onclick="importaMercadoLivre();"/>
                               
                            </li>
  
                           
                        </ol>
                    </fieldset>
                </form>
                 <table class="tbllistagem" id="tbllistagem" style="width:100%; float: left; clear: left;">
                        <tr>

                            <th >
                                Id
                            </th>
                            <th style="text-align: center;">
                                Título
                            </th>
                            <th style="text-align: center;">
                                Preço
                            </th>
                            <th style="text-align: center;">
                                Qtd disp.
                            </th>
                            <th style="text-align: center;">
                                Status
                            </th>
                            <th style="text-align: center;">
                                Vendedor
                            </th>   
                            <th style="text-align: center;">
                                Thumb
                            </th>                            
                            <th style="text-align: center;">
                                Excluir
                            </th>
                        </tr>
                        <?php
                            require_once "../App_Code/Conexao.php";
                            $conexao = new Conexao();
                            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                            $mysqli->set_charset("utf8");
                            $query = "select * from mlbdados_produto where cd_produto=".$cd_produto;
                            $rs = $mysqli->query($query);

                           while ($row = $rs->fetch_assoc()) {
                                echo "<tr>";
                                echo "<td>".$row ["mlb_id"]."</td>";
                                echo "<td>".$row ["mlb_title"]."</td>";
                               // number_format(row ["mlb_price"], 2,',','.')
                                echo "<td>".number_format($row ["mlb_price"], 2,',','.')."</td>";
                                echo "<td style='text-align: center;'>".$row ["mlb_available_quantity"]."</td>";
                                echo "<td>".$row ["mlb_status"]."</td>";
                                echo "<td>".$row ["mlb_seller_id"]."</td>";
                                echo "<td ><img syle='margin:auto;' src='".$row ["mlb_thumbnail"]."' /></td>";
                                 ?>
                                <td style="text-align: center;">
                                <a href="produtomercadolivre.php?mlbdados_produto_id=<?php echo $row ["mlbdados_produto_id"] ?>
                                   &cd_produto=<?php echo $cd_produto ?>&acao=excluir"
                                   onclick="return confirm('Confirma a exclusão do item <?php echo $row ["mlb_title"] ?>?');">
                                    <img src='imagens/lixeira.png' /></a>
                                </td>
                                <?php
                                echo "</tr>";
                            }
                            $rs->free();
                        ?>
                        
                 </table>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
