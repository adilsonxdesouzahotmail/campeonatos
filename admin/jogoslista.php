﻿<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Jogos.php";
require_once "../App_Code/Conexao.php";
require_once "../App_Code/Config.php";
$conexao = new Conexao();

$jogo = new Jogos();
$acao = "";
if (isset($_GET["acao"]))
    $acao = $_GET["acao"];

$cd_roduto = "";
if (isset($_GET["cd_jogo"]))
    $cd_jogo = $_GET["cd_jogo"];


if ($acao == "excluir") {
    $jogo->Excluir($cd_jogo);
    if ($jogo->erro == "")
        echo "<script> alert('Jogo exclu\u00CDdo com sucesso.'); window.location = 'jogoslista.php';</script>";
    else {
        ?>
        <script >                alert("<?php echo $jogo->erro ?>");
            window.location = 'jogoslista.php';</script>
        <?php
    }
}
?>

<html >
    <head>
        <?php include "includes/head2.php" ?> 
          <script type="text/javascript">
            function carregasubcategorias()
            {
                var url = "subcategoriaarqauxiliares/carregasubcategorias.php";
                formdata = "cd_categoria=" + $("#cd_categoria").val();
                $.ajax({
                    url: url,
                    data: formdata,
                    type: 'POST',
                    dataType: 'html',
                    success: function(data) {
                        $("#cd_subcategoria").html("<option value='0'>Selecione</option>" + data);
                    }
                });

            }
        </script>
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <strong>Jogos</strong>
                </nav>
                <div class="contbtnnovo">
                    <input type="button" class="botaopadrao" value="Novo Jogo" onclick="window.location = 'jogocadastro.php';" />
                </div>

                <!------------Busca inicio----------->
                <form action="" method="get"
                      onsubmit="return validaformjogos();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Busca</legend>
                        <ol>
                            <li>
                                <label style="width:95px;">
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_jogo" id="nm_jogo" >
                                <label>
                                    Destaque:</label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_destaque"  style="margin-top: 8px;" />&nbsp;&nbsp;
                                N&atilde;o&nbsp;<input type="radio" value="0" name="ic_destaque" />&nbsp;&nbsp; 
                            </li>
                            Todos&nbsp;<input type="radio" value="3" name="ic_destaque" checked style="margin-top: 8px;"/>&nbsp;&nbsp;
                            </li>
                            <li>
                                <label style="width:95px;">
                                    Categoria:
                                </label>
                                <select name="cd_categoria" id="cd_categoria" onchange="carregasubcategorias();" class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    //$mysqli->set_charset("utf8");
                                    $query = "select * from categorias";
                                    $rs = $mysqli->query($query);

                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_categoria"] . ">" . $row ["nm_categoria"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                                <label>
                                    Subcategoria:
                                </label>
                                <select name="cd_subcategoria" id="cd_subcategoria" class="Campos">
                                    <option value="0">Selecione</option>
                                </select>
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                </form>
                <!------------Busca Fim-------------->


                <?php include "jogoarqauxiliares/jogopaginacao.php" ?>  

                <table class="tbllistagem" id="tbllistagem" >
                    <tr>
                        <th style="width: 30px;">
                            C&oacute;d.
                        </th>
                        <th style="width: 200px;">
                            Nome
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Ativo
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Categoria
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Editar
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Excluir
                        </th>
                    </tr>
                    <?php
                    $jogo->ListaTodosGridAdmin($limit, $subquery);
                    for ($i = 0; $i < count($jogo->meuarray); $i++) {
                        echo "<tr>";
                        echo "<td>" . $jogo->meuarray[$i]->getCd_jogo() . "</td>";

                        echo "<td>" . $jogo->meuarray[$i]->getNm_jogo() . "</td>";

                        if ($jogo->meuarray[$i]->getIc_ativo() != 0)
                            $checked = "checked";
                        else
                            $checked = "";
                        echo "<td align='center'><input type='checkbox' value=''" . $checked . " disabled /></td>";

                        echo "<td>" . $jogo->meuarray[$i]->getNm_categoria() . "</td>";
                        
                        echo"<td align='center'>";
                        echo "<a href='jogoedicao.php?cd_jogo=" . $jogo->meuarray[$i]->getCd_jogo() . "'><img src='imagens/lapis.png'/></a>";
                        echo "</td>";

                        echo "<td align='center'>";
                        ?>
                        <a href="jogoslista.php?cd_jogo=<?php echo $jogo->meuarray[$i]->getCd_jogo() ?>&acao=excluir"
                           onclick="return confirm('Confirma a exclus\u00E3o do Jogo <?php echo $jogo->meuarray[$i]->getNm_jogo() ?>?\u000A Aten\u00E7\u00E3o: imagens e outras informa\u00E7\u00F5es associadas ao jogo tamb\u00E9m ser\u0007\o excluidas.');">
                            <img src='imagens/lixeira.png' /></a>
                        <?php
                        echo "</td>";
                        echo "</tr>";
                        } // fecha for da linha 150
                        ?>
                </table>
           </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
