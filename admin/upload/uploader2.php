<?php
 error_reporting ( E_ALL );
 ini_set ( 'display_errors', '1' );

  $cd_produto = "";
 if(isset($_GET["cd_produto"]))
 {
     $cd_produto= $_GET["cd_produto"];
 }
//******** Configurações ***************/
require_once "../../App_Code/Config.php";
$largura = galeria_largura;
$altura = galeria_altura;
$arquivodestino= "imagem1.png";
$paginaorigem = "../produtogaleriafotos.php";

/*********** Posição da Foto Início ********/
 require_once "../../App_Code/ProdutosFotos.php";
 $produto_foto = new ProdutosFotos();
 $vl_posicao= $produto_foto->GetVlPosicao($cd_produto);

$pastagrande = "../../fotos/".$cd_produto."/g/";
if (!file_exists($pastagrande)) {
    mkdir($pastagrande, 0777, true);
}
$pastamedia = "../../fotos/".$cd_produto."/m/";
if (!file_exists($pastamedia)) {
    mkdir($pastamedia, 0777, true);
}
$pastapequena = "../../fotos/".$cd_produto."/p/";
if (!file_exists($pastapequena)) {
    mkdir($pastapequena, 0777, true);
}
$pastathumb = "../../fotos/".$cd_produto."/pp/";
if (!file_exists($pastathumb)) {
    mkdir($pastathumb, 0777, true);
}

if(isset($_POST) && $_SERVER['REQUEST_METHOD'] == "POST")
{
    $file_name		= strip_tags($_FILES['upload_file']['name']);
    $file_id 		= strip_tags($_POST['upload_file_ids']);
    $file_size 		= $_FILES['upload_file']['size'];
    $nm_url_foto  = "foto".$cd_produto.$vl_posicao.".jpg";
    $file_location 	= $pastagrande."fotoprov.jpg";
    if(move_uploaded_file(strip_tags($_FILES['upload_file']['tmp_name']), $file_location)){
        $produto_foto->Inserir($cd_produto, $nm_url_foto,$vl_posicao);
    }
    else{
            echo 'system_error';
    }
}

require_once "../../App_Code/resizepicture.php";


$erroresize="";  
$file = $file_location;

$resizedFile = $pastagrande.$nm_url_foto;   
smart_resize_image($file, 500, 640, true, $resizedFile, false, false, 100);

$resizedFile = $pastamedia.$nm_url_foto;   
smart_resize_image($file, 250, 320, true, $resizedFile, false, false, 100);

$resizedFile = $pastapequena.$nm_url_foto;
smart_resize_image($file, 180, 200, true, $resizedFile, false, false, 100);

$resizedFile = $pastathumb.$nm_url_foto;
smart_resize_image($file, 60, 60, true, $resizedFile, false, false, 100);

 echo $file_id;
?>