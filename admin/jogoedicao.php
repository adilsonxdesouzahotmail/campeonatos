<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();
require_once "../App_Code/Jogos.php";
$jogo = new Jogos();
$cd_jogo = "";
if (isset($_GET["cd_jogo"]))
    $cd_jogo = $_GET["cd_jogo"];

$jogo->getUmItem($cd_jogo);
?>

<html>
    <head >
        <?php include "includes/head2.php" ?> 

        <script src="ckeditor/ckeditor.js"></script>
        <style>
            .ckeditorcontainer{
                width:100%;float:left;clear:left;
            }
        </style>
    </head>
    <body>
        <?php include "includes/topoelateral.php" ?> 
        <section id="colunadireita">
            <div class="container">

                <nav class="breadcrumb">
                    <a href="jogoslista.php">Jogos</a> > <strong>Editar</strong>
                </nav>
                <nav class="navabas">
                    <div class="aba" onclick="window.location = 'jogoedicao.php?cd_jogo=<?php echo $cd_jogo ?>'">
                        Jogo
                    </div>

                    <div class="aba2" style="width: 120px;"
                         onclick="window.location = 'jogogaleriafotos.php?cd_jogo=<?php echo $cd_jogo ?>'" >
                        Galeria de Fotos
                    </div>

                </nav>
                <form action="jogoarqauxiliares/jogocadastroeditar.php" method="post" id="cadastro" name="cadastro"
                      onsubmit="return validaformjogos();">

                    <fieldset ><legend style="margin-left:30px"> Editar dados do jogo</legend>
                        <ol>
                            <li>
                                <label>
                                    Cód:
                                </label>
                                <input class="Campos required" disabled name="cd_jogo" value="<?php echo $jogo->getCd_jogo() ?>" >
                                <input type="hidden" name="cd_jogo" value="<?php echo $jogo->getCd_jogo() ?>" >
                            </li> 

                            <li>
                                <label>
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_jogo" id="nm_jogo" value="<?php echo $jogo->getNm_jogo() ?>" >
                                <label>
                                    Ativo:</label>
                                Sim&nbsp;<input type="radio" <?php if ($jogo->getIc_ativo() == 1) echo "checked"; ?> value="1" name="ic_ativo" style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" <?php if ($jogo->getIc_ativo() == 0) echo "checked"; ?> value="0" name="ic_ativo" />&nbsp;&nbsp; </li>
                            <li>


                                <label>
                                    Categoria:
                                </label>
                                <select name="cd_categoria" id="cd_categoria"  class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from categorias";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                            if ($row ["cd_categoria"] == $jogo->getCd_categoria()) {
                                                 echo "<option value=" . $row ["cd_categoria"] . " selected>" . $row ["nm_categoria"] . "</option>";
                                            }else {
                                                 echo "<option value=" . $row ["cd_categoria"] . ">" . $row ["nm_categoria"] . "</option>";
                                            }
                                        
                                    }
                                    $rs->free();
                                    ?>
                                </select>                        

                            </li>
                            <li >
                                <label>
                                    Descrição:</label>

                                <div class="ckeditorcontainer">
                                    <textarea   id="editor1" name="ds_jogo" rows="10" cols="80" >
                                        <?php echo $jogo->getDs_jogo(); ?>
                                    </textarea>
                                </div>
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                    <script>
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('editor1');
                    </script>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
