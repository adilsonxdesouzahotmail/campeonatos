<?php 
header ("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");  
header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");  
header ("Cache-Control: no-cache, must-revalidate");  
header ("Pragma: no-cache");

    session_start();
    require_once "includes/autentica.php";
    
     $cod_categoria="";
     if(isset($_GET["cod_categoria"]))
         $cod_categoria=$_GET["cod_categoria"];
     
    require_once "App_Code/Guia_categorias.php";
    $categoria = new Guia_categorias();
    $categoria->getUmItem($cod_categoria);

?>

<html >
<head>
   <?php include "includes/head.php" ?>
</head>
<body>
       <?php include "includes/topo.php" ?>
       <div class="container">
        
       <nav>
            <div class="aba" onclick="window.location='categoriaedicao.php?cod_categoria=<?php echo $cod_categoria ?>';">
                Dados
            </div>
            <div class="aba" onclick="window.location='categoriaicone.php?cod_categoria=<?php echo $cod_categoria ?>';">
                Ícone
            </div>
           
        </nav>
           <form action="categoriaarqauxiliares/categoriauploadicone.php" method="post"  
        id="cadastro" enctype="multipart/form-data" onsubmit="return displayaguarde();">
            <input type="hidden" value="<?php echo $cod_categoria ?>" name="cod_categoria"/>
            <h3>Ícone da categoria <?php echo $categoria->getNome_categoria(); ?> - Tamanho base: 26px por 26px</h3>
            <div class="containerfoto">
                <div class="fotobanner"  align="center">
                    <?php 
                        if($categoria->getImagem() =="")
                        {
                            echo "Sem ícone cadastrado";
                        }
                        else {
                            echo " <img src='../secoes/".$categoria->getImagem()."' alt='Ícone cadastrado' />";
                        }

                    ?>
                   
                </div>
                <div class="areaupload">
                   
                    <input id="file" class="inputselecfoto" name="file"  type="file">
                    
                     <input class="btnuploadfoto"  value="Upload" type="submit">
                     <div id="aguarde">Aguarde</div>
                 </div> 
            </div>
        </form>
       </div> 
     <?php include "includes/rodape.php" ?>
</body>
</html>

