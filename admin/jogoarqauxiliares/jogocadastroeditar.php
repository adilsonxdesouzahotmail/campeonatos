<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
require_once "../../App_Code/Jogos.php";
$cd_jogo="";
$nm_jogo="";
$cd_categoria="";

if(isset($_POST["cd_jogo"]))
    $cd_jogo=$_POST["cd_jogo"];

if(isset($_POST["nm_jogo"]))
    $nm_jogo=$_POST["nm_jogo"];  
  
if(isset($_POST["cd_categoria"]))
    $cd_categoria=$_POST["cd_categoria"];  

$jogo = new Jogos();

$jogo->setNm_jogo($nm_jogo);
$jogo->setCd_jogo($cd_jogo);
$jogo->setCd_categoria($cd_categoria);

$jogo->Atualizar($jogo);

if($jogo->erro=="")
{
   echo "<script>alert('Dados da jogo atualizados com sucesso.');".
           "window.location= '../jogoslista.php';</script>";
}
 else {
     ?>
        <script>alert("<?php echo $jogo->erro ?>");history.back();</script>
    <?php
    
}
    
?>


