<?php
session_start();
require_once "includes/autentica.php";
$cd_usuario = "";
if (isset($_GET["cd_usuario"]))
    $cd_usuario = $_GET["cd_usuario"];

require_once "../App_Code/Usuarios.php";
$usuario = new Usuarios();
$usuario->getUmItem($cd_usuario);
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
        <script>
            function limpaerros()
            {
                $(".erro").hide();
            }
            function validaformusuario()
            {
                limpaerros();
                var ok = true;
                $nm_usuario = formusuario.nm_usuario.value;
                $nm_login = formusuario.nm_login.value;
                $nm_email = formusuario.nm_email.value;
                if ($nm_usuario == "")
                {
                    $("#erro_nm_usuario").show();
                    ok = false;
                }
                if ($nm_email == "")
                {
                    $("#erro_nm_email1").show();
                    ok = false;
                }
                else
                {
                    if (!IsValidEmail($nm_email))
                    {
                        $("#erro_nm_email2").show();
                        ok = false;
                    }
                }


                return ok;
            }
            function IsValidEmail(email)
            {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                return filter.test(email);

            }
        </script>
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="usuarioslista.php">Usuarios</a> > <strong>Editar</strong>
                </nav>
                <form action="usuarioarqauxiliares/usuariocadastroeditar.php" method="post" name="formusuario" id="cadastro" onsubmit="return validaformusuario();">
                    <fieldset ><legend >Editar usuario</legend>
                        <ol>
                            <li>
                                <label>
                                    Cod:
                                </label>
                                <label class="Campos required"><?php echo $usuario->getCd_usuario() ?></label>
                                <input type="hidden" name="cd_usuario" value="<?php echo $usuario->getCd_usuario() ?>"/>

                            </li>
                            <li>
                                <label>
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_usuario" value="<?php echo $usuario->getNm_usuario(); ?>">
                                <label class="erro" id="erro_nm_usuario">
                                    Digite o nome.
                                </label>
                            </li>
                            <li>
                                <label>
                                    E-mail:
                                </label>
                                <input class="Campos required" name="nm_email" value="<?php echo $usuario->getNm_email(); ?>">
                                <label class="erro" id="erro_nm_email1">
                                    Digite o mail
                                </label>
                                <label class="erro" id="erro_nm_email2">
                                    E-mail inválido
                                </label>
                            </li>
                            <li>
                                <label>
                                    Login:
                                </label>
                                <input type="text" class="Campos required" disabled  value="<?php echo $usuario->getNm_login(); ?>" >
                                <input type="hidden"  name="nm_login" value="<?php echo $usuario->getNm_login(); ?>" >
                            </li>
                            <li>
                                <label>
                                    Nível:
                                </label>
                                <select name="qt_nivel"   class="Campos" >
                                    <option value="0" <?php if ($usuario->getQt_nivel() == 0) echo "selected"; ?>>
                                        Administrador
                                    </option>
                                    <option value="1" <?php if ($usuario->getQt_nivel() == 1) echo "selected"; ?>>
                                        Operador
                                    </option>
                                </select>
                            </li>
                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
