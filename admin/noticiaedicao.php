<?php
error_reporting(E_ALL);
session_start();

require_once "includes/autentica.php";
$cd_noticia = "";
if (isset($_GET["cd_noticia"]))
    $cd_noticia = $_GET["cd_noticia"];

require_once "../App_Code/Noticias.php";
$noticia = new Noticias();
$noticia->getUmItem($cd_noticia);
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
          <script src="ckeditor/ckeditor.js"></script>
        <style>
        .ckeditorcontainer{
            width:100%;float:left;clear:left;
        }
        </style>
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="noticiaslista.php">Noticias</a> > <strong>Editar</strong>
                </nav>
                <nav class="navabas">
                    <div class="aba" onclick="window.location = 'noticiaedicao.php?cd_noticia=<?php echo $cd_noticia ?>'">
                        Notícia
                    </div>
                    <div class="aba2" style="width: 120px;"
                         onclick="window.location = 'noticiaimagem1.php?cd_noticia=<?php echo $cd_noticia ?>'" >
                        Imagem Título
                    </div>
                    
                    <div class="aba2" style="width: 120px;"
                         onclick="window.location = 'noticiagaleriafotos.php?cd_noticia=<?php echo $cd_noticia ?>'" >
                        Galeria de Fotos
                    </div>
                    <div class="aba2" style="width: 120px;"
                         onclick="window.location = 'noticiacomentarios.php?cd_noticia=<?php echo $cd_noticia ?>'" >
                        Comentários
                    </div>                     
                </nav>
                <form action="noticiaarqauxiliares/noticiacadastroeditar.php" method="post" id="cadastro" onsubmit="return validaform();">
                    
                    <fieldset ><legend >Editar noticia</legend>
                        <ol>
                            <li>
                                <label>
                                    Cod:
                                </label>
                                <label class="Campos required"><?php echo $noticia->getCd_noticia() ?></label>
                                <label>
                                    Postado por:
                                </label>
                                <?php
                                    $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $noticia->getDt_noticia());
                                    $dt_noticia = $myDateTime->format('d/m/Y H:i:s');
                                ?>
                                <label class="Campos required" style="width:250px;"><?php echo $noticia->getNm_usuario() ?> em <?php echo $dt_noticia ?> </label>                               
                                <input type="hidden" name="cd_noticia" value="<?php echo $noticia->getCd_noticia() ?>"/>
                                <input type="hidden" name="nm_url_foto" value="<?php echo $noticia->getNm_url_foto() ?>"/>
                               <input type="hidden" name="cd_menu" value="1"/>  
                            </li>
                            <li>
                                <label>
                                    Titulo:
                                </label>
                                <input class="Campos required" name="nm_titulo" value="<?php echo $noticia->getNm_titulo(); ?>" maxlength="100">
                                <label>
                                    Ativo:</label>
                                  <?php
                                    if($noticia->getIc_ativo()==1)
                                    {
                                    ?>
                                      Sim&nbsp;<input type="radio" value="1" name="ic_ativo" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                      Não&nbsp;<input type="radio" value="0" name="ic_ativo"  />&nbsp;&nbsp;                         
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                      Sim&nbsp;<input type="radio" value="1" name="ic_ativo"  style="margin-top: 8px;"  />&nbsp;&nbsp;
                                      Não&nbsp;<input type="radio" value="0" name="ic_ativo" checked  />&nbsp;&nbsp;                         
                                    <?php
                                    }

                                ?>                                
                            </li>
                            <li>
                                <label>
                                    campeonato:
                                </label>
                                <select name="cd_campeonato" id="cd_campeonato"    class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from campeonatos";
                                    $rs = $mysqli->query($query);

                                    while ($row = $rs->fetch_assoc()) {
                                        
                                        if ($row ["cd_campeonato"] == $noticia->getCd_campeonato()) {
                                            echo "<option value=" . $row ["cd_campeonato"] . " selected>" . $row ["nm_campeonato"] . "</option>";
                                        } else
                                        {
                                          echo "<option value=" . $row ["cd_campeonato"] . ">" . $row ["nm_campeonato"] . "</option>";  
                                        }
                                    }
                                    $rs->free();
                                    ?>
  

                                ?>                                
                                </select>
                               <label>
                                    Mostra Carrossel:</label>
                                  <?php
                                    if($noticia->getIc_mostra_carrossel()==1)
                                    {
                                    ?>
                                      Sim&nbsp;<input type="radio" value="1" name="ic_mostra_carrossel" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                      Não&nbsp;<input type="radio" value="0" name="ic_mostra_carrossel"  />&nbsp;&nbsp;                         
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                      Sim&nbsp;<input type="radio" value="1" name="ic_mostra_carrossel"  style="margin-top: 8px;"  />&nbsp;&nbsp;
                                      Não&nbsp;<input type="radio" value="0" name="ic_mostra_carrossel" checked  />&nbsp;&nbsp;                         
                                    <?php
                                    }

                                ?>
                            </li>                            
<!--                             <li>
                                <label>
                                    Link Youtube:
                                </label>
                                <input class="Campos required" name="nm_url_youtube" value="<?php echo $noticia->getNm_url_youtube(); ?>" maxlength="40">
                            </li>                           
                            
                            <li>
                                <label>
                                    Menu:
                                </label>
                               <select name="cd_menu" id="cd_menu"  class="Campos" >
                                    <?php
                                        echo "cdmenu".$noticia->getCd_menu();
                                        require_once "../App_Code/Conexao.php";
                                        $conexao = new Conexao();
                                        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                        $mysqli->set_charset("utf8");
                                        $query = "select * from menus";
                                        $rs = $mysqli->query($query);
                                        
                                        while ($row = $rs->fetch_assoc()) {
                                            $selected ="";
                                            if($row ["cd_menu"] ==$noticia->getCd_menu() )
                                            {
                                              $selected = "selected";
                                            }
                                            echo "<option ".$selected." value=" . $row ["cd_menu"] . ">" . $row ["nm_menu"] . "</option>";
                                        }
                                        $rs->free();
                                        ?>
                                 </select>
                            </li>-->

                            <li>
                                <label>
                                    Conteúdo:
                                </label>

                                <div class="ckeditorcontainer">
                                    <textarea   id="editor1" name="ds_noticia" rows="10" cols="80" >
                                        <?php echo $noticia->getDs_noticia(); ?>
                                    </textarea>
                                </div>
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                    <script>
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('editor1');
                    </script>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
         <script>
             $("#colunaesquerda").height(950);
        </script>
    </body>
</html>
