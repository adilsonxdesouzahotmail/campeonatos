
<html >
<head>
    <title></title>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/estilologin.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript">
          function validaform()
          {
              var ok=true;
              var mensagem ="";
              var nm_login = formlogin.nm_login.value;
              var nm_senha = formlogin.nm_senha.value;
              
              if(nm_login=="")
              {
                  mensagem += "Digite seu login. \u000A";
                  ok=false;
              }
              if (nm_senha == "") {
                  mensagem += "Digite sua senha. \u000A";
                  ok = false;
              }
              if (!ok) {
                  alert(mensagem);
              }
              return ok;
           }
     </script>
</head>
<body>
    <div class="titadmin">
        <div class="titbordaesquerda">
        </div>
        <div class="titadmintexto">
            Administração</div>
        <div class="titbordadireita">
        </div>
    </div>
    <div class="boxprincipal">
        <div class="boxcentro">
            <h1>Login da Área Restrita</h1>
            <div class="texto">
                Use um nome de usuário e senha válidos para ter acesso ao painel de administração.
            </div>
            <a href="../index.php">Ir para a página inicial do site.</a>
            <div class="cadeado">
            </div>
            <div class="boxlogin">
            <form action="loginautentica.php" method="post" id="formlogin" onsubmit="return validaform();">
                <label>
                    Login</label>
                <input name="nm_login" />
                <label>
                    Senha</label>
                <input name="nm_senha" type="password"/>
                <input type="submit" class="btnentrar" value="Entrar" />
            </form>
            </div>
        
        </div>
       <footer class="rodape"></footer>
    </div>
</body>
</html>
