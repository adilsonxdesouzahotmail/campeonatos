<?php
session_start();

require_once "includes/autentica.php";
$cd_noticia_comentario = "";
if (isset($_GET["cd_noticia_comentario"]))
    $cd_noticia_comentario = $_GET["cd_noticia_comentario"];

require_once "../App_Code/NoticiasComentarios.php";
$noticiacomentario = new NoticiasComentarios();
$noticiacomentario->getUmItem($cd_noticia_comentario);
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
          <script src="ckeditor/ckeditor.js"></script>
        <style>
        .ckeditorcontainer{
            width:100%;float:left;clear:left;
        }
        #cke_1_contents
        {
            height:200px !important;
        }
        #cke_2_contents
        {
            height:200px !important;
        }
        </style>
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="noticiaslista.php">Noticias</a> > <strong>Editar</strong>
                </nav>
                <nav class="navabas">
                    <div class="aba2" onclick="window.location = 'noticiaedicao.php?cd_noticia=<?php echo $cd_noticia ?>'">
                        Notícia
                    </div>
                    <div class="aba2" style="width: 120px;"
                         onclick="window.location = 'noticiaimagem1.php?cd_noticia=<?php echo $cd_noticia ?>'" >
                        Imagem Título
                    </div>
                    
                    <div class="aba2" style="width: 120px;"
                         onclick="window.location = 'noticiagaleriafotos.php?cd_noticia=<?php echo $cd_noticia ?>'" >
                        Galeria de Fotos
                    </div>
                    <div class="aba" style="width: 120px;"
                         onclick="window.location = 'noticiacomentarios.php?cd_noticia=<?php echo $cd_noticia ?>'" >
                        Comentários
                    </div>                     
                </nav>
                <form action="noticiaarqauxiliares/noticiacadastroeditar.php" method="post" id="cadastro" onsubmit="return validaform();">
                    
                    <fieldset ><legend >Editar noticia</legend>
                        <ol>
                            <li>
                                <label style='font-weight:bold;'>
                                    Cod:
                                </label>
                                <label class="Campos required"><?php echo $noticiacomentario->getCd_noticia_comentario() ?></label>
                            </li>
                            <li>
                                <label style='font-weight:bold;'>
                                  Cliente:
                                </label>
                                <label class="Campos required"><?php echo $noticiacomentario->getNome_cliente() ?></label>
                                <?php
                                    $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $noticiacomentario->getDt_comentario());
                                    $dt_comentario = $myDateTime->format('d/m/Y H:i:s');
                                ?>
                                <label style='font-weight:bold;'>
                                  Postado em:
                                </label>
                                 <label class="Campos required"><?php echo $dt_comentario ?></label>
                            </li>
                            <li>    
                               <label style='font-weight:bold;'>Aprovado:</label>
                                <?php
                                    if($noticiacomentario->getIc_aprovado()==1)
                                    {
                                    ?>
                                      Sim&nbsp;<input type="radio" value="1" name="ic_aprovado" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                      Não&nbsp;<input type="radio" value="0" name="ic_aprovado"  />&nbsp;&nbsp;                         
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                      Sim&nbsp;<input type="radio" value="1" name="ic_aprovado"  style="margin-top: 8px;"  />&nbsp;&nbsp;
                                      Não&nbsp;<input type="radio" value="0" name="ic_aprovado" checked  />&nbsp;&nbsp;                         
                                    <?php
                                    }

                                ?>

                            </li>   
                            <li>
                                <label style='font-weight:bold;'>
                                    Comentário:
                                </label>

                                <div class="ckeditorcontainer">
                                    <textarea   id="editor1" name="ds_noticia" rows="5" cols="80" >
                                        <?php echo $noticiacomentario->getDs_comentario(); ?>
                                    </textarea>
                                </div>
                            </li>
                            <li>
                                <label style='font-weight:bold;'>
                                  Respondido por:
                                </label>
                                <?php
                                    $nm_usuario="Não Respondido";
                                    if($noticiacomentario->getNm_usuario()!=null && $noticiacomentario->getNm_usuario()!="")
                                    {
                                         $nm_usuario =  $noticiacomentario->getNm_usuario();
                                    }

                                ?>                                
                                <label class="Campos required"><?php echo $nm_usuario ?></label>
                                <?php
                                    $dt_resposta="";
                                    if($noticiacomentario->getDt_resposta()!=null && $noticiacomentario->getDt_resposta()!="")
                                    {
                                         $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $noticiacomentario->getDt_resposta());
                                         $dt_resposta = $myDateTime->format('d/m/Y H:i:s');
                                    }

                                ?>
                                <label style='font-weight:bold;'>
                                  Respondido em:
                                </label>
                                 <label class="Campos required"><?php echo $dt_resposta ?></label>
                            </li>
                            <li>
                                <label style='font-weight:bold;'>
                                    Resposta:
                                </label>

                                <div class="ckeditorcontainer">
                                    <textarea   id="editor2" name="ds_resposta" rows="5" cols="80" >
                                        <?php echo $noticiacomentario->getDs_resposta(); ?>
                                    </textarea>
                                </div>
                            </li>                            
                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                    <script>
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('editor1');
                        CKEDITOR.replace('editor2');
                    </script>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
         <script>
             $("#colunaesquerda").height(950);
        </script>
    </body>
</html>
