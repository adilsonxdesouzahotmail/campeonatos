<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();

$acao = "";
if (isset($_GET["acao"]))
    $acao = $_GET["acao"];


$cd_usuario = "";
if (isset($_GET["cd_usuario"]))
    $cd_usuario = $_GET["cd_usuario"];

require_once "../App_Code/Usuarios.php";
if ($acao == "excluir") {

    $usuario = new Usuarios();
    $usuario->Excluir($cd_usuario);

    if ($usuario->erro == "") {
        echo "<script>alert('Usuário excluído com sucesso.');window.location='usuarioslista.php?cd_usuario=" . $cd_usuario . "';</script>";
    } else {
        ?>
        <script >alert("<?php echo $usuario->erro ?>");
            window.location = "usuarioslista.php?cd_usuario="<?php echo $cd_usuario ?>;</script>
        <?php
    }
    // header("Location:usuariolista.php");
}
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <div style="width: 100%; float: left; clear: left;">

                    <?php
                    require_once "../App_Code/Usuarios.php";
                    $usuario = new Usuarios();
                    ?>
                    <nav class="breadcrumb">
                        <strong>Usuários</strong>
                    </nav>
                    <div class="contbtnnovo">
                        <input type="button" class="botaopadrao" value="NOVO USUÁRIO" onclick="window.location = 'usuariocadastro.php';" />
                    </div>
                    <table class="tbllistagem" id="tbllistagem" style="width:100%; float: left; clear: left;">
                        <tr>
                            <th style="width: 30px;">
                                Cod.
                            </th>
                            <th style="width: 50px; text-align: center;">
                                Nome
                            </th>
                            <th style="width: 50px; text-align: center;">
                                Login
                            </th>
                            <th style="width: 50px; text-align: center;">
                                Editar
                            </th>
                            <th style="width: 50px; text-align: center;">
                                Excluir
                            </th>
                        </tr>

                        <?php
                        $user = "";
                        if (isset($_SESSION["cd_usuario"]))
                            $user = $_SESSION["cd_usuario"];
                        $usuario->getTodosItens($user);
                        for ($i = 0; $i < count($usuario->meuarray); $i++) {
                            echo "<tr>";
                            echo "<td>" . $usuario->meuarray[$i]->getCd_usuario() . "</td>";
                            echo "<td>" . $usuario->meuarray[$i]->getNm_usuario() . "</td>";
                            echo "<td>" . $usuario->meuarray[$i]->getNm_login() . "</td>";
                            echo "<td align='center' >";
                            echo "<a href='usuarioedicao.php?cd_usuario=" . $usuario->meuarray[$i]->getCd_usuario() . "'><img src='imagens/lapis.png'/></a>";
                            echo "</td>";

                            echo "<td align='center'>";
                            ?>
                            <a href="usuarioslista.php?cd_usuario=<?php echo $usuario->meuarray[$i]->getCd_usuario() ?>&acao=excluir"
                               onclick="return confirm('Confirma a exclusão do usuário <?php echo $usuario->meuarray[$i]->getCd_usuario() ?>?');">
                                <img src='imagens/lixeira.png' /></a>
                            <?php
                            echo "</td>";
                            echo "</tr>";
                        }
                        ?>
                    </table>
                </div>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
