<?php
session_start();
error_reporting(E_ALL);
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
require_once "../App_Code/Clientes.php";
require_once "../App_Code/Pedidos.php";

$conexao = new Conexao();

$cliente = new Clientes();
$cd_cliente = "";
if (isset($_GET["cd_cliente"]))
    $cd_cliente = $_GET["cd_cliente"];
$cliente->getUmItem($cd_cliente);

$pedido = new Pedidos();

?>

<html>
<head >
    <?php include "includes/head2.php" ?> 
    
</head>
<body>
     <?php include "includes/topoelateral.php" ?> 
    <section id="colunadireita">
          <div class="container">
           
            <nav class="breadcrumb">
                <a href="clienteslista.php">Clientes</a> > <strong>Consultar Dados</strong>
            </nav>
            <nav class="navabas">
                <div class="aba2" onclick="window.location='clientedetalhe.php?cd_cliente=<?php echo $cd_cliente;?>';" >
                    Dados do cliente
                </div>
                  <div class="aba" >
                      Pedidos do cliente <?php echo $cliente->getNm_cliente(); ?>
                </div>
               
            </nav>
            <form id="cadastro"  name="formidentcliente"  >
                <?php 
                     $origem="";
                    if(isset($_GET["origem"]))
                       $origem = $_GET["origem"];
                ?>
                <input  class="Campos required"  disabled type="hidden" name="origem" value="<?php echo $origem; ?>"/>
                <fieldset ><legend style="margin-left:30px"> Pedidos do cliente c&oacute;d: <?php echo $cd_cliente ?></legend>
                   <table class="tbllistagem" id="tbllistagem" >
                    <tr>
                        <th style="width: 30px;">
                            C&oacute;d.
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Criado em 
                        </th>
                        <th style="width: 40px; text-align: center;">
                            &Uacute;lt. atualiza&ccedil;&atilde;o 
                        </th>
                        <th style="text-align: left;">
                           Cliente
                        </th>
                       <th style="text-align: center;">
                           Status
                        </th>
                        <th style="text-align: right;">
                           Valor
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Editar
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Excluir
                        </th>
                    </tr>
                    <?php
                    $subquery="";
                    if($cd_cliente !="")
                    {
                        $subquery.=" and pe.cd_cliente=$cd_cliente ";
                    }
                    $limit="";
                    $pedido->ListaTodosGridAdmin($limit, $subquery);
                    
                    for ($i = 0; $i < count($pedido->meuarray); $i++) {
                       
                        echo "<tr>";
                        echo "<td>" . $pedido->meuarray[$i]->getCd_pedido() . "</td>";
                        $data_criacao="";
                        $data_atualizacao="";
                        if ($pedido->meuarray[$i]->getDt_data_criacao() !="")
                            $data_criacao = date("d/m/Y", strtotime($pedido->meuarray[$i]->getDt_data_criacao()));
                        if ($pedido->meuarray[$i]->getDt_data_atualizacao() !="")
                            $data_atualizacao = date("d/m/Y", strtotime($pedido->meuarray[$i]->getDt_data_atualizacao()));
                        echo "<td align='center'>" .$data_criacao . "</td>";
                        echo "<td align='center'>" .$data_atualizacao . "</td>";
                        echo "<td style='padding:5px;'>" .$pedido->meuarray[$i]->getNm_cliente(). "</td>";
                        echo "<td align='center'>" .$pedido->meuarray[$i]->getNm_pedido_status(). "</td>";
                         echo "<td style='padding:5px;text-align:right;'>" ."R$ ".number_format($pedido->meuarray[$i]->getVl_total(), 2,',',''). "</td>";
                        echo"<td align='center'>";
                        echo "<a href='pedidoedicao.php?cd_pedido=" . $pedido->meuarray[$i]->getCd_pedido() . "'><img src='imagens/lapis.png'/></a>";
                        echo "</td>";

                        echo "<td align='center'>";
                        ?>
                            <a href="pedidoslista.php?cd_pedido=<?php echo $pedido->meuarray[$i]->getCd_pedido() ?>&acao=excluir"
                           onclick="return confirm('Confirma a exclus\u00E3o do Pedido <?php echo $pedido->meuarray[$i]->getCd_pedido() ?>?\u000A Aten\u00E7\u00E3o: imagens e outras informa\u00E7\u00F5es associadas ao pedido tamb\u00E9m ser\u0007\o excluidas.');">
                            <img src='imagens/lixeira.png' /></a>
                         <?php
                          echo "</td>";
                          echo "</tr>";
                        }
                        ?>
                </table>
                </fieldset>
           </form>
        </div>
    </section>
    <?php include "includes/rodape.php" ?> 
</body>
</html>
