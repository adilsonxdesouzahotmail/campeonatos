﻿<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Banners.php";
require_once "../App_Code/Conexao.php";
require_once "../App_Code/Config.php";
$conexao = new Conexao();

$banner = new Banners();
$acao = "";
if (isset($_GET["acao"]))
    $acao = $_GET["acao"];

$cd_roduto = "";
if (isset($_GET["cd_banner"]))
    $cd_banner = $_GET["cd_banner"];


if ($acao == "excluir") {
    $banner->Excluir($cd_banner);
    if ($banner->erro == "")
        echo "<script> alert('Banner exclu\u00CDdo com sucesso.'); window.location = 'bannerslista.php';</script>";
    else {
        ?>
        <script >                alert("<?php echo $banner->erro ?>");
            window.location = 'bannerslista.php';</script>
        <?php
    }
}
?>

<html >
    <head>
        <?php include "includes/head2.php" ?> 
          <script type="text/javascript">
            function carregasubcategorias()
            {
                var url = "subcategoriaarqauxiliares/carregasubcategorias.php";
                formdata = "cd_categoria=" + $("#cd_categoria").val();
                $.ajax({
                    url: url,
                    data: formdata,
                    type: 'POST',
                    dataType: 'html',
                    success: function(data) {
                        $("#cd_subcategoria").html("<option value='0'>Selecione</option>" + data);
                    }
                });

            }
        </script>
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <strong>Banners</strong>
                </nav>
                <div class="contbtnnovo">
                    <input type="button" class="botaopadrao" value="Novo Banner" onclick="window.location = 'bannercadastro.php';" />
                </div>

                <!------------Busca inicio----------->
                <form action="" method="get"
                      onsubmit="return validaformbanners();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Busca</legend>
                        <ol>
                            <li>
                                <label style="width:95px;">
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_banner" id="nm_banner" >
                                <label>
                                    Destaque:</label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_destaque"  style="margin-top: 8px;" />&nbsp;&nbsp;
                                N&atilde;o&nbsp;<input type="radio" value="0" name="ic_destaque" />&nbsp;&nbsp; 
                            </li>
                            Todos&nbsp;<input type="radio" value="3" name="ic_destaque" checked style="margin-top: 8px;"/>&nbsp;&nbsp;
                            </li>
                            <li>
                                <label style="width:95px;">
                                    Categoria:
                                </label>
                                <select name="cd_categoria" id="cd_categoria" onchange="carregasubcategorias();" class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from banners";
                                    $rs = $mysqli->query($query);

                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_categoria"] . ">" . $row ["nm_categoria"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                                <label>
                                    Subcategoria:
                                </label>
                                <select name="cd_subcategoria" id="cd_subcategoria" class="Campos">
                                    <option value="0">Selecione</option>
                                </select>
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                </form>
                <!------------Busca Fim-------------->




                <table class="tbllistagem" id="tbllistagem" >
                    <tr>
                        <th style="width: 30px;">
                            C&oacute;d.
                        </th>
                        <th style="width: 200px;">
                            Banner
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Imagem
                        </th>
                       
                        <th style="width: 40px; text-align: center;">
                            Editar
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Excluir
                        </th>
                    </tr>
               <?php
                        require_once "../App_Code/Conexao.php";
                        $conexao = new Conexao();
                        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                        $mysqli->set_charset("utf8");
                        $query = "select * from banners";
                        $rs = $mysqli->query($query);

                        while ($row = $rs->fetch_assoc()) {
                            echo "<tr>";
                            echo "<td>".$row ["cd_banner"]."</td>";
                            echo "<td>".$row ["nm_campeonato"]."</td>";
                            echo "<td>";
                              if($row ["nm_url_foto"] != "")
                              {
                                  echo "<img src='../banners/".$row ["nm_url_foto"]."' />";
                              }
                              else
                              {
                                  echo "<img src='../imagens/semfotologo.jpg' />";
                              }
                            echo "</td>";
                        echo"<td align='center'>";
                        echo "<a href='banneredicao.php?cd_banner=" . $banner->meuarray[$i]->getCd_banner() . "'><img src='imagens/lapis.png'/></a>";
                        echo "</td>";

                        echo "<td align='center'>";
                        ?>
                        <a href="bannerslista.php?cd_banner=<?php echo $banner->meuarray[$i]->getCd_banner() ?>&acao=excluir"
                           onclick="return confirm('Confirma a exclus\u00E3o do Banner <?php echo $banner->meuarray[$i]->getNm_banner() ?>?\u000A Aten\u00E7\u00E3o: imagens e outras informa\u00E7\u00F5es associadas ao banner tamb\u00E9m ser\u0007\o excluidas.');">
                            <img src='imagens/lixeira.png' /></a>
                        <?php
                          echo "</td>";
                          echo "</tr>";
                          
                        }
                        $rs->free();
                    ?>

                </table>
           </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
