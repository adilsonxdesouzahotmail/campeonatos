<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();

$acao = "";
if (isset($_GET["acao"]))
    $acao = $_GET["acao"];


$cd_pagina = "";
if (isset($_GET["cd_pagina"]))
    $cd_pagina = $_GET["cd_pagina"];

require_once "../App_Code/Paginas.php";
if ($acao == "excluir") {
    
    $pagina = new Paginas();
    $pagina->Excluir($cd_pagina);

    if ($pagina->erro == "")
    {
        echo "<script>alert('Página excluída com sucesso.');window.location='paginaslista.php?cd_pagina=".$cd_pagina."';</script>";
    }
    else {
        ?>
        <script >alert("<?php echo $pagina->erro ?>");window.location = "paginaslista.php?cd_pagina="<?php echo $cd_pagina ?>;</script>
        <?php
    }
    // header("Location:paginalista.php");
}
?>

<html >
   <?php include "includes/head.php" ?>
</head>
<body>
    <?php include "includes/topo.php" ?>
    <div class="container">
        <div style="width: 100%; float: left; clear: left;">
            
            <?php 
                require_once "../App_Code/Paginas.php";
                $pagina = new Paginas();
               
                
            ?>
            <nav class="breadcrumb">
                <strong>Páginas</strong>
            </nav>
             <div class="contbtnnovo">
                <input type="button" class="botaopadrao" value="NOVA PÁGINA" onclick="window.location = 'paginacadastro.php';" />
            </div>
            <table class="tbllistagem" id="tbllistagem" style="width:100%; float: left; clear: left;">
                <tr>
                    <th style="width: 30px;">
                        Cod.
                    </th>
                   <th style="width: 50px; text-align: center;">
                        Título
                    </th>
                    <th style="width: 50px; text-align: center;">
                        Menu
                    </th>
                    <th style="width: 50px; text-align: center;">
                        Editar
                    </th>
                    <th style="width: 50px; text-align: center;">
                        Excluir
                    </th>
                </tr>

                <?php
                $pagina->getTodosItens();
                 for($i=0;$i< count($pagina->meuarray);$i++)
                 {
                    echo "<tr>";
                    echo "<td>".$pagina->meuarray[$i]->getCd_pagina()."</td>";
                    echo "<td>".$pagina->meuarray[$i]->getNm_titulo()."</td>";
                    echo "<td>".$pagina->meuarray[$i]->getNm_menu()."</td>";
                    echo "<td align='center' >";
                    echo "<a href='paginaedicao.php?cd_pagina=".$pagina->meuarray[$i]->getCd_pagina(). "'><img src='imagens/lapis.png'/></a>";
                    echo "</td>";

                    echo "<td align='center'>";
                    ?>
                    <a href="paginaslista.php?cd_pagina=<?php echo $pagina->meuarray[$i]->getCd_pagina() ?>&acao=excluir"
                       onclick="return confirm('Confirma a exclusão da pagina <?php echo $pagina->meuarray[$i]->getCd_pagina() ?>?');">
                        <img src='imagens/lixeira.png' /></a>
                    <?php
                echo "</td>";
                echo "</tr>";
                          
                 }
            ?>


            </table>

        </div>
       
    </div>
    <?php 
         include "includes/rodape.php" ;
    ?>
</body>
</html>
