<?php 
    session_start();
    require_once "includes/autentica.php";
     $cd_categoria = "";
    if (isset($_GET["cd_categoria"]))
        $cd_categoria = $_GET["cd_categoria"];
    require_once "../App_Code/Categorias.php";
    $categoria = new Categorias();
    $categoria->getUmItem($cd_categoria);
    
     $cd_subcategoria="";
     if(isset($_GET["cd_subcategoria"]))
         $cd_subcategoria=$_GET["cd_subcategoria"];
     
    require_once "../App_Code/Subcategorias.php";
    $subcategoria = new Subcategorias();
    $subcategoria->getUmItem($cd_subcategoria);

?>

<html >
    <?php include "includes/head.php" ?>
</head>
<body>
    <?php include "includes/topo.php" ?>
    <div class="container">
        <nav class="breadcrumb">
                <a href="categoriaslista.php">Categorias</a> > 
                <a href="subcategoriaslista.php?cd_categoria=<?php echo $cd_categoria?>">
                    Subcategorias de <?php echo $categoria->getNm_categoria(); ?></a> > <strong>Editar </strong>
           </nav>
        <form action="subcategoriaarqauxiliares/subcategoriacadastroeditar.php" method="post" id="cadastro" onsubmit="return validaform();">
        <fieldset ><legend >Editar subcategoria</legend>
             <ol>
            <li>
                <label>
                   Cod:
                </label>
                <label class="Campos required"><?php echo $subcategoria->getCd_subcategoria() ?></label>
                    <input type="hidden" name="cd_subcategoria" value="<?php echo $subcategoria->getCd_subcategoria() ?>"/>
                    <input type="hidden" name="cd_categoria" value="<?php echo $subcategoria->getCd_categoria() ?>"/>
            </li>
            <li>
                <label>
                    Subcategoria:
                </label>
                <input class="Campos required" name="nm_subcategoria" value="<?php echo $subcategoria->getNm_subcategoria(); ?>">
            </li>
           
            <li style="width:100%;">
                <input type="submit" class="btnenviar" value="Enviar" />
              </li>
        </ol>
        </fieldset>
        </form>
    </div>
    <?php 
         include "includes/rodape.php" ;
    ?>
</body>
</html>
