<?php
    session_start();
    require_once "includes/autentica.php";
     require_once "../App_Code/Paginas.php";
     $pagina = new Paginas();
 ?>

<html >
    <?php include "includes/head.php" ?>
    <script src="ckeditor/ckeditor.js"></script>
    <style>
    .ckeditorcontainer{
        width:800px;float:left;clear:left;
        
    }
   
    </style>
</head>
<body>
    <?php include "includes/topo.php" ?>
    <div class="container">
        <nav class="breadcrumb">
                <a href="paginaslista.php">Páginas</a> > <strong>Cadastrar</strong>
        </nav>
        <form action="paginaarqauxiliares/paginacadastroinserir.php" method="post" id="cadastro" onsubmit="return validaform();">
        <fieldset ><legend >Cadastrar nova pagina</legend>
        <ol>
            
            <li>
                <label>
                    Titulo:
                </label>
                <input class="Campos required" name="nm_titulo">
            </li>
             <li>
                <label>
                    Menu:
                </label>
                <input class="Campos required" name="nm_menu">
            </li>
            
              <li>
                <label>
                    Conteúdo:
                </label>
<!--               <textarea id="txtdescricao" rows="3" name="ds_pagina"></textarea>-->
                  <div class="ckeditorcontainer">
                      <textarea   id="editor1" name="ds_pagina" rows="10" cols="80" >
                    
                      </textarea>
                  </div>

            </li>
            
            <li style="width:100%;">
                <input type="submit" class="btnenviar" value="Enviar" />
              </li>
        </ol>
        </fieldset>
             <script>
                    // Replace the <textarea id="editor1"> with a CKEditor
                    // instance, using default configuration.
                    CKEDITOR.replace('editor1');
            </script>
        </form>
    </div>
    <?php include "includes/rodape.php" ?>
</body>
</html>
