<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();
require_once "../App_Code/Produtos.php";
$produto = new Produtos();
$cd_produto = "";
if (isset($_GET["cd_produto"]))
    $cd_produto = $_GET["cd_produto"];

$produto->getUmItem($cd_produto);
?>
<html>
<head>
    <?php include "includes/head.php" ?>
        <script type="text/javascript">
            
            
            $(document).ready(function() {
                carregasubcategorias($("#hdcd_subcategoria").val());
            });
            function carregasubcategorias($cd_subcategoria)
            {
                $cd_categoria = $("#cd_categoria").val();
                var url = "subcategoriaarqauxiliares/carregasubcategorias.php";
                formdata = "cd_categoria=" + $cd_categoria + "&cd_subcategoria=" + $cd_subcategoria
                $.ajax({
                    url: url,
                    data: formdata,
                    type: 'POST',
                    dataType: 'html',
                    success: function(data) {
                        $("#cd_subcategoria").html("<option value='0'>Selecione</option>" + data);
                    }
                });

            }
        </script>
    </head>
    <body>
         <?php include "includes/topo.php" ?>
        <div class="container">
           
            <nav class="breadcrumb">
                <a href="produtoslista.php">Produtos</a> > <strong>Editar</strong>
            </nav>
            <nav class="navabas">
                <div class="aba" onclick="window.location = 'produtoedicao.php?cd_produto=<?php echo $cd_produto ?>'">
                    Dados
                </div>
                <div class="aba2" onclick="window.location = 'produtoimagem1.php?cd_produto=<?php echo $cd_produto ?>'" >
                    Imagem 1
                </div>
                <div class="aba2" onclick="window.location = 'produtoimagem2.php?cd_produto=<?php echo $cd_produto ?>'">
                    Imagem 2
                </div>
                <div class="aba2" style="width: 200px;"
                     onclick="window.location = 'produtogaleriafotos.php?cd_produto=<?php echo $cd_produto ?>'" >
                    Galeria de Fotos
                </div>
            </nav>
            <form action="produtoarqauxiliares/produtocadastroeditar.php" method="post" id="cadastro">
              
                <fieldset ><legend style="margin-left:30px"> Editar dados do produto</legend>
                <ol>
                    <li>
                        <label>
                            Cód:
                        </label>
                        <input class="Campos required" disabled name="cd_produto" value="<?php echo $produto->getCd_produto() ?>" >
                        <input type="hidden" name="cd_produto" value="<?php echo $produto->getCd_produto() ?>" >
                    </li> 

                    <li>
                        <label>
                            Nome:
                        </label>
                        <input class="Campos required" name="nm_produto" value="<?php echo $produto->getNm_produto() ?>" >
                            <label>
                                Ativo:</label>
                            Sim&nbsp;<input type="radio" <?php if($produto->getIc_ativo()==1) echo "checked"; ?> value="1" name="ic_ativo" style="margin-top: 8px;" />&nbsp;&nbsp;
                            Não&nbsp;<input type="radio" <?php if($produto->getIc_ativo()==0) echo "checked"; ?> value="0" name="ic_ativo" />&nbsp;&nbsp; </li>
                    <li>
                        <label>
                            Categoria:
                        </label>
                        <select name="cd_categoria" id="cd_categoria" onchange="carregasubcategorias();" class="Campos" >
                            <option value="0">Selecione</option>
                            <?php
                            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                            $mysqli->set_charset("utf8");
                            $query = "select * from categorias";
                            $rs = $mysqli->query($query);


                            while ($row = $rs->fetch_assoc()) {
                                if ($row ["cd_categoria"] == $produto->getCd_categoria()) {
                                    echo "<option value=" . $row ["cd_categoria"] . " selected>" . $row ["nm_categoria"] . "</option>";
                                } else
                                    echo "<option value=" . $row ["cd_categoria"] . ">" . $row ["nm_categoria"] . "</option>";
                            }
                            $rs->free();
                            ?>
                        </select>
                        <label>
                            Subcategoria:
                            <input type="hidden" id="hdcd_subcategoria"  value="<?php echo $produto->getCd_subcategoria(); ?>"/>
                        </label>
                        <select name="cd_subcategoria" id="cd_subcategoria" class="Campos">
                            <option value="0">Selecione</option>
                        </select>
                    </li>
                    <li>
                        <label>
                            Tamanhos:</label>
                        <table class="tbltamanhos">
                            <?php
                            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                            $mysqli->set_charset("utf8");
                            $query = "select * from tamanhos";
                            $rs = $mysqli->query($query);
                            
                            require_once "../App_Code/Tamanhos_produto.php";
                            $tamanhoprod = new Tamanhos_produto();
                            $cont = 1;
                            echo "<tr>";
                            while ($row = $rs->fetch_assoc()) {
                                echo "<td>";
                               if($tamanhoprod->temTamanho($produto->getCd_produto(), $row ["cd_tamanho"])>0)
                               {
                                    echo "<input type='checkbox' checked  value='" . $row ["cd_tamanho"] . "' name='cd_tamanho[]' />&nbsp;" . $row ["nm_tamanho"] . "&nbsp;";
                               }
                                else {
                                    echo "<input type='checkbox' value='" . $row ["cd_tamanho"] . "' name='cd_tamanho[]' />&nbsp;" . $row ["nm_tamanho"] . "&nbsp;";
                                }
                                echo "</td>";
                                if ($cont % 5 == 0)
                                    echo "</tr><tr>";
                                $cont++;
                            }
                            echo "</tr>";
                            $rs->free();
                            ?>
                        </table>
                    </li>
                    <li>
                        <label>
                            Campo 1:
                        </label>
                        <input class="Campos required" name="nm_campo_extra_1" value="<?php echo $produto->getNm_campo_extra_1() ?>">
                    </li>
                    <li>
                        <label>
                            Campo 2:
                        </label>
                        <input class="Campos required" name="nm_campo_extra_2" value="<?php echo $produto->getNm_campo_extra_2() ?>">
                    </li>
                    <li>
                        <label>
                            Características:</label>
                        <table class="tblcaracteristicas">
                            <?php
                            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                            $mysqli->set_charset("utf8");
                            $query = "select * from caracteristicas";
                            $rs = $mysqli->query($query);
                            
                            require_once "../App_Code/Caracteristicas_produto.php";
                            $caracprod = new Caracteristicas_produto();
                            $cont = 1;
                            echo "<tr>";
                            while ($row = $rs->fetch_assoc()) {
                                echo "<td>";
                               if($caracprod->temCarac($produto->getCd_produto(), $row ["cd_caracteristica"])>0)
                               {
                                    echo "<input type='checkbox' checked  value='" . $row ["cd_caracteristica"] . "' name='cd_caracteristica[]' />&nbsp;" . $row ["nm_caracteristica"] . "&nbsp;";
                               }
                                else {
                                    echo "<input type='checkbox' value='" . $row ["cd_caracteristica"] . "' name='cd_caracteristica[]' />&nbsp;" . $row ["nm_caracteristica"] . "&nbsp;";
                                }
                                echo "</td>";
                                if ($cont % 5 == 0)
                                    echo "</tr><tr>";
                                $cont++;
                            }
                            echo "</tr>";
                            $rs->free();
                            ?>
                        </table>
                    </li>
                    <li >
                        <label>
                            Descrição:</label>
                        <textarea id="txtdescricao" rows="5" class="Campos required" name="ds_produto"><?php echo $produto->getDs_produto() ?></textarea>
                    </li>
                    <li>
                        <label>
                            Valor de:
                           
                        </label>
                        <input class="Campos required" name="vl_de" value="<?php echo "R$ ".number_format($produto->getVl_de(), 2,',','') ?>">
                    </li>
                    <li>
                        <label>
                            Valor por:
                        </label>
                        <input class="Campos required" name="vl_por" value="<?php echo "R$ ".number_format($produto->getVl_por(), 2,',','') ?>">
                    </li>
                    <li>
                        <label>
                            Destaque:
                        </label>
                        Sim&nbsp;<input type="radio" value="1" name="ic_destaque" <?php if($produto->getIc_destaque()==1) echo "checked"; ?> style="margin-top: 8px;" />&nbsp;&nbsp;
                        Não&nbsp;<input type="radio" value="0" name="ic_destaque" <?php if($produto->getIc_destaque()==0) echo "checked"; ?> />&nbsp;&nbsp; 
                    </li>
                    <li>
                        <label>
                            Oferta:
                        </label>
                        Sim&nbsp;<input type="radio" value="1" name="ic_oferta" <?php if($produto->getIc_oferta()==1) echo "checked"; ?> style="margin-top: 8px;" />&nbsp;&nbsp;
                        Não&nbsp;<input type="radio" value="0" name="ic_oferta" <?php if($produto->getIc_oferta()==0) echo "checked"; ?> />&nbsp;&nbsp; 
                    </li>
                    <li style="width:100%;">
                        <input type="submit" class="btnenviar" value="Enviar" />
                    </li>
                </ol>
                </fieldset>
            </form>
        </div>
        <?php include "includes/rodape.php" ; ?>
    </body>
</html>
