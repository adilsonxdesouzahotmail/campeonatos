<?php
    session_start();
 ?>
﻿
<html >
<head>
   <?php include "includes/head.php" ?>
</head>
<body>
    <?php include "includes/topo.php" ?>
    <div class="container">
       
       <?php 
            $cd_categoria = "";
            if (isset($_GET["cd_categoria"]))
                $cd_categoria = $_GET["cd_categoria"];
            require_once "../App_Code/Categorias.php";
            $categoria = new Categorias();
            $categoria->getUmItem($cd_categoria);
            
            require_once "../App_Code/Subcategorias.php";
            $subcategoria = new Subcategorias();
            $subcategoria->proxCdSubcategoria();
                
        ?>
         <nav class="breadcrumb">
                <a href="categoriaslista.php">Categorias</a> > 
                <a href="subcategoriaslista.php?cd_categoria=<?php echo $cd_categoria?>">
                    Subcategorias de <?php echo $categoria->getNm_categoria(); ?></a> > <strong>Cadastrar </strong>
           </nav>
        <form action="subcategoriaarqauxiliares/subcategoriacadastroinserir.php" method="post" id="cadastro" onsubmit="return validaform();">
      
        <fieldset ><legend >Cadastrar nova subcategoria</legend>
             <ol>
            <li>
                <label>
                   Cod:
                </label>
                <label class="Campos required"><?php echo $subcategoria->getProx_cd_subcategoria() ?></label>
                   
            </li>
            <li>
                <label>
                    Subcategoria:
                </label>
                <input class="Campos required" name="nm_subcategoria">
                <input type="hidden" name="cd_categoria" value="<?php echo $cd_categoria ?>" />
            </li>
            
            <li style="width:100%;">
                <input type="submit" class="btnenviar" value="Enviar" />
              </li>
        </ol>
        </fildset>
        </form>
    </div>
  <?php 
             include "includes/rodape.php" ;
   ?>
</body>
</html>
