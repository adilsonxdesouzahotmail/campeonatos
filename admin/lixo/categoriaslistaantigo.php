<?php
    session_start();
    require_once "includes/autentica.php";
     require_once "../App_Code/Conexao.php";
     $conexao= new Conexao();
    
    if(isset($_GET["acao"]))
           $acao = $_GET["acao"];
       else
            $acao ="";
     if(isset($_GET["cd_categoria"]))
           $cd_categoria = $_GET["cd_categoria"];
       else
            $cd_categoria ="";
     if($acao=="excluir") 
    {
         require_once "../App_Code/Categorias.php";
         $categoria = new Categorias();
         $categoria->Excluir($cd_categoria);
       
         if($categoria->erro =="")
             echo "<script>alert('Categoria excluída com sucesso.');window.location='categoriaslista.php';</script>";
         else {
             ?>
                <script >alert("<?php echo $categoria->erro ?>");window.location="categoriaslista.php";</script>
             <?php
         }
        // header("Location:categorialista.php");
    }
?>

<html>
  <?php include "includes/head.php" ?>
</head>
<body>
    <?php include "includes/topo.php" ?>
    <div class="container">
        <nav class="breadcrumb">
                <strong>Categorias</strong>
         </nav>
        <div style="width: 100%; float: left; clear: left;">
           
             <div class="contbtnnovo">
                
                 <input type="button" class="botaopadrao" value="NOVA CATEGORIA" onclick="window.location='categoriacadastro.php';" />
            </div>
            <table class="tbllistagem" id="tbllistagem" style="width:100%; float: left; clear: left;">
                <tr>
                    <th style="width: 30px;">
                        Cod.
                    </th>
                    <th style="width: 200px;">
                        Categoria
                    </th>
                   
                    <th style="width: 50px; text-align: center;">
                        Subcategorias
                    </th>
                    <th style="width: 50px; text-align: center;">
                        Editar
                    </th>
                    <th style="width: 50px; text-align: center;">
                        Excluir
                    </th>
                </tr>

              <?php 
             
		 $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(),$conexao->getNm_bd());
                  $mysqli->set_charset("utf8");
                $query="select * from categorias";
		$rs = $mysqli->query ($query );
		
		
		while ( $row = $rs->fetch_assoc () ) {
                     echo "<tr><td >";
                        echo $row ["cd_categoria"];
                        echo "<input type='hidden' name= 'cd_categoria' id='hd_cd_categoria' value='".$row ["cd_categoria"]."'/>";
                        echo "</td>";
                        
                        echo "<td >";
                        echo $row ["nm_categoria"];
                        echo "</td>";
                        
                       
                         
                          echo "<td align='center' >";
                         echo "<a href='subcategoriaslista.php?cd_categoria=".$row ["cd_categoria"]."'><img src='imagens/subitens.png'/></a>";
                          echo "</td>";
                          
                         echo "<td align='center' >";
                         echo "<a href='categoriaedicao.php?cd_categoria=".$row ["cd_categoria"]."'><img src='imagens/lapis.png'/></a>";
                          echo "</td>";
                         
                          echo "<td align='center'>";
                        ?>
                             <a href="categoriaslista.php?cd_categoria=<?php echo $row["cd_categoria"]?>&acao=excluir"
                                onclick="return confirm('Confirma a exclusão da categoria <?php echo $row["nm_categoria"]?>?');">
                            <img src='imagens/lixeira.png' /></a>
                        <?php
                        echo "</td>";
                        echo "</tr>";
                         
		}
		$rs->close ();
		$mysqli->close ();
              ?>
        
               
            </table>
          
        </div>
        
    </div>
   <?php include "includes/rodape.php" ?>
</body>
</html>
