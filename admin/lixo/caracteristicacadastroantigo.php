<?php
session_start();
require_once "includes/autentica.php";
?>

<html >
<head>
    <?php include "includes/head.php" ?>
</head>
<body>
     <?php include "includes/topo.php" ?>
    <div class="container">
          <nav class="breadcrumb">
                <a href="caracteristicaslista.php">Características</a> > <strong>Cadastrar</strong>
          </nav>
        <form action="caracteristicacadastroinserir.php" method="post" id="cadastro">
       <fieldset ><legend >Cadastrar nova característica</legend>
        <ol>
            
            <li>
                <label>
                    Característica:
                </label>
                <input class="Campos required" name="nm_caracteristica">
            </li>
           
             <li style="width:100%;">
                <input type="submit" class="btnenviar" value="Enviar" />
              </li>
        </ol>
          </fieldset>
        </form>
          
       
    </div>
     <?php include "includes/rodape.php" ?>
</body>
</html>