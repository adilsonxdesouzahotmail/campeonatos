<?php 
    session_start();
    require_once "includes/autentica.php";
     $cd_pagina="";
     if(isset($_GET["cd_pagina"]))
         $cd_pagina=$_GET["cd_pagina"];
     
    require_once "../App_Code/Paginas.php";
    $pagina = new Paginas();
    $pagina->getUmItem($cd_pagina);

?>

<html>
    <?php include "includes/head.php" ?>
     <script src="ckeditor/ckeditor.js"></script>
    <style>
    .ckeditorcontainer{
        width:800px;float:left;clear:left;
       
    }
   
    </style>
</head>
<body>
    <?php include "includes/topo.php" ?>
    <div class="container">
         <nav class="breadcrumb">
                <a href="paginaslista.php">Paginas</a> > <strong>Editar</strong>
         </nav>
       <form action="paginaarqauxiliares/paginacadastroeditar.php" method="post" id="cadastro" onsubmit="return validaform();">
        <fieldset ><legend >Editar pagina</legend>
        <ol>
            <li>
                <label>
                   Cod:
                </label>
                <label class="Campos required"><?php echo $pagina->getCd_pagina() ?></label>
                    <input type="hidden" name="cd_pagina" value="<?php echo $pagina->getCd_pagina() ?>"/>
               
            </li>
            <li>
                <label>
                    Titulo:
                </label>
                <input class="Campos required" name="nm_titulo" value="<?php echo $pagina->getNm_titulo(); ?>">
            </li>
             <li>
                <label>
                    Menu:
                </label>
                <input class="Campos required" name="nm_menu" value="<?php echo $pagina->getNm_menu(); ?>">
            </li>
            
              <li>
                <label>
                    Conteúdo:
                </label>
                
                  <div class="ckeditorcontainer">
                      <textarea   id="editor1" name="ds_pagina" rows="10" cols="80" >
                        <?php echo $pagina->getDs_pagina(); ?>
                      </textarea>
                  </div>
            </li>
   
            <li style="width:100%;">
                <input type="submit" class="btnenviar" value="Enviar" />
              </li>
        </ol>
         </fieldset>
            <script>
                    // Replace the <textarea id="editor1"> with a CKEditor
                    // instance, using default configuration.
                    CKEDITOR.replace('editor1');
            </script>
        </form>
    </div>
    <?php include "includes/rodape.php" ?>
</body>
</html>
