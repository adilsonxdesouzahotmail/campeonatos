<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();

$acao = "";
if (isset($_GET["acao"]))
    $acao = $_GET["acao"];

$cd_categoria = "";
if (isset($_GET["cd_categoria"]))
    $cd_categoria = $_GET["cd_categoria"];


$cd_subcategoria = "";
if (isset($_GET["cd_subcategoria"]))
    $cd_subcategoria = $_GET["cd_subcategoria"];


if ($acao == "excluir") {
    require_once "../App_Code/Subcategorias.php";
    $subcategoria = new Subcategorias();
    $subcategoria->Excluir($cd_subcategoria);

    if ($subcategoria->erro == "") {
        echo "<script>alert('Subcategoria excluída com sucesso.');window.location='subcategoriaslista.php?cd_categoria=" . $cd_categoria . "';</script>";
    } else {
        ?>
        <script >alert("<?php echo $subcategoria->erro ?>");
            window.location = "subcategoriaslista.php?cd_categoria="<?php echo $cd_categoria ?>;</script>
        <?php
    }
    // header("Location:categorialista.php");
}
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <div style="width: 100%; float: left; clear: left;">

                    <?php
                    require_once "../App_Code/Categorias.php";
                    $categoria = new Categorias();
                    $categoria->getUmItem($cd_categoria);
                    ?>
                    <nav class="breadcrumb">
                        <a href="categoriaslista.php">Categorias</a> > <strong>Subcategorias de <?php echo $categoria->getNm_categoria(); ?>
                        </strong>
                    </nav>
                    <div class="contbtnnovo">
                        <input type="button" class="botaopadrao" value="NOVA SUBCATEGORIA" onclick="window.location = 'subcategoriacadastro.php?cd_categoria=<?php echo $cd_categoria ?>';" />
                    </div>
                    <table class="tbllistagem" id="tbllistagem" style="width:100%; float: left; clear: left;">
                        <tr>
                            <th style="width: 30px;">
                                Cod.
                            </th>
                            <th style="width: 50px; text-align: center;">
                                Subcategoria
                            </th>
                            <th style="width: 50px; text-align: center;">
                                Editar
                            </th>
                            <th style="width: 50px; text-align: center;">
                                Excluir
                            </th>
                        </tr>

                        <?php
                        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                        $mysqli->set_charset("utf8");
                        $query = "select * from subcategorias where cd_categoria=" . $cd_categoria;

                        $rs = $mysqli->query($query);
                        while ($row = $rs->fetch_assoc()) {
                            echo "<tr><td >";
                            echo $row ["cd_subcategoria"];
                            echo "<input type='hidden' name= 'cd_subcategoria' id='hd_cd_categoria' value='" . $row ["cd_subcategoria"] . "'/>";
                            echo "</td>";

                            echo "<td >";
                            echo $row ["nm_subcategoria"];
                            echo "</td>";


                            echo "<td align='center' >";
                            echo "<a href='subcategoriaedicao.php?cd_categoria=" . $cd_categoria . "&cd_subcategoria=" . $row ["cd_subcategoria"] . "'><img src='imagens/lapis.png'/></a>";
                            echo "</td>";

                            echo "<td align='center'>";
                            ?>
                            <a href="subcategoriaslista.php?cd_subcategoria=<?php echo $row["cd_subcategoria"] ?>&cd_categoria=<?php echo $row["cd_categoria"] ?>&acao=excluir"
                               onclick="return confirm('Confirma a exclusão da subcategoria <?php echo $row["nm_subcategoria"] ?>?');">
                                <img src='imagens/lixeira.png' /></a>
                            <?php
                            echo "</td>";
                            echo "</tr>";
                        }
                        $rs->close();
                        $mysqli->close();
                        ?>
                    </table>
                </div>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
