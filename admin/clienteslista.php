﻿<?php
session_start();
error_reporting(E_ALL);
require_once "includes/autentica.php";
require_once "../App_Code/Clientes.php";
require_once "../App_Code/Conexao.php";
require_once "../App_Code/Config.php";
$conexao = new Conexao();

$cliente = new Clientes();
$acao = "";
if (isset($_GET["acao"]))
    $acao = $_GET["acao"];

$cd_cliente = "";
if (isset($_GET["cd_cliente"]))
    $cd_cliente = $_GET["cd_cliente"];

$nm_cliente = "";
if (isset($_GET["nm_cliente"]))
    $nm_cliente = $_GET["nm_cliente"];


if ($acao == "excluir") {
    $cliente->Excluir($cd_cliente);
    echo "exc".$cd_cliente;
    if ($cliente->erro == "")
        echo "<script> alert('Cliente exclu\u00CDdo com sucesso.'); window.location = 'clienteslista.php';</script>";
    else {
        ?>
        <script >                alert("<?php echo $cliente->erro ?>");
            window.location = 'clienteslista.php';</script>
        <?php
    }
}
?>

<html >
    <head>
        <?php include "includes/head2.php" ?> 
       
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <strong>Clientes</strong>
                </nav>
              
                <!------------Busca inicio----------->
                <form action="" method="get"
                      onsubmit="return validaformclientes();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Filtrar Resultados</legend>
                        <ol>
                            <li>
                                <label style="width:40px;">
                                    C&oacute;d:
                                </label>
                                <input class="Campos required" name="cd_cliente" id="cd_cliente" style="width:85px;">
                                <label style="width:53px;margin-left:15px;">
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_cliente" id="nm_cliente" >
                                <label style="width:205px;margin-left:15px;">
                                    <input type="checkbox" name="com_pedidos_pendentes" />&nbsp;S&oacute; com pedidos pendentes
                                </label>
                               
                            </li>
                            <li style="width:100%;min-height:0px;margin-bottom:0px;">
                                 <input type="submit" class="btnenviar" value="Buscar" style="float:right;"/>
                            </li>
                            
                        </ol>
                    </fieldset>
                </form>
                <!------------Busca Fim-------------->


                <?php include "clientearqauxiliares/clientepaginacao.php" ?>  

                <table class="tbllistagem" id="tbllistagem" >
                    <tr>
                        <th style="width: 30px;">
                            C&oacute;d.
                        </th>
                        <th style="width: 200px;">
                            Nome
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Telefone
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Qtd Campeonatos
                        </th>
                       
                        <th style="width: 40px; text-align: center;">
                            Ver
                        </th>
                    </tr>
                    <?php
                    $cliente->ListaTodosGridAdmin($limit, $subquery);
                    for ($i = 0; $i < count($cliente->meuarray); $i++) {
                        echo "<tr>";
                        echo "<td>" . $cliente->meuarray[$i]->getCd_cliente() . "</td>";

                        echo "<td>" . $cliente->meuarray[$i]->getNome_cliente() . "</td>";
                        echo "<td>" ."(".$cliente->meuarray[$i]->getDdd_cliente().") ". $cliente->meuarray[$i]->getTelefone_cliente() . "</td>";

                        echo "<td align='center'>".$cliente->meuarray[$i]->getQt_campeonatos()."</td>";

                        echo"<td align='center'>";
                        echo "<a href='clientedetalhe.php?cd_cliente=" . $cliente->meuarray[$i]->getCd_cliente() . "'><img src='imagens/lupa.png'/></a>";
                        echo "</td>";
                        echo "</tr>";
                        }
                        ?>
                </table>
           </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
