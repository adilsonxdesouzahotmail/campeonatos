<?php
session_start();
require_once "includes/autentica.php";
$cd_jogo = "";
if (isset($_GET["cd_jogo"]))
    $cd_jogo = $_GET["cd_jogo"];
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="jogoslista.php">Jogos</a> > <strong>Editar</strong>
                </nav>

                <nav class="navabas">
                    <div class="aba2" onclick="window.location = 'jogoedicao.php?cd_jogo=<?php echo $cd_jogo ?>'">
                        Dados
                    </div>
                    <div class="aba2" onclick="window.location = 'jogoimagem1.php?cd_jogo=<?php echo $cd_jogo ?>'" >
                        Imagem 1
                    </div>
                    <div class="aba" onclick="window.location = 'jogoimagem2.php?cd_jogo=<?php echo $cd_jogo ?>'">
                        Imagem 2
                    </div>
                    <div class="aba2" style="width: 200px;"
                         onclick="window.location = 'jogogaleriafotos.php?cd_jogo=<?php echo $cd_jogo ?>'" >
                        Galeria de Fotos
                    </div>
                </nav>
                <form action="jogoarqauxiliares/jogouploadimagem2.php" method="post"  
                      id="cadastro" enctype="multipart/form-data" onsubmit="return displayaguarde();">
                    <fieldset ><legend style="margin-left:30px"> Editar imagem 2</legend>
                        <input type="hidden" name="cd_jogo" value="<?php echo $cd_jogo ?>" />
                        <div class="containerfoto" id="containerfoto">
                            <div class="fotobanner" style="width:600px;"  align="center">
                                <?php
                                require_once "../App_Code/Jogos.php";
                                $jogo = new Jogos();
                                $jogo->getUmItem($cd_jogo);

                                if ($jogo->getNm_url_2() != "" && $jogo->getNm_url_2() != "semimagen.png") {

                                    echo " <img  src='../fotos/" . $cd_jogo . "/" . $jogo->getNm_url_2() . "' alt='Imagem cadastrada' id='fotocad' />";
                                } else {
                                    echo " <img  src='imagens/semimagem.png' alt='Sem imagem cadastrada' />";
                                }
                                ?>

                            </div>
                            <div class="areaupload">

                                <input id="file" class="inputselecfoto" name="file"  type="file">

                                <input class="btnuploadfoto"  value="Upload" type="submit">
                                <div id="aguarde">Aguarde</div>
                            </div> 
                        </div>
                    </fieldset>
                </form>


            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
