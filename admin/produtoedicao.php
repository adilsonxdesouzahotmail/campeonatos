<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();
require_once "../App_Code/Produtos.php";
$produto = new Produtos();
$cd_produto = "";
if (isset($_GET["cd_produto"]))
    $cd_produto = $_GET["cd_produto"];

$produto->getUmItem($cd_produto);
?>

<html>
<head >
    <?php include "includes/head2.php" ?> 

       <script src="ckeditor/ckeditor.js"></script>
        <style>
        .ckeditorcontainer{
            width:100%;float:left;clear:left;
        }
        </style>
</head>
<body>
     <?php include "includes/topoelateral.php" ?> 
    <section id="colunadireita">
          <div class="container">
           
            <nav class="breadcrumb">
                <a href="produtoslista.php">Produtos</a> > <strong>Editar</strong>
            </nav>
              <nav class="navabas">
                  <div class="aba" onclick="window.location = 'produtoedicao.php?cd_produto=<?php echo $cd_produto ?>'">
                      Dados
                  </div>

                  <div class="aba2" style="width: 120px;"
                       onclick="window.location = 'produtogaleriafotos.php?cd_produto=<?php echo $cd_produto ?>'" >
                      Galeria de Fotos
                  </div>
                  <div class="aba2" style="width: 120px;"
                       onclick="window.location = 'produtomercadolivre.php?cd_produto=<?php echo $cd_produto ?>'">
                      Mercado Livre
                  </div>
              </nav>
            <form action="produtoarqauxiliares/produtocadastroeditar.php" method="post" id="cadastro" name="cadastro"
                  onsubmit="return validaformprodutos();">
              
                <fieldset ><legend style="margin-left:30px"> Editar dados do produto</legend>
                <ol>
                    <li>
                        <label>
                            Cód:
                        </label>
                        <input class="Campos required" disabled name="cd_produto" value="<?php echo $produto->getCd_produto() ?>" >
                        <input type="hidden" name="cd_produto" value="<?php echo $produto->getCd_produto() ?>" >
                    </li> 

                    <li>
                        <label>
                            Nome:
                        </label>
                        <input class="Campos required" name="nm_produto" id="nm_produto" value="<?php echo $produto->getNm_produto() ?>" >
                            <label>
                                Ativo:</label>
                            Sim&nbsp;<input type="radio" <?php if($produto->getIc_ativo()==1) echo "checked"; ?> value="1" name="ic_ativo" style="margin-top: 8px;" />&nbsp;&nbsp;
                            Não&nbsp;<input type="radio" <?php if($produto->getIc_ativo()==0) echo "checked"; ?> value="0" name="ic_ativo" />&nbsp;&nbsp; </li>
                    <li>
                        <label>
                            Plataforma:
                        </label>
                        <select name="cd_plataforma" id="cd_plataforma" id="cd_plataforma" onchange="carregasubplataformas();" class="Campos" >
                            <option value="0">Selecione</option>
                            <?php
                            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                            $mysqli->set_charset("utf8");
                            $query = "select * from plataformas";
                            $rs = $mysqli->query($query);


                            while ($row = $rs->fetch_assoc()) {
                                if ($row ["cd_plataforma"] == $produto->getCd_plataforma()) {
                                    echo "<option value=" . $row ["cd_plataforma"] . " selected>" . $row ["nm_plataforma"] . "</option>";
                                } else
                                    echo "<option value=" . $row ["cd_plataforma"] . ">" . $row ["nm_plataforma"] . "</option>";
                            }
                            $rs->free();
                            ?>
                        </select>

                    </li>
                    <li>
                        <label>
                            Gêneros:</label>
                        <table class="tblcaracteristicas" >
                            <?php
                            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                            $mysqli->set_charset("utf8");
                            $query = "select * from caracteristicas";
                            $rs = $mysqli->query($query);
                            
                            require_once "../App_Code/Caracteristicas_produto.php";
                            $caracprod = new Caracteristicas_produto();
                            $cont = 1;
                            echo "<tr>";
                            while ($row = $rs->fetch_assoc()) {
                                echo "<td style='height:35px;'>";
                               if($caracprod->temCarac($produto->getCd_produto(), $row ["cd_caracteristica"])>0)
                               {
                                    echo "<input type='checkbox' checked  value='" . $row ["cd_caracteristica"] . "' name='cd_caracteristica[]' />&nbsp;" . $row ["nm_caracteristica"] . "&nbsp;";
                               }
                                else {
                                    echo "<input type='checkbox' value='" . $row ["cd_caracteristica"] . "' name='cd_caracteristica[]' />&nbsp;" . $row ["nm_caracteristica"] . "&nbsp;";
                                }
                                echo "</td>";
                                if ($cont % 4 == 0)
                                    echo "</tr><tr>";
                                $cont++;
                            }
                            echo "</tr>";
                            $rs->free();
                            ?>
                        </table>
                    </li>
                    <!---------------------->
                    <li>
                        <label>
                            Idiomas:</label>
                        <table class="tblcaracteristicas">
                            <?php
                            require_once "../App_Code/Conexao.php";
                            $conexao = new Conexao();
                            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                            $mysqli->set_charset("utf8");
                            $query = "select * from idiomas";
                            $rs = $mysqli->query($query);
                            require_once "../App_Code/Idiomas_produto.php";
                            $idiomaprod = new Idiomas_produto();
                            $cont = 1;
                            echo "<tr>";
                            while ($row = $rs->fetch_assoc()) {
                                echo "<td style='padding-right:10px;'>";
                                /////////////////////////////
                                if ($idiomaprod->temIdioma($produto->getCd_produto(), $row ["cd_idioma"]) > 0) {
                                    echo "<input type='checkbox' checked value='" . $row ["cd_idioma"] . "' name='cd_idioma[]' />&nbsp;<img style='width: 30px;height: 22px;' src='../imagens/flags/" . $row ["url_flag"] . "' />&nbsp;&nbsp;";
                                } else {
                                    echo "<input type='checkbox' value='" . $row ["cd_idioma"] . "' name='cd_idioma[]' />&nbsp;<img style='width: 30px;height: 22px;' src='../imagens/flags/" . $row ["url_flag"] . "' />&nbsp;&nbsp;";
                                }
                                //////////////////////////////
                               // echo "<input type='checkbox' value='" . $row ["cd_idioma"] . "' name='cd_idioma[]' />&nbsp;<img style='width: 30px;height: 22px;' src='../imagens/flags/" . $row ["url_flag"] . "' />&nbsp;&nbsp;";
                                echo "</td>";
                                if ($cont % 5 == 0)
                                    echo "</tr><tr>";
                                $cont++;
                            }
                            echo "</tr>";
                            $rs->free();
                            ?>
                        </table>
                    </li>                            
                    <!----------------------> 
                     <li>
                        <label>
                            Legendas:</label>
                        <table class="tblcaracteristicas">
                            <?php
                            require_once "../App_Code/Conexao.php";
                            $conexao = new Conexao();
                            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                            $mysqli->set_charset("utf8");
                            $query = "select * from legendas";
                            $rs = $mysqli->query($query);
                            require_once "../App_Code/Legendas_produto.php";
                            $legendaprod = new Legendas_produto();
                            $cont = 1;
                            echo "<tr>";
                            while ($row = $rs->fetch_assoc()) {
                                echo "<td style='padding-right:10px;'>";
                                /////////////////////////////
                                if ($legendaprod->temLegenda($produto->getCd_produto(), $row ["cd_legenda"]) > 0) {
                                    echo "<input type='checkbox' checked value='" . $row ["cd_legenda"] . "' name='cd_legenda[]' />&nbsp;<img style='width: 30px;height: 22px;' src='../imagens/flags/" . $row ["url_flag"] . "' />&nbsp;&nbsp;";
                                } else {
                                    echo "<input type='checkbox' value='" . $row ["cd_legenda"] . "' name='cd_legenda[]' />&nbsp;<img style='width: 30px;height: 22px;' src='../imagens/flags/" . $row ["url_flag"] . "' />&nbsp;&nbsp;";
                                }
                                //////////////////////////////
                               // echo "<input type='checkbox' value='" . $row ["cd_legenda"] . "' name='cd_legenda[]' />&nbsp;<img style='width: 30px;height: 22px;' src='../imagens/flags/" . $row ["url_flag"] . "' />&nbsp;&nbsp;";
                                echo "</td>";
                                if ($cont % 5 == 0)
                                    echo "</tr><tr>";
                                $cont++;
                            }
                            echo "</tr>";
                            $rs->free();
                            ?>
                        </table>
                    </li>    
                    <li >
                        <label>
                            Descrição:</label>
                       
                        <div class="ckeditorcontainer">
                            <textarea   id="editor1" name="ds_produto" rows="10" cols="80" >
                                <?php echo $produto->getDs_produto(); ?>
                            </textarea>
                        </div>
                    </li>
                    <li>
                        <label>
                            Valor de:
                           
                        </label>
                        <input class="Campos required" name="vl_de" id="vl_de" value="<?php echo "R$ ".number_format($produto->getVl_de(), 2,',','.') ?>">
                         <input type="hidden" name="vl_dehd" id="vl_dehd">
                         <script type="text/javascript">  $("#vl_de").maskMoney({ prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: true }); </script>
                        <label>
                            Valor por:
                        </label>
                         <?php
                            $estilo="";
                            if($produto->getVl_por() <= $produto->getMlb_preco_max())
                                $estilo=" style='color:red;' ";
                         ?>
                        <input class="Campos required" <?php echo $estilo; ?> name="vl_por" id="vl_por" value="<?php echo "R$ ".number_format($produto->getVl_por(), 2,',','.') ?>">
                        <input type="hidden" name="vl_porhd" id="vl_porhd">
                        <script type="text/javascript">  $("#vl_por").maskMoney({ prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: true }); </script>
                    </li>
                    <li>
                        <label>
                            MLB Val. Min.:
                           
                        </label>
                        <input class="Campos required"  disabled  name="mlb_preco_min" id="mlb_preco_min" value="<?php echo "R$ ".number_format($produto->getMlb_preco_min(), 2,',','.') ?>">
                       
                        <label>
                            MLB Val. Max:
                        </label>
                        <input class="Campos required"  disabled  name="mlb_preco_max" id="mlb_preco_max" value="<?php echo "R$ ".number_format($produto->getMlb_preco_max(), 2,',','.') ?>">
                       
                    </li>                    
                    
                    <li>
                        <label>
                            MLB Estoque.:
                           
                        </label>
                        <input class="Campos required"  disabled  name="mlb_qt_estoque" id="mlb_qt_estoque" value="<?php echo $produto->getMlb_qt_estoque(); ?>">
           
                       
                    </li>                    
<!--                    <li>
                        <label>
                            Peso (gramas):
                        </label>
                        <input class="Campos required" name="qt_peso" id="qt_peso" value="<?php echo $produto->getQt_peso() ?>">
                    </li>-->

<!--                    <li>
                        <label>
                            Estoque:
                        </label>
                        <input class="Campos required" name="qt_estoque" id="qt_estoque" value="<?php echo $produto->getQt_estoque() ?>">

                        <label>
                            Estoque min.:
                        </label>
                        <input class="Campos required" name="qt_estoque_minimo" id="qt_estoque_minimo" value="<?php echo $produto->getQt_estoque_minimo() ?>">
                    </li>-->
                    <li>
                        <label>
                            Destaque:
                        </label>
                        Sim&nbsp;<input type="radio" value="1" name="ic_destaque" <?php if($produto->getIc_destaque()==1) echo "checked"; ?> style="margin-top: 8px;" />&nbsp;&nbsp;
                        Não&nbsp;<input type="radio" value="0" name="ic_destaque" <?php if($produto->getIc_destaque()==0) echo "checked"; ?> />&nbsp;&nbsp; 
                    </li>
                    <li>
                        <label>
                            Oferta:
                        </label>
                        Sim&nbsp;<input type="radio" value="1" name="ic_oferta" <?php if($produto->getIc_oferta()==1) echo "checked"; ?> style="margin-top: 8px;" />&nbsp;&nbsp;
                        Não&nbsp;<input type="radio" value="0" name="ic_oferta" <?php if($produto->getIc_oferta()==0) echo "checked"; ?> />&nbsp;&nbsp; 
                    </li>
                    <li style="width:100%;">
                        <input type="submit" class="btnenviar" value="Enviar" />
                    </li>
                </ol>
                </fieldset>
                <script>
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('editor1');
               </script>
            </form>
        </div>
    </section>
    <?php include "includes/rodape.php" ?> 
</body>
</html>
