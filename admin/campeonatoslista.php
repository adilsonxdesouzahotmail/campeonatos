﻿<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Campeonatos.php";
require_once "../App_Code/Conexao.php";
require_once "../App_Code/Config.php";
$conexao = new Conexao();

$campeonato = new Campeonatos();
$acao = "";
if (isset($_GET["acao"]))
    $acao = $_GET["acao"];

$cd_roduto = "";
if (isset($_GET["cd_campeonato"]))
    $cd_campeonato = $_GET["cd_campeonato"];


if ($acao == "excluir") {
    $campeonato->Excluir($cd_campeonato);
    if ($campeonato->erro == "")
        echo "<script> alert('Campeonato exclu\u00CDdo com sucesso.'); window.location = 'campeonatoslista.php';</script>";
    else {
        ?>
        <script >                alert("<?php echo $campeonato->erro ?>");
            window.location = 'campeonatoslista.php';</script>
        <?php
    }
}
?>

<html >
    <head>
        <?php include "includes/head2.php" ?> 
          <script type="text/javascript">
            function carregasubcategorias()
            {
                var url = "subcategoriaarqauxiliares/carregasubcategorias.php";
                formdata = "cd_categoria=" + $("#cd_categoria").val();
                $.ajax({
                    url: url,
                    data: formdata,
                    type: 'POST',
                    dataType: 'html',
                    success: function(data) {
                        $("#cd_subcategoria").html("<option value='0'>Selecione</option>" + data);
                    }
                });

            }
        </script>
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <strong>Campeonatos</strong>
                </nav>
                <div class="contbtnnovo">
                    <input type="button" class="botaopadrao" value="Novo Campeonato" onclick="window.location = 'campeonatocadastro.php';" />
                </div>

                <!------------Busca inicio----------->
                <form action="" method="get"
                      onsubmit="return validaformcampeonatos();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Busca</legend>
                        <ol>
                            <li>
                                <label style="width:95px;">
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_campeonato" id="nm_campeonato" >
                                <label>
                                    Destaque:</label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_destaque"  style="margin-top: 8px;" />&nbsp;&nbsp;
                                N&atilde;o&nbsp;<input type="radio" value="0" name="ic_destaque" />&nbsp;&nbsp; 
                            </li>
                            Todos&nbsp;<input type="radio" value="3" name="ic_destaque" checked style="margin-top: 8px;"/>&nbsp;&nbsp;
                            </li>
                            <li>
                                <label style="width:95px;">
                                    Categoria:
                                </label>
                                <select name="cd_categoria" id="cd_categoria" onchange="carregasubcategorias();" class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from categorias";
                                    $rs = $mysqli->query($query);

                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_categoria"] . ">" . $row ["nm_categoria"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                                <label>
                                    Subcategoria:
                                </label>
                                <select name="cd_subcategoria" id="cd_subcategoria" class="Campos">
                                    <option value="0">Selecione</option>
                                </select>
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                </form>
                <!------------Busca Fim-------------->


                <?php include "campeonatoarqauxiliares/campeonatopaginacao.php" ?>  

                <table class="tbllistagem" id="tbllistagem" >
                    <tr>
                        <th style="width: 30px;">
                            C&oacute;d.
                        </th>
                        <th style="width: 200px;">
                            Nome
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Ativo
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Jogo
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Plataformas
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Valor Inscrição
                        </th>                        
                        <th style="width: 40px; text-align: center;">
                            Data
                        </th>  
                        <th style="width: 40px; text-align: center;">
                            Qtd. part. prev.
                        </th>  
                        <th style="width: 40px; text-align: center;">
                            Qtd. part. inscritos
                        </th>    
                        <th style="width: 40px; text-align: center;">
                            Qtd. part. pagantes
                        </th>                         
                        <th style="width: 40px; text-align: center;">
                            Qtd. jogadores/time
                        </th>    
                        <th style="width: 40px; text-align: center;">
                            Encerrado
                        </th>                         
                        <th style="width: 40px; text-align: center;">
                            Editar
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Excluir
                        </th>
                    </tr>
                    <?php
                    $campeonato->ListaTodosGridAdmin($limit, $subquery);
                    for ($i = 0; $i < count($campeonato->meuarray); $i++) {
                        $estilocorlinha="";
                         if($campeonato->meuarray[$i]->getIc_encerrado()==1 )
                        {
                            $estilocorlinha =" style='background-color:#4df380'";
                        }
                        echo "<tr ".$estilocorlinha.">";
                        echo "<td>" . $campeonato->meuarray[$i]->getCd_campeonato() . "</td>";

                        echo "<td>" . $campeonato->meuarray[$i]->getNm_campeonato() . "</td>";

                        if ($campeonato->meuarray[$i]->getIc_ativo() != 0)
                            $checked = "checked";
                        else
                            $checked = "";
                        echo "<td align='center'><input type='checkbox' value=''" . $checked . " disabled /></td>";
                        echo "<td>" . $campeonato->meuarray[$i]->getNm_jogo() . "</td>";
                        echo "<td>" . $campeonato->meuarray[$i]->getNm_plataforma() . "</td>";
                        echo "<td>" . number_format($campeonato->meuarray[$i]->getVl_inscricao(), 2,',','.') . "</td>";                        
                        echo "<td>" . $campeonato->meuarray[$i]->getDt_campeonato() . "</td>";
                        echo "<td>" . $campeonato->meuarray[$i]->getQt_participantes_previsto() . "</td>";
                        echo "<td>" . $campeonato->meuarray[$i]->getQt_participantes_inscritos() . "</td>";
                        echo "<td>" . $campeonato->meuarray[$i]->getQt_participantes_pagantes() . "</td>";
                        echo "<td>" . $campeonato->meuarray[$i]->getQt_jogadores_time() . "</td>";  
                        $estilo="";
                        $encerrado="Não";
                        if($campeonato->meuarray[$i]->getIc_encerrado()==1 )
                        {
                            $estilo =" style='font-weight:bold;'";
                            $encerrado = "Sim";
                        }
                        echo "<td ".$estilo.">" . $encerrado. "</td>";  
                        echo"<td align='center'>";
                        echo "<a href='campeonatoedicao.php?cd_campeonato=" . $campeonato->meuarray[$i]->getCd_campeonato() . "'><img src='imagens/lapis.png'/></a>";
                        echo "</td>";

                        echo "<td align='center'>";
                        ?>
                        <a href="campeonatoslista.php?cd_campeonato=<?php echo $campeonato->meuarray[$i]->getCd_campeonato() ?>&acao=excluir"
                           onclick="return confirm('Confirma a exclus\u00E3o do Campeonato <?php echo $campeonato->meuarray[$i]->getNm_campeonato() ?>?\u000A Aten\u00E7\u00E3o: imagens e outras informa\u00E7\u00F5es associadas ao campeonato tamb\u00E9m ser\u0007\o excluidas.');">
                            <img src='imagens/lixeira.png' /></a>
                        <?php
                        echo "</td>";
                        echo "</tr>";
                        } // fecha for da linha 150
                        ?>
                </table>
           </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
