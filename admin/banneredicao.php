<?php
error_reporting(E_ALL);
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();
require_once "../App_Code/Banners.php";
$banner = new Banners();
$cd_banner = "";
if (isset($_GET["cd_banner"]))
    $cd_banner = $_GET["cd_banner"];

$banner->getUmItem($cd_banner);
?>
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
      

    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="bannerslista.php">Banners</a> > <strong>Cadastrar</strong>
                </nav>
                <nav class="navabas">
                    <div class="aba">
                        Banner
                    </div>
                    <div class="abadisabled" style="width: 120px;">
                        Foto
                    </div>                  
                </nav>
                <form action="bannerarqauxiliares/bannercadastroinserir.php" method="post"
                      onsubmit="return validaformbanners();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Cadastrar novo banner</legend>
                        <ol>
                            <li>
                                <label>
                                    Texto 1:
                                </label>
                                <input class="Campos required" name="nm_texto1" id="nm_texto1" >
                                <label>
                                    Ativo:</label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_ativo" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" value="0" name="ic_ativo" />&nbsp;&nbsp; 
                            </li>
                            <li>
                                <label>
                                    Texto 2:
                                </label>
                                <input class="Campos required" name="nm_texto2" id="nm_texto2" >
                              
                            </li>  
                            <li>
                                <label>
                                    Texto 3:
                                </label>
                                <input class="Campos required" name="nm_texto3" id="nm_texto3" >
                              
                            </li>                              
                            <li>
                               
                                 <label>
                                    Campeonato
                                </label>
                                <select name="cd_campeonato" id="cd_campeonato"  class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from campeonatos where ic_ativo=true and ic_encerrado= false";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_campeonato"] . ">" . $row ["nm_campeonato"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                            </li>



                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>

                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
