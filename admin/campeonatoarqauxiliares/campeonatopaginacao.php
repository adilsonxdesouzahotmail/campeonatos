  <?php
 
         $querystring="&";
         $subquery=" where 1=1 ";
         if(isset($_GET["nm_campeonato"]))
         {

             if($_GET["nm_campeonato"]!="")
             {
                $subquery .= " and nm_campeonato like '%".$_GET["nm_campeonato"]."%'";
                $querystring.="nm_campeonato=".$_GET["nm_campeonato"];
             }
             else {
                $querystring.="nm_campeonato=";
             }
         }
         else {
             $querystring.="nm_campeonato=";
         }

         if(isset($_GET["cd_categoria"]))
         {
             if($_GET["cd_categoria"]!="0" && $_GET["cd_categoria"]!="")
             {
                 $querystring.="&cd_categoria=".$_GET["cd_categoria"];
                 $subquery .= " and cd_categoria =".$_GET["cd_categoria"];
             }
              else {
                $querystring.="&cd_categoria=";
              }
         }
          else {
             $querystring.="&cd_categoria=";
         }

          if(isset($_GET["cd_subcategoria"]))
          {

              if($_GET["cd_subcategoria"]!="0" && $_GET["cd_subcategoria"]!="")
              {
                $subquery .= " and cd_subcategoria =".$_GET["cd_subcategoria"];
                 $querystring.="&cd_subcategoria=".$_GET["cd_subcategoria"];
              }
              else {
                    $querystring.="&cd_subcategoria=";
              }
          }
          else {
             $querystring.="&cd_subcategoria=";
         }

          if(isset($_GET["ic_destaque"]))
          {
              if($_GET["ic_destaque"]!="3" && $_GET["ic_destaque"]!="")
              {
                 $subquery .= " and ic_destaque =".$_GET["ic_destaque"];
                 $querystring.="&ic_destaque=".$_GET["ic_destaque"];
              }
              else {
                    $querystring.="&ic_destaque=";
              }
          }

         $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
         $mysqli->set_charset("utf8");
         $query="select count(cd_campeonato)as qtd_campeonatos from campeonatos ".$subquery;
        // echo $query;
         $rs = $mysqli->query($query);
         while ( $row = $rs->fetch_assoc () ) {
             $rows =  $row ["qtd_campeonatos"];
         }

        // This is the number of results we want displayed per page
        $page_rows = qtItensPagAdmin;

        $last = ceil($rows / $page_rows);
        // This makes sure $last cannot be less than 1

        if ($last < 1) {
            $last = 1;
        }
        // Establish the $pagenum variable
        $pagenum = 1;
        // Get pagenum from URL vars if it is present, else it is = 1
        if (isset($_GET['pn'])) {
            $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
        }
        // This makes sure the page number isn't below 1, or more than our $last page
        if ($pagenum < 1) {
            $pagenum = 1;
        } else {
            if ($pagenum > $last) {
                $pagenum = $last;
            }
        }
        // This sets the range of rows to query for the chosen $pagenum
        $limit = 'LIMIT ' . ($pagenum - 1) * $page_rows . ',' . $page_rows;

        // This is your query again, it is for grabbing just one page worth of rows by applying $limit

//                $query ="select cd_campeonato from campeonatos $subquery $limit";
//                $rs = $mysqli->query($query);

        $textline1 = "<p style='width:100%;float:left;clear:left;'>Total de itens encontrados (<strong>$rows</strong>)</p>";
        $textline2 = "<p style='width:100%;float:left;clear:left;'>Página <b>$pagenum</b> de <b>$last</b></p>";
        // Establish the $paginationCtrls variable
        $paginationCtrls = '';
        // If there is more than 1 page worth of results
        if ($last != 1) {
            /* First we check if we are on page one. If we are then we don't need a link to the previous page or the first page so we do nothing. If we aren't then we generate links to the first page, and to the previous page. */
            if ($pagenum > 1) {
                $previous = $pagenum - 1;
                $paginationCtrls .= '<a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous .$querystring.'">Anterior</a> &nbsp; &nbsp; ';
                // Render clickable number links that should appear on the left of the target page number
                for ($i = $pagenum - 10; $i < $pagenum; $i++) {
                    if ($i > 0) {
                        $paginationCtrls .= '<a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $i .$querystring. '">' . $i . '</a> &nbsp; ';
                    }
                }
            }
            // Render the target page number, but without it being a link
            $paginationCtrls .= '' . $pagenum . ' &nbsp; ';
            for ($i = $pagenum + 1; $i <= $last; $i++) {
                $paginationCtrls .= '<a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $i .$querystring. '">' . $i . '</a> &nbsp; ';
                if ($i >= $pagenum + 10) {
                    break;
                }
            }
            // This does the same as above, only checking if we are on the last page, and then generating the "Next"
            if ($pagenum != $last) {
                $next = $pagenum + 1;
                $paginationCtrls .= ' &nbsp; &nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $next .$querystring. '">Próxima</a> ';
            }
        }
        ?>
         <div style="width: 100%; float: left; clear: left; margin-top: 10px; cursor: pointer;">
        <h2>
            <?php echo $textline1; ?>

        </h2> 
        <p style="text-align:center;"><?php echo $textline2; ?></p> 

        <div id="pagination_controls" style="text-align:center;"><?php echo $paginationCtrls; ?></div> 
    </div>