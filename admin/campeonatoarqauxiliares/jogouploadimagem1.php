<?php
//******** Configurações ***************/
require_once "../../App_Code/Constantes.php";
$largura = imagem1_largura;
$altura = imagem1_altura;
$arquivodestino= "imagem1.png";
$paginaorigem = "jogoimagem1";
//**************************************/

    $cd_jogo="";
     if(isset($_POST["cd_jogo"]))
         $cd_jogo=$_POST["cd_jogo"];
     
     $pastafotosjogo = "../../fotos/".$cd_jogo."/";
     
    if (!file_exists($pastafotosjogo)) {
        mkdir($pastafotosjogo, 0777, true);
    }
   
    
require_once "../../App_Code/resizepicture.php";

$erroresize="";
if (is_uploaded_file($_FILES['file']['tmp_name'])) {
    copy($_FILES['file']['tmp_name'], $pastafotosjogo."foto1prov.png");
} else {
    $erroresize= "Arquivo não aceito";
}
//indicate which file to resize (can be any type jpg/png/gif/etc...)

if($erroresize=="")
{
    $file = $pastafotosjogo.'foto1prov.png';

    //indicate the path and name for the new resized file

    $resizedFile = $pastafotosjogo.$arquivodestino;
    require_once "../../App_Code/Jogos.php";
    $jogo= new Jogos();
    $jogo->getUmItem($cd_jogo);
    $jogo->setNm_url_1($arquivodestino);
    $jogo->Atualizar($jogo);
    

    smart_resize_image($file, $largura, $altura, true, $resizedFile, false, false, 50);
   
    if($jogo->erro=="")
    {
       echo "<script>alert('Imagem atualizada com sucesso.');".
               "window.location= '../".$paginaorigem.".php?cd_jogo=".$cd_jogo."';</script>";
    }
     else {
         ?>
            <script>//alert(<?php echo $local->erro ?>");history.back();</script>
        <?php

    }
}
 else 
{
    echo "<script>alert('".$erroresize."'>);history.back();</script>";
}


?> 