<?php
session_start();
require_once "../includes/autentica.php";
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
//echo $_POST["cd_caracteristica"];
//echo implode(",", $_POST["cd_caracteristica"]);

require_once "../../App_Code/Campeonatos.php";
$campeonato = new Campeonatos();
$campeonato->setNm_campeonato($_POST["nm_campeonato"]);
//echo $_POST["nm_campeonato"]."-".$_POST["cd_jogo"];
$campeonato->setCd_jogo($_POST["cd_jogo"]);
$campeonato->setDt_campeonato($_POST["dt_campeonato"]);
$campeonato->setDs_campeonato($_POST["ds_campeonato"]);
$campeonato->setDs_hora_campeonato($_POST["ds_hora_campeonato"]);
$campeonato->setQt_participantes_previsto($_POST["qt_participantes_previsto"]);
$campeonato->setQt_participantes_real(0);
$campeonato->setIc_ativo($_POST["ic_ativo"]);
$campeonato->setVl_inscricao($_POST["vl_inscricao"]);
$campeonato->setQt_jogadores_time($_POST["qt_jogadores_time"]);
$campeonato->Inserir($campeonato);


if ($campeonato->erro == "") 
{
   $cd_campeonato = $campeonato->UltimoCampeonato();
   $plataformas = "";
    if (isset($_POST["cd_plataforma"]))
        $plataformas = $_POST["cd_plataforma"];
    if ($plataformas != "") {
        require_once "../../App_Code/Plataformas_campeonato.php";
        
        $platcampeonato = new Plataformas_campeonato();
        $platcampeonato->Atualizar($cd_campeonato, $plataformas);
        if ($platcampeonato->erro != "") {
            ?>
                 <script >alert("<?php echo "Erro no cadastro das plataformas" . $platcampeonato->erro ?>");</script>
            <?php
        }
    }
    echo "<script>alert('Campeonato cadastrado com sucesso.');window.location='../campeonatoedicao.php" .
        "?cd_campeonato=" . $campeonato->UltimoCampeonato() ."';</script>";
}
else 
{
    ?>
            <script >alert("<?php echo $campeonato->erro ?>");history.back();</script>
    <?php
}


?>
