<?php
error_reporting(E_ALL);
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Noticias.php";
$noticia = new Noticias();
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
         <script src="ckeditor/ckeditor.js"></script>
        <style>
        .ckeditorcontainer{
            width:100%;float:left;clear:left;

        }

        </style>
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="noticiaslista.php">Noticias</a> > <strong>Cadastrar</strong>
                </nav>
                <form action="noticiaarqauxiliares/noticiacadastroinserir.php" method="post" id="cadastro" onsubmit="return validaform();">
              
                    <fieldset ><legend >Cadastrar nova noticia</legend>
                        <ol>

                            <li>
                                <label>
                                    Titulo:
                                </label>
                                <input class="Campos required" name="nm_titulo" maxlength="100">
                                <input type="hidden" name="cd_menu" value="1"/>
                                <label>
                                    Ativo:</label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_ativo" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" value="0" name="ic_ativo" />&nbsp;&nbsp;                                 
                            </li>

                            <li>
                                <label>
                                    Campeonato associado:
                                </label>
                                <select name="cd_campeonato" id="cd_jogo"     class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from campeonatos";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_campeonato"] . ">" . $row ["nm_campeonato"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                                <label>
                                 Mostra Carrossel:</label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_mostra_carrossel" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" value="0" name="ic_mostra_carrossel" />&nbsp;&nbsp;                                
                            </li>       
                            
<!--                            <li>
                                <label>
                                    Menu:
                                </label>
                                <select name="cd_menu" id="cd_menu"  class="Campos" >
                                    <?php
                                        require_once "../App_Code/Conexao.php";
                                        $conexao = new Conexao();
                                        $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                        $mysqli->set_charset("utf8");
                                        $query = "select * from menus";
                                        $rs = $mysqli->query($query);


                                        while ($row = $rs->fetch_assoc()) {
                                            echo "<option value=" . $row ["cd_menu"] . ">" . $row ["nm_menu"] . "</option>";
                                        }
                                        $rs->free();
                                        ?>
                                 </select>
                            </li>-->

                            <li>
                                <label>
                                    Conteúdo:
                                </label>
                <!--               <textarea id="txtdescricao" rows="3" name="ds_noticia"></textarea>-->
                                <div class="ckeditorcontainer">
                                    <textarea   id="editor1" name="ds_noticia" rows="10" cols="80" >
                    
                                    </textarea>
                                </div>

                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                    <script>
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('editor1');
                    </script>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
        <script>
             $("#colunaesquerda").height(950);
        </script>
    </body>
</html>
