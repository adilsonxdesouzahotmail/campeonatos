<?php
session_start();
require_once "includes/autentica.php";
$cd_jogo = "";
if (isset($_GET["cd_jogo"]))
    $cd_jogo = $_GET["cd_jogo"];
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
        <script type="text/javascript" src="upload/js/uploader.js"></script>
        <link type="text/css" href="upload/css/uploader.css" rel="stylesheet" />
        <script type="text/javascript">
            var clicado = 0;
            $(document).ready(function() {

                $(".tbllistagemgaleria tr").each(function(index) {
                    $(this).click(function() {
                        $("#containerbotoesdirecionais").show();
                        clicado = index;
                        mudaClicado(clicado);
                    });

                });
            });
            function mudaClicado(clicado) {
                $(".tbllistagemgaleria tr").each(function(index) {
                    if (index > 0) {
                        if (index != clicado)
                            $(this).css("background-color", "#fff");
                        else
                            $(this).css("background-color", "#dadada");
                    }
                });
            }
            function setaPraCima() {
                if (clicado > 1) {
                    conteudofotoanteriorantigo = $(".tbllistagemgaleria tr").eq(clicado - 1).html();
                    conteudofotoclicadoantigo = $(".tbllistagemgaleria tr").eq(clicado).html();
                    $(".tbllistagemgaleria tr").eq(clicado - 1).html(conteudofotoclicadoantigo);
                    $(".tbllistagemgaleria tr").eq(clicado).html(conteudofotoanteriorantigo);
                    clicado--;
                    mudaClicado(clicado);
                }
            }
            function setaPraBaixo() {
                ultimo = $(".tbllistagemgaleria tr").last().index();
                if (clicado < ultimo) {
                    conteudofotoposteriorantigo = $(".tbllistagemgaleria tr").eq(clicado + 1).html();
                    conteudofotoclicadoantigo = $(".tbllistagemgaleria tr").eq(clicado).html();
                    $(".tbllistagemgaleria tr").eq(clicado + 1).html(conteudofotoclicadoantigo);
                    $(".tbllistagemgaleria tr").eq(clicado).html(conteudofotoposteriorantigo);
                    clicado++;
                    mudaClicado(clicado);
                }
            }
            function salvarAlteracoes() {

                contador = 1;

                $("#aguardealtseqfotos").show();

                $(".tbllistagemgaleria tr td input[id*=hd_cd_jogo_foto]").each(function(index) {
                    ultimo = $(".tbllistagemgaleria tr td input[id*=cd_jogo_foto]").size();
                    //                  window.location='jogoarqauxiliares/atualizaordemfotos.php?vl_posicao=' + (index + 1) + '&cd_jogo_foto=' + $(this).val();
                    $.get('jogoarqauxiliares/atualizaordemfotos.php?vl_posicao=' + (index + 1) + '&cd_jogo_foto=' + $(this).val(), function(data) {

                        if (ultimo == (index + 1)) {
                            alert("Sequencia de imagens atualizada com sucesso.");
                            window.location = window.location;
                        }
                    });
                });
            }
            function mostracadfotos() {
                if ($('#enviofotos').css('display') == 'none')
                    $('#enviofotos').show('slow');
                else
                    $('#enviofotos').hide('slow');
            }
            ;
            function showbtnencerrar() {
                $("#btnencerrar").show();
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                new multiple_file_uploader
                        ({
                            form_id: "fileUpload",
                            autoSubmit: true,
                            server_url: "upload/uploaderfotosjogos.php?cd_jogo=<?php echo $_GET["cd_jogo"] ?>" // PHP file for uploading the browsed files
                        });
            });
            function showbtnencerrar() {
                $("#btnencerrar").show();
            }
        </script>
    </head>
    <body >

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita"  >
            <div class="container">
                <nav class="breadcrumb">
                    <a href="jogoslista.php">Jogos</a> > <strong>Editar</strong>
                </nav>

                <nav class="navabas">
                    <div class="aba2" onclick="window.location = 'jogoedicao.php?cd_jogo=<?php echo $cd_jogo ?>'">
                        Jogo
                    </div>

                    <div class="aba" style="width: 120px;"
                         onclick="window.location = 'jogogaleriafotos.php?cd_jogo=<?php echo $cd_jogo ?>'" >
                        Galeria de Fotos
                    </div>
                </nav>
                <fieldset style="float:left;clear:left;position:relative;width:756px;" ><legend style="margin-left:30px"> Editar galeria de fotos</legend>
                    <div style="width:300px;float:left;clear:left;min-height:300px;">
                        <?php
                        $acao = "";
                        if (isset($_GET["acao"]))
                            $acao = $_GET["acao"];
                        if ($acao == "excluir") {
                            require_once "../App_Code/JogosFotos.php";
                            $jogo_foto = new JogosFotos();
                            $cd_jogo_foto = $_GET["cd_jogo_foto"];
                            $jogo_foto->Excluir($cd_jogo_foto);
                            if ($jogo_foto->erro == "") {
                                echo "<script>alert('Foto excluída com sucesso');window.location='jogogaleriafotos.php?cd_jogo=" . $cd_jogo . "';</script>";
                            } else {
                                ?>
                                <script>alert("<?php echo $jogo_foto->erro; ?>");</script>
                                <?php
                            }
                        }
                        ?>
                        <table class="tbllistagemgaleria">
                            <tr>
                                <th style="width: 150px;">
                                    Foto
                                </th>

                                <th style="width: 30px; text-align: center;">
                                    Excluir
                                </th>
                            </tr>
                            <?php
                            require_once "../App_Code/Conexao.php";
                            $conexao = new Conexao();
                            $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                            $mysqli->set_charset("utf8");
                            $query = "select * from jogos_fotos where cd_jogo=" . $cd_jogo . " order by vl_posicao";
                            $rs = $mysqli->query($query);


                            while ($row = $rs->fetch_assoc()) {
                                //******************
                                ?>
                                <tr>
                                    <td align="center">
                                        <img width="100px" height="100px" src="../fotosjogos/<?php echo $cd_jogo ?>/fotos/<?php echo $row["nm_url_foto"] ?>"  />
                                    </td>
                                    <td align="center">
                                        <a href="jogogaleriafotos.php?cd_jogo=<?php echo $cd_jogo ?>&cd_jogo_foto=<?php echo $row["cd_jogo_foto"] ?>&acao=excluir" onclick="return confirm('Confirma a exclusão da imagem?');"><img src='imagens/lixeira.png'/></a>
                                        <input type="hidden" value="<?php echo $row["cd_jogo_foto"] ?>" id="hd_cd_jogo_foto" />
                                    </td>
                                </tr>
                                <?php
                            }
                            $rs->free();
                            ?>

                        </table>
                        <div id="containerbotoesdirecionais">
                            <div id="containerbotoesdirecionaisint">
                                <div class="setapracima" onclick="setaPraCima();">
                                </div>
                                <div class="setaprabaixo" onclick="setaPraBaixo();">
                                </div>
                                <div class="btnsalvaralteracoes" onclick="salvarAlteracoes();">
                                </div>
                                <div id="aguardealtseqfotos">
                                    Aguarde</div>
                            </div>
                        </div>
                    </div>
                    <div style="float: left;">
                        <div class="upload_box" >
                            <form name="fileUpload" id="fileUpload" action="javascript:showbtnencerrar();void(0);" enctype="multipart/form-data">
                                <div class="file_browser">
                                    <input type="file" name="multiple_files[]" id="_multiple_files" class="hide_broswe"
                                           multiple /></div>
                                <div class="file_upload" style="margin-left:117px;">
                                    <input type="submit" value="Upload" class="upload_button" />
                                </div>
                            </form>
                        </div>
                        <div class="file_boxes">
                            <span id="removed_files"></span>
                        </div>
                    </div>
                    <div style="width:386px; float:right;margin-right:70px;margin-top:10px;" id="btnencerrar">
                        <div  class="btnmenu"  style="margin-left:0px;" onclick="window.location = window.location;"  >Encerrar</div> &nbsp;
                        <div style="font-size:10px;float:left;height:28px;line-height:28px;padding-left:5px;">
                            *clique aqui após carregar todos os arquivos
                        </div>
                    </div>
                </fieldset>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
