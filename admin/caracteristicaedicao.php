﻿<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Caracteristicas.php";
$caracteristica = new Caracteristicas();
if (isset($_GET["cd_caracteristica"]))
    $cd_caracteristica = $_GET["cd_caracteristica"];
else
    $cd_caracteristica = "";
$caracteristica->getUmItem($cd_caracteristica);
?>

<html >
    <head>
        <?php include "includes/head2.php" ?> 
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="caracteristicaslista.php">Gêneros</a> > <strong>Editar</strong>
                </nav>
                <form action="caracteristicaarqauxiliares/caracteristicacadastroeditar.php" method="post" id="cadastro">
                    <fieldset ><legend >Editar gênero</legend>
                        <ol>
                            <li>
                                <label>
                                    C&oacute;d:
                                </label>
                                <label class="Campos required">
                                    <?php echo $cd_caracteristica ?>
                                </label>
                                <input type="hidden" name="cd_caracteristica"  value="<?php echo $cd_caracteristica ?>">
                            </li>
                            <li>
                                <label>
                                    Caracteristica:
                                </label>
                                <input class="Campos required" name="nm_caracteristica" value="<?php echo $caracteristica->getNm_caracteristica() ?>">
                            </li>
                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                </form>


            </div>
        </section>
          <?php include "includes/rodape.php" ?> 
    </body>
</html>
