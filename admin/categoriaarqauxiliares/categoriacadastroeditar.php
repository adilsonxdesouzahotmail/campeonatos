<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
require_once "../../App_Code/Categorias.php";
$cd_categoria="";
$nm_categoria="";

if(isset($_POST["cd_categoria"]))
    $cd_categoria=$_POST["cd_categoria"];

if(isset($_POST["nm_categoria"]))
    $nm_categoria=$_POST["nm_categoria"];  
  


$categoria = new Categorias();

$categoria->setNm_categoria($nm_categoria);
$categoria->setCd_categoria($cd_categoria);

$categoria->Atualizar($categoria);

if($categoria->erro=="")
{
   echo "<script>alert('Dados da categoria atualizados com sucesso.');".
           "window.location= '../categoriaslista.php';</script>";
}
 else {
     ?>
        <script>alert("<?php echo $categoria->erro ?>");history.back();</script>
    <?php
    
}
    
?>


