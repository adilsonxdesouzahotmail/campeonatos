<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
require_once "../App_Code/Constantes.php";
require_once "../App_Code/Guia_categorias.php";
$cod_categoria="";
$cod_status ="";
$nome_categoria="";
$classe="";
if(isset($_POST["cod_categoria"]))
    $cod_categoria=$_POST["cod_categoria"];
if(isset($_POST["cod_status"]))
    $cod_status=$_POST["cod_status"];
if(isset($_POST["nome_categoria"]))
    $nome_categoria=$_POST["nome_categoria"];  
if(isset($_POST["classe"]))
    $classe=$_POST["classe"];  


$categoria = new Guia_categorias();
$categoria->setCod_empresa(cod_empresa);
$categoria->setCod_portal(cod_portal);
$categoria->setNome_categoria($nome_categoria);
$categoria->setCod_categoria($cod_categoria);
$categoria->setCod_status($cod_status);
$categoria->setNivel("3");
$categoria->setClasse($classe);
$categoria->setImagem("");
$categoria->Atualizar($categoria);

if($categoria->erro=="")
{
   echo "<script>alert('Dados da subcategoria atualizados com sucesso.');".
           "window.location= '../subcategorialistagem.php?cod_categoria=".substr($cod_categoria,0,5)."';</script>";
   
}
 else {
     ?>
        <script>alert("<?php echo $categoria->erro ?>");history.back();</script>
    <?php
    
}
    
?>


