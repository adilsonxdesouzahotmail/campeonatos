<?php
session_start();
require_once "includes/autentica.php";
$cd_noticia = "";
if (isset($_GET["cd_noticia"]))
    $cd_noticia = $_GET["cd_noticia"];
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="noticiaslista.php">noticias</a> > <strong>Editar</strong>
                </nav>

                <nav class="navabas">
                    <div class="aba2" onclick="window.location = 'noticiaedicao.php?cd_noticia=<?php echo $cd_noticia ?>'">
                        Notícia
                    </div>
                    <div class="aba" onclick="window.location = 'noticiaimagem1.php?cd_noticia=<?php echo $cd_noticia ?>'" >
                        Imagem 1
                    </div>
                    <div class="aba2" style="width: 120px;"
                         onclick="window.location = 'noticiagaleriafotos.php?cd_noticia=<?php echo $cd_noticia ?>'" >
                        Galeria de Fotos
                    </div>
                   
                </nav>
                <form action="noticiaarqauxiliares/noticiauploadimagem1.php" method="post"  
                      id="cadastro" enctype="multipart/form-data" onsubmit="return displayaguarde();">
                    <fieldset ><legend style="margin-left:30px"> Editar imagem 1</legend>
                        <input type="hidden" name="cd_noticia" value="<?php echo $cd_noticia ?>" />
                        <div class="containerfoto" id="containerfoto">
                            <div class="fotobanner" style="width:600px;"  align="center">
                                <?php
                                require_once "../App_Code/Noticias.php";
                                $noticia = new Noticias();
                                $noticia->getUmItem($cd_noticia);

                                if ($noticia->getNm_url_foto() != "" && $noticia->getNm_url_foto() != "semimagen.png") {

                                    echo " <img style='width:300px;height:300px;' src='../fotosnoticias/" . $cd_noticia . "/" . $noticia->getNm_url_foto() . "' alt='Imagem cadastrada' id='fotocad' />";
                                } else {
                                    echo " <img  src='imagens/semimagem.png' alt='Sem imagem cadastrada' />";
                                }
                                ?>

                            </div>
                            <div class="areaupload">

                                <input id="file" class="inputselecfoto" name="file"  type="file">

                                <input class="btnuploadfoto"  value="Upload" type="submit">
                                <div id="aguarde">Aguarde</div>
                            </div> 
                            <input type="hidden" name="nm_posicao_foto" value="teste<?php echo $noticia->getNm_posicao_foto();?>">
                        </div>
                    </fieldset>
                </form>


            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
