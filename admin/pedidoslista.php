﻿<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Pedidos.php";
require_once "../App_Code/Conexao.php";
require_once "../App_Code/Config.php";
$conexao = new Conexao();

$pedido = new Pedidos();
$acao = "";
if (isset($_GET["acao"]))
    $acao = $_GET["acao"];

$cd_pedido = "";
if (isset($_GET["cd_pedido"]))
    $cd_pedido = $_GET["cd_pedido"];

$cd_pedido_status = "";
if (isset($_GET["cd_pedido_status"]))
    $cd_pedido_status = $_GET["cd_pedido_status"];

if ($acao == "excluir") {
    $pedido->Excluir($cd_pedido);
    if ($pedido->erro == "")
        echo "<script> alert('Pedido exclu\u00CDdo com sucesso.'); window.location = 'pedidoslista.php';</script>";
    else {
        ?>
        <script >                alert("<?php echo $pedido->erro ?>");
            window.location = 'pedidoslista.php';</script>
        <?php
    }
}
?>

<html >
    <head>
        <?php include "includes/head2.php" ?> 
       
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <strong>Pedidos</strong>
                </nav>
              

                <!------------Busca inicio----------->
                <form action="" method="get"
                      onsubmit="return validaformpedidos();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Filtrar Resultados</legend>
                        <ol>
                            <li>
                                <label style="width:40px;">
                                    C&oacute;d:
                                </label>
                                <input class="Campos required" name="cd_pedido" id="cd_pedido" value="<?php echo $cd_pedido?>" style="width:85px;">
                                <label style="width:53px;margin-left:15px;">
                                    Status:
                                </label>
                                <select name="cd_pedido_status" id="cd_pedido_status"  class="Campos" >
                                    <option value="">Selecione</option>
                                    <?php
                                    
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from pedidos_status";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                        if ($row ["cd_pedido_status"] == $cd_pedido_status) {
                                            echo "<option value=" . $row ["cd_pedido_status"] . " selected>" . $row ["nm_pedido_status"] . "</option>";
                                        } else
                                            echo "<option value=" . $row ["cd_pedido_status"] . ">" . $row ["nm_pedido_status"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                                <input type="submit" class="btnenviar" value="Buscar" style="margin-left:10px;"/>
                            </li>
                           
                            
                        </ol>
                    </fieldset>
                </form>
                <!------------Busca Fim-------------->


                <?php include "pedidoarqauxiliares/pedidopaginacao.php" ?>  

                <table class="tbllistagem" id="tbllistagem" >
                    <tr>
                        <th style="width: 30px;">
                            C&oacute;d.
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Criado em 
                        </th>
                        <th style="width: 40px; text-align: center;">
                            &Uacute;lt. atualiza&ccedil;&atilde;o 
                        </th>
                        <th style="text-align: left;">
                           Cliente
                        </th>
                       <th style="text-align: center;">
                           Status
                        </th>
                        <th style="text-align: right;">
                           Valor
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Editar
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Excluir
                        </th>
                    </tr>
                    <?php
                    if($cd_cliente !="")
                    {
                        $subquery.="and cd_cliente=$cd_cliente";
                    }
                    $pedido->ListaTodosGridAdmin($limit, $subquery);
                    
                    for ($i = 0; $i < count($pedido->meuarray); $i++) {
                       
                        echo "<tr>";
                        echo "<td>" . $pedido->meuarray[$i]->getCd_pedido() . "</td>";
                        $data_criacao="";
                        $data_atualizacao="";
                        if ($pedido->meuarray[$i]->getDt_data_criacao() !="")
                            $data_criacao = date("d/m/Y", strtotime($pedido->meuarray[$i]->getDt_data_criacao()));
                        if ($pedido->meuarray[$i]->getDt_data_atualizacao() !="")
                            $data_atualizacao = date("d/m/Y", strtotime($pedido->meuarray[$i]->getDt_data_atualizacao()));
                        echo "<td align='center'>" .$data_criacao . "</td>";
                        echo "<td align='center'>" .$data_atualizacao . "</td>";
                        echo "<td style='padding:5px;'>" .$pedido->meuarray[$i]->getNm_cliente(). "</td>";
                        echo "<td align='center'>" .$pedido->meuarray[$i]->getNm_pedido_status(). "</td>";
                         echo "<td style='padding:5px;text-align:right;'>" ."R$ ".number_format($pedido->meuarray[$i]->getVl_total(), 2,',',''). "</td>";
                        echo"<td align='center'>";
                        echo "<a href='pedidoedicao.php?cd_pedido=" . $pedido->meuarray[$i]->getCd_pedido() . "'><img src='imagens/lapis.png'/></a>";
                        echo "</td>";

                        echo "<td align='center'>";
                        ?>
                            <a href="pedidoslista.php?cd_pedido=<?php echo $pedido->meuarray[$i]->getCd_pedido() ?>&acao=excluir"
                           onclick="return confirm('Confirma a exclus\u00E3o do Pedido <?php echo $pedido->meuarray[$i]->getCd_pedido() ?>?');">
                            <img src='imagens/lixeira.png' /></a>
                         <?php
                          echo "</td>";
                          echo "</tr>";
                        }
                        ?>
                </table>
           </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
