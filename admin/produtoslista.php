﻿<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Produtos.php";
require_once "../App_Code/Conexao.php";
require_once "../App_Code/Config.php";
$conexao = new Conexao();

$produto = new Produtos();
$acao = "";
if (isset($_GET["acao"]))
    $acao = $_GET["acao"];

$cd_roduto = "";
if (isset($_GET["cd_produto"]))
    $cd_produto = $_GET["cd_produto"];


if ($acao == "excluir") {
    $produto->Excluir($cd_produto);
    if ($produto->erro == "")
        echo "<script> alert('Produto exclu\u00CDdo com sucesso.'); window.location = 'produtoslista.php';</script>";
    else {
        ?>
        <script >                alert("<?php echo $produto->erro ?>");
            window.location = 'produtoslista.php';</script>
        <?php
    }
}
?>

<html >
    <head>
        <?php include "includes/head2.php" ?> 
          <script type="text/javascript">
            function carregasubcategorias()
            {
                var url = "subcategoriaarqauxiliares/carregasubcategorias.php";
                formdata = "cd_categoria=" + $("#cd_categoria").val();
                $.ajax({
                    url: url,
                    data: formdata,
                    type: 'POST',
                    dataType: 'html',
                    success: function(data) {
                        $("#cd_subcategoria").html("<option value='0'>Selecione</option>" + data);
                    }
                });

            }
        </script>
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <strong>Produtos</strong>
                </nav>
                <div class="contbtnnovo">
                    <input type="button" class="botaopadrao" value="Novo Produto" onclick="window.location = 'produtocadastro.php';" />
                </div>

                <!------------Busca inicio----------->
                <form action="" method="get"
                      onsubmit="return validaformprodutos();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Busca</legend>
                        <ol>
                            <li>
                                <label style="width:95px;">
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_produto" id="nm_produto" >
                                <label>
                                    Destaque:</label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_destaque"  style="margin-top: 8px;" />&nbsp;&nbsp;
                                N&atilde;o&nbsp;<input type="radio" value="0" name="ic_destaque" />&nbsp;&nbsp; 
                            </li>
                            Todos&nbsp;<input type="radio" value="3" name="ic_destaque" checked style="margin-top: 8px;"/>&nbsp;&nbsp;
                            </li>
                            <li>
                                <label style="width:95px;">
                                    Categoria:
                                </label>
                                <select name="cd_categoria" id="cd_categoria" onchange="carregasubcategorias();" class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from categorias";
                                    $rs = $mysqli->query($query);

                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_categoria"] . ">" . $row ["nm_categoria"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                                <label>
                                    Subcategoria:
                                </label>
                                <select name="cd_subcategoria" id="cd_subcategoria" class="Campos">
                                    <option value="0">Selecione</option>
                                </select>
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                </form>
                <!------------Busca Fim-------------->


                <?php include "produtoarqauxiliares/produtopaginacao.php" ?>  

                <table class="tbllistagem" id="tbllistagem" >
                    <tr>
                        <th style="width: 30px;">
                            C&oacute;d.
                        </th>
                        <th style="width: 200px;">
                            Nome
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Ativo
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Destaque
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Oferta
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Estoque
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Editar
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Excluir
                        </th>
                    </tr>
                    <?php
                    $produto->ListaTodosGridAdmin($limit, $subquery);
                    for ($i = 0; $i < count($produto->meuarray); $i++) {
                        echo "<tr>";
                        echo "<td>" . $produto->meuarray[$i]->getCd_produto() . "</td>";

                        echo "<td>" . $produto->meuarray[$i]->getNm_produto() . "</td>";

                        if ($produto->meuarray[$i]->getIc_ativo() != 0)
                            $checked = "checked";
                        else
                            $checked = "";
                        echo "<td align='center'><input type='checkbox' value=''" . $checked . " disabled /></td>";

                        if ($produto->meuarray[$i]->getIc_destaque() != 0)
                            $checked = "checked";
                        else
                            $checked = "";
                        echo "<td align='center'><input type='checkbox' value=''" . $checked . " disabled /></td>";

                        if ($produto->meuarray[$i]->getIc_oferta() != 0)
                            $checked = "checked";
                        else
                            $checked = "";
                        echo "<td align='center'><input type='checkbox' value=''" . $checked . " disabled /></td>";
                        
                        $style="";
//                        if ($produto->meuarray[$i]->getQt_estoque() <  $produto->meuarray[$i]->getQt_estoque_minimo())
//                            $style="style='color:red;'";
                        $qt_estoque = $produto->meuarray[$i]->getMlb_qt_estoque();
                        if($qt_estoque == "")
                            $qt_estoque="0";
                        $estilo="";
                        if($qt_estoque =="0")
                            $estilo = " style='color:red;' ";
                            
                        echo "<td align='center'><span ".$estilo." >".$qt_estoque."</span>";
                       
                        
                        echo"<td align='center'>";
                        echo "<a href='produtoedicao.php?cd_produto=" . $produto->meuarray[$i]->getCd_produto() . "'><img src='imagens/lapis.png'/></a>";
                        echo "</td>";

                        echo "<td align='center'>";
                        ?>
                        <a href="produtoslista.php?cd_produto=<?php echo $produto->meuarray[$i]->getCd_produto() ?>&acao=excluir"
                           onclick="return confirm('Confirma a exclus\u00E3o do Produto <?php echo $produto->meuarray[$i]->getNm_produto() ?>?\u000A Aten\u00E7\u00E3o: imagens e outras informa\u00E7\u00F5es associadas ao produto tamb\u00E9m ser\u0007\o excluidas.');">
                            <img src='imagens/lixeira.png' /></a>
                        <?php
                        echo "</td>";
                        echo "</tr>";
                        } // fecha for da linha 150
                        ?>
                </table>
           </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
