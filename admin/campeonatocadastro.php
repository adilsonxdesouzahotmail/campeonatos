<?php
error_reporting(E_ALL);
session_start();
require_once "includes/autentica.php";
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
        <script src="js/jquery.maskMoney.js" type="text/javascript"></script>

        <script src="ckeditor/ckeditor.js"></script>
        <style>
            .ckeditorcontainer{
                width:100%;float:left;clear:left;

            }

        </style>

    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="campeonatoslista.php">Campeonatos</a> > <strong>Cadastrar</strong>
                </nav>
                <nav class="navabas">
                    <div class="aba">
                        Campeonato
                    </div>
                    <div class="abadisabled" style="width: 120px;">
                        Galeria de Fotos
                    </div>
                    <div class="abadisabled" style="width: 120px;">
                        Clientes
                    </div>                    
                </nav>
                <form action="campeonatoarqauxiliares/campeonatocadastroinserir.php" method="post"
                      onsubmit="return validaformcampeonatos();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Cadastrar novo campeonato</legend>
                        <ol>
                            <li>
                                <label>
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_campeonato" id="nm_campeonato" >
                                <label>
                                    Ativo:</label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_ativo" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" value="0" name="ic_ativo" />&nbsp;&nbsp; 
                            </li>
                            <li>
                                <label>
                                    Jogo:
                                </label>
                                <select name="cd_jogo" id="cd_jogo"     class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from jogos";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_jogo"] . ">" . $row ["nm_jogo"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                                
                            </li>
                            <!---------------------->
                            <li>
                                <label>
                                    Plataformas:</label>
                                <table class="tblcaracteristicas" style="width:500px;">
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from plataformas";
                                    $rs = $mysqli->query($query);
                                    //require_once "../App_Code/Plataformas_jogo.php";
                                    echo "<tr>";
                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<td style='padding-right:10px;'>";
                                        echo "<input type='checkbox' value='" . $row ["cd_plataforma"] . "' name='cd_plataforma[]' />&nbsp;" . $row ["nm_plataforma"];
                                        echo "</td>";
                                    }
                                    echo "</tr>";
                                    $rs->free();
                                    ?>
                                </table>
                            </li>                            
                            <!---------------------->                             
                            <li>
                                <label>
                                    Qtd jogadores prevista:
                                </label>
                                <input class="Campos required" name="qt_participantes_previsto" id="qt_participantes_previsto" >
                                <label style="width: 100px;">
                                    Data prevista:
                                </label>
                                <input class="Campos required" name="dt_campeonato" id="dt_campeonato" style="width: 100px;">
                                <label style="width: 50px;">
                                    Hora:
                                </label>
                                <input class="Campos required" name="ds_hora_campeonato" id="ds_hora_campeonato" style="width: 100px;">
                            </li>                          
                            <!----------------------> 
                            
                            <li>
                                <label>
                                    Valor inscrição:
                                </label>
                                <input class="Campos required" name="vl_inscricao" id="vl_inscricao" >
                             </li> 
                             
                             <li>
                                <label>
                                    Qtd Jogadores/Time:
                                </label>
                                <select name="qt_jogadores_time" id="qt_jogadores_time"    class="Campos" >
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from times_grupos";
                                    $rs = $mysqli->query($query);
                                    while ($row = $rs->fetch_assoc()) {
                                          echo "<option value=" . $row ["qt_jogadores"] . ">" . $row ["nm_qtd_jogadores"] . "</option>";  
                                    }
                                    $rs->free();
                                    ?>
                                </select>                                          
                             </li>                             
                            <li style="margin-top: 10px;">
                                <label>
                                    Descrição:</label>
<!--                                <textarea id="txtdescricao" rows="5" class="Campos required" name="ds_campeonato"></textarea>-->
                                <div class="ckeditorcontainer">
                                    <textarea   id="editor1" name="ds_campeonato" rows="10" cols="80" >
                    
                                    </textarea>
                                </div>
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                    <script>
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('editor1');
                    </script>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
