<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Categorias.php";
$categoria = new Categorias();
$categoria->proxCdCategoria();
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="categoriaslista.php">Categorias</a> > <strong>Cadastrar</strong>
                </nav>
                <form action="categoriaarqauxiliares/categoriacadastroinserir.php" method="post" id="cadastro" onsubmit="return validaform();">
                    <fieldset ><legend >Cadastrar nova categoria</legend>
                        <ol>
                            <li>
                                <label>
                                    Cod:
                                </label>
                                <label class="Campos required"><?php echo $categoria->getProx_cd_categoria() ?></label>
                                <input type="hidden" name="cd_categoria" value="<?php echo $categoria->getProx_cd_categoria() ?>"/>
                            </li>
                            <li>
                                <label>
                                    Categoria:
                                </label>
                                <input class="Campos required" name="nm_categoria">
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
