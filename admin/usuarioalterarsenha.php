<?php
session_start();
require_once "includes/autentica.php";
$cd_usuario = "";
if (isset($_SESSION["cd_usuario"]))
    $cd_usuario = $_SESSION["cd_usuario"];

require_once "../App_Code/Usuarios.php";
$usuario = new Usuarios();
$usuario->getUmItem($cd_usuario);
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
        <script>
            function limpaerros()
            {
                $(".erro").hide();
            }
            function validaformusuario()
            {

                limpaerros();
                var ok = true;
                $nm_senha_atual = formusuario.nm_senha_atual.value;
                $nm_senha1 = formusuario.nm_senha1.value;
                $nm_senha2 = formusuario.nm_senha2.value;

                if ($nm_senha_atual == "")
                {
                    $("#erro_nm_senha_atual").show();
                    ok = false;
                }

                if ($nm_senha1 == "")
                {
                    $("#erro_nm_senha1").show();
                    ok = false;
                }
                if ($nm_senha2 == "")
                {
                    $("#erro_nm_senha2").show();
                    ok = false;
                }

                if (($nm_senha1 != "" && $nm_senha2 != "") && ($nm_senha1 != $nm_senha2))
                {

                    $("#erro_nm_senha22").show();
                    ok = false;
                }
                
                return ok;
            }

        </script>
    </head>
    <style>
        label
        {
            width:120px !important;
        }

    </style>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="usuarioslista.php">Usuarios</a> > <strong>Cadastrar</strong>
                </nav>
                <form action="usuarioarqauxiliares/usuarioalterarsenha.php" method="post" name="formusuario" id="cadastro" onsubmit=" return validaformusuario();">
                    <fieldset ><legend >Alterar Senha</legend>
                        <ol>
                            <li>
                                <label>
                                    Cod:
                                </label>
                                <label class="Campos required"><?php echo $usuario->getCd_usuario() ?></label>
                                <input type="hidden" name="cd_usuario" value="<?php echo $usuario->getCd_usuario() ?>"/>

                            </li>
                            <li>
                                <label>
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_usuario" disabled value="<?php echo $usuario->getNm_usuario(); ?>">
                            </li>
                            <li>
                                <label>
                                    E-mail:
                                </label>
                                <input class="Campos required" name="nm_email" disabled value="<?php echo $usuario->getNm_email(); ?>">
                            </li>
                            <li>
                                <label>
                                    Login:
                                </label>
                                <input class="Campos required" name="nm_login" disabled value="<?php echo $usuario->getNm_login(); ?>" >
                            </li>
                            <li>
                                <label>
                                    Nível:
                                </label>
                                <select name="qt_nivel"  disabled  class="Campos" >
                                    <option value="0" <?php if ($usuario->getQt_nivel() == 0) echo "selected"; ?>>
                                        Administrador
                                    </option>
                                    <option value="1" <?php if ($usuario->getQt_nivel() == 1) echo "selected"; ?>>
                                        Operador
                                    </option>
                                </select>
                            </li>
                            <li>
                                <label>
                                    Senha Atual:
                                </label>
                                <input type="password" class="Campos required" name="nm_senha_atual">
                                <label class="erro" id="erro_nm_senha_atual">
                                    Digite a senha
                                </label>
                            </li>
                            <li>
                                <label>
                                    Nova Senha:
                                </label>
                                <input type="password" class="Campos required" name="nm_senha1">
                                <label class="erro" id="erro_nm_senha1">
                                    Digite a senha
                                </label>
                            </li>
                            <li>
                                <label>
                                    Confirme a Senha:
                                </label>
                                <input type="password" class="Campos required" name="nm_senha2">
                                <label class="erro" id="erro_nm_senha2" style="width:200px !important;">
                                    Digite a confirmação da senha
                                </label>
                                <label class="erro" id="erro_nm_senha22" style="width:300px !important;">
                                    A senha e a confirmação devem coincidir
                                </label>
                            </li>
                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar"  />
                            </li>
                        </ol>
                    </fieldset>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
