<?php
session_start();
require_once "includes/autentica.php";


error_reporting(E_ALL);

require_once "../App_Code/NoticiasComentarios.php";
require_once "../App_Code/Conexao.php";
require_once "../App_Code/Config.php";
$conexao = new Conexao();

$cd_noticia = "";
if (isset($_GET["cd_noticia"]))
    $cd_noticia = $_GET["cd_noticia"];

                    
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 

    </head>
    <body >

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita"  >
            <div class="container">
                <nav class="breadcrumb">
                    <a href="noticiaslista.php">Noticias</a> > <strong>Editar</strong>
                </nav>
                <nav class="navabas">
                    <div class="aba2" onclick="window.location = 'noticiaedicao.php?cd_noticia=<?php echo $cd_noticia ?>'">
                        Notícia
                    </div>
                    <div class="aba2" style="width: 120px;"
                         onclick="window.location = 'noticiaimagem1.php?cd_noticia=<?php echo $cd_noticia ?>'" >
                        Imagem Título
                    </div>
                    
                    <div class="aba2" style="width: 120px;"
                         onclick="window.location = 'noticiagaleriafotos.php?cd_noticia=<?php echo $cd_noticia ?>'" >
                        Galeria de Fotos
                    </div>
                    <div class="aba" style="width: 120px;"
                         onclick="window.location = 'noticiacomentarios.php?cd_noticia=<?php echo $cd_noticia ?>'" >
                        Comentários
                    </div>                    
                </nav>
               

                <table class="tbllistagem" id="tbllistagem" >
                    <tr>
                        <th style="width: 30px;">
                            C&oacute;d.
                        </th>
                        <th style="width: 100px;">
                           Cliente
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Data
                        </th>                        
                        <th style="width: 40px; text-align: center;">
                            Lido
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Aprovado
                        </th>
                        
                         <th style="width: 40px; text-align: center;">
                            Editar
                        </th>                        
                       
                        <th style="width: 40px; text-align: center;">
                            Excluir
                        </th>
                        
                    </tr>
                    <?php


                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                    $mysqli->set_charset("utf8");
                    $query = "select noco.*,noti.nm_titulo,cli.nome_cliente,".
                            "(case  ".
                               "when noco.ic_lido is null then 0 ".
                               "else noco.ic_lido ".
                            "end) as lido, ".
                            "(case  ".
                            "   when noco.ic_aprovado is null then 0 ".
                            "   else noco.ic_aprovado ".
                            "end) as aprovado ".
                            "from noticias_comentarios noco ".
                            "join clientes cli on noco.cd_cliente = cli.cd_cliente ".
                            "join noticias noti on noco.cd_noticia = noti.cd_noticia ".
                            "where noti.cd_noticia=".$cd_noticia." order by lido";
                   // echo "<br/>".$query."<br/>";
                    $rs = $mysqli->query($query);

                       while ($row = $rs->fetch_assoc()) {
                        $estilocorlinha="";
                         if($row["lido"]==0 )
                        {
                            $estilocorlinha =" style='background-color:#4df380'";
                        }
                          echo "<tr ".$estilocorlinha.">";
                           echo "<td style='padding-right:10px;'>".$row["cd_noticia_comentario"]. "</td>";
                           echo "<td style='padding-right:10px;'>".$row["nome_cliente"]. "</td>";  
                           echo "<td style='padding-right:10px;'>".$row["dt_comentario"]. "</td>"; 
                        if ($row["lido"] != "0")
                            $checked = "checked";
                        else
                            $checked = "";
                        echo "<td align='center'><input type='checkbox' value=''" . $checked . " disabled /></td>";                           
                        if ($row["aprovado"] != "0")
                            $checked = "checked";
                        else
                            $checked = "";
                        echo "<td align='center'><input type='checkbox' value=''" . $checked . " disabled /></td>";
                       
                        echo"<td align='center'>";
                        echo "<a href='noticiacomentarioedicao.php?cd_noticia_comentario=" . $row["cd_noticia_comentario"] . "'><img src='imagens/lapis.png'/></a>";
                        echo "</td>";
                           
                        ?>
                        <td align='center'>
                        <a href="noticiacomentario.php?cd_noticia_comentario=<?php echo  $row["cd_noticia_comentario"] ?>&acao=excluir"
                           onclick="return confirm('Confirma a exclus\u00E3o do comentario <?php  $row["cd_noticia_comentario"] ?>?.');">
                            <img src='imagens/lixeira.png' /></a>
                        <?php
                        echo "</td>";                        
                          
                        echo "</tr>";
                        }

                    $rs->free();
                    ?>
                 
                </table>

            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
