
function limpaerros()
{
    $("input").css("background-color","transparent");
    $("select").css("background-color","transparent");
}
function validaformprodutos()
{
   limpaerros();
    var ok = true;
    var mensagem = "";
    var nm_produto =  $("#nm_produto").val();
    var cd_plataforma =  $("#cd_plataforma").val();
    //var cd_subplataforma = $("#cd_subplataforma").val();
    var vl_de = cadastro.vl_de.value;
    var vl_por = cadastro.vl_por.value;
    //var qt_peso = cadastro.qt_peso.value;
//    var qt_estoque = cadastro.qt_estoque.value;
//    var qt_estoque_minimo = cadastro.qt_estoque_minimo.value;
   
    if(nm_produto.trim()=="")
    {
         mensagem += "Informe o nome do produto.  \u000A";
         $("#nm_produto").css("background-color","#f3c6c6");
        ok = false;
    }
     if (cd_plataforma=="0") {
        mensagem += "Selecione a plataforma.  \u000A";
         $("#cd_plataforma").css("background-color","#f3c6c6");
        ok = false;
    }
//    if (cd_subplataforma=="0") {
//        mensagem += "Selecione a subplataforma.  \u000A";
//         $("#cd_subplataforma").css("background-color","#f3c6c6");
//        ok = false;
//    }
 
    vl_de = vl_de.replace("R$", "");
    vl_de = vl_de.replace(".", "");
    vl_de = vl_de.replace(",", ".");
    if (vl_de == "") {
        vl_de = "0.00";
         $("#vl_de").css("background-color","#f3c6c6");
        mensagem += "Digite o valor de. \u000A";
        ok = false;
    }
    cadastro.vl_dehd.value = vl_de;
     vl_por = vl_por.replace("R$", "");
    vl_por = vl_por.replace(".", "");
    vl_por = vl_por.replace(",", ".");
    
    if (vl_por == "") {
        vl_por = "0.00";
        $("#vl_por").css("background-color","#f3c6c6");
        mensagem += "Digite o valor de. \u000A";
        ok = false;
    }
    else
    { 
       
        if (parseInt(vl_por)>parseInt(vl_de))
        {
           
            $("#vl_por").css("background-color","#f3c6c6");
            mensagem += "Campo 'Valor (por)' não pode ser manior que  'Valor (de)'. \u000A";
            ok = false;
        }
     }
      cadastro.vl_porhd.value = vl_por;
    
//    if(isNaN(qt_peso.replace(",",".")) && qt_peso !="")
//     {
//           $("#qt_peso").css("background-color","#f3c6c6");
//            mensagem += "Digite Apenas números para peso. \u000A";
//            ok = false;
//     }
//     else
//     {
//         if(parseInt(qt_peso.replace(",",".")) != qt_peso && qt_peso !="")
//         {
//            $("#qt_estoque").css("background-color","#f3c6c6");
//            mensagem += "Digite Apenas valores inteiros para peso. \u000A";
//            ok = false;
//         }
//     }
     
//     if(isNaN(qt_estoque.replace(",",".")) && qt_estoque !="")
//     {
//           $("#qt_estoque").css("background-color","#f3c6c6");
//            mensagem += "Digite Apenas números para estoque. \u000A";
//            ok = false;
//     }
//     else
//     {
//         if(parseInt(qt_estoque.replace(",",".")) != qt_estoque && qt_estoque !="")
//         {
//            $("#qt_estoque").css("background-color","#f3c6c6");
//            mensagem += "Digite Apenas valores inteiros para estoque. \u000A";
//            ok = false;
//         }
//     }
//     
//    if(isNaN(qt_estoque_minimo.replace(",",".")) && qt_estoque_minimo !="")
//     {
//           $("#qt_estoque_minimo").css("background-color","#f3c6c6");
//            mensagem += "Digite Apenas números para estoque_minimo. \u000A";
//            ok = false;
//     }
//     else
//     {
//         if(parseInt(qt_estoque_minimo.replace(",",".")) != qt_estoque_minimo && qt_estoque_minimo !="")
//         {
//            $("#qt_estoque_minimo").css("background-color","#f3c6c6");
//            mensagem += "Digite Apenas valores inteiros para estoque_minimo. \u000A";
//            ok = false;
//         }
//     }
//     
     if (!ok) {
        alert(mensagem);
    }
    
    return ok;
}

function validaformjogos()
{
   limpaerros();
    var ok = true;
    var mensagem = "";
    var nm_jogo =  $("#nm_jogo").val();
    var cd_categoria =  $("#cd_categoria").val();
   
    if(nm_jogo.trim()=="")
    {
         mensagem += "Informe o nome do jogo.  \u000A";
         $("#nm_jogo").css("background-color","#f3c6c6");
        ok = false;
    }
     if (cd_categoria=="0") {
        mensagem += "Selecione a categoria.  \u000A";
         $("#cd_categoria").css("background-color","#f3c6c6");
        ok = false;
    }
     if (!ok) {
        alert(mensagem);
    }
    
    return ok;
}
 function displayaguarde() 
 {
     if($("#file").val())
     {
        $("#aguarde").show();
        return true;
     }
     else
     {
         alert("Selecione a imagem para upload");
         return false;
     }
 }
 
$(document).ready(function () {

    //***Acertar altura de conteudo de acordo com área disponível***
    alturatela = $(window).height() - 49;
    alturaesquerda = $("#colunaesquerda").height();
    if (alturaesquerda <= alturatela)
        $("#colunaesquerda").height(alturatela);
    larguradireita = $("#topointeiro").width() - 235;
    //alert($("#topointeiro").width());
    $("#colunadireita").width(larguradireita);

    alturaesquerda = $("#colunaesquerda").height();
    alturadireita = $("#colunadireita").height();
    //alert("dir "+alturadireita+" esq"+alturaesquerda);
    if (alturadireita >= alturaesquerda)
        $("#colunaesquerda").height(alturadireita);

});