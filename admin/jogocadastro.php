<?php
error_reporting(E_ALL);
session_start();
require_once "includes/autentica.php";
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
      
          <script src="ckeditor/ckeditor.js"></script>
        <style>
        .ckeditorcontainer{
            width:100%;float:left;clear:left;

        }

        </style>
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="jogoslista.php">Jogos</a> > <strong>Cadastrar</strong>
                </nav>
                <nav class="navabas">
                    <div class="aba">
                        Jogo
                    </div>
                    <div class="abadisabled" style="width: 120px;">
                        Galeria de Fotos
                    </div>                  
                </nav>
                <form action="jogoarqauxiliares/jogocadastroinserir.php" method="post"
                      onsubmit="return validaformjogos();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Cadastrar novo jogo</legend>
                        <ol>
                            <li>
                                <label>
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_jogo" id="nm_jogo" >
                                <label>
                                    Ativo:</label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_ativo" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" value="0" name="ic_ativo" />&nbsp;&nbsp; 
                            </li>
                            <li>
                               
                                 <label>
                                    Categoria:
                                </label>
                                <select name="cd_categoria" id="cd_categoria"  class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from categorias";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_categoria"] . ">" . $row ["nm_categoria"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                            </li>

                            <li style="margin-top: 10px;">
                                <label>
                                    Descrição:</label>
<!--                                <textarea id="txtdescricao" rows="5" class="Campos required" name="ds_jogo"></textarea>-->
                              <div class="ckeditorcontainer">
                                    <textarea   id="editor1" name="ds_jogo" rows="10" cols="80" >
                    
                                    </textarea>
                                </div>
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                     <script>
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('editor1');
                    </script>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
