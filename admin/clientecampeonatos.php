<?php
session_start();
require_once "includes/autentica.php";


error_reporting(E_ALL);

require_once "../App_Code/Clientes.php";
require_once "../App_Code/Conexao.php";
require_once "../App_Code/Config.php";
$conexao = new Conexao();

$cliente = new Clientes();

$cd_cliente = "";
if (isset($_GET["cd_cliente"]))
    $cd_cliente = $_GET["cd_cliente"];

     require_once "../App_Code/Clientes.php";
      $cliente = new Clientes();
     $cliente->getUmItem($cd_cliente);                    
                    
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 

    </head>
    <body >

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita"  >
            <div class="container">
             <nav class="breadcrumb">
                <a href="clienteslista.php">Clientes</a> > <strong>Campeonatos</strong>
            </nav>
            <nav class="navabas">
                <div class="aba2" onclick="window.location='clientedetalhe.php?cd_cliente=<?php echo $cd_cliente;?>';">
                      Dados do cliente
                </div>
                <div class="aba"  >
                    Campeonatos do cliente <?php echo $cliente->getNome_cliente(); ?>
                </div>
               
            </nav>
               

                <table class="tbllistagem" id="tbllistagem" >
                    <tr>
                        <th style="width: 30px;">
                            C&oacute;d.
                        </th>
                        <th style="width: 100px;">
                            Campeonato
                        </th>
                        <th style="width: 40px; text-align: center;">
                            Data
                        </th>      
                        <th style="width: 40px; text-align: center;">
                            Jogo
                        </th>                         
                    </tr>
                    <?php


                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                    $mysqli->set_charset("utf8");
                    $query = "select ca.cd_campeonato,ca.nm_campeonato,ca.dt_campeonato,j.nm_jogo from ".
                             "clientes_campeonatos clica ".
                             "join campeonatos ca on clica.cd_campeonato = ca.cd_campeonato ".
                             "join jogos j on ca.cd_jogo = j.cd_jogo ".
                             "where clica.cd_cliente = ".$cd_cliente;
                   // echo "<br/>".$query."<br/>";
                    $rs = $mysqli->query($query);

                       while ($row = $rs->fetch_assoc()) {
                          echo "<tr>";
                           echo "<td style='padding-right:10px;'>".$row["cd_campeonato"]. "</td>";
                           echo "<td style='padding-right:10px;'>".$row["nm_campeonato"]. "</td>";    
                           echo "<td style='padding-right:10px;'>".$row["dt_campeonato"]. "</td>";     
                           echo "<td style='padding-right:10px;'>".$row["nm_jogo"]. "</td>";                                   
                          echo "</tr>";
                        }

                    $rs->free();
                    ?>
                 
                </table>

            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
