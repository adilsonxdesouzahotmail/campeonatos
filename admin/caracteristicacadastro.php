<?php
session_start();
require_once "includes/autentica.php";
?>

<html >
    <head>
        <?php include "includes/head2.php" ?> 
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="caracteristicaslista.php">Gêneros</a> > <strong>Cadastrar</strong>
                </nav>
                <form action="caracteristicaarqauxiliares/caracteristicacadastroinserir.php" method="post" id="cadastro">
                    <fieldset ><legend >Cadastrar novo gênero</legend>
                        <ol>

                            <li>
                                <label>
                                    Gênero:
                                </label>
                                <input class="Campos required" name="nm_caracteristica">
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                </form>


            </div>
        </section>
         <?php include "includes/rodape.php" ?> 
    </body>
</html>
