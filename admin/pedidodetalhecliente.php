<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();
require_once "../App_Code/Pedidos.php";
$pedido = new Pedidos();

require_once "../App_Code/PedidoDetalhes.php";
$pedidodetalhe = new PedidoDetalhes();

$cd_pedido = "";
if (isset($_GET["cd_pedido"]))
    $cd_pedido = $_GET["cd_pedido"];

//$pedido->getUmItem($cd_pedido);

require_once "../App_Code/Clientes.php";
$cliente = new Clientes();
$cd_cliente = "";
if (isset($_GET["cd_cliente"]))
    $cd_cliente = $_GET["cd_cliente"];

$cliente->getUmItem($cd_cliente);
if($cliente->erro!="")
    echo "erro:".$cliente->erro;
?>
?>

<html>
<head >
    <?php include "includes/head2.php" ?> 
    
</head>
<body>
     <?php include "includes/topoelateral.php" ?> 
    <section id="colunadireita">
          <div class="container">
           
            <nav class="breadcrumb">
                <a href="pedidoslista.php">Pedidos</a> > <strong>Consultar Dados</strong>
            </nav>
            <nav class="navabas">
                 <div class="aba2" onclick="window.location='pedidoedicao.php?&cd_pedido=<?php echo $cd_pedido; ?>';" >
                    Dados do pedido <?php echo $cd_pedido; ?>
                </div>
                <div class="aba" >
                    Dados do Cliente
                </div>
                 
               
            </nav>
             <?php include "clientearqauxiliares/clientedetalhe.php" ?>
        </div>
    </section>
    <?php include "includes/rodape.php" ?> 
</body>
</html>
