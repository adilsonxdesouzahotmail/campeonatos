<?php
error_reporting(E_ALL);
session_start();
require_once "includes/autentica.php";
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
        <script src="js/jquery.maskMoney.js" type="text/javascript"></script>
      
          <script src="ckeditor/ckeditor.js"></script>
        <style>
        .ckeditorcontainer{
            width:100%;float:left;clear:left;

        }

        </style>
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="produtoslista.php">Produtos</a> > <strong>Cadastrar</strong>
                </nav>
                <nav class="navabas">
                    <div class="aba">
                        Dados
                    </div>
                    <div class="abadisabled" style="width: 120px;">
                        Galeria de Fotos
                    </div>
                    <div class="abadisabled" style="width: 120px;">
                        Mercado Livre
                    </div>                    
                </nav>
                <form action="produtoarqauxiliares/produtocadastroinserir.php" method="post"
                      onsubmit="return validaformprodutos();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Cadastrar novo produto</legend>
                        <ol>
                            <li>
                                <label>
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_produto" id="nm_produto" >
                                <label>
                                    Ativo:</label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_ativo" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" value="0" name="ic_ativo" />&nbsp;&nbsp; 
                            </li>
                            <li>
                                <label>
                                    Plataforma:
                                </label>
                                <select name="cd_plataforma" id="cd_plataforma"  class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from plataformas";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<option value=" . $row ["cd_plataforma"] . ">" . $row ["nm_plataforma"] . "</option>";
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                               
                            </li>
                            <li>
                                <label>
                                    Características:</label>
                                <table class="tblcaracteristicas">
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from caracteristicas";
                                    $rs = $mysqli->query($query);
                                    $cont = 1;
                                    echo "<tr>";
                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<td  style='height:35px;'>";
                                        echo "<input type='checkbox' value='" . $row ["cd_caracteristica"] . "' name='cd_caracteristica[]' />&nbsp;" . $row ["nm_caracteristica"] . "&nbsp;";
                                        echo "</td>";
                                        if ($cont % 5 == 0)
                                            echo "</tr><tr>";
                                        $cont++;
                                    }
                                    echo "</tr>";
                                    $rs->free();
                                    ?>
                                </table>
                            </li>
                            <!---------------------->
                            <li>
                                <label>
                                    Idiomas:</label>
                                <table class="tblcaracteristicas">
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from idiomas";
                                    $rs = $mysqli->query($query);
                                    $cont = 1;
                                    echo "<tr>";
                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<td style='padding-right:10px;'>";
                                        echo "<input type='checkbox' value='" . $row ["cd_idioma"] . "' name='cd_idioma[]' />&nbsp;<img style='width: 30px;height: 22px;' src='../imagens/flags/" . $row ["url_flag"] . "' />&nbsp;&nbsp;";
                                        echo "</td>";
                                        if ($cont % 5 == 0)
                                            echo "</tr><tr>";
                                        $cont++;
                                    }
                                    echo "</tr>";
                                    $rs->free();
                                    ?>
                                </table>
                            </li>                            
                            <!----------------------> 
                            <li>
                                <label>
                                    legendas:</label>
                                <table class="tblcaracteristicas">
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from legendas";
                                    $rs = $mysqli->query($query);
                                    $cont = 1;
                                    echo "<tr>";
                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<td style='padding-right:10px;'>";
                                        echo "<input type='checkbox' value='" . $row ["cd_legenda"] . "' name='cd_legenda[]' />&nbsp;<img style='width: 30px;height: 22px;' src='../imagens/flags/" . $row ["url_flag"] . "' />&nbsp;&nbsp;";
                                        echo "</td>";
                                        if ($cont % 5 == 0)
                                            echo "</tr><tr>";
                                        $cont++;
                                    }
                                    echo "</tr>";
                                    $rs->free();
                                    ?>
                                </table>
                            </li>                            
                            <!----------------------> 
                            <li style="margin-top: 10px;">
                                <label>
                                    Descrição:</label>
<!--                                <textarea id="txtdescricao" rows="5" class="Campos required" name="ds_produto"></textarea>-->
                              <div class="ckeditorcontainer">
                                    <textarea   id="editor1" name="ds_produto" rows="10" cols="80" >
                    
                                    </textarea>
                                </div>
                            </li>
                            <li>
                                <label>
                                    Valor (de):
                                </label>
                                <input class="Campos required" name="vl_de" id="vl_de">
                                <input type="hidden" name="vl_dehd" id="vl_dehd">
                                <script type="text/javascript">  $("#vl_de").maskMoney({prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: true});</script>
                                <label>
                                    Valor (por):
                                </label>
                                <input class="Campos required" name="vl_por" id="vl_por">
                                <input type="hidden" name="vl_porhd" id="vl_porhd">
                                <script type="text/javascript">  $("#vl_por").maskMoney({prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: true});</script>
                            </li>
                    <li>
                        <label>
                            MLB Val. Min.:
                           
                        </label>
                        <input class="Campos required"  disabled  name="mlb_preco_min" id="mlb_preco_min" value="R$ 0,00">
                       
                        <label>
                            MLB Val. Max:
                        </label>
                        <input class="Campos required"  disabled  name="mlb_preco_max" id="mlb_preco_max" value="R$ 0,00">
                       
                    </li>                    
                    
                    <li>
                        <label>
                            MLB Estoque.:
                           
                        </label>
                        <input class="Campos required"  disabled  name="mlb_qt_estoque" id="mlb_qt_estoque" value="0">
           
                       
                    </li>                                 
<!--                            <li>
                                <label>
                                    Peso (gramas):
                                </label>
                                <input class="Campos required" name="qt_peso" id="qt_peso"    >
                            </li>-->

<!--                            <li>
                                <label>
                                    Estoque:
                                </label>
                                <input class="Campos required" name="qt_estoque" id="qt_estoque">

                                <label>
                                    Estoque min.:
                                </label>
                                <input class="Campos required" name="qt_estoque_minimo" id="qt_estoque_minimo">
                            </li>-->

                            <li>
                                <label>
                                    Destaque:
                                </label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_destaque" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" value="0" name="ic_destaque" />&nbsp;&nbsp; 
                            </li>
                            <li>
                                <label>
                                    Oferta:
                                </label>
                                Sim&nbsp;<input type="radio" value="1" name="ic_oferta" style="margin-top: 8px;" />&nbsp;&nbsp;
                                Não&nbsp;<input type="radio" value="0" name="ic_oferta" checked  />&nbsp;&nbsp; 
                            </li>
                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                     <script>
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('editor1');
                    </script>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
