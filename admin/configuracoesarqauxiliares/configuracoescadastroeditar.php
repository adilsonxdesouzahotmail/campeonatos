<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
require_once "../../App_Code/Configuracoes.php";

$nm_telefones_linha1="";
if(isset($_POST["nm_telefones_linha1"]))
    $nm_telefones_linha1=$_POST["nm_telefones_linha1"];  

$nm_telefones_linha2="";
if(isset($_POST["nm_telefones_linha2"]))
    $nm_telefones_linha2=$_POST["nm_telefones_linha2"];

$nm_endereco_linha1="";
if(isset($_POST["nm_endereco_linha1"]))
    $nm_endereco_linha1=$_POST["nm_endereco_linha1"]; 

$nm_endereco_linha2="";
if(isset($_POST["nm_endereco_linha2"]))
    $nm_endereco_linha2=$_POST["nm_endereco_linha2"]; 

$nm_email="";
if(isset($_POST["nm_email"]))
    $nm_email=$_POST["nm_email"];  

$nm_url_facebook="";
if(isset($_POST["nm_url_facebook"]))
    $nm_url_facebook=$_POST["nm_url_facebook"]; 

$nm_url_twitter="";
if(isset($_POST["nm_url_twitter"]))
    $nm_url_twitter=$_POST["nm_url_twitter"]; 


$configuracoes = new Configuracoes();
$configuracoes->setNm_telefones_linha1($nm_telefones_linha1);
$configuracoes->setNm_telefones_linha2($nm_telefones_linha2);
$configuracoes->setNm_endereco_linha1($nm_endereco_linha1);
$configuracoes->setNm_endereco_linha2($nm_endereco_linha2);
$configuracoes->setNm_email($nm_email);
$configuracoes->setNm_url_facebook($nm_url_facebook);
$configuracoes->setNm_url_twitter($nm_url_twitter);
 
$configuracoes->Atualizar($configuracoes) ;

if($configuracoes->erro=="")
{
   echo "<script>alert('Cadastro atualizado com sucesso.');".
           "window.location= '../configuracoes.php';</script>";
}
 else {
     ?>
        <script>alert("<?php echo $configuracoes->erro ?>");history.back();</script>
    <?php
    
}
    
?>

