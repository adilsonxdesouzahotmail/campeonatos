<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();

$acao = "";
if (isset($_GET["acao"]))
    $acao = $_GET["acao"];


$cd_noticia = "";
if (isset($_GET["cd_noticia"]))
    $cd_noticia = $_GET["cd_noticia"];

require_once "../App_Code/Noticias.php";
$noticia = new Noticias();
if ($acao == "excluir") {

  
    $noticia->Excluir($cd_noticia);

    if ($noticia->erro == "") {
        echo "<script>alert('Noticia excluída com sucesso.');window.location='noticiaslista.php?cd_noticia=" . $cd_noticia . "';</script>";
    } else {
        ?>
        <script >alert("<?php echo $noticia->erro ?>");
            window.location = "noticiaslista.php?cd_noticia="<?php echo $cd_noticia ?>;</script>
        <?php
    }
    // header("Location:noticialista.php");
}
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <div style="width: 100%; float: left; clear: left;">

                  
                    <nav class="breadcrumb">
                        <strong>Noticias</strong>
                    </nav>
                    <div class="contbtnnovo">
                         <input type="button" class="botaopadrao" value="NOVA NOTICIA" onclick="window.location = 'noticiacadastro.php';" />
                       
                    </div>
                    <table class="tbllistagem" id="tbllistagem" style="width:100%; float: left; clear: left;">
                        <tr>
                            <th style="width: 30px;">
                                Cod.
                            </th>
                            <th style="width: 50px; text-align: center;">
                                Título
                            </th>
                            <th style="width: 50px; text-align: center;">
                                Tem Comentário
                            </th>
                            <th style="width: 50px; text-align: center;">
                                Tem Comentário não lido
                            </th>                            
                            <th style="width: 50px; text-align: center;">
                                Editar
                            </th>
                            <th style="width: 50px; text-align: center;">
                                Excluir
                            </th>
                        </tr>

                        <?php
                        $noticia->getTodosItens("");
                        for ($i = 0; $i < count($noticia->meuarray); $i++) {
                            
                        $estilocorlinha="";
                         if($noticia->meuarray[$i]->getQt_comentario()!= 0 && $noticia->meuarray[$i]->getQt_comentario_nao_lido()!= 0 )
                        {
                            $estilocorlinha =" style='background-color:#4df380'";
                        }
                          echo "<tr ".$estilocorlinha.">";
                          echo "<td>" . $noticia->meuarray[$i]->getCd_noticia() . "</td>";
                            echo "<td>" . $noticia->meuarray[$i]->getNm_titulo() . "</td>";

                        $checked = "";                            
                        if ($noticia->meuarray[$i]->getQt_comentario()!= 0)
                            $checked = "checked";

                        echo "<td align='center'><input type='checkbox' value=''" . $checked . " disabled /></td>";
    
                        $checked = "";                           
                        if ($noticia->meuarray[$i]->getQt_comentario_nao_lido()!= 0)
                            $checked = "checked";

                        echo "<td align='center'><input type='checkbox' value=''" . $checked . " disabled /></td>";                        
                        
         
                            echo "<td align='center' >";
                            echo "<a href='noticiaedicao.php?cd_noticia=" . $noticia->meuarray[$i]->getCd_noticia() . "'><img src='imagens/lapis.png'/></a>";
                            echo "</td>";

                            echo "<td align='center'>";
                            ?>
                            <a href="noticiaslista.php?cd_noticia=<?php echo $noticia->meuarray[$i]->getCd_noticia() ?>&acao=excluir"
                               onclick="return confirm('Confirma a exclusão da noticia <?php echo $noticia->meuarray[$i]->getCd_noticia() ?>?');">
                                <img src='imagens/lixeira.png' /></a>
                            <?php
                            echo "</td>";
                            echo "</tr>";
                        }
                        ?>


                    </table>

                </div>

            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
