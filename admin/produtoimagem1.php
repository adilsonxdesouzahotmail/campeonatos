<?php
session_start();
require_once "includes/autentica.php";
$cd_produto = "";
if (isset($_GET["cd_produto"]))
    $cd_produto = $_GET["cd_produto"];
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="produtoslista.php">Produtos</a> > <strong>Editar</strong>
                </nav>

                <nav class="navabas">
                    <div class="aba2" onclick="window.location = 'produtoedicao.php?cd_produto=<?php echo $cd_produto ?>'">
                        Dados
                    </div>
                    <div class="aba" onclick="window.location = 'produtoimagem1.php?cd_produto=<?php echo $cd_produto ?>'" >
                        Imagem 1
                    </div>
                    <div class="aba2" onclick="window.location = 'produtoimagem2.php?cd_produto=<?php echo $cd_produto ?>'">
                        Imagem 2
                    </div>
                    <div class="aba2" style="width: 200px;"
                         onclick="window.location = 'produtogaleriafotos.php?cd_produto=<?php echo $cd_produto ?>'" >
                        Galeria de Fotos
                    </div>
                </nav>
                <form action="produtoarqauxiliares/produtouploadimagem1.php" method="post"  
                      id="cadastro" enctype="multipart/form-data" onsubmit="return displayaguarde();">
                    <fieldset ><legend style="margin-left:30px"> Editar imagem 1</legend>
                        <input type="hidden" name="cd_produto" value="<?php echo $cd_produto ?>" />
                        <div class="containerfoto" id="containerfoto">
                            <div class="fotobanner" style="width:600px;"  align="center">
                                <?php
                                require_once "../App_Code/Produtos.php";
                                $produto = new Produtos();
                                $produto->getUmItem($cd_produto);

                                if ($produto->getNm_url_1() != "" && $produto->getNm_url_1() != "semimagen.png") {

                                    echo " <img  src='../fotos/" . $cd_produto . "/" . $produto->getNm_url_1() . "' alt='Imagem cadastrada' id='fotocad' />";
                                } else {
                                    echo " <img  src='imagens/semimagem.png' alt='Sem imagem cadastrada' />";
                                }
                                ?>

                            </div>
                            <div class="areaupload">

                                <input id="file" class="inputselecfoto" name="file"  type="file">

                                <input class="btnuploadfoto"  value="Upload" type="submit">
                                <div id="aguarde">Aguarde</div>
                            </div> 
                        </div>
                    </fieldset>
                </form>


            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
