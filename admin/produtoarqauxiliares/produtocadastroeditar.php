<?php
session_start();
require_once "../includes/autentica.php";
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php

require_once "../../App_Code/Produtos.php";
$produto = new Produtos();
$produto->setCd_produto($_POST["cd_produto"]);
$produto->setNm_produto($_POST["nm_produto"]);
$produto->setCd_plataforma($_POST["cd_plataforma"]);
//$produto->setNm_campo_extra_1($_POST["nm_campo_extra_1"]);
//$produto->setNm_campo_extra_2($_POST["nm_campo_extra_2"]);
$produto->setDs_produto($_POST["ds_produto"]);
$produto->setVl_de($_POST["vl_dehd"]);
$produto->setVl_por($_POST["vl_porhd"]);
$produto->setIc_destaque($_POST["ic_destaque"]);
$produto->setIc_oferta($_POST["ic_oferta"]);
$produto->setIc_ativo($_POST["ic_ativo"]);
//$produto->setQt_peso($_POST["qt_peso"]);
//$produto->setQt_estoque($_POST["qt_estoque"]);
//$produto->setQt_estoque_minimo($_POST["qt_estoque_minimo"]);
$produto->Atualizar($produto);

$erroaux ="";
if ($produto->erro == "") 
{
    require_once "../../App_Code/Caracteristicas_produto.php";
    $caracprod = new Caracteristicas_produto();
    $caracprod->Excluir($produto->getCd_produto());
    
    $caracs="";
    if(isset($_POST["cd_caracteristica"]))
        $caracs = $_POST["cd_caracteristica"];
    if($caracs!="")
    {
        
        $caracprod->Atualizar($produto->getCd_produto(), $caracs);
        //echo "categs".$categs;
        if ($caracprod->erro != "") 
        {
           $erroaux = "características";
        }
//        if ($caracprod->erro == "") 
//        {
//              //---------- Tamanhos Inicio--------------/
//              $tamanhos = $_POST["cd_tamanho"];
//              $tamanhos_qtd = $_POST["cd_tamanho_qtd"];
//              
//              require_once "../../App_Code/Tamanhos_produto.php";
//              $tamanhoprod = new Tamanhos_produto();
//              $tamanhoprod->Excluir($produto->getCd_produto());
//            
//              //$tamanhoprod ->Atualizar($produto->getCd_produto(), $tamanhos);
//              $tamanhoprod ->Atualizar2($produto->getCd_produto(), $tamanhos,$tamanhos_qtd);
//              if ($tamanhoprod->erro == "") 
//             {
//                  ?>
                   <!--<script >//alert("//<?php //echo "erro tamanho".$tamanhoprod->erro ?>");</script>-->
                    //<?php
//              }
//            
//            //---------- Tamanhos Fim-----------------/
//            echo "<script>alert('Produto Atualizado com sucesso.');window.location='../produtoedicao.php?cd_produto=" . $produto->getCd_produto() . "';</script>";
//        } 
//        else 
//        {
//            ?>
                <script >//alert("<?php echo "erro prod atu".$caracprod->erro ?>");</script>
           //<?php
//         }
     }
    
    $idiomas="";
    
    require_once "../../App_Code/Idiomas_produto.php";
    $idiomaprod = new Idiomas_produto();
    $idiomaprod->Excluir($produto->getCd_produto());
    
    if(isset($_POST["cd_idioma"]))
        $idiomas = $_POST["cd_idioma"];
    if($idiomas!="")
    {
        $idiomaprod->Atualizar($produto->getCd_produto(), $idiomas);
        //echo "categs".$categs;
        if ($idiomaprod->erro != "") 
        {
           $erroaux.= " ,idiomas";
        }
    }
    //--------Legendas ------------
        $legendas="";
    
        require_once "../../App_Code/Legendas_produto.php";
        $legendaprod = new Legendas_produto();
        $legendaprod->Excluir($produto->getCd_produto());

        if(isset($_POST["cd_legenda"]))
            $legendas = $_POST["cd_legenda"];
        if($legendas!="")
        {
            $legendaprod->Atualizar($produto->getCd_produto(), $legendas);
            //echo "categs".$categs;
            if ($legendaprod->erro != "") 
            {
               $erroaux.= " ,legendas";
            }
        }
     if($erroaux !="")
     {
      ?>
            <script >alert("Produto foi atualizado maa houve erro em :<?php echo $erroaux ?>");history.back();</script>
       <?php
     }
     else {
        ?>
            <script >alert("Produto foi atualizado com sucesso");history.back();</script>
       <?php 
     }
} 
else 
{
    ?>
            <script >alert("<?php echo $produto->erro ?>");history.back();</script>
    <?php
}
?>

