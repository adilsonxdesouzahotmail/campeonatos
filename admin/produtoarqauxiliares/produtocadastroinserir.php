<?php
session_start();
require_once "../includes/autentica.php";
?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
//echo $_POST["cd_caracteristica"];
//echo implode(",", $_POST["cd_caracteristica"]);

require_once "../../App_Code/Produtos.php";
$produto = new Produtos();
$produto->setNm_produto($_POST["nm_produto"]);
$produto->setCd_plataforma($_POST["cd_plataforma"]);
$produto->setDs_produto($_POST["ds_produto"]);
$produto->setVl_de($_POST["vl_dehd"]); 
$produto->setVl_por($_POST["vl_porhd"]);
$produto->setIc_destaque($_POST["ic_destaque"]);
$produto->setIc_oferta($_POST["ic_oferta"]);
$produto->setIc_ativo($_POST["ic_ativo"]);
$produto->Inserir($produto);


if ($produto->erro == "") 
{
    $categs="";
    if(isset($_POST["cd_caracteristica"]))
        $categs = $_POST["cd_caracteristica"];
    if ($categs != "") 
    {
        $categs = $_POST["cd_caracteristica"];
        require_once "../../App_Code/Caracteristicas_produto.php";
        $caracprod = new Caracteristicas_produto();
        $caracprod->Atualizar($produto->UltimoProduto(), $categs);
        if ($caracprod->erro != "") 
        {
           
            ?>
               <script >alert("<?php echo "Erro no cadastro das características".$caracprod->erro ?>");</script>
            <?php
        } 
        
    }
      //-----------Idiomas----------------------/
    $idiomas="";
    if(isset($_POST["cd_idioma"]))
        $idiomas = $_POST["cd_idioma"];
    if ($idiomas != "") 
    {
        $idiomas = $_POST["cd_idioma"];
        require_once "../../App_Code/Idiomas_produto.php";
        $idiomaprod = new Idiomas_produto();
        $idiomaprod->Atualizar($produto->UltimoProduto(), $idiomas);
        if ($idiomaprod->erro != "") 
        {
           
            ?>
               <script >alert("<?php echo "Erro no cadastro dos idiomas".$caracprod->erro ?>");</script>
            <?php
        } 
        
    }
          //-----------legendas----------------------/
        $legendas="";
        if(isset($_POST["cd_legenda"]))
            $legendas = $_POST["cd_legenda"];
        if ($legendas != "") 
        {
            $legendas = $_POST["cd_legenda"];
            require_once "../../App_Code/Legendas_produto.php";
            $legendaprod = new Legendas_produto();
            $legendaprod->Atualizar($produto->UltimoProduto(), $legendas);
            if ($legendaprod->erro != "") 
            {

                ?>
                   <script >alert("<?php echo "Erro no cadastro dos legendas".$caracprod->erro ?>");</script>
                <?php
            } 

        }
        echo "<script>alert('Produto cadastrado com sucesso.');window.location='../produtoedicao.php" .
        "?cd_produto=" . $produto->UltimoProduto() . "';</script>";
}
else 
{
    ?>
            <script >alert("<?php echo $produto->erro ?>");history.back();</script>
    <?php
}


?>
