<?php
//******** Configurações ***************/
require_once "../../App_Code/Constantes.php";
$largura = imagem1_largura;
$altura = imagem1_altura;
$arquivodestino= "imagem1.png";
$paginaorigem = "produtoimagem1";
//**************************************/

    $cd_produto="";
     if(isset($_POST["cd_produto"]))
         $cd_produto=$_POST["cd_produto"];
     
     $pastafotosproduto = "../../fotos/".$cd_produto."/";
     
    if (!file_exists($pastafotosproduto)) {
        mkdir($pastafotosproduto, 0777, true);
    }
   
    
require_once "../../App_Code/resizepicture.php";

$erroresize="";
if (is_uploaded_file($_FILES['file']['tmp_name'])) {
    copy($_FILES['file']['tmp_name'], $pastafotosproduto."foto1prov.png");
} else {
    $erroresize= "Arquivo não aceito";
}
//indicate which file to resize (can be any type jpg/png/gif/etc...)

if($erroresize=="")
{
    $file = $pastafotosproduto.'foto1prov.png';

    //indicate the path and name for the new resized file

    $resizedFile = $pastafotosproduto.$arquivodestino;
    require_once "../../App_Code/Produtos.php";
    $produto= new Produtos();
    $produto->getUmItem($cd_produto);
    $produto->setNm_url_1($arquivodestino);
    $produto->Atualizar($produto);
    

    smart_resize_image($file, $largura, $altura, true, $resizedFile, false, false, 50);
   
    if($produto->erro=="")
    {
       echo "<script>alert('Imagem atualizada com sucesso.');".
               "window.location= '../".$paginaorigem.".php?cd_produto=".$cd_produto."';</script>";
    }
     else {
         ?>
            <script>//alert(<?php echo $local->erro ?>");history.back();</script>
        <?php

    }
}
 else 
{
    echo "<script>alert('".$erroresize."'>);history.back();</script>";
}


?> 