<?php
session_start();
require_once "includes/autentica.php";
$cd_categoria = "";
if (isset($_GET["cd_categoria"]))
    $cd_categoria = $_GET["cd_categoria"];

require_once "../App_Code/Categorias.php";
$categoria = new Categorias();
$categoria->getUmItem($cd_categoria);
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="categoriaslista.php">Categorias</a> > <strong>Editar</strong>
                </nav>
                <form action="categoriaarqauxiliares/categoriacadastroeditar.php" method="post" id="cadastro" onsubmit="return validaform();">
                    <fieldset ><legend >Editar categoria</legend>
                        <ol>
                            <li>
                                <label>
                                    Cod:
                                </label>
                                <label class="Campos required"><?php echo $categoria->getCd_categoria() ?></label>
                                <input type="hidden" name="cd_categoria" value="<?php echo $categoria->getCd_categoria() ?>"/>

                            </li>
                            <li>
                                <label>
                                    Categoria:
                                </label>
                                <input class="Campos required" name="nm_categoria" value="<?php echo $categoria->getNm_categoria(); ?>">
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                </form>
            </div>
        </section>
         <?php include "includes/rodape.php" ?> 
    </body>
</html>
