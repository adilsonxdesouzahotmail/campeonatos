<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
require_once "../../App_Code/Subcategorias.php";
$cd_subcategoria="";
$cd_categoria="";
$nm_subcategoria="";

if(isset($_POST["cd_subcategoria"]))
    $cd_subcategoria=$_POST["cd_subcategoria"];

if(isset($_POST["cd_categoria"]))
    $cd_categoria=$_POST["cd_categoria"];

if(isset($_POST["nm_subcategoria"]))
    $nm_subcategoria=$_POST["nm_subcategoria"];  
  


$subcategoria = new Subcategorias();

$subcategoria->setNm_subcategoria($nm_subcategoria);
$subcategoria->setCd_subcategoria($cd_subcategoria);

$subcategoria->Atualizar($subcategoria);

if($subcategoria->erro=="")
{
   echo "<script>alert('Dados da subcategoria atualizados com sucesso.');".
           "window.location= '../subcategoriaslista.php?cd_categoria=".$cd_categoria."';</script>";
}
 else {
     ?>
        <script>alert("<?php echo $subcategoria->erro ?>");history.back();</script>
    <?php
    
}
    
?>


