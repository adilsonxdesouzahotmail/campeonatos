<?php
error_reporting(E_ALL);
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();
require_once "../App_Code/Campeonatos.php";
$campeonato = new Campeonatos();
$cd_campeonato = "";
if (isset($_GET["cd_campeonato"]))
    $cd_campeonato = $_GET["cd_campeonato"];
$campeonato->getUmItem($cd_campeonato);
//echo "dttt".$campeonato->getDt_campeonato();
?>

<html >
    <head>
        <?php include "includes/head2.php" ?> 
        <script src="js/jquery.maskMoney.js" type="text/javascript"></script>
      
          <script src="ckeditor/ckeditor.js"></script>
        <style>
        .ckeditorcontainer{
            width:100%;float:left;clear:left;

        }

        </style>

    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="campeonatoslista.php">Campeonatos</a> > <strong>Editar</strong>
                </nav>
                <nav class="navabas">
                    <div class="aba">
                        Campeonato
                    </div>
                  <div class="aba2" style="width: 120px;"
                       onclick="window.location = 'campeonatogaleriafotos.php?cd_campeonato=<?php echo $cd_campeonato ?>'" >
                      Galeria de Fotos
                  </div>   
                  <div class="aba2" style="width: 120px;"
                       onclick="window.location = 'campeonatoclientes.php?cd_campeonato=<?php echo $cd_campeonato ?>'" >
                      Clientes
                  </div>                   
                </nav>
                <form action="campeonatoarqauxiliares/campeonatocadastroeditar.php" method="post"
                      onsubmit="return validaformcampeonatos();" name="cadastro" id="cadastro">
                    <fieldset ><legend >Cadastrar novo campeonato</legend>
                        <ol>
                    <li>
                        <label>
                            Cód:
                        </label>
                        <input class="Campos required" disabled name="cd_campeonato" value="<?php echo $campeonato->getCd_campeonato() ?>" >
                        <input type="hidden" name="cd_campeonato" value="<?php echo $campeonato->getCd_campeonato() ?>" >
                        <label>
                         Encerrado:</label>
                        <?php
                            if($campeonato->getIc_encerrado()==1)
                            {
                            ?>
                              Sim&nbsp;<input type="radio" value="1" name="ic_encerrado" checked disabled style="margin-top: 8px;" disabled />&nbsp;&nbsp;
                              Não&nbsp;<input type="radio" value="0" name="ic_encerrado"  />&nbsp;&nbsp;                         
                            <?php
                            }
                            else
                            {
                            ?>
                              Sim&nbsp;<input type="radio" value="1" name="ic_encerrado"  style="margin-top: 8px;" disabled />&nbsp;&nbsp;
                              Não&nbsp;<input type="radio" value="0" name="ic_encerrado" checked disabled />&nbsp;&nbsp;                         
                            <?php
                            }
                            
                        ?>

                    </li>                             
                            <li>
                                <label>
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_campeonato" value="<?php echo $campeonato->getNm_campeonato() ?>" id="nm_campeonato" >
                                <label>
                                    Ativo:</label>
                                  <?php
                                    if($campeonato->getIc_ativo()==1)
                                    {
                                    ?>
                                      Sim&nbsp;<input type="radio" value="1" name="ic_ativo" checked style="margin-top: 8px;" />&nbsp;&nbsp;
                                      Não&nbsp;<input type="radio" value="0" name="ic_ativo"  />&nbsp;&nbsp;                         
                                    <?php
                                    }
                                    else
                                    {
                                    ?>
                                      Sim&nbsp;<input type="radio" value="1" name="ic_ativo"  style="margin-top: 8px;"  />&nbsp;&nbsp;
                                      Não&nbsp;<input type="radio" value="0" name="ic_ativo" checked  />&nbsp;&nbsp;                         
                                    <?php
                                    }

                                ?>
                            </li>
                            <li>
                                <label>
                                    Jogo:
                                </label>
                                <select name="cd_jogo" id="cd_jogo"    class="Campos" >
                                    <option value="0">Selecione</option>
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from jogos";
                                    $rs = $mysqli->query($query);

                                    while ($row = $rs->fetch_assoc()) {
                                        
                                        if ($row ["cd_jogo"] == $campeonato->getCd_jogo()) {
                                            echo "<option value=" . $row ["cd_jogo"] . " selected>" . $row ["nm_jogo"] . "</option>";
                                        } else
                                        {
                                          echo "<option value=" . $row ["cd_jogo"] . ">" . $row ["nm_jogo"] . "</option>";  
                                        }
                                    }
                                    $rs->free();
                                    ?>
                                </select>
                            </li>
                            <!---------------------->
                            <li>
                                <label>
                                    Plataformas:</label>
                                <table class="tblcaracteristicas" style="width:500px;">
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from plataformas";
                                    $rs = $mysqli->query($query);
                                    require_once "../App_Code/Plataformas_campeonato.php";
                                    $platcampeonato = new Plataformas_campeonato();
                                    echo "<tr>";
                                    while ($row = $rs->fetch_assoc()) {
                                        echo "<td style='padding-right:10px;'>";
                                        if($platcampeonato->temPlataforma($campeonato->getCd_campeonato(),$row["cd_plataforma"])>0)
                                        {
                                             echo "<input type='checkbox' checked   value='" . $row ["cd_plataforma"] . "' name='cd_plataforma[]' />&nbsp;" . $row ["nm_plataforma"];
                                         }
                                         else 
                                         {
                                           echo "<input type='checkbox' value='" . $row ["cd_plataforma"] . "' name='cd_plataforma[]' />&nbsp;" . $row ["nm_plataforma"];
                                        echo "</td>";
                                         }

                                    }
                                    echo "</tr>";
                                    $rs->free();
                                    ?>
                                </table>
                            </li>                            
                            <!---------------------->                               
                            <li>
                                <label>
                                    Qtd jogadores prevista:
                                </label>
                                <input class="Campos required" name="qt_participantes_previsto" id="qt_participantes_previsto" 
                                       value="<?php echo $campeonato->getQt_participantes_previsto() ?>">
                                 <label style="width: 100px;">
                                    Data prevista:
                                </label>
                                <input class="Campos required" name="dt_campeonato" id="dt_campeonato" 
                                       value="<?php echo $campeonato->getDt_campeonato() ?>" style="width: 100px;">
                                <label style="width: 50px;">
                                    Hora:
                                </label>
                                <input class="Campos required" name="ds_hora_campeonato" id="ds_hora_campeonato" 
                                       value="<?php echo $campeonato->getDs_hora_campeonato() ?>" style="width: 100px;">
                            </li>           
                            <!----------------------> 
                            
                            <li>
                                <label>
                                    Valor inscrição:
                                </label>
                                <input class="Campos required" name="vl_inscricao" id="vl_inscricao" 
                                       value="<?php echo $campeonato->getVl_inscricao() ?>">
                             </li> 
                             
                             <li>
                                <label>
                                    Qtd Jogadores/Time:
                                </label>
                                <select name="qt_jogadores_time" id="qt_jogadores_time"    class="Campos" >
                                    <?php
                                    require_once "../App_Code/Conexao.php";
                                    $conexao = new Conexao();
                                    $mysqli = new mysqli($conexao->getNm_servidor(), $conexao->getNm_usuario(), $conexao->getNm_senha(), $conexao->getNm_bd());
                                    $mysqli->set_charset("utf8");
                                    $query = "select * from times_grupos";
                                    $rs = $mysqli->query($query);


                                    while ($row = $rs->fetch_assoc()) {
                                       // echo $row ["qt_jogadores"] ."--". $campeonato->getQt_jogadores_time()."<br>";
                                        if ($row ["qt_jogadores"] == $campeonato->getQt_jogadores_time()) {
                                            echo "<option value=" . $row ["qt_jogadores"] . " selected>" . $row ["nm_qtd_jogadores"] . "</option>";
                                        } else
                                        {
                                          echo "<option value=" . $row ["qt_jogadores"] . ">" . $row ["nm_qtd_jogadores"] . "</option>";  
                                        }
                                          
                                    }
                                    $rs->free();
                                    ?>
                                </select>                                 
                             </li>                              
                            <!----------------------> 
                            <li style="margin-top: 10px;">
                                <label>
                                    Descrição:</label>
<!--                                <textarea id="txtdescricao" rows="5" class="Campos required" name="ds_campeonato"></textarea>-->
                              <div class="ckeditorcontainer">
                                    <textarea   id="editor1" name="ds_campeonato" rows="10" cols="80" >
                                   <?php echo $campeonato->getDs_campeonato(); ?>
                                    </textarea>
                                </div>
                            </li>

                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                     <script>
                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.
                        CKEDITOR.replace('editor1');
                    </script>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
