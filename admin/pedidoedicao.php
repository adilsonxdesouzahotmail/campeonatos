<?php
session_start();
error_reporting(E_ALL);
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();
require_once "../App_Code/Pedidos.php";
$pedido = new Pedidos();

require_once "../App_Code/PedidoDetalhes.php";
$pedidodetalhe = new PedidoDetalhes();

$cd_pedido = "";
if (isset($_GET["cd_pedido"]))
    $cd_pedido = $_GET["cd_pedido"];

$pedido->getUmItem($cd_pedido);
?>

<html>
<head >
    <?php include "includes/head2.php" ?> 
    
</head>
<body>
     <?php include "includes/topoelateral.php" ?> 
    <section id="colunadireita">
          <div class="container">
           
            <nav class="breadcrumb">
                <a href="pedidoslista.php">Pedidos</a> > <strong>Consultar Dados</strong>
            </nav>
            <nav class="navabas">
                 <div class="aba"  >
                    Dados do pedido <?php echo $pedido->getCd_pedido(); ?>
                </div>
                <div class="aba2" onclick="window.location='pedidodetalhecliente.php?cd_cliente=<?php echo $pedido->getCd_cliente(); ?>&cd_pedido=<?php echo $cd_pedido; ?>';" >
                    Dados do Cliente
                </div>
                 
               
            </nav>
              <form id="cadastro" action="pedidoarqauxiliares/pedidocadastroeditar.php" method="post">
                  <input type="hidden" name="cd_pedido" value="<?php echo $cd_pedido ?>"/>
                  <fieldset ><legend style="margin-left:30px"> Dados do pedido </legend>
                      <ol>
                          <li style="min-height:20px;margin-bottom:0px;">
                              <label style="width:42px;font-weight:bold;line-height:15px;height:15px;">
                                   C&oacute;d:
                              </label>
                              <label name="nm_complemento" style="width:26px;line-height:15px;height:15px;"><?php echo $pedido->getCd_pedido() ?></label>
                               
                              <label style="width:67px;font-weight:bold;line-height:15px;height:15px;">
                                 Cliente:
                              </label>
                               <label name="nm_complemento" style="width:245px;line-height:15px;height:15px;"><?php echo $pedido->getNm_cliente() ?></label>
                          </li>
                          <?php
                            $data_criacao="";
                            $data_atualizacao="";
                            if ($pedido->getDt_data_criacao() !="")
                                $data_criacao = date("d/m/Y H:i:s", strtotime($pedido->getDt_data_criacao()));
                            if ($pedido->getDt_data_atualizacao() !="")
                                $data_atualizacao = date("d/m/Y H:i:s", strtotime($pedido->getDt_data_atualizacao()));
                          
                          ?>
                          <li style="min-height:20px;margin-bottom:0px;">
                               <label style="width:83px;font-weight:bold;line-height:15px;height:15px;">
                                  Criado em:</label>
                               
                               </label>
                               <label name="nm_complemento" style="width:160px;line-height:15px;height:15px;"><?php echo $data_criacao ?></label>
                               
                               <label style="width:103px;font-weight:bold;line-height:15px;height:15px;">
                                  Atualizado em:
                               </label>
                               <label name="nm_complemento" style="width:160px;line-height:15px;height:15px;"><?php echo $data_atualizacao ?></label>
                          </li>
                           <li style="margin-top: 10px;">
                                <label>
                                    Cometários:</label>
                               <textarea id="txtdescricao" rows="5" class="Campos required" name="ds_comentarios"><?php echo $pedido->getDs_comentarios() ?></textarea>
                            </li>
                            <li>
                                <label>
                                    Status:
                                </label>
                                <label style="width:240px;"><?php echo $pedido->getNm_pedido_status(); ?></label>
                                <input type="hidden" name="cd_pedido_status" value="<?php echo $pedido->getCd_pedido_status() ?>" />
                                  
                                 <input type="submit" class="btnenviar" value="Enviar" style="margin-left:156px;"/>
                            </li>
                      </ol>
                      <h2 style="margin-bottom:0px;text-align:center;width:100%;margin-top:10px;">Itens do Pedido</h2>
                      <table class="tbllistagem" id="tbllistagem" >
                          <tr>
                              <th style="width: 70px; text-align: center;">
                                  C&oacute;d. prod.
                              </th>
                              <th style="text-align: left;">
                                  Produto
                              </th>
                              <th style="width: 40px; text-align: center;">
                                  Qtde
                              </th>
                              <th style="width: 90px; text-align: right;">
                                 Valor unitário
                              </th>
                              <th style="width: 90px; text-align: right;">
                                 Subtotal
                              </th>
                          </tr>
                          <?php
                           $pedidodetalhe->GetDetalhesPedido($cd_pedido);
                           $total=0;
                            for ($i = 0; $i < count($pedidodetalhe->meuarray); $i++) {

                                echo "<tr>";
                                echo "<td align='center'>" . $pedidodetalhe->meuarray[$i]->getCd_produto() . "</td>";
                                echo "<td align='left'>" .$pedidodetalhe->meuarray[$i]->getNm_produto()." Tamanho: ".$pedidodetalhe->meuarray[$i]->getNm_tamanho(). "</td>";
                                echo "<td align='center'>" .$pedidodetalhe->meuarray[$i]->getQt_quantidade(). "</td>";
                                echo "<td style='width: 90px; text-align: right;'>R$ " .number_format($pedidodetalhe->meuarray[$i]->getVl_valor(), 2,',',''). "</td>";
                                echo "<td style='width: 90px; text-align: right;'>R$ " .number_format($pedidodetalhe->meuarray[$i]->getVl_subtotal(), 2,',',''). "</td>";
                                echo "</tr>";
                                
                                $total+=$pedidodetalhe->meuarray[$i]->getVl_subtotal();
                            }
                            ?>
                          <tr>
                              <td colspan="4"><strong>Total do pedido</strong></td>
                              <td style='text-align: right;'><strong>R$ <?php echo number_format($total, 2,',','')?></strong></td>
                          </tr>
                           <tr>
                              <td colspan="4"><strong>Frete</strong></td>
                              <td style='text-align: right;'><strong>R$ <?php echo number_format($pedido->getVl_frete(), 2,',','')?></strong></td>
                          </tr>
                          <tr>
                              <td colspan="4"><strong>Total geral</strong></td>
                              <td style='text-align: right;'><strong>R$ <?php echo number_format($pedido->getVl_frete()+$total, 2,',','')?></strong></td>
                          </tr>
                      </table>
                  </fieldset>
              </form>
        </div>
    </section>
    <?php include "includes/rodape.php" ?> 
</body>
</html>
