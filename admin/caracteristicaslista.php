<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Caracteristicas.php";
$caracteristica = new Caracteristicas();
if (isset($_GET["acao"]))
    $acao = $_GET["acao"];
else
    $acao = "";
if (isset($_GET["cd_caracteristica"]))
    $cd_caracteristica = $_GET["cd_caracteristica"];
else
    $cd_roduto = "";
if ($acao == "excluir") {
    $caracteristica->Excluir($cd_caracteristica);
    if ($caracteristica->erro == "")
        echo "<script>alert('Gênero excluída com sucesso.');window.location='caracteristicaslista.php';</script>";
    else {
        ?>
        <script >alert("<?php echo $caracteristica->erro ?>");
                window.location = "caracteristicaslista.php"</script>
        <?php
    }
}
?>

<html >
    <head>
         <?php include "includes/head2.php" ?> 
    </head>
<body>
    
    <?php include "includes/topoelateral.php" ?> 
  
    <section id="colunadireita">
       
        <div class="container" >
             <nav class="breadcrumb">
                <strong>Gêneros</strong>
            </nav>
            <div class="contbtnnovo">
                <input type="button" class="botaopadrao" value="Novo Gênero" onclick="window.location = 'caracteristicacadastro.php';" />
            </div>
            <div style="width: 100%; float: left; clear: left;">
                <table class="tbllistagem" id="tbllistagem" style="width: 100%; float: left; clear: left;">
                    <tr>

                        <th style="width: 30px;">
                            Cód.
                        </th>
                        <th style="width: 200px;">
                            Gênero
                        </th>

                        <th style="width: 50px; text-align: center;">
                            Editar
                        </th>
                        <th style="width: 50px; text-align: center;">
                            Excluir
                        </th>
                    </tr>
                        <?php
                        $caracteristica->getTodosItens();
                        for ($i = 0; $i < count($caracteristica->meuarray); $i++) {
                            echo "<tr>";

                            echo "<td>" . $caracteristica->meuarray[$i]->getCd_caracteristica() . "</td>";
                            echo "<td>" . $caracteristica->meuarray[$i]->getNm_caracteristica() . "</td>";


                            echo"<td align='center'>";
                            echo "<a href='caracteristicaedicao.php?cd_caracteristica=" . $caracteristica->meuarray[$i]->getCd_caracteristica() . "'><img src='imagens/lapis.png'/></a>";
                            echo "</td>";

                            echo "<td align='center'>";
                            ?>
                        <a href="caracteristicaslista.php?cd_caracteristica=<?php echo $caracteristica->meuarray[$i]->getCd_caracteristica() ?>&acao=excluir"
                           onclick="return confirm('Confirma a exclusão do gênero <?php echo $caracteristica->meuarray[$i]->getNm_caracteristica() ?>?');">
                            <img src='imagens/lixeira.png' /></a>
                        <?php
                        echo "</td>";
                        echo "</tr>";
                    }
                    ?>
                </table>

            </div>
           
        </div>

    </section>
      <?php include "includes/rodape.php" ?> 
</body>
</html>
