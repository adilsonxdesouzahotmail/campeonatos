<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Conexao.php";
$conexao = new Conexao();
require_once "../App_Code/Clientes.php";
$cliente = new Clientes();
$cd_cliente = "";
if (isset($_GET["cd_cliente"]))
    $cd_cliente = $_GET["cd_cliente"];

$cliente->getUmItem($cd_cliente);
?>

<html>
<head >
    <?php include "includes/head2.php" ?> 
    
</head>
<body>
     <?php include "includes/topoelateral.php" ?> 
    <section id="colunadireita">
          <div class="container">
           
            <nav class="breadcrumb">
                <a href="clienteslista.php">Clientes</a> > <strong>Consultar Dados</strong>
            </nav>
            <nav class="navabas">
                <div class="aba" >
                    Dados do cliente
                </div>
                  <div class="aba2"  onclick="window.location='clientecampeonatos.php?cd_cliente=<?php echo $cd_cliente;?>';">
                    Campeonatos do cliente <?php echo $cliente->getNome_cliente(); ?>
                </div>
               
            </nav>
           <?php include "clientearqauxiliares/clientedetalhe.php" ?>
        </div>
    </section>
    <?php include "includes/rodape.php" ?> 
</body>
</html>
