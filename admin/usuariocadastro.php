<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Usuarios.php";
$usuario = new Usuarios();
?>

<html >
    <head>
        <?php include "includes/head2.php" ?> 
        <script>
            function limpaerros()
            {
                $(".erro").hide();
            }
            function validaformusuario()
            {

                limpaerros();
                var ok = true;
                $nm_usuario = formusuario.nm_usuario.value;
                $nm_login = formusuario.nm_login.value;
                $nm_email = formusuario.nm_email.value;
                $nm_senha1 = formusuario.nm_senha1.value;
                $nm_senha2 = formusuario.nm_senha2.value;
                if ($nm_usuario == "")
                {
                    $("#erro_nm_usuario").show();
                    ok = false;
                }
                if ($nm_email == "")
                {
                    $("#erro_nm_email1").show();
                    ok = false;
                }
                else
                {
                    if (!IsValidEmail($nm_email))
                    {
                        $("#erro_nm_email2").show();
                        ok = false;
                    }
                }
                if ($nm_login == "")
                {
                    $("#erro_nm_login").show();
                    ok = false;
                }
                if ($nm_senha1 == "")
                {
                    $("#erro_nm_senha1").show();
                    ok = false;
                }
                if ($nm_senha2 == "")
                {
                    $("#erro_nm_senha2").show();
                    ok = false;
                }

                if (($nm_senha1 != "" && $nm_senha2 != "") && ($nm_senha1 != $nm_senha2))
                {
                    $("#erro_nm_senha22").show();
                    ok = false;
                }

                return ok;
            }
            function IsValidEmail(email)
            {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                return filter.test(email);

            }
        </script>
        <style>
            label
            {
                width:120px !important;
            }

        </style>
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="usuarioslista.php">Usuarios</a> > <strong>Cadastrar</strong>
                </nav>
                <form action="usuarioarqauxiliares/usuariocadastroinserir.php" method="post" name="formusuario" id="cadastro" onsubmit="return validaformusuario();">
                    <fieldset ><legend >Cadastrar novo usuario</legend>
                        <ol>

                            <li>
                                <label>
                                    Nome:
                                </label>
                                <input class="Campos required" name="nm_usuario">
                                <label class="erro" id="erro_nm_usuario">
                                    Digite o nome.
                                </label>
                            </li>
                            <li>
                                <label>
                                    E-mail:
                                </label>
                                <input class="Campos required" name="nm_email">
                                <label class="erro" id="erro_nm_email1">
                                    Digite o mail
                                </label>
                                <label class="erro" id="erro_nm_email2">
                                    E-mail inválido
                                </label>
                            </li>
                            <li>
                                <label>
                                    Login:
                                </label>
                                <input class="Campos required" name="nm_login">
                                <label class="erro" id="erro_nm_login">
                                    Digite o login
                                </label>
                            </li>
                            <li>
                                <label>
                                    Nível:
                                </label>
                                <select name="qt_nivel"   class="Campos" >
                                    <option value="0">Administrador</option>
                                    <option value="1">Operador</option>
                                </select>

                            </li>
                            <li>
                                <label>
                                    Senha:
                                </label>
                                <input type="password" class="Campos required" name="nm_senha1">
                                <label class="erro" id="erro_nm_senha1">
                                    Digite a senha
                                </label>
                            </li>
                            <li>
                                <label>
                                    Confirme a Senha:
                                </label>
                                <input type="password" class="Campos required" name="nm_senha2">
                                <label class="erro" id="erro_nm_senha2" style="width:200px !important;">
                                    Digite a confirmação da senha
                                </label>
                                <label class="erro" id="erro_nm_senha22" style="width:300px !important;">
                                    A senha e a confirmação devem coincidir
                                </label>
                            </li>
                            <li style="width:100%;">
                                <input type="submit" class="btnenviar"  value="Enviar"  />
                            </li>
                        </ol>
                    </fieldset>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
