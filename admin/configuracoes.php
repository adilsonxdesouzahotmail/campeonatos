<?php
session_start();
require_once "includes/autentica.php";
require_once "../App_Code/Configuracoes.php";
$configuracoes = new Configuracoes();
$configuracoes->getUmItem();
?>
<html >
    <head>
        <?php include "includes/head2.php" ?> 
        <script>
            function limpaerros()
            {
                $(".erro").hide();
            }
            function validaformconfiguracoes()
            {
                limpaerros();
                var ok = true;
                $nm_configuracoes = formconfiguracoes.nm_configuracoes.value;
                $nm_login = formconfiguracoes.nm_login.value;
                $nm_email = formconfiguracoes.nm_email.value;
                if ($nm_configuracoes == "")
                {
                    $("#erro_nm_configuracoes").show();
                    ok = false;
                }
                if ($nm_email == "")
                {
                    $("#erro_nm_email1").show();
                    ok = false;
                }
                else
                {
                    if (!IsValidEmail($nm_email))
                    {
                        $("#erro_nm_email2").show();
                        ok = false;
                    }
                }


                return ok;
            }
            function IsValidEmail(email)
            {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                return filter.test(email);

            }
        </script>
        <style type="text/css">
          
        </style>
               
    </head>
    <body>

        <?php include "includes/topoelateral.php" ?> 

        <section id="colunadireita">
            <div class="container">
                <nav class="breadcrumb">
                    <a href="configuracoes.php">Configuracoes</a> > 
                </nav>
                <form action="configuracoesarqauxiliares/configuracoescadastroeditar.php" method="post" name="formconfiguracoes" id="cadastro" onsubmit="return validaformconfiguracoes();">
                    <fieldset ><legend >Editar configurações</legend>
                        <ol>
                            
                            <li>
                                <label  style="width:155px;">
                                    Telefones - linha 1:
                                </label>
                                <input class="Campos required" name="nm_telefones_linha1" value="<?php echo $configuracoes->getNm_telefones_linha1(); ?>">
                                
                            </li>
                            
                            <li>
                                <label style="width:155px;">
                                    Telefones - linha 2:
                                </label>
                                <input class="Campos required" name="nm_telefones_linha2" value="<?php echo $configuracoes->getNm_telefones_linha2(); ?>">
                                
                            </li>
                            
                            <li>
                                <label style="width:155px;">
                                    Endereço - linha1:
                                </label>
                                <input class="Campos required" name="nm_endereco_linha1" value="<?php echo $configuracoes->getNm_endereco_linha1(); ?>">
                            </li>
                            
                            <li>
                                <label style="width:155px;">
                                    Endereço - linha2:
                                </label>
                                <input class="Campos required" name="nm_endereco_linha2" value="<?php echo $configuracoes->getNm_endereco_linha2(); ?>">
                            </li>
                            
                            <li>
                                <label style="width:155px;">
                                    E-mail:
                                </label>
                                <input class="Campos required" name="nm_email" value="<?php echo $configuracoes->getNm_email(); ?>">
                            </li>
                            
                            <li>
                                <label style="width:155px;">
                                    Endereço Facebook:
                                </label>
                                <input class="Campos required"  name="nm_url_facebook" value="<?php echo $configuracoes->getNm_url_facebook(); ?>" >
                            </li>
                            
                            <li>
                                <label style="width:155px;">
                                    Endereço Twitter:
                                </label>
                                <input class="Campos required"  name="nm_url_twitter" value="<?php echo $configuracoes->getNm_url_twitter(); ?>" >
                            </li>
                          
                            <li style="width:100%;">
                                <input type="submit" class="btnenviar" value="Enviar" />
                            </li>
                        </ol>
                    </fieldset>
                </form>
            </div>
        </section>
        <?php include "includes/rodape.php" ?> 
    </body>
</html>
