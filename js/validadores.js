            function IsValidEmail(email)
            {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                return filter.test(email);

            }
            function TestaCPF(strCPF) {
                var Soma;
                var Resto;
                Soma = 0;
                if (strCPF == "00000000000")
                    return false;

                for (i = 1; i <= 9; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
                Resto = (Soma * 10) % 11;

                if ((Resto == 10) || (Resto == 11))
                    Resto = 0;
                if (Resto != parseInt(strCPF.substring(9, 10)))
                    return false;

                Soma = 0;
                for (i = 1; i <= 10; i++)
                    Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
                Resto = (Soma * 10) % 11;

                if ((Resto == 10) || (Resto == 11))
                    Resto = 0;
                if (Resto != parseInt(strCPF.substring(10, 11)))
                    return false;
                return true;
            }
            
            function isNumberKey(evt,componto) 
           {
              var charCode = (evt.which) ? evt.which : event.keyCode;
              if(!componto && charCode == 46)
              {
                  return false;
              }
              if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
              } else {
                // If the number field already has . then don't allow to enter . again.
                if (evt.target.value.search(/\./) > -1 && charCode == 46) {
                    return false;
                }
                return true;
             }
           }

